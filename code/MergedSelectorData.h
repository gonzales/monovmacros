//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sat Mar 28 17:16:22 2020 by ROOT version 6.14/04
// from TTree ggVV/
// found on file: /eos/user/g/gonzales/MonoV/merged/test/ggVV.root
//////////////////////////////////////////////////////////

#ifndef MergedSelectorData_h
#define MergedSelectorData_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Headers needed by this particular selector
#include <vector>
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "MergeJob.h"
#include "MergedTree.C"
#include "PlotList.h"
#include "PlotTools.C"

class MergedSelectorData : public TSelector {

private:
   MergeJob * m_mergeJob; //->
   MergedTree * shortTree; //!

   TString m_channel; //!
   TString m_tables; //!

   map<TString,PlotTools*> map_plotTools; //!
   map<TString,PlotTools*> map_avgPlotTools; //!
   map<TString,TH1D*> map_cutflow_yields; //!
   map<TString,TH1D*> map_cutflow_sumw; //!
   
   map<pair<ULong64_t,Int_t>,int> m_listOfEvents; //!

   map<Int_t,Int_t> map_rawEvents; //!

   TGraph * graph_xSec; //!
   TGraph * graph_filterEff; //!
   TGraph * graph_kFactor; //!
   TGraph * graph_MCsum; //!
   TGraph * graph_rawEvents; //!

public :
   TTreeReader     fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

   virtual void FillMinitree();

   map<TString,PlotTools*> BuildMapPTools(MergeJob * mergeJob) {
      map<TString,PlotTools*> building_map;
      for (TString selection : mergeJob->GetSelections()) {
         TString outFolder = mergeJob->GetChannelName() + "_" + selection;
         gSystem->Exec("mkdir -p " + mergeJob->GetOutputsPlotFolder() + "/" + mergeJob->GetChannelName());
         gSystem->Exec("mkdir -p " + mergeJob->GetOutputsPlotFolder() + "/" + mergeJob->GetChannelName() + "/" + outFolder);
         PlotTools * pTools = new PlotTools(mergeJob->GetChannelName() + "_" + selection, mergeJob->GetOutputsPlotFolder() + "/" + mergeJob->GetChannelName() + "/" + outFolder);
         pTools->addHistogramsFromMap(PlotList::map_binning());
         if (mergeJob->GetChannelName().Contains("invH")) pTools->splitByDSIDs(SampleList::getDSIDs(mergeJob->GetChannelName()));
         building_map[selection] = pTools;
      }
      return building_map;
   }

   map<TString,PlotTools*> BuildAvgMapPtools(MergeJob * mergeJob) {
      map<TString,PlotTools*> building_map;
      for (TString selection : mergeJob->GetSelections()) {
         TString outFolder = mergeJob->GetChannelName() + "_" + selection;
         gSystem->Exec("mkdir -p " + mergeJob->GetOutputsPlotFolder() + "/" + mergeJob->GetChannelName());
         gSystem->Exec("mkdir -p " + mergeJob->GetOutputsPlotFolder() + "/" + mergeJob->GetChannelName() + "/" + outFolder);
         PlotTools * pTools = new PlotTools(mergeJob->GetChannelName() + "_" + selection, mergeJob->GetOutputsPlotFolder() + "/" + mergeJob->GetChannelName() + "/" + outFolder);
         pTools->addAveragingHistogramsFromMap(PlotList::map_avgHist_binning(), PlotList::map_avgHist_variables());
         building_map[selection] = pTools;
      }
      return building_map;
   }

   PlotTools * GetMetTurnOnPTool(TString trigger, TString outputPlotFolder) {
      gSystem->Exec("mkdir -p " + outputPlotFolder + "/" + m_channel);
      gSystem->Exec("mkdir -p " + outputPlotFolder + "/" + m_channel + "/METTurnOn");
      PlotTools * pTools = new PlotTools(m_channel + "_METTurnOn", outputPlotFolder + "/" + m_channel + "/METTurnOn");
      pTools->addHistogram(trigger + "_num", 25, 0, 250, PlotList::getXTitle("met_nomuon_tst_et"), "");
      pTools->addHistogram(trigger + "_den", 25, 0, 250, PlotList::getXTitle("met_nomuon_tst_et"), "");
      pTools->addHistogram(trigger + "_stdMET_num", 25, 0, 250, PlotList::getXTitle("met_tst_et"), "");
      pTools->addHistogram(trigger + "_stdMET_den", 25, 0, 250, PlotList::getXTitle("met_tst_et"), "");
      return pTools;
   }

   PlotTools * GetN1PTool(TString N1_selection, TString n1_tag, TString outputPlotFolder) {
      TString plotName = PlotList::getVariableForSelection(N1_selection);
      gSystem->Exec("mkdir -p " + outputPlotFolder + "/" + m_channel);
      gSystem->Exec("mkdir -p " + outputPlotFolder + "/" + m_channel + "/N1Plots");
      PlotTools * pTools = new PlotTools(m_channel + "_N1Plots_" + n1_tag + "_" + N1_selection, outputPlotFolder + "/" + m_channel + "/N1Plots/");
      pTools->addHistogram(plotName, PlotList::map_binning()[plotName]);
      return pTools;
   }

   void AddToOutput(PlotTools * pTools, TList * fOutput) {
      for (auto kv_hist : pTools->histograms) {
         TString name = kv_hist.first;
         TH1D * hist = kv_hist.second;
         fOutput->Add(hist);
         if (pTools->splitDSIDs) {
            for (auto kv_dsid : pTools->perDSIDHists[name]) {
               fOutput->Add(kv_dsid.second);
            }
         }
      }
      for (auto kv_avgHist : pTools->histograms_averaging) {
         TString name = kv_avgHist.first;
         TH1D * hist = kv_avgHist.second;
         fOutput->Add(hist);
      }
   }

   void SetParameters(MergeJob * mergeJob) {
      m_channel = mergeJob->GetChannelName();
      m_tables = mergeJob->GetDoTables();
   }

   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<Int_t> run = {fReader, "run"};
   TTreeReaderValue<ULong64_t> event = {fReader, "event"};
   TTreeReaderValue<Int_t> year = {fReader, "year"};
   TTreeReaderValue<Float_t> met_tst_sig = {fReader, "met_tst_sig"};
   TTreeReaderValue<Int_t> n_tau_baseline = {fReader, "n_tau_baseline"};
   TTreeReaderValue<Float_t> mconly_weight = {fReader, "mconly_weight"};
   TTreeReaderValue<Float_t> syst_weight = {fReader, "syst_weight"};
   TTreeReaderValue<Float_t> pu_weight = {fReader, "pu_weight"};
   TTreeReaderValue<Float_t> btag_weight = {fReader, "btag_weight"};
   TTreeReaderValue<Float_t> jvt_weight = {fReader, "jvt_weight"};
   TTreeReaderValue<Float_t> truth_V_bare_pt = {fReader, "truth_V_bare_pt"};
   TTreeReaderValue<Float_t> truth_V_dressed_pt = {fReader, "truth_V_dressed_pt"};
   TTreeReaderValue<Float_t> truth_V_simple_pt = {fReader, "truth_V_simple_pt"};
   TTreeReaderValue<Float_t> dPhiLCTopoJetMet = {fReader, "dPhiLCTopoJetMet"};
   TTreeReaderValue<Float_t> dPhiTCCJetMet = {fReader, "dPhiTCCJetMet"};
   TTreeReaderValue<Float_t> m_LCTopoJet = {fReader, "m_LCTopoJet"};
   TTreeReaderValue<Float_t> m_TCCJet = {fReader, "m_TCCJet"};
   TTreeReaderValue<Int_t> n_jet_central = {fReader, "n_jet_central"};
   TTreeReaderValue<Float_t> dPhiDijetMet = {fReader, "dPhiDijetMet"};
   TTreeReaderValue<Float_t> dPhiDijet = {fReader, "dPhiDijet"};
   TTreeReaderValue<Float_t> dRDijet = {fReader, "dRDijet"};
   TTreeReaderValue<Float_t> DijetSumPt = {fReader, "DijetSumPt"};
   TTreeReaderValue<Float_t> TrijetSumPt = {fReader, "TrijetSumPt"};
   TTreeReaderValue<Float_t> DijetMass = {fReader, "DijetMass"};
   TTreeReaderValue<Int_t> n_trackjet = {fReader, "n_trackjet"};
   TTreeReaderValue<Int_t> n_bcentralJet = {fReader, "n_bcentralJet"};
   TTreeReaderValue<Int_t> n_btrackJet = {fReader, "n_btrackJet"};
   TTreeReaderValue<Int_t> MJ_passDeltaPhi = {fReader, "MJ_passDeltaPhi"};
   TTreeReaderValue<Int_t> n_trackLCTopoAssociatedBjet = {fReader, "n_trackLCTopoAssociatedBjet"};
   TTreeReaderValue<Int_t> n_trackLCTopoAssociatedNotBjet = {fReader, "n_trackLCTopoAssociatedNotBjet"};
   TTreeReaderValue<Int_t> n_trackLCTopoSeparatedBjet = {fReader, "n_trackLCTopoSeparatedBjet"};
   TTreeReaderValue<Int_t> n_trackLCTopoSeparatedNotBjet = {fReader, "n_trackLCTopoSeparatedNotBjet"};
   TTreeReaderValue<Int_t> n_trackTCCAssociatedBjet = {fReader, "n_trackTCCAssociatedBjet"};
   TTreeReaderValue<Int_t> n_trackTCCAssociatedNotBjet = {fReader, "n_trackTCCAssociatedNotBjet"};
   TTreeReaderValue<Int_t> n_trackTCCSeparatedBjet = {fReader, "n_trackTCCSeparatedBjet"};
   TTreeReaderValue<Int_t> n_trackTCCSeparatedNotBjet = {fReader, "n_trackTCCSeparatedNotBjet"};
   TTreeReaderValue<Float_t> ptV_1Muon_pt = {fReader, "ptV_1Muon_pt"};
   TTreeReaderValue<Float_t> ptV_1Muon_eta = {fReader, "ptV_1Muon_eta"};
   TTreeReaderValue<Float_t> ptV_1Muon_phi = {fReader, "ptV_1Muon_phi"};
   TTreeReaderValue<Float_t> ptV_2Lepton_pt = {fReader, "ptV_2Lepton_pt"};
   TTreeReaderValue<Float_t> ptV_2Lepton_eta = {fReader, "ptV_2Lepton_eta"};
   TTreeReaderValue<Float_t> ptV_2Lepton_phi = {fReader, "ptV_2Lepton_phi"};
   TTreeReaderValue<Int_t> n_jet = {fReader, "n_jet"};
   TTreeReaderValue<Int_t> n_bjet = {fReader, "n_bjet"};
   TTreeReaderValue<Int_t> n_el = {fReader, "n_el"};
   TTreeReaderValue<Int_t> n_el_baseline = {fReader, "n_el_baseline"};
   TTreeReaderValue<Int_t> n_mu_baseline = {fReader, "n_mu_baseline"};
   TTreeReaderValue<Int_t> n_mu_baseline_bad = {fReader, "n_mu_baseline_bad"};
   TTreeReaderValue<Int_t> n_allmu_bad = {fReader, "n_allmu_bad"};
   TTreeReaderValue<Int_t> n_tau = {fReader, "n_tau"};
   TTreeReaderValue<Int_t> n_mu = {fReader, "n_mu"};
   TTreeReaderValue<Float_t> jvt_all_weight = {fReader, "jvt_all_weight"};
   TTreeReaderValue<Int_t> n_smallJet = {fReader, "n_smallJet"};
   TTreeReaderValue<Int_t> n_truthFatJet = {fReader, "n_truthFatJet"};
   TTreeReaderValue<Int_t> n_LCTopoJet = {fReader, "n_LCTopoJet"};
   TTreeReaderValue<Int_t> n_TCCJet = {fReader, "n_TCCJet"};
   TTreeReaderValue<Int_t> n_tau_truth = {fReader, "n_tau_truth"};
   TTreeReaderValue<Int_t> n_truthTop = {fReader, "n_truthTop"};
   TTreeReaderValue<Float_t> averageIntPerXing = {fReader, "averageIntPerXing"};
   TTreeReaderValue<Float_t> actualIntPerXing = {fReader, "actualIntPerXing"};
   TTreeReaderValue<Float_t> corAverageIntPerXing = {fReader, "corAverageIntPerXing"};
   TTreeReaderValue<Float_t> corActualIntPerXing = {fReader, "corActualIntPerXing"};
   TTreeReaderValue<Int_t> n_vx = {fReader, "n_vx"};
   TTreeReaderValue<Float_t> pu_hash = {fReader, "pu_hash"};
   TTreeReaderValue<Int_t> trigger_matched_electron = {fReader, "trigger_matched_electron"};
   TTreeReaderValue<Int_t> trigger_matched_muon = {fReader, "trigger_matched_muon"};
   TTreeReaderValue<Int_t> trigger_HLT_e120_lhloose = {fReader, "trigger_HLT_e120_lhloose"};
   TTreeReaderValue<Int_t> trigger_HLT_e140_lhloose_nod0 = {fReader, "trigger_HLT_e140_lhloose_nod0"};
   TTreeReaderValue<Int_t> trigger_HLT_e24_lhmedium_L1EM20VH = {fReader, "trigger_HLT_e24_lhmedium_L1EM20VH"};
   TTreeReaderValue<Int_t> trigger_HLT_e24_lhtight_nod0_ivarloose = {fReader, "trigger_HLT_e24_lhtight_nod0_ivarloose"};
   TTreeReaderValue<Int_t> trigger_HLT_e26_lhtight_nod0_ivarloose = {fReader, "trigger_HLT_e26_lhtight_nod0_ivarloose"};
   TTreeReaderValue<Int_t> trigger_HLT_e60_lhmedium = {fReader, "trigger_HLT_e60_lhmedium"};
   TTreeReaderValue<Int_t> trigger_HLT_e60_lhmedium_nod0 = {fReader, "trigger_HLT_e60_lhmedium_nod0"};
   TTreeReaderValue<Int_t> trigger_HLT_g140_loose = {fReader, "trigger_HLT_g140_loose"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100_mht_L1XE50 = {fReader, "trigger_HLT_xe100_mht_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe110_mht_L1XE50 = {fReader, "trigger_HLT_xe110_mht_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe130_mht_L1XE50 = {fReader, "trigger_HLT_xe130_mht_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe70 = {fReader, "trigger_HLT_xe70"};
   TTreeReaderValue<Int_t> trigger_HLT_xe80_tc_lcw_L1XE50 = {fReader, "trigger_HLT_xe80_tc_lcw_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe90_mht_L1XE50 = {fReader, "trigger_HLT_xe90_mht_L1XE50"};
   TTreeReaderValue<Int_t> trigger_matched_HLT_e60_lhmedium = {fReader, "trigger_matched_HLT_e60_lhmedium"};
   TTreeReaderValue<Int_t> trigger_matched_HLT_e120_lhloose = {fReader, "trigger_matched_HLT_e120_lhloose"};
   TTreeReaderValue<Int_t> trigger_matched_HLT_e24_lhmedium_L1EM18VH = {fReader, "trigger_matched_HLT_e24_lhmedium_L1EM18VH"};
   TTreeReaderValue<Int_t> trigger_matched_HLT_e24_lhmedium_L1EM20VH = {fReader, "trigger_matched_HLT_e24_lhmedium_L1EM20VH"};
   TTreeReaderValue<Int_t> flag_bib = {fReader, "flag_bib"};
   TTreeReaderValue<Int_t> flag_bib_raw = {fReader, "flag_bib_raw"};
   TTreeReaderValue<Int_t> trigger_HLT_2e17_loose = {fReader, "trigger_HLT_2e17_loose"};
   TTreeReaderValue<Int_t> trigger_HLT_3j175 = {fReader, "trigger_HLT_3j175"};
   TTreeReaderValue<Int_t> trigger_HLT_4j85 = {fReader, "trigger_HLT_4j85"};
   TTreeReaderValue<Int_t> trigger_HLT_e120_lhloose_nod0 = {fReader, "trigger_HLT_e120_lhloose_nod0"};
   TTreeReaderValue<Int_t> trigger_HLT_e24_lhmedium_L1EM18VH = {fReader, "trigger_HLT_e24_lhmedium_L1EM18VH"};
   TTreeReaderValue<Int_t> trigger_HLT_e24_lhmedium_iloose_L1EM20VH = {fReader, "trigger_HLT_e24_lhmedium_iloose_L1EM20VH"};
   TTreeReaderValue<Int_t> trigger_HLT_e24_lhtight_iloose = {fReader, "trigger_HLT_e24_lhtight_iloose"};
   TTreeReaderValue<Int_t> trigger_HLT_e28_tight_iloose = {fReader, "trigger_HLT_e28_tight_iloose"};
   TTreeReaderValue<Int_t> trigger_HLT_g120_loose = {fReader, "trigger_HLT_g120_loose"};
   TTreeReaderValue<Int_t> trigger_HLT_g160_loose = {fReader, "trigger_HLT_g160_loose"};
   TTreeReaderValue<Int_t> trigger_HLT_g300_etcut = {fReader, "trigger_HLT_g300_etcut"};
   TTreeReaderValue<Int_t> trigger_HLT_ht700_L1J100 = {fReader, "trigger_HLT_ht700_L1J100"};
   TTreeReaderValue<Int_t> trigger_HLT_ht850_L1J100 = {fReader, "trigger_HLT_ht850_L1J100"};
   TTreeReaderValue<Int_t> trigger_HLT_mu24_imedium = {fReader, "trigger_HLT_mu24_imedium"};
   TTreeReaderValue<Int_t> trigger_HLT_mu60_0eta105_msonly = {fReader, "trigger_HLT_mu60_0eta105_msonly"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100 = {fReader, "trigger_HLT_xe100"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100_mht = {fReader, "trigger_HLT_xe100_mht"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100_pueta = {fReader, "trigger_HLT_xe100_pueta"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100_pufit = {fReader, "trigger_HLT_xe100_pufit"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100_pufit_L1XE50 = {fReader, "trigger_HLT_xe100_pufit_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100_pufit_L1XE55 = {fReader, "trigger_HLT_xe100_pufit_L1XE55"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100_tc_em = {fReader, "trigger_HLT_xe100_tc_em"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100_tc_lcw = {fReader, "trigger_HLT_xe100_tc_lcw"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100_tc_lcw_L1XE50 = {fReader, "trigger_HLT_xe100_tc_lcw_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100_tc_lcw_L1XE60 = {fReader, "trigger_HLT_xe100_tc_lcw_L1XE60"};
   TTreeReaderValue<Int_t> trigger_HLT_xe110_pufit_L1XE50 = {fReader, "trigger_HLT_xe110_pufit_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe110_pufit_L1XE55 = {fReader, "trigger_HLT_xe110_pufit_L1XE55"};
   TTreeReaderValue<Int_t> trigger_HLT_xe110_pufit_xe65_L1XE50 = {fReader, "trigger_HLT_xe110_pufit_xe65_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe110_pufit_xe70_L1XE50 = {fReader, "trigger_HLT_xe110_pufit_xe70_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe120_pufit_L1XE50 = {fReader, "trigger_HLT_xe120_pufit_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe70_pueta = {fReader, "trigger_HLT_xe70_pueta"};
   TTreeReaderValue<Int_t> trigger_HLT_xe70_pufit = {fReader, "trigger_HLT_xe70_pufit"};
   TTreeReaderValue<Int_t> trigger_HLT_xe70_tc_em = {fReader, "trigger_HLT_xe70_tc_em"};
   TTreeReaderValue<Int_t> trigger_HLT_xe70_tc_lcw = {fReader, "trigger_HLT_xe70_tc_lcw"};
   TTreeReaderValue<Int_t> trigger_HLT_xe80 = {fReader, "trigger_HLT_xe80"};
   TTreeReaderValue<Int_t> trigger_HLT_xe80_pueta = {fReader, "trigger_HLT_xe80_pueta"};
   TTreeReaderValue<Int_t> trigger_HLT_xe90_pufit_L1XE50 = {fReader, "trigger_HLT_xe90_pufit_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe90_tc_lcw_L1XE50 = {fReader, "trigger_HLT_xe90_tc_lcw_L1XE50"};
   TTreeReaderValue<Int_t> trigger_L1_XE50 = {fReader, "trigger_L1_XE50"};
   TTreeReaderValue<Int_t> trigger_L1_XE70 = {fReader, "trigger_L1_XE70"};
   TTreeReaderValue<Int_t> trigger_L2_2J15_XE55 = {fReader, "trigger_L2_2J15_XE55"};
   TTreeReaderValue<Int_t> trigger_ht700_L1J75 = {fReader, "trigger_ht700_L1J75"};
   TTreeReaderValue<Int_t> trigger_ht850_L1J75 = {fReader, "trigger_ht850_L1J75"};
   TTreeReaderValue<Int_t> n_ph = {fReader, "n_ph"};
   TTreeReaderValue<Int_t> n_ph_tight = {fReader, "n_ph_tight"};
   TTreeReaderValue<Int_t> n_ph_baseline = {fReader, "n_ph_baseline"};
   TTreeReaderValue<Int_t> n_ph_baseline_tight = {fReader, "n_ph_baseline_tight"};
   TTreeReaderValue<Int_t> n_jet_truth = {fReader, "n_jet_truth"};
   TTreeReaderValue<Float_t> truth_V_bare_eta = {fReader, "truth_V_bare_eta"};
   TTreeReaderValue<Float_t> truth_V_bare_phi = {fReader, "truth_V_bare_phi"};
   TTreeReaderValue<Float_t> truth_V_bare_m = {fReader, "truth_V_bare_m"};
   TTreeReaderValue<Float_t> truth_V_dressed_eta = {fReader, "truth_V_dressed_eta"};
   TTreeReaderValue<Float_t> truth_V_dressed_phi = {fReader, "truth_V_dressed_phi"};
   TTreeReaderValue<Float_t> truth_V_dressed_m = {fReader, "truth_V_dressed_m"};
   TTreeReaderValue<Float_t> truth_V_simple_eta = {fReader, "truth_V_simple_eta"};
   TTreeReaderValue<Float_t> truth_V_simple_phi = {fReader, "truth_V_simple_phi"};
   TTreeReaderValue<Float_t> truth_V_simple_m = {fReader, "truth_V_simple_m"};
   TTreeReaderValue<Float_t> met_eleterm_et = {fReader, "met_eleterm_et"};
   TTreeReaderValue<Float_t> met_eleterm_phi = {fReader, "met_eleterm_phi"};
   TTreeReaderValue<Float_t> met_eleterm_etx = {fReader, "met_eleterm_etx"};
   TTreeReaderValue<Float_t> met_eleterm_ety = {fReader, "met_eleterm_ety"};
   TTreeReaderValue<Float_t> met_eleterm_sumet = {fReader, "met_eleterm_sumet"};
   TTreeReaderValue<Float_t> met_jetterm_et = {fReader, "met_jetterm_et"};
   TTreeReaderValue<Float_t> met_jetterm_phi = {fReader, "met_jetterm_phi"};
   TTreeReaderValue<Float_t> met_jetterm_etx = {fReader, "met_jetterm_etx"};
   TTreeReaderValue<Float_t> met_jetterm_ety = {fReader, "met_jetterm_ety"};
   TTreeReaderValue<Float_t> met_jetterm_sumet = {fReader, "met_jetterm_sumet"};
   TTreeReaderValue<Float_t> met_muonterm_et = {fReader, "met_muonterm_et"};
   TTreeReaderValue<Float_t> met_muonterm_phi = {fReader, "met_muonterm_phi"};
   TTreeReaderValue<Float_t> met_muonterm_etx = {fReader, "met_muonterm_etx"};
   TTreeReaderValue<Float_t> met_muonterm_ety = {fReader, "met_muonterm_ety"};
   TTreeReaderValue<Float_t> met_muonterm_sumet = {fReader, "met_muonterm_sumet"};
   TTreeReaderValue<Float_t> met_muonterm_tst_et = {fReader, "met_muonterm_tst_et"};
   TTreeReaderValue<Float_t> met_muonterm_tst_phi = {fReader, "met_muonterm_tst_phi"};
   TTreeReaderValue<Float_t> met_muonterm_tst_etx = {fReader, "met_muonterm_tst_etx"};
   TTreeReaderValue<Float_t> met_muonterm_tst_ety = {fReader, "met_muonterm_tst_ety"};
   TTreeReaderValue<Float_t> met_muonterm_tst_sumet = {fReader, "met_muonterm_tst_sumet"};
   TTreeReaderValue<Float_t> met_noelectron_tst_et = {fReader, "met_noelectron_tst_et"};
   TTreeReaderValue<Float_t> met_noelectron_tst_phi = {fReader, "met_noelectron_tst_phi"};
   TTreeReaderValue<Float_t> met_noelectron_tst_etx = {fReader, "met_noelectron_tst_etx"};
   TTreeReaderValue<Float_t> met_noelectron_tst_ety = {fReader, "met_noelectron_tst_ety"};
   TTreeReaderValue<Float_t> met_noelectron_tst_sumet = {fReader, "met_noelectron_tst_sumet"};
   TTreeReaderValue<Float_t> met_nomuon_tst_et = {fReader, "met_nomuon_tst_et"};
   TTreeReaderValue<Float_t> met_nomuon_tst_phi = {fReader, "met_nomuon_tst_phi"};
   TTreeReaderValue<Float_t> met_nomuon_tst_etx = {fReader, "met_nomuon_tst_etx"};
   TTreeReaderValue<Float_t> met_nomuon_tst_ety = {fReader, "met_nomuon_tst_ety"};
   TTreeReaderValue<Float_t> met_nomuon_tst_sumet = {fReader, "met_nomuon_tst_sumet"};
   TTreeReaderValue<Float_t> met_nophoton_tst_et = {fReader, "met_nophoton_tst_et"};
   TTreeReaderValue<Float_t> met_nophoton_tst_phi = {fReader, "met_nophoton_tst_phi"};
   TTreeReaderValue<Float_t> met_nophoton_tst_etx = {fReader, "met_nophoton_tst_etx"};
   TTreeReaderValue<Float_t> met_nophoton_tst_ety = {fReader, "met_nophoton_tst_ety"};
   TTreeReaderValue<Float_t> met_nophoton_tst_sumet = {fReader, "met_nophoton_tst_sumet"};
   TTreeReaderValue<Float_t> met_phterm_et = {fReader, "met_phterm_et"};
   TTreeReaderValue<Float_t> met_phterm_phi = {fReader, "met_phterm_phi"};
   TTreeReaderValue<Float_t> met_phterm_etx = {fReader, "met_phterm_etx"};
   TTreeReaderValue<Float_t> met_phterm_ety = {fReader, "met_phterm_ety"};
   TTreeReaderValue<Float_t> met_phterm_sumet = {fReader, "met_phterm_sumet"};
   TTreeReaderValue<Float_t> met_softerm_tst_et = {fReader, "met_softerm_tst_et"};
   TTreeReaderValue<Float_t> met_softerm_tst_phi = {fReader, "met_softerm_tst_phi"};
   TTreeReaderValue<Float_t> met_softerm_tst_etx = {fReader, "met_softerm_tst_etx"};
   TTreeReaderValue<Float_t> met_softerm_tst_ety = {fReader, "met_softerm_tst_ety"};
   TTreeReaderValue<Float_t> met_softerm_tst_sumet = {fReader, "met_softerm_tst_sumet"};
   TTreeReaderValue<Float_t> met_track_et = {fReader, "met_track_et"};
   TTreeReaderValue<Float_t> met_track_phi = {fReader, "met_track_phi"};
   TTreeReaderValue<Float_t> met_track_etx = {fReader, "met_track_etx"};
   TTreeReaderValue<Float_t> met_track_ety = {fReader, "met_track_ety"};
   TTreeReaderValue<Float_t> met_track_sumet = {fReader, "met_track_sumet"};
   TTreeReaderValue<Float_t> met_truth_et = {fReader, "met_truth_et"};
   TTreeReaderValue<Float_t> met_truth_phi = {fReader, "met_truth_phi"};
   TTreeReaderValue<Float_t> met_truth_etx = {fReader, "met_truth_etx"};
   TTreeReaderValue<Float_t> met_truth_ety = {fReader, "met_truth_ety"};
   TTreeReaderValue<Float_t> met_truth_sumet = {fReader, "met_truth_sumet"};
   TTreeReaderValue<Float_t> met_tst_et = {fReader, "met_tst_et"};
   TTreeReaderValue<Float_t> met_tst_phi = {fReader, "met_tst_phi"};
   TTreeReaderValue<Float_t> met_tst_etx = {fReader, "met_tst_etx"};
   TTreeReaderValue<Float_t> met_tst_ety = {fReader, "met_tst_ety"};
   TTreeReaderValue<Float_t> met_tst_sumet = {fReader, "met_tst_sumet"};
   TTreeReaderValue<Int_t> LCTopoJet_ntrk = {fReader, "LCTopoJet_ntrk"};
   TTreeReaderValue<Int_t> TCCJet_ntrk = {fReader, "TCCJet_ntrk"};
   TTreeReaderValue<Int_t> tau_loose_multiplicity = {fReader, "tau_loose_multiplicity"};
   TTreeReaderValue<Int_t> tau_medium_multiplicity = {fReader, "tau_medium_multiplicity"};
   TTreeReaderValue<Int_t> tau_tight_multiplicity = {fReader, "tau_tight_multiplicity"};
   TTreeReaderValue<Int_t> tau_baseline_loose_multiplicity = {fReader, "tau_baseline_loose_multiplicity"};
   TTreeReaderValue<Int_t> tau_baseline_medium_multiplicity = {fReader, "tau_baseline_medium_multiplicity"};
   TTreeReaderValue<Int_t> tau_baseline_tight_multiplicity = {fReader, "tau_baseline_tight_multiplicity"};
   TTreeReaderValue<Float_t> weight = {fReader, "weight"};
   TTreeReaderValue<Float_t> mc_weight_sum = {fReader, "mc_weight_sum"};
   TTreeReaderValue<Float_t> extra_weight = {fReader, "extra_weight"};
   TTreeReaderValue<Float_t> campaign_lumi = {fReader, "campaign_lumi"};
   TTreeReaderValue<Float_t> lead_jet_central_pt = {fReader, "lead_jet_central_pt"};

    TTreeReaderValue<vector<float>>   mu_pt = {fReader, "mu_pt"};
    TTreeReaderValue<vector<double>>  mu_SF = {fReader, "mu_SF"};
    TTreeReaderValue<vector<float>>   mu_eta = {fReader, "mu_eta"};
    TTreeReaderValue<vector<float>>   mu_phi = {fReader, "mu_phi"};
    TTreeReaderValue<vector<double>>  mu_SF_iso = {fReader, "mu_SF_iso"};
    TTreeReaderValue<vector<float>>   mu_m = {fReader, "mu_m"};
    TTreeReaderValue<vector<float>>   mu_charge = {fReader, "mu_charge"};
    TTreeReaderValue<vector<float>>   mu_id_pt = {fReader, "mu_id_pt"};
    TTreeReaderValue<vector<float>>   mu_id_eta = {fReader, "mu_id_eta"};
    TTreeReaderValue<vector<float>>   mu_id_phi = {fReader, "mu_id_phi"};
    TTreeReaderValue<vector<float>>   mu_id_m = {fReader, "mu_id_m"};
    TTreeReaderValue<vector<float>>   mu_me_pt = {fReader, "mu_me_pt"};
    TTreeReaderValue<vector<float>>   mu_me_eta = {fReader, "mu_me_eta"};
    TTreeReaderValue<vector<float>>   mu_me_phi = {fReader, "mu_me_phi"};
    TTreeReaderValue<vector<float>>   mu_me_m = {fReader, "mu_me_m"};
    TTreeReaderValue<vector<float>>   mu_ptcone20 = {fReader, "mu_ptcone20"};
    TTreeReaderValue<vector<float>>   mu_ptvarcone20 = {fReader, "mu_ptvarcone20"};
    TTreeReaderValue<vector<float>>   mu_etcone20 = {fReader, "mu_etcone20"};
    TTreeReaderValue<vector<float>>   mu_topoetcone20 = {fReader, "mu_topoetcone20"};
    TTreeReaderValue<vector<float>>   mu_ptcone30 = {fReader, "mu_ptcone30"};
    TTreeReaderValue<vector<float>>   mu_ptvarcone30 = {fReader, "mu_ptvarcone30"};
    TTreeReaderValue<vector<float>>   mu_ptvarcone30_TightTTVA_pt1000 = {fReader, "mu_ptvarcone30_TightTTVA_pt1000"};
    TTreeReaderValue<vector<float>>   mu_etcone30 = {fReader, "mu_etcone30"};
    TTreeReaderValue<vector<float>>   mu_topoetcone30 = {fReader, "mu_topoetcone30"};
    TTreeReaderValue<vector<float>>   mu_ptcone40 = {fReader, "mu_ptcone40"};
    TTreeReaderValue<vector<float>>   mu_ptvarcone40 = {fReader, "mu_ptvarcone40"};
    TTreeReaderValue<vector<float>>   mu_etcone40 = {fReader, "mu_etcone40"};
    TTreeReaderValue<vector<float>>   mu_topoetcone40 = {fReader, "mu_topoetcone40"};
    TTreeReaderValue<vector<int>>     mu_author = {fReader, "mu_author"};
    TTreeReaderValue<vector<int>>     mu_quality = {fReader, "mu_quality"};
    TTreeReaderValue<vector<int>>     mu_isSA = {fReader, "mu_isSA"};
    TTreeReaderValue<vector<float>>   mu_met_nomuon_dphi = {fReader, "mu_met_nomuon_dphi"};
    TTreeReaderValue<vector<float>>   mu_met_wmuon_dphi = {fReader, "mu_met_wmuon_dphi"};
    TTreeReaderValue<vector<int>>     mu_truth_type = {fReader, "mu_truth_type"};
    TTreeReaderValue<vector<int>>     mu_truth_origin = {fReader, "mu_truth_origin"};
    TTreeReaderValue<vector<float>>   mu_baseline_pt = {fReader, "mu_baseline_pt"};
    TTreeReaderValue<vector<double>>  mu_baseline_SF = {fReader, "mu_baseline_SF"};
    TTreeReaderValue<vector<float>>   mu_baseline_eta = {fReader, "mu_baseline_eta"};
    TTreeReaderValue<vector<float>>   mu_baseline_phi = {fReader, "mu_baseline_phi"};
    TTreeReaderValue<vector<bool>>     mu_baseline_isLooseID = {fReader, "mu_baseline_isLooseID"};
    TTreeReaderValue<vector<bool>>     mu_baseline_isMediumID = {fReader, "mu_baseline_isMediumID"};
    TTreeReaderValue<vector<bool>>     mu_baseline_isTightID = {fReader, "mu_baseline_isTightID"};
    TTreeReaderValue<vector<float>>   el_pt = {fReader, "el_pt"};
    TTreeReaderValue<vector<float>>   el_eta = {fReader, "el_eta"};
    TTreeReaderValue<vector<float>>   el_phi = {fReader, "el_phi"};
    TTreeReaderValue<vector<double>>  el_SF = {fReader, "el_SF"};
    TTreeReaderValue<vector<double>>  el_SF_iso = {fReader, "el_SF_iso"};
    TTreeReaderValue<vector<double>>  el_SF_trigger = {fReader, "el_SF_trigger"};
    TTreeReaderValue<vector<double>>  el_eff_trigger = {fReader, "el_eff_trigger"};
    TTreeReaderValue<vector<float>>   el_m = {fReader, "el_m"};
    TTreeReaderValue<vector<float>>   el_charge = {fReader, "el_charge"};
    TTreeReaderValue<vector<float>>   el_id_pt = {fReader, "el_id_pt"};
    TTreeReaderValue<vector<float>>   el_id_eta = {fReader, "el_id_eta"};
    TTreeReaderValue<vector<float>>   el_id_phi = {fReader, "el_id_phi"};
    TTreeReaderValue<vector<float>>   el_id_m = {fReader, "el_id_m"};
    TTreeReaderValue<vector<float>>   el_cl_pt = {fReader, "el_cl_pt"};
    TTreeReaderValue<vector<float>>   el_cl_eta = {fReader, "el_cl_eta"};
    TTreeReaderValue<vector<float>>   el_cl_etaBE2 = {fReader, "el_cl_etaBE2"};
    TTreeReaderValue<vector<float>>   el_cl_phi = {fReader, "el_cl_phi"};
    TTreeReaderValue<vector<float>>   el_cl_m = {fReader, "el_cl_m"};
    TTreeReaderValue<vector<float>>   el_ptcone20 = {fReader, "el_ptcone20"};
    TTreeReaderValue<vector<float>>   el_ptvarcone20 = {fReader, "el_ptvarcone20"};
    TTreeReaderValue<vector<float>>   el_ptvarcone20_TightTTVA_pt1000 = {fReader, "el_ptvarcone20_TightTTVA_pt1000"};
    TTreeReaderValue<vector<float>>   el_etcone20 = {fReader, "el_etcone20"};
    TTreeReaderValue<vector<float>>   el_topoetcone20 = {fReader, "el_topoetcone20"};
    TTreeReaderValue<vector<float>>   el_ptcone30 = {fReader, "el_ptcone30"};
    TTreeReaderValue<vector<float>>   el_ptvarcone30 = {fReader, "el_ptvarcone30"};
    TTreeReaderValue<vector<float>>   el_etcone30 = {fReader, "el_etcone30"};
    TTreeReaderValue<vector<float>>   el_topoetcone30 = {fReader, "el_topoetcone30"};
    TTreeReaderValue<vector<float>>   el_ptcone40 = {fReader, "el_ptcone40"};
    TTreeReaderValue<vector<float>>   el_ptvarcone40 = {fReader, "el_ptvarcone40"};
    TTreeReaderValue<vector<float>>   el_etcone40 = {fReader, "el_etcone40"};
    TTreeReaderValue<vector<float>>   el_topoetcone40 = {fReader, "el_topoetcone40"};
    TTreeReaderValue<vector<int>>     el_author = {fReader, "el_author"};
    TTreeReaderValue<vector<int>>     el_isConv = {fReader, "el_isConv"};
    TTreeReaderValue<vector<float>>   el_truth_pt = {fReader, "el_truth_pt"};
    TTreeReaderValue<vector<float>>   el_truth_eta = {fReader, "el_truth_eta"};
    TTreeReaderValue<vector<float>>   el_truth_phi = {fReader, "el_truth_phi"};
    TTreeReaderValue<vector<int>>     el_truth_status = {fReader, "el_truth_status"};
    TTreeReaderValue<vector<int>>     el_truth_type = {fReader, "el_truth_type"};
    TTreeReaderValue<vector<int>>     el_truth_origin = {fReader, "el_truth_origin"};
    TTreeReaderValue<vector<float>>   el_met_nomuon_dphi = {fReader, "el_met_nomuon_dphi"};
    TTreeReaderValue<vector<float>>   el_met_wmuon_dphi = {fReader, "el_met_wmuon_dphi"};
    TTreeReaderValue<vector<float>>   el_met_noelectron_dphi = {fReader, "el_met_noelectron_dphi"};
    TTreeReaderValue<vector<float>>   el_baseline_pt = {fReader, "el_baseline_pt"};
    TTreeReaderValue<vector<double>>  el_baseline_SF = {fReader, "el_baseline_SF"};
    TTreeReaderValue<vector<float>>   el_baseline_eta = {fReader, "el_baseline_eta"};
    TTreeReaderValue<vector<float>>   el_baseline_phi = {fReader, "el_baseline_phi"};
    TTreeReaderValue<vector<bool>>     el_baseline_isLooseID = {fReader, "el_baseline_isLooseID"};
    TTreeReaderValue<vector<bool>>     el_baseline_isMediumID = {fReader, "el_baseline_isMediumID"};
    TTreeReaderValue<vector<bool>>     el_baseline_isTightID = {fReader, "el_baseline_isTightID"};
    TTreeReaderValue<vector<float>>   jet_pt = {fReader, "jet_pt"};
    TTreeReaderValue<vector<float>>   jet_eta = {fReader, "jet_eta"};
    TTreeReaderValue<vector<float>>   jet_phi = {fReader, "jet_phi"};
    TTreeReaderValue<vector<float>>   jet_m = {fReader, "jet_m"};
    TTreeReaderValue<vector<float>>   jet_fmax = {fReader, "jet_fmax"};
    TTreeReaderValue<vector<float>>   jet_fch = {fReader, "jet_fch"};
    TTreeReaderValue<vector<float>>   jet_MV2c10_discriminant = {fReader, "jet_MV2c10_discriminant"};
    TTreeReaderValue<vector<float>>   jet_MV2c20_discriminant = {fReader, "jet_MV2c20_discriminant"};
    TTreeReaderValue<vector<int>>     jet_isbjet = {fReader, "jet_isbjet"};
    TTreeReaderValue<vector<int>>     jet_PartonTruthLabelID = {fReader, "jet_PartonTruthLabelID"};
    TTreeReaderValue<vector<int>>     jet_ConeTruthLabelID = {fReader, "jet_ConeTruthLabelID"};
    TTreeReaderValue<vector<float>>   jet_met_nomuon_dphi = {fReader, "jet_met_nomuon_dphi"};
    TTreeReaderValue<vector<float>>   jet_met_wmuon_dphi = {fReader, "jet_met_wmuon_dphi"};
    TTreeReaderValue<vector<float>>   jet_met_noelectron_dphi = {fReader, "jet_met_noelectron_dphi"};
    TTreeReaderValue<vector<float>>   jet_met_nophoton_dphi = {fReader, "jet_met_nophoton_dphi"};
    TTreeReaderValue<vector<float>>   jet_weight = {fReader, "jet_weight"};
    TTreeReaderValue<vector<float>>   jet_raw_pt = {fReader, "jet_raw_pt"};
    TTreeReaderValue<vector<float>>   jet_raw_eta = {fReader, "jet_raw_eta"};
    TTreeReaderValue<vector<float>>   jet_raw_phi = {fReader, "jet_raw_phi"};
    TTreeReaderValue<vector<float>>   jet_raw_m = {fReader, "jet_raw_m"};
    TTreeReaderValue<vector<float>>   jet_timing = {fReader, "jet_timing"};
    TTreeReaderValue<vector<float>>   jet_emfrac = {fReader, "jet_emfrac"};
    TTreeReaderValue<vector<float>>   jet_hecf = {fReader, "jet_hecf"};
    TTreeReaderValue<vector<float>>   jet_hecq = {fReader, "jet_hecq"};
    TTreeReaderValue<vector<float>>   jet_larq = {fReader, "jet_larq"};
    TTreeReaderValue<vector<float>>   jet_avglarq = {fReader, "jet_avglarq"};
    TTreeReaderValue<vector<float>>   jet_negE = {fReader, "jet_negE"};
    TTreeReaderValue<vector<float>>   jet_lambda = {fReader, "jet_lambda"};
    TTreeReaderValue<vector<float>>   jet_lambda2 = {fReader, "jet_lambda2"};
    TTreeReaderValue<vector<float>>   jet_jvtxf = {fReader, "jet_jvtxf"};
    TTreeReaderValue<vector<int>>     jet_fmaxi = {fReader, "jet_fmaxi"};
    TTreeReaderValue<vector<int>>     jet_isbjet_loose = {fReader, "jet_isbjet_loose"};
    TTreeReaderValue<vector<float>>   jet_jvt = {fReader, "jet_jvt"};
    TTreeReaderValue<vector<float>>   jet_fjvt = {fReader, "jet_fjvt"};
    TTreeReaderValue<vector<int>>     jet_cleaning = {fReader, "jet_cleaning"};
    TTreeReaderValue<vector<int>>     jet_DFCommonJets_QGTagger_NTracks = {fReader, "jet_DFCommonJets_QGTagger_NTracks"};
    TTreeReaderValue<vector<float>>   jet_DFCommonJets_QGTagger_TracksWidth = {fReader, "jet_DFCommonJets_QGTagger_TracksWidth"};
    TTreeReaderValue<vector<float>>   jet_DFCommonJets_QGTagger_TracksC1 = {fReader, "jet_DFCommonJets_QGTagger_TracksC1"};
    TTreeReaderValue<vector<float>>   trackjet_pt = {fReader, "trackjet_pt"};
    TTreeReaderValue<vector<float>>   trackjet_eta = {fReader, "trackjet_eta"};
    TTreeReaderValue<vector<float>>   trackjet_phi = {fReader, "trackjet_phi"};
    TTreeReaderValue<vector<float>>   trackjet_m = {fReader, "trackjet_m"};
    TTreeReaderValue<vector<float>>   trackjet_fmax = {fReader, "trackjet_fmax"};
    TTreeReaderValue<vector<float>>   trackjet_fch = {fReader, "trackjet_fch"};
    TTreeReaderValue<vector<float>>   trackjet_MV2c10_discriminant = {fReader, "trackjet_MV2c10_discriminant"};
    TTreeReaderValue<vector<float>>   trackjet_MV2c20_discriminant = {fReader, "trackjet_MV2c20_discriminant"};
    TTreeReaderValue<vector<int>>     trackjet_isbjet = {fReader, "trackjet_isbjet"};
    TTreeReaderValue<vector<int>>     trackjet_PartonTruthLabelID = {fReader, "trackjet_PartonTruthLabelID"};
    TTreeReaderValue<vector<int>>     trackjet_ConeTruthLabelID = {fReader, "trackjet_ConeTruthLabelID"};
    TTreeReaderValue<vector<float>>   trackjet_met_nomuon_dphi = {fReader, "trackjet_met_nomuon_dphi"};
    TTreeReaderValue<vector<float>>   trackjet_met_wmuon_dphi = {fReader, "trackjet_met_wmuon_dphi"};
    TTreeReaderValue<vector<float>>   trackjet_met_noelectron_dphi = {fReader, "trackjet_met_noelectron_dphi"};
    TTreeReaderValue<vector<float>>   trackjet_met_nophoton_dphi = {fReader, "trackjet_met_nophoton_dphi"};
    TTreeReaderValue<vector<float>>   trackjet_weight = {fReader, "trackjet_weight"};
    TTreeReaderValue<vector<float>>   trackjet_raw_pt = {fReader, "trackjet_raw_pt"};
    TTreeReaderValue<vector<float>>   trackjet_raw_eta = {fReader, "trackjet_raw_eta"};
    TTreeReaderValue<vector<float>>   trackjet_raw_phi = {fReader, "trackjet_raw_phi"};
    TTreeReaderValue<vector<float>>   trackjet_raw_m = {fReader, "trackjet_raw_m"};
    TTreeReaderValue<vector<float>>   trackjet_timing = {fReader, "trackjet_timing"};
    TTreeReaderValue<vector<float>>   trackjet_emfrac = {fReader, "trackjet_emfrac"};
    TTreeReaderValue<vector<float>>   trackjet_hecf = {fReader, "trackjet_hecf"};
    TTreeReaderValue<vector<float>>   trackjet_hecq = {fReader, "trackjet_hecq"};
    TTreeReaderValue<vector<float>>   trackjet_larq = {fReader, "trackjet_larq"};
    TTreeReaderValue<vector<float>>   trackjet_avglarq = {fReader, "trackjet_avglarq"};
    TTreeReaderValue<vector<float>>   trackjet_negE = {fReader, "trackjet_negE"};
    TTreeReaderValue<vector<float>>   trackjet_lambda = {fReader, "trackjet_lambda"};
    TTreeReaderValue<vector<float>>   trackjet_lambda2 = {fReader, "trackjet_lambda2"};
    TTreeReaderValue<vector<float>>   trackjet_jvtxf = {fReader, "trackjet_jvtxf"};
    TTreeReaderValue<vector<int>>     trackjet_fmaxi = {fReader, "trackjet_fmaxi"};
    TTreeReaderValue<vector<int>>     trackjet_isbjet_loose = {fReader, "trackjet_isbjet_loose"};
    TTreeReaderValue<vector<float>>   trackjet_jvt = {fReader, "trackjet_jvt"};
    TTreeReaderValue<vector<float>>   trackjet_fjvt = {fReader, "trackjet_fjvt"};
    TTreeReaderValue<vector<int>>     trackjet_cleaning = {fReader, "trackjet_cleaning"};
    TTreeReaderValue<vector<int>>     trackjet_DFCommonJets_QGTagger_NTracks = {fReader, "trackjet_DFCommonJets_QGTagger_NTracks"};
    TTreeReaderValue<vector<float>>   trackjet_DFCommonJets_QGTagger_TracksWidth = {fReader, "trackjet_DFCommonJets_QGTagger_TracksWidth"};
    TTreeReaderValue<vector<float>>   trackjet_DFCommonJets_QGTagger_TracksC1 = {fReader, "trackjet_DFCommonJets_QGTagger_TracksC1"};
    TTreeReaderValue<vector<float>>   LCTopoJet_pt = {fReader, "LCTopoJet_pt"};
    TTreeReaderValue<vector<float>>   LCTopoJet_eta = {fReader, "LCTopoJet_eta"};
    TTreeReaderValue<vector<float>>   LCTopoJet_phi = {fReader, "LCTopoJet_phi"};
    TTreeReaderValue<vector<float>>   LCTopoJet_m = {fReader, "LCTopoJet_m"};
    TTreeReaderValue<vector<float>>   LCTopoJet_tau21 = {fReader, "LCTopoJet_tau21"};
    TTreeReaderValue<vector<float>>   LCTopoJet_D2 = {fReader, "LCTopoJet_D2"};
    TTreeReaderValue<vector<float>>   LCTopoJet_C2 = {fReader, "LCTopoJet_C2"};
    TTreeReaderValue<vector<int>>     LCTopoJet_nConstit = {fReader, "LCTopoJet_nConstit"};
    TTreeReaderValue<vector<int>>     LCTopoJet_passD2_W50 = {fReader, "LCTopoJet_passD2_W50"};
    TTreeReaderValue<vector<int>>     LCTopoJet_passD2_Z50 = {fReader, "LCTopoJet_passD2_Z50"};
    TTreeReaderValue<vector<int>>     LCTopoJet_passD2_W80 = {fReader, "LCTopoJet_passD2_W80"};
    TTreeReaderValue<vector<int>>     LCTopoJet_passD2_Z80 = {fReader, "LCTopoJet_passD2_Z80"};
    TTreeReaderValue<vector<int>>     LCTopoJet_passMass_W50 = {fReader, "LCTopoJet_passMass_W50"};
    TTreeReaderValue<vector<int>>     LCTopoJet_passMass_Z50 = {fReader, "LCTopoJet_passMass_Z50"};
    TTreeReaderValue<vector<int>>     LCTopoJet_passMass_W80 = {fReader, "LCTopoJet_passMass_W80"};
    TTreeReaderValue<vector<int>>     LCTopoJet_passMass_Z80 = {fReader, "LCTopoJet_passMass_Z80"};
    TTreeReaderValue<vector<float>>   LCTopoJet_cutD2_W50 = {fReader, "LCTopoJet_cutD2_W50"};
    TTreeReaderValue<vector<float>>   LCTopoJet_cutD2_Z50 = {fReader, "LCTopoJet_cutD2_Z50"};
    TTreeReaderValue<vector<float>>   LCTopoJet_cutD2_W80 = {fReader, "LCTopoJet_cutD2_W80"};
    TTreeReaderValue<vector<float>>   LCTopoJet_cutD2_Z80 = {fReader, "LCTopoJet_cutD2_Z80"};
    TTreeReaderValue<vector<float>>   LCTopoJet_cutMlow_W50 = {fReader, "LCTopoJet_cutMlow_W50"};
    TTreeReaderValue<vector<float>>   LCTopoJet_cutMlow_Z50 = {fReader, "LCTopoJet_cutMlow_Z50"};
    TTreeReaderValue<vector<float>>   LCTopoJet_cutMlow_W80 = {fReader, "LCTopoJet_cutMlow_W80"};
    TTreeReaderValue<vector<float>>   LCTopoJet_cutMlow_Z80 = {fReader, "LCTopoJet_cutMlow_Z80"};
    TTreeReaderValue<vector<float>>   LCTopoJet_cutMhigh_W50 = {fReader, "LCTopoJet_cutMhigh_W50"};
    TTreeReaderValue<vector<float>>   LCTopoJet_cutMhigh_Z50 = {fReader, "LCTopoJet_cutMhigh_Z50"};
    TTreeReaderValue<vector<float>>   LCTopoJet_cutMhigh_W80 = {fReader, "LCTopoJet_cutMhigh_W80"};
    TTreeReaderValue<vector<float>>   LCTopoJet_cutMhigh_Z80 = {fReader, "LCTopoJet_cutMhigh_Z80"};
    TTreeReaderValue<vector<float>>   LCTopoJet_weight = {fReader, "LCTopoJet_weight"};
    TTreeReaderValue<vector<float>>   TCCJet_pt = {fReader, "TCCJet_pt"};
    TTreeReaderValue<vector<float>>   TCCJet_eta = {fReader, "TCCJet_eta"};
    TTreeReaderValue<vector<float>>   TCCJet_phi = {fReader, "TCCJet_phi"};
    TTreeReaderValue<vector<float>>   TCCJet_m = {fReader, "TCCJet_m"};
    TTreeReaderValue<vector<float>>   TCCJet_tau21 = {fReader, "TCCJet_tau21"};
    TTreeReaderValue<vector<float>>   TCCJet_D2 = {fReader, "TCCJet_D2"};
    TTreeReaderValue<vector<float>>   TCCJet_C2 = {fReader, "TCCJet_C2"};
    TTreeReaderValue<vector<int>>     TCCJet_nConstit = {fReader, "TCCJet_nConstit"};
    TTreeReaderValue<vector<int>>     TCCJet_passD2_W50 = {fReader, "TCCJet_passD2_W50"};
    TTreeReaderValue<vector<int>>     TCCJet_passD2_Z50 = {fReader, "TCCJet_passD2_Z50"};
    TTreeReaderValue<vector<int>>     TCCJet_passD2_W80 = {fReader, "TCCJet_passD2_W80"};
    TTreeReaderValue<vector<int>>     TCCJet_passD2_Z80 = {fReader, "TCCJet_passD2_Z80"};
    TTreeReaderValue<vector<int>>     TCCJet_passMass_W50 = {fReader, "TCCJet_passMass_W50"};
    TTreeReaderValue<vector<int>>     TCCJet_passMass_Z50 = {fReader, "TCCJet_passMass_Z50"};
    TTreeReaderValue<vector<int>>     TCCJet_passMass_W80 = {fReader, "TCCJet_passMass_W80"};
    TTreeReaderValue<vector<int>>     TCCJet_passMass_Z80 = {fReader, "TCCJet_passMass_Z80"};
    TTreeReaderValue<vector<float>>   TCCJet_cutD2_W50 = {fReader, "TCCJet_cutD2_W50"};
    TTreeReaderValue<vector<float>>   TCCJet_cutD2_Z50 = {fReader, "TCCJet_cutD2_Z50"};
    TTreeReaderValue<vector<float>>   TCCJet_cutD2_W80 = {fReader, "TCCJet_cutD2_W80"};
    TTreeReaderValue<vector<float>>   TCCJet_cutD2_Z80 = {fReader, "TCCJet_cutD2_Z80"};
    TTreeReaderValue<vector<float>>   TCCJet_cutMlow_W50 = {fReader, "TCCJet_cutMlow_W50"};
    TTreeReaderValue<vector<float>>   TCCJet_cutMlow_Z50 = {fReader, "TCCJet_cutMlow_Z50"};
    TTreeReaderValue<vector<float>>   TCCJet_cutMlow_W80 = {fReader, "TCCJet_cutMlow_W80"};
    TTreeReaderValue<vector<float>>   TCCJet_cutMlow_Z80 = {fReader, "TCCJet_cutMlow_Z80"};
    TTreeReaderValue<vector<float>>   TCCJet_cutMhigh_W50 = {fReader, "TCCJet_cutMhigh_W50"};
    TTreeReaderValue<vector<float>>   TCCJet_cutMhigh_Z50 = {fReader, "TCCJet_cutMhigh_Z50"};
    TTreeReaderValue<vector<float>>   TCCJet_cutMhigh_W80 = {fReader, "TCCJet_cutMhigh_W80"};
    TTreeReaderValue<vector<float>>   TCCJet_cutMhigh_Z80 = {fReader, "TCCJet_cutMhigh_Z80"};
    TTreeReaderValue<vector<float>>   TCCJet_weight = {fReader, "TCCJet_weight"};
    TTreeReaderValue<vector<float>>   tau_pt = {fReader, "tau_pt"};
    TTreeReaderValue<vector<double>>  tau_SF = {fReader, "tau_SF"};
    TTreeReaderValue<vector<int>>     tau_idtool_pass_veryloose = {fReader, "tau_idtool_pass_veryloose"};
    TTreeReaderValue<vector<int>>     tau_idtool_pass_loose = {fReader, "tau_idtool_pass_loose"};
    TTreeReaderValue<vector<int>>     tau_idtool_pass_medium = {fReader, "tau_idtool_pass_medium"};
    TTreeReaderValue<vector<int>>     tau_idtool_pass_tight = {fReader, "tau_idtool_pass_tight"};
    TTreeReaderValue<vector<float>>   tau_eta = {fReader, "tau_eta"};
    TTreeReaderValue<vector<float>>   tau_phi = {fReader, "tau_phi"};
    TTreeReaderValue<vector<float>>   tau_baseline_pt = {fReader, "tau_baseline_pt"};
    TTreeReaderValue<vector<double>>  tau_baseline_SF = {fReader, "tau_baseline_SF"};
    TTreeReaderValue<vector<int>>     tau_baseline_idtool_pass_veryloose = {fReader, "tau_baseline_idtool_pass_veryloose"};
    TTreeReaderValue<vector<int>>     tau_baseline_idtool_pass_loose = {fReader, "tau_baseline_idtool_pass_loose"};
    TTreeReaderValue<vector<int>>     tau_baseline_idtool_pass_medium = {fReader, "tau_baseline_idtool_pass_medium"};
    TTreeReaderValue<vector<int>>     tau_baseline_idtool_pass_tight = {fReader, "tau_baseline_idtool_pass_tight"};
    TTreeReaderValue<vector<float>>   tau_truth_pt = {fReader, "tau_truth_pt"};
    TTreeReaderValue<vector<float>>   tau_truth_eta = {fReader, "tau_truth_eta"};
    TTreeReaderValue<vector<float>>   tau_truth_phi = {fReader, "tau_truth_phi"};


   MergedSelectorData(TTree * /*tree*/ =0) {

      graph_xSec = 0;
      graph_filterEff = 0;
      graph_kFactor = 0;
      graph_MCsum = 0;
      graph_rawEvents = 0;

   }
   virtual ~MergedSelectorData() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(MergedSelectorData,0);

};

#endif

#ifdef MergedSelectorData_cxx
void MergedSelectorData::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t MergedSelectorData::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void MergedSelectorData::FillMinitree()
{
   shortTree->run = *run;
   shortTree->event = *event;
   shortTree->year = *year;
   shortTree->met_tst_sig = *met_tst_sig;
   shortTree->n_tau_baseline = *n_tau_baseline;
   shortTree->mconly_weight = *mconly_weight;
   shortTree->syst_weight = *syst_weight;
   shortTree->pu_weight = *pu_weight;
   shortTree->btag_weight = *btag_weight;
   shortTree->jvt_weight = *jvt_weight;
   shortTree->truth_V_bare_pt = *truth_V_bare_pt;
   shortTree->truth_V_dressed_pt = *truth_V_dressed_pt;
   shortTree->truth_V_simple_pt = *truth_V_simple_pt;
   shortTree->dPhiLCTopoJetMet = *dPhiLCTopoJetMet;
   shortTree->dPhiTCCJetMet = *dPhiTCCJetMet;
   shortTree->m_LCTopoJet = *m_LCTopoJet;
   shortTree->m_TCCJet = *m_TCCJet;
   shortTree->n_jet_central = *n_jet_central;
   shortTree->dPhiDijetMet = *dPhiDijetMet;
   shortTree->dPhiDijet = *dPhiDijet;
   shortTree->dRDijet = *dRDijet;
   shortTree->DijetSumPt = *DijetSumPt;
   shortTree->TrijetSumPt = *TrijetSumPt;
   shortTree->DijetMass = *DijetMass;
   shortTree->n_trackjet = *n_trackjet;
   shortTree->n_bcentralJet = *n_bcentralJet;
   shortTree->n_btrackJet = *n_btrackJet;
   shortTree->MJ_passDeltaPhi = *MJ_passDeltaPhi;
   shortTree->n_trackLCTopoAssociatedBjet = *n_trackLCTopoAssociatedBjet;
   shortTree->n_trackLCTopoAssociatedNotBjet = *n_trackLCTopoAssociatedNotBjet;
   shortTree->n_trackLCTopoSeparatedBjet = *n_trackLCTopoSeparatedBjet;
   shortTree->n_trackLCTopoSeparatedNotBjet = *n_trackLCTopoSeparatedNotBjet;
   shortTree->n_trackTCCAssociatedBjet = *n_trackTCCAssociatedBjet;
   shortTree->n_trackTCCAssociatedNotBjet = *n_trackTCCAssociatedNotBjet;
   shortTree->n_trackTCCSeparatedBjet = *n_trackTCCSeparatedBjet;
   shortTree->n_trackTCCSeparatedNotBjet = *n_trackTCCSeparatedNotBjet;
   shortTree->ptV_1Muon_pt = *ptV_1Muon_pt;
   shortTree->ptV_1Muon_eta = *ptV_1Muon_eta;
   shortTree->ptV_1Muon_phi = *ptV_1Muon_phi;
   shortTree->ptV_2Lepton_pt = *ptV_2Lepton_pt;
   shortTree->ptV_2Lepton_eta = *ptV_2Lepton_eta;
   shortTree->ptV_2Lepton_phi = *ptV_2Lepton_phi;
   shortTree->n_jet = *n_jet;
   shortTree->n_bjet = *n_bjet;
   shortTree->n_el = *n_el;
   shortTree->n_el_baseline = *n_el_baseline;
   shortTree->n_mu_baseline = *n_mu_baseline;
   shortTree->n_mu_baseline_bad = *n_mu_baseline_bad;
   shortTree->n_allmu_bad = *n_allmu_bad;
   shortTree->n_tau = *n_tau;
   shortTree->n_mu = *n_mu;
   shortTree->jvt_all_weight = *jvt_all_weight;
   shortTree->n_smallJet = *n_smallJet;
   shortTree->n_truthFatJet = *n_truthFatJet;
   shortTree->n_LCTopoJet = *n_LCTopoJet;
   shortTree->n_TCCJet = *n_TCCJet;
   shortTree->n_tau_truth = *n_tau_truth;
   shortTree->n_truthTop = *n_truthTop;
   shortTree->averageIntPerXing = *averageIntPerXing;
   shortTree->actualIntPerXing = *actualIntPerXing;
   shortTree->corAverageIntPerXing = *corAverageIntPerXing;
   shortTree->corActualIntPerXing = *corActualIntPerXing;
   shortTree->n_vx = *n_vx;
   shortTree->pu_hash = *pu_hash;
   shortTree->trigger_matched_electron = *trigger_matched_electron;
   shortTree->trigger_matched_muon = *trigger_matched_muon;
   shortTree->trigger_HLT_e120_lhloose = *trigger_HLT_e120_lhloose;
   shortTree->trigger_HLT_e140_lhloose_nod0 = *trigger_HLT_e140_lhloose_nod0;
   shortTree->trigger_HLT_e24_lhmedium_L1EM20VH = *trigger_HLT_e24_lhmedium_L1EM20VH;
   shortTree->trigger_HLT_e24_lhtight_nod0_ivarloose = *trigger_HLT_e24_lhtight_nod0_ivarloose;
   shortTree->trigger_HLT_e26_lhtight_nod0_ivarloose = *trigger_HLT_e26_lhtight_nod0_ivarloose;
   shortTree->trigger_HLT_e60_lhmedium = *trigger_HLT_e60_lhmedium;
   shortTree->trigger_HLT_e60_lhmedium_nod0 = *trigger_HLT_e60_lhmedium_nod0;
   shortTree->trigger_HLT_g140_loose = *trigger_HLT_g140_loose;
   shortTree->trigger_HLT_xe100_mht_L1XE50 = *trigger_HLT_xe100_mht_L1XE50;
   shortTree->trigger_HLT_xe110_mht_L1XE50 = *trigger_HLT_xe110_mht_L1XE50;
   shortTree->trigger_HLT_xe130_mht_L1XE50 = *trigger_HLT_xe130_mht_L1XE50;
   shortTree->trigger_HLT_xe70 = *trigger_HLT_xe70;
   shortTree->trigger_HLT_xe80_tc_lcw_L1XE50 = *trigger_HLT_xe80_tc_lcw_L1XE50;
   shortTree->trigger_HLT_xe90_mht_L1XE50 = *trigger_HLT_xe90_mht_L1XE50;
   shortTree->trigger_matched_HLT_e60_lhmedium = *trigger_matched_HLT_e60_lhmedium;
   shortTree->trigger_matched_HLT_e120_lhloose = *trigger_matched_HLT_e120_lhloose;
   shortTree->trigger_matched_HLT_e24_lhmedium_L1EM18VH = *trigger_matched_HLT_e24_lhmedium_L1EM18VH;
   shortTree->trigger_matched_HLT_e24_lhmedium_L1EM20VH = *trigger_matched_HLT_e24_lhmedium_L1EM20VH;
   shortTree->flag_bib = *flag_bib;
   shortTree->flag_bib_raw = *flag_bib_raw;
   shortTree->trigger_HLT_2e17_loose = *trigger_HLT_2e17_loose;
   shortTree->trigger_HLT_3j175 = *trigger_HLT_3j175;
   shortTree->trigger_HLT_4j85 = *trigger_HLT_4j85;
   shortTree->trigger_HLT_e120_lhloose_nod0 = *trigger_HLT_e120_lhloose_nod0;
   shortTree->trigger_HLT_e24_lhmedium_L1EM18VH = *trigger_HLT_e24_lhmedium_L1EM18VH;
   shortTree->trigger_HLT_e24_lhmedium_iloose_L1EM20VH = *trigger_HLT_e24_lhmedium_iloose_L1EM20VH;
   shortTree->trigger_HLT_e24_lhtight_iloose = *trigger_HLT_e24_lhtight_iloose;
   shortTree->trigger_HLT_e28_tight_iloose = *trigger_HLT_e28_tight_iloose;
   shortTree->trigger_HLT_g120_loose = *trigger_HLT_g120_loose;
   shortTree->trigger_HLT_g160_loose = *trigger_HLT_g160_loose;
   shortTree->trigger_HLT_g300_etcut = *trigger_HLT_g300_etcut;
   shortTree->trigger_HLT_ht700_L1J100 = *trigger_HLT_ht700_L1J100;
   shortTree->trigger_HLT_ht850_L1J100 = *trigger_HLT_ht850_L1J100;
   shortTree->trigger_HLT_mu24_imedium = *trigger_HLT_mu24_imedium;
   shortTree->trigger_HLT_mu60_0eta105_msonly = *trigger_HLT_mu60_0eta105_msonly;
   shortTree->trigger_HLT_xe100 = *trigger_HLT_xe100;
   shortTree->trigger_HLT_xe100_mht = *trigger_HLT_xe100_mht;
   shortTree->trigger_HLT_xe100_pueta = *trigger_HLT_xe100_pueta;
   shortTree->trigger_HLT_xe100_pufit = *trigger_HLT_xe100_pufit;
   shortTree->trigger_HLT_xe100_pufit_L1XE50 = *trigger_HLT_xe100_pufit_L1XE50;
   shortTree->trigger_HLT_xe100_pufit_L1XE55 = *trigger_HLT_xe100_pufit_L1XE55;
   shortTree->trigger_HLT_xe100_tc_em = *trigger_HLT_xe100_tc_em;
   shortTree->trigger_HLT_xe100_tc_lcw = *trigger_HLT_xe100_tc_lcw;
   shortTree->trigger_HLT_xe100_tc_lcw_L1XE50 = *trigger_HLT_xe100_tc_lcw_L1XE50;
   shortTree->trigger_HLT_xe100_tc_lcw_L1XE60 = *trigger_HLT_xe100_tc_lcw_L1XE60;
   shortTree->trigger_HLT_xe110_pufit_L1XE50 = *trigger_HLT_xe110_pufit_L1XE50;
   shortTree->trigger_HLT_xe110_pufit_L1XE55 = *trigger_HLT_xe110_pufit_L1XE55;
   shortTree->trigger_HLT_xe110_pufit_xe65_L1XE50 = *trigger_HLT_xe110_pufit_xe65_L1XE50;
   shortTree->trigger_HLT_xe110_pufit_xe70_L1XE50 = *trigger_HLT_xe110_pufit_xe70_L1XE50;
   shortTree->trigger_HLT_xe120_pufit_L1XE50 = *trigger_HLT_xe120_pufit_L1XE50;
   shortTree->trigger_HLT_xe70_pueta = *trigger_HLT_xe70_pueta;
   shortTree->trigger_HLT_xe70_pufit = *trigger_HLT_xe70_pufit;
   shortTree->trigger_HLT_xe70_tc_em = *trigger_HLT_xe70_tc_em;
   shortTree->trigger_HLT_xe70_tc_lcw = *trigger_HLT_xe70_tc_lcw;
   shortTree->trigger_HLT_xe80 = *trigger_HLT_xe80;
   shortTree->trigger_HLT_xe80_pueta = *trigger_HLT_xe80_pueta;
   shortTree->trigger_HLT_xe90_pufit_L1XE50 = *trigger_HLT_xe90_pufit_L1XE50;
   shortTree->trigger_HLT_xe90_tc_lcw_L1XE50 = *trigger_HLT_xe90_tc_lcw_L1XE50;
   shortTree->trigger_L1_XE50 = *trigger_L1_XE50;
   shortTree->trigger_L1_XE70 = *trigger_L1_XE70;
   shortTree->trigger_L2_2J15_XE55 = *trigger_L2_2J15_XE55;
   shortTree->trigger_ht700_L1J75 = *trigger_ht700_L1J75;
   shortTree->trigger_ht850_L1J75 = *trigger_ht850_L1J75;
   shortTree->n_ph = *n_ph;
   shortTree->n_ph_tight = *n_ph_tight;
   shortTree->n_ph_baseline = *n_ph_baseline;
   shortTree->n_ph_baseline_tight = *n_ph_baseline_tight;
   shortTree->n_jet_truth = *n_jet_truth;
   shortTree->truth_V_bare_eta = *truth_V_bare_eta;
   shortTree->truth_V_bare_phi = *truth_V_bare_phi;
   shortTree->truth_V_bare_m = *truth_V_bare_m;
   shortTree->truth_V_dressed_eta = *truth_V_dressed_eta;
   shortTree->truth_V_dressed_phi = *truth_V_dressed_phi;
   shortTree->truth_V_dressed_m = *truth_V_dressed_m;
   shortTree->truth_V_simple_eta = *truth_V_simple_eta;
   shortTree->truth_V_simple_phi = *truth_V_simple_phi;
   shortTree->truth_V_simple_m = *truth_V_simple_m;
   shortTree->met_eleterm_et = *met_eleterm_et;
   shortTree->met_eleterm_phi = *met_eleterm_phi;
   shortTree->met_eleterm_etx = *met_eleterm_etx;
   shortTree->met_eleterm_ety = *met_eleterm_ety;
   shortTree->met_eleterm_sumet = *met_eleterm_sumet;
   shortTree->met_jetterm_et = *met_jetterm_et;
   shortTree->met_jetterm_phi = *met_jetterm_phi;
   shortTree->met_jetterm_etx = *met_jetterm_etx;
   shortTree->met_jetterm_ety = *met_jetterm_ety;
   shortTree->met_jetterm_sumet = *met_jetterm_sumet;
   shortTree->met_muonterm_et = *met_muonterm_et;
   shortTree->met_muonterm_phi = *met_muonterm_phi;
   shortTree->met_muonterm_etx = *met_muonterm_etx;
   shortTree->met_muonterm_ety = *met_muonterm_ety;
   shortTree->met_muonterm_sumet = *met_muonterm_sumet;
   shortTree->met_muonterm_tst_et = *met_muonterm_tst_et;
   shortTree->met_muonterm_tst_phi = *met_muonterm_tst_phi;
   shortTree->met_muonterm_tst_etx = *met_muonterm_tst_etx;
   shortTree->met_muonterm_tst_ety = *met_muonterm_tst_ety;
   shortTree->met_muonterm_tst_sumet = *met_muonterm_tst_sumet;
   shortTree->met_noelectron_tst_et = *met_noelectron_tst_et;
   shortTree->met_noelectron_tst_phi = *met_noelectron_tst_phi;
   shortTree->met_noelectron_tst_etx = *met_noelectron_tst_etx;
   shortTree->met_noelectron_tst_ety = *met_noelectron_tst_ety;
   shortTree->met_noelectron_tst_sumet = *met_noelectron_tst_sumet;
   shortTree->met_nomuon_tst_et = *met_nomuon_tst_et;
   shortTree->met_nomuon_tst_phi = *met_nomuon_tst_phi;
   shortTree->met_nomuon_tst_etx = *met_nomuon_tst_etx;
   shortTree->met_nomuon_tst_ety = *met_nomuon_tst_ety;
   shortTree->met_nomuon_tst_sumet = *met_nomuon_tst_sumet;
   shortTree->met_nophoton_tst_et = *met_nophoton_tst_et;
   shortTree->met_nophoton_tst_phi = *met_nophoton_tst_phi;
   shortTree->met_nophoton_tst_etx = *met_nophoton_tst_etx;
   shortTree->met_nophoton_tst_ety = *met_nophoton_tst_ety;
   shortTree->met_nophoton_tst_sumet = *met_nophoton_tst_sumet;
   shortTree->met_phterm_et = *met_phterm_et;
   shortTree->met_phterm_phi = *met_phterm_phi;
   shortTree->met_phterm_etx = *met_phterm_etx;
   shortTree->met_phterm_ety = *met_phterm_ety;
   shortTree->met_phterm_sumet = *met_phterm_sumet;
   shortTree->met_softerm_tst_et = *met_softerm_tst_et;
   shortTree->met_softerm_tst_phi = *met_softerm_tst_phi;
   shortTree->met_softerm_tst_etx = *met_softerm_tst_etx;
   shortTree->met_softerm_tst_ety = *met_softerm_tst_ety;
   shortTree->met_softerm_tst_sumet = *met_softerm_tst_sumet;
   shortTree->met_track_et = *met_track_et;
   shortTree->met_track_phi = *met_track_phi;
   shortTree->met_track_etx = *met_track_etx;
   shortTree->met_track_ety = *met_track_ety;
   shortTree->met_track_sumet = *met_track_sumet;
   shortTree->met_truth_et = *met_truth_et;
   shortTree->met_truth_phi = *met_truth_phi;
   shortTree->met_truth_etx = *met_truth_etx;
   shortTree->met_truth_ety = *met_truth_ety;
   shortTree->met_truth_sumet = *met_truth_sumet;
   shortTree->met_tst_et = *met_tst_et;
   shortTree->met_tst_phi = *met_tst_phi;
   shortTree->met_tst_etx = *met_tst_etx;
   shortTree->met_tst_ety = *met_tst_ety;
   shortTree->met_tst_sumet = *met_tst_sumet;
   shortTree->LCTopoJet_ntrk = *LCTopoJet_ntrk;
   shortTree->TCCJet_ntrk = *TCCJet_ntrk;
   shortTree->tau_loose_multiplicity = *tau_loose_multiplicity;
   shortTree->tau_medium_multiplicity = *tau_medium_multiplicity;
   shortTree->tau_tight_multiplicity = *tau_tight_multiplicity;
   shortTree->tau_baseline_loose_multiplicity = *tau_baseline_loose_multiplicity;
   shortTree->tau_baseline_medium_multiplicity = *tau_baseline_medium_multiplicity;
   shortTree->tau_baseline_tight_multiplicity = *tau_baseline_tight_multiplicity;
   shortTree->weight = *weight;
   shortTree->mc_weight_sum = *mc_weight_sum;
   shortTree->extra_weight = *extra_weight;
   shortTree->campaign_lumi = *campaign_lumi;
   shortTree->lead_jet_central_pt = *lead_jet_central_pt;

   shortTree->mu_pt = &(*mu_pt);
   shortTree->mu_SF = &(*mu_SF);
   shortTree->mu_eta = &(*mu_eta);
   shortTree->mu_phi = &(*mu_phi);
   shortTree->mu_SF_iso = &(*mu_SF_iso);
   shortTree->mu_m = &(*mu_m);
   shortTree->mu_charge = &(*mu_charge);
   shortTree->mu_id_pt = &(*mu_id_pt);
   shortTree->mu_id_eta = &(*mu_id_eta);
   shortTree->mu_id_phi = &(*mu_id_phi);
   shortTree->mu_id_m = &(*mu_id_m);
   shortTree->mu_me_pt = &(*mu_me_pt);
   shortTree->mu_me_eta = &(*mu_me_eta);
   shortTree->mu_me_phi = &(*mu_me_phi);
   shortTree->mu_me_m = &(*mu_me_m);
   shortTree->mu_ptcone20 = &(*mu_ptcone20);
   shortTree->mu_ptvarcone20 = &(*mu_ptvarcone20);
   shortTree->mu_etcone20 = &(*mu_etcone20);
   shortTree->mu_topoetcone20 = &(*mu_topoetcone20);
   shortTree->mu_ptcone30 = &(*mu_ptcone30);
   shortTree->mu_ptvarcone30 = &(*mu_ptvarcone30);
   shortTree->mu_ptvarcone30_TightTTVA_pt1000 = &(*mu_ptvarcone30_TightTTVA_pt1000);
   shortTree->mu_etcone30 = &(*mu_etcone30);
   shortTree->mu_topoetcone30 = &(*mu_topoetcone30);
   shortTree->mu_ptcone40 = &(*mu_ptcone40);
   shortTree->mu_ptvarcone40 = &(*mu_ptvarcone40);
   shortTree->mu_etcone40 = &(*mu_etcone40);
   shortTree->mu_topoetcone40 = &(*mu_topoetcone40);
   shortTree->mu_author = &(*mu_author);
   shortTree->mu_quality = &(*mu_quality);
   shortTree->mu_isSA = &(*mu_isSA);
   shortTree->mu_met_nomuon_dphi = &(*mu_met_nomuon_dphi);
   shortTree->mu_met_wmuon_dphi = &(*mu_met_wmuon_dphi);
   shortTree->mu_truth_type = &(*mu_truth_type);
   shortTree->mu_truth_origin = &(*mu_truth_origin);
   shortTree->mu_baseline_pt = &(*mu_baseline_pt);
   shortTree->mu_baseline_SF = &(*mu_baseline_SF);
   shortTree->mu_baseline_eta = &(*mu_baseline_eta);
   shortTree->mu_baseline_phi = &(*mu_baseline_phi);
   shortTree->mu_baseline_isLooseID = &(*mu_baseline_isLooseID);
   shortTree->mu_baseline_isMediumID = &(*mu_baseline_isMediumID);
   shortTree->mu_baseline_isTightID = &(*mu_baseline_isTightID);
   shortTree->el_pt = &(*el_pt);
   shortTree->el_eta = &(*el_eta);
   shortTree->el_phi = &(*el_phi);
   shortTree->el_SF = &(*el_SF);
   shortTree->el_SF_iso = &(*el_SF_iso);
   shortTree->el_SF_trigger = &(*el_SF_trigger);
   shortTree->el_eff_trigger = &(*el_eff_trigger);
   shortTree->el_m = &(*el_m);
   shortTree->el_charge = &(*el_charge);
   shortTree->el_id_pt = &(*el_id_pt);
   shortTree->el_id_eta = &(*el_id_eta);
   shortTree->el_id_phi = &(*el_id_phi);
   shortTree->el_id_m = &(*el_id_m);
   shortTree->el_cl_pt = &(*el_cl_pt);
   shortTree->el_cl_eta = &(*el_cl_eta);
   shortTree->el_cl_etaBE2 = &(*el_cl_etaBE2);
   shortTree->el_cl_phi = &(*el_cl_phi);
   shortTree->el_cl_m = &(*el_cl_m);
   shortTree->el_ptcone20 = &(*el_ptcone20);
   shortTree->el_ptvarcone20 = &(*el_ptvarcone20);
   shortTree->el_ptvarcone20_TightTTVA_pt1000 = &(*el_ptvarcone20_TightTTVA_pt1000);
   shortTree->el_etcone20 = &(*el_etcone20);
   shortTree->el_topoetcone20 = &(*el_topoetcone20);
   shortTree->el_ptcone30 = &(*el_ptcone30);
   shortTree->el_ptvarcone30 = &(*el_ptvarcone30);
   shortTree->el_etcone30 = &(*el_etcone30);
   shortTree->el_topoetcone30 = &(*el_topoetcone30);
   shortTree->el_ptcone40 = &(*el_ptcone40);
   shortTree->el_ptvarcone40 = &(*el_ptvarcone40);
   shortTree->el_etcone40 = &(*el_etcone40);
   shortTree->el_topoetcone40 = &(*el_topoetcone40);
   shortTree->el_author = &(*el_author);
   shortTree->el_isConv = &(*el_isConv);
   shortTree->el_truth_pt = &(*el_truth_pt);
   shortTree->el_truth_eta = &(*el_truth_eta);
   shortTree->el_truth_phi = &(*el_truth_phi);
   shortTree->el_truth_status = &(*el_truth_status);
   shortTree->el_truth_type = &(*el_truth_type);
   shortTree->el_truth_origin = &(*el_truth_origin);
   shortTree->el_met_nomuon_dphi = &(*el_met_nomuon_dphi);
   shortTree->el_met_wmuon_dphi = &(*el_met_wmuon_dphi);
   shortTree->el_met_noelectron_dphi = &(*el_met_noelectron_dphi);
   shortTree->el_baseline_pt = &(*el_baseline_pt);
   shortTree->el_baseline_SF = &(*el_baseline_SF);
   shortTree->el_baseline_eta = &(*el_baseline_eta);
   shortTree->el_baseline_phi = &(*el_baseline_phi);
   shortTree->el_baseline_isLooseID = &(*el_baseline_isLooseID);
   shortTree->el_baseline_isMediumID = &(*el_baseline_isMediumID);
   shortTree->el_baseline_isTightID = &(*el_baseline_isTightID);
   shortTree->jet_pt = &(*jet_pt);
   shortTree->jet_eta = &(*jet_eta);
   shortTree->jet_phi = &(*jet_phi);
   shortTree->jet_m = &(*jet_m);
   shortTree->jet_fmax = &(*jet_fmax);
   shortTree->jet_fch = &(*jet_fch);
   shortTree->jet_MV2c10_discriminant = &(*jet_MV2c10_discriminant);
   shortTree->jet_MV2c20_discriminant = &(*jet_MV2c20_discriminant);
   shortTree->jet_isbjet = &(*jet_isbjet);
   shortTree->jet_PartonTruthLabelID = &(*jet_PartonTruthLabelID);
   shortTree->jet_ConeTruthLabelID = &(*jet_ConeTruthLabelID);
   shortTree->jet_met_nomuon_dphi = &(*jet_met_nomuon_dphi);
   shortTree->jet_met_wmuon_dphi = &(*jet_met_wmuon_dphi);
   shortTree->jet_met_noelectron_dphi = &(*jet_met_noelectron_dphi);
   shortTree->jet_met_nophoton_dphi = &(*jet_met_nophoton_dphi);
   shortTree->jet_weight = &(*jet_weight);
   shortTree->jet_raw_pt = &(*jet_raw_pt);
   shortTree->jet_raw_eta = &(*jet_raw_eta);
   shortTree->jet_raw_phi = &(*jet_raw_phi);
   shortTree->jet_raw_m = &(*jet_raw_m);
   shortTree->jet_timing = &(*jet_timing);
   shortTree->jet_emfrac = &(*jet_emfrac);
   shortTree->jet_hecf = &(*jet_hecf);
   shortTree->jet_hecq = &(*jet_hecq);
   shortTree->jet_larq = &(*jet_larq);
   shortTree->jet_avglarq = &(*jet_avglarq);
   shortTree->jet_negE = &(*jet_negE);
   shortTree->jet_lambda = &(*jet_lambda);
   shortTree->jet_lambda2 = &(*jet_lambda2);
   shortTree->jet_jvtxf = &(*jet_jvtxf);
   shortTree->jet_fmaxi = &(*jet_fmaxi);
   shortTree->jet_isbjet_loose = &(*jet_isbjet_loose);
   shortTree->jet_jvt = &(*jet_jvt);
   shortTree->jet_fjvt = &(*jet_fjvt);
   shortTree->jet_cleaning = &(*jet_cleaning);
   shortTree->jet_DFCommonJets_QGTagger_NTracks = &(*jet_DFCommonJets_QGTagger_NTracks);
   shortTree->jet_DFCommonJets_QGTagger_TracksWidth = &(*jet_DFCommonJets_QGTagger_TracksWidth);
   shortTree->jet_DFCommonJets_QGTagger_TracksC1 = &(*jet_DFCommonJets_QGTagger_TracksC1);
   shortTree->trackjet_pt = &(*trackjet_pt);
   shortTree->trackjet_eta = &(*trackjet_eta);
   shortTree->trackjet_phi = &(*trackjet_phi);
   shortTree->trackjet_m = &(*trackjet_m);
   shortTree->trackjet_fmax = &(*trackjet_fmax);
   shortTree->trackjet_fch = &(*trackjet_fch);
   shortTree->trackjet_MV2c10_discriminant = &(*trackjet_MV2c10_discriminant);
   shortTree->trackjet_MV2c20_discriminant = &(*trackjet_MV2c20_discriminant);
   shortTree->trackjet_isbjet = &(*trackjet_isbjet);
   shortTree->trackjet_PartonTruthLabelID = &(*trackjet_PartonTruthLabelID);
   shortTree->trackjet_ConeTruthLabelID = &(*trackjet_ConeTruthLabelID);
   shortTree->trackjet_met_nomuon_dphi = &(*trackjet_met_nomuon_dphi);
   shortTree->trackjet_met_wmuon_dphi = &(*trackjet_met_wmuon_dphi);
   shortTree->trackjet_met_noelectron_dphi = &(*trackjet_met_noelectron_dphi);
   shortTree->trackjet_met_nophoton_dphi = &(*trackjet_met_nophoton_dphi);
   shortTree->trackjet_weight = &(*trackjet_weight);
   shortTree->trackjet_raw_pt = &(*trackjet_raw_pt);
   shortTree->trackjet_raw_eta = &(*trackjet_raw_eta);
   shortTree->trackjet_raw_phi = &(*trackjet_raw_phi);
   shortTree->trackjet_raw_m = &(*trackjet_raw_m);
   shortTree->trackjet_timing = &(*trackjet_timing);
   shortTree->trackjet_emfrac = &(*trackjet_emfrac);
   shortTree->trackjet_hecf = &(*trackjet_hecf);
   shortTree->trackjet_hecq = &(*trackjet_hecq);
   shortTree->trackjet_larq = &(*trackjet_larq);
   shortTree->trackjet_avglarq = &(*trackjet_avglarq);
   shortTree->trackjet_negE = &(*trackjet_negE);
   shortTree->trackjet_lambda = &(*trackjet_lambda);
   shortTree->trackjet_lambda2 = &(*trackjet_lambda2);
   shortTree->trackjet_jvtxf = &(*trackjet_jvtxf);
   shortTree->trackjet_fmaxi = &(*trackjet_fmaxi);
   shortTree->trackjet_isbjet_loose = &(*trackjet_isbjet_loose);
   shortTree->trackjet_jvt = &(*trackjet_jvt);
   shortTree->trackjet_fjvt = &(*trackjet_fjvt);
   shortTree->trackjet_cleaning = &(*trackjet_cleaning);
   shortTree->trackjet_DFCommonJets_QGTagger_NTracks = &(*trackjet_DFCommonJets_QGTagger_NTracks);
   shortTree->trackjet_DFCommonJets_QGTagger_TracksWidth = &(*trackjet_DFCommonJets_QGTagger_TracksWidth);
   shortTree->trackjet_DFCommonJets_QGTagger_TracksC1 = &(*trackjet_DFCommonJets_QGTagger_TracksC1);
   shortTree->LCTopoJet_pt = &(*LCTopoJet_pt);
   shortTree->LCTopoJet_eta = &(*LCTopoJet_eta);
   shortTree->LCTopoJet_phi = &(*LCTopoJet_phi);
   shortTree->LCTopoJet_m = &(*LCTopoJet_m);
   shortTree->LCTopoJet_tau21 = &(*LCTopoJet_tau21);
   shortTree->LCTopoJet_D2 = &(*LCTopoJet_D2);
   shortTree->LCTopoJet_C2 = &(*LCTopoJet_C2);
   shortTree->LCTopoJet_nConstit = &(*LCTopoJet_nConstit);
   shortTree->LCTopoJet_passD2_W50 = &(*LCTopoJet_passD2_W50);
   shortTree->LCTopoJet_passD2_Z50 = &(*LCTopoJet_passD2_Z50);
   shortTree->LCTopoJet_passD2_W80 = &(*LCTopoJet_passD2_W80);
   shortTree->LCTopoJet_passD2_Z80 = &(*LCTopoJet_passD2_Z80);
   shortTree->LCTopoJet_passMass_W50 = &(*LCTopoJet_passMass_W50);
   shortTree->LCTopoJet_passMass_Z50 = &(*LCTopoJet_passMass_Z50);
   shortTree->LCTopoJet_passMass_W80 = &(*LCTopoJet_passMass_W80);
   shortTree->LCTopoJet_passMass_Z80 = &(*LCTopoJet_passMass_Z80);
   shortTree->LCTopoJet_cutD2_W50 = &(*LCTopoJet_cutD2_W50);
   shortTree->LCTopoJet_cutD2_Z50 = &(*LCTopoJet_cutD2_Z50);
   shortTree->LCTopoJet_cutD2_W80 = &(*LCTopoJet_cutD2_W80);
   shortTree->LCTopoJet_cutD2_Z80 = &(*LCTopoJet_cutD2_Z80);
   shortTree->LCTopoJet_cutMlow_W50 = &(*LCTopoJet_cutMlow_W50);
   shortTree->LCTopoJet_cutMlow_Z50 = &(*LCTopoJet_cutMlow_Z50);
   shortTree->LCTopoJet_cutMlow_W80 = &(*LCTopoJet_cutMlow_W80);
   shortTree->LCTopoJet_cutMlow_Z80 = &(*LCTopoJet_cutMlow_Z80);
   shortTree->LCTopoJet_cutMhigh_W50 = &(*LCTopoJet_cutMhigh_W50);
   shortTree->LCTopoJet_cutMhigh_Z50 = &(*LCTopoJet_cutMhigh_Z50);
   shortTree->LCTopoJet_cutMhigh_W80 = &(*LCTopoJet_cutMhigh_W80);
   shortTree->LCTopoJet_cutMhigh_Z80 = &(*LCTopoJet_cutMhigh_Z80);
   shortTree->LCTopoJet_weight = &(*LCTopoJet_weight);
   shortTree->TCCJet_pt = &(*TCCJet_pt);
   shortTree->TCCJet_eta = &(*TCCJet_eta);
   shortTree->TCCJet_phi = &(*TCCJet_phi);
   shortTree->TCCJet_m = &(*TCCJet_m);
   shortTree->TCCJet_tau21 = &(*TCCJet_tau21);
   shortTree->TCCJet_D2 = &(*TCCJet_D2);
   shortTree->TCCJet_C2 = &(*TCCJet_C2);
   shortTree->TCCJet_nConstit = &(*TCCJet_nConstit);
   shortTree->TCCJet_passD2_W50 = &(*TCCJet_passD2_W50);
   shortTree->TCCJet_passD2_Z50 = &(*TCCJet_passD2_Z50);
   shortTree->TCCJet_passD2_W80 = &(*TCCJet_passD2_W80);
   shortTree->TCCJet_passD2_Z80 = &(*TCCJet_passD2_Z80);
   shortTree->TCCJet_passMass_W50 = &(*TCCJet_passMass_W50);
   shortTree->TCCJet_passMass_Z50 = &(*TCCJet_passMass_Z50);
   shortTree->TCCJet_passMass_W80 = &(*TCCJet_passMass_W80);
   shortTree->TCCJet_passMass_Z80 = &(*TCCJet_passMass_Z80);
   shortTree->TCCJet_cutD2_W50 = &(*TCCJet_cutD2_W50);
   shortTree->TCCJet_cutD2_Z50 = &(*TCCJet_cutD2_Z50);
   shortTree->TCCJet_cutD2_W80 = &(*TCCJet_cutD2_W80);
   shortTree->TCCJet_cutD2_Z80 = &(*TCCJet_cutD2_Z80);
   shortTree->TCCJet_cutMlow_W50 = &(*TCCJet_cutMlow_W50);
   shortTree->TCCJet_cutMlow_Z50 = &(*TCCJet_cutMlow_Z50);
   shortTree->TCCJet_cutMlow_W80 = &(*TCCJet_cutMlow_W80);
   shortTree->TCCJet_cutMlow_Z80 = &(*TCCJet_cutMlow_Z80);
   shortTree->TCCJet_cutMhigh_W50 = &(*TCCJet_cutMhigh_W50);
   shortTree->TCCJet_cutMhigh_Z50 = &(*TCCJet_cutMhigh_Z50);
   shortTree->TCCJet_cutMhigh_W80 = &(*TCCJet_cutMhigh_W80);
   shortTree->TCCJet_cutMhigh_Z80 = &(*TCCJet_cutMhigh_Z80);
   shortTree->TCCJet_weight = &(*TCCJet_weight);
   shortTree->tau_pt = &(*tau_pt);
   shortTree->tau_SF = &(*tau_SF);
   shortTree->tau_idtool_pass_veryloose = &(*tau_idtool_pass_veryloose);
   shortTree->tau_idtool_pass_loose = &(*tau_idtool_pass_loose);
   shortTree->tau_idtool_pass_medium = &(*tau_idtool_pass_medium);
   shortTree->tau_idtool_pass_tight = &(*tau_idtool_pass_tight);
   shortTree->tau_eta = &(*tau_eta);
   shortTree->tau_phi = &(*tau_phi);
   shortTree->tau_baseline_pt = &(*tau_baseline_pt);
   shortTree->tau_baseline_SF = &(*tau_baseline_SF);
   shortTree->tau_baseline_idtool_pass_veryloose = &(*tau_baseline_idtool_pass_veryloose);
   shortTree->tau_baseline_idtool_pass_loose = &(*tau_baseline_idtool_pass_loose);
   shortTree->tau_baseline_idtool_pass_medium = &(*tau_baseline_idtool_pass_medium);
   shortTree->tau_baseline_idtool_pass_tight = &(*tau_baseline_idtool_pass_tight);
   shortTree->tau_truth_pt = &(*tau_truth_pt);
   shortTree->tau_truth_eta = &(*tau_truth_eta);
   shortTree->tau_truth_phi = &(*tau_truth_phi);
}


#endif // #ifdef MergedSelectorData_cxx
