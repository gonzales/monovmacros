#ifndef Cuts_h
#define Cuts_h

#include "methods.h"

namespace CUTS {
    typedef enum {
        MET_TRIGGER = 1,
        FCH = 2,
        EMFRAC = 3,
        TIMING = 4,
        MIN_DPHI_MULTIJET = 5,
        PRESELECTION = 6,
        LEPTON_VETO = 7,
        MET250 = 8,
        MET150 = 9,
        VETO_LARGE_JET = 10,
        GEQ_ONE_LARGE_JET = 11,
        TWO_CENTRAL_JETS = 12,
        DPHI_FATJET_MET = 13,
        DPHI_DIJET_MET = 14,
        DPHI_DIJET = 15,
        VETO_ASSOCIATED_TRACKBJET = 16,
        ONE_ASSOCIATED_TRACKBJET = 17,
        TWO_ASSOCIATED_TRACKBJET = 18,
        ONE_OR_TWO_ASSOCIATED_TRACKBJET = 19,
        LEQ_TWO_ASSOCIATED_TRACKBJET = 20,
        VETO_SEPARATED_TRACKBJET = 21,
        VETO_CENTRAL_BJET = 22,
        ONE_CENTRAL_BJET = 23,
        TWO_CENTRAL_BJET = 24,
        ONE_OR_TWO_CENTRAL_BJET = 25,
        LEQ_TWO_CENTRAL_BJET = 26,
        VTAG_80_D2 = 27,
        VTAG_50_D2 = 28,
        VTAG_80_MASS = 29,
        VTAG_50_MASS = 30,
        VTAG_80_NTRKS = 31,
        VTAG_50_NTRKS = 32,
        FAIL_80MERGED = 33,
        FAIL_50MERGED = 34,
        DIJET_DELTAR = 35,
        DIJET_MASS = 36,
        LEAD_CENTRAL_JET_PT45 = 37,
        SUMPT = 38,
        MET_TRACK = 39,
        TEST = 40,
        C2 = 41,
        C2_HARD = 42,
        PRESELECTION_NOMUON = 43,
        ONE_TIGHT_ISO_MUON = 44,
        MET_NOMUON_MASS = 45,
        MET_NOMUON150 = 46,
        SUBLEAD_CENTRAL_JET_PT20 = 47,
        GEQ_ONE_CENTRAL_BJET = 48,
        DPHI_DIJET_MET_NOMUON = 49,
        MIN_DPHI_MULTIJET_NOMUON = 50,
        MET_TRACK_NOMUON = 51,
        NO_CUT = 52,
        MET_SIG8 = 53,
        MET_SIG10 = 54,
        MET_SIG12 = 55,
        MET_SIG14 = 56,
        TRIGGER_HLT_xe70_mht = 57,
        TRIGGER_HLT_xe90_mht_L1XE50 = 58,
        TRIGGER_HLT_xe100_mht_L1XE50 = 59,
        TRIGGER_HLT_xe110_mht_L1XE50 = 60,
        TRIGGER_HLT_xe110_pufit_L1XE55 = 61,
        TRIGGER_HLT_xe110_pufit_L1XE50 = 62,
        TRIGGER_HLT_xe110_pufit_xe70_L1XE50 = 63,
        TRIGGER_HLT_xe110_pufit_xe65_L1XE50 = 64,
        MUNU_MT = 65,
        IS_2015 = 66,
        IS_2016 = 67,
        IS_2016_A = 68,
        IS_2016_B = 69,
        IS_2016_C = 70,
        IS_2017 = 71,
        IS_2017_A = 72,
        IS_2017_B = 73,
        IS_2018 = 74,
        IS_2018_A = 75,
        IS_2018_B = 76,
        TWO_LOOSE_ONE_MEDIUM_MUON = 77,
        DIMUON_MASS = 78,
        MET_NOMUON250 = 79,
        DPHI_FATJET_MET_NOMUON = 80,
        GEQ_ONE_ASSOCIATED_TRACKBJET = 81,
        ACTUALMU_LEQ20 = 82,
        ACTUALMU_GEQ20_LEQ40 = 83,
        ACTUALMU_GEQ40 = 84,
        AVERAGEMU_LEQ20 = 85,
        AVERAGEMU_GEQ20_LEQ40 = 86,
        AVERAGEMU_GEQ40 = 87,
        PRESELECTION_NOELECTRON = 88,
        MIN_DPHI_MULTIJET_NOELECTRON = 89,
        MET_TRACK_NOELECTRON = 90,
        ONE_TIGHT_ISO_ELECTRON = 91,
        EL_TRIGGER = 92,
        ENU_MT = 93,
        MET_NOELECTRON150 = 94,
        MET_NOELECTRON250 = 95,
        DPHI_DIJET_MET_NOELECTRON = 96,
        TWO_LOOSE_ONE_MEDIUM_ELECTRON = 97,
        DIELECTRON_MASS = 98,
        DPHI_FATJET_MET_NOELECTRON = 99,
        TRIGGER_HLT_e24_lhmedium_L1EM20VH = 100,
        TRIGGER_HLT_e60_lhmedium = 101,
        TRIGGER_HLT_e120_lhloose = 102,
        TRIGGER_HLT_e26_lhtight_nod0_ivarloose = 103,
        TRIGGER_HLT_e60_lhmedium_nod0 = 104,
        TRIGGER_HLT_e140_lhloose_nod0 = 105,
        PRESELECTION_NOELECTRON_USEMETTRIGGER = 106,
        ONE_MEDIUM_ID_TIGHT_ISO_ELECTRON = 107,
        MET70 = 108,
        POOR_METSIG5 = 109,
        JIANGLIU_PRESELECTION = 110,
        JIANGLIU_ELECTRON_ID = 111,
        JIANGLIU_DPHI_DIJET_MET_NOELECTRON = 112,
        JET_CLEANING = 113,
        JIANGLIU_ELECTRON_ID_WP = 114,
        JIANGLIU_ELECTRON_ID_ISO = 115,
        MUON_VETO = 116,
        PH_VETO = 117,
        TAU_VETO = 118,
        RUN_A = 119,
        RUN_B = 120,
        RUN_C = 121,
        RUN_D = 122,
        RUN_E = 123,
        RUN_F = 124,
        RUN_G = 125,
        RUN_H = 126,
        RUN_I = 127,
        RUN_J = 128,
        RUN_K = 129,
        RUN_L = 130,
        RUN_M = 131,
        RUN_N = 132,
        EL_TRIGGER_INCLUSIVE = 133,
        PRESELECTION_NOELECTRON_INCLUSIVE_EL_TRIGGER = 134,
        PRESELECTION_NOELECTRON_WITH_JET_CLEANING = 135,
        ALL_EL_ISO = 136,
        EL_ISO_PTVARCONE_NONZERO = 137,
        JIANGLIU_TWO_TIGHTID_ISO_MUONS = 138,
        JIANGLIU_PRESELECTION_NOMUON = 139,
        MET_TRIGGER_FROM_MJ_NOTE = 140,
        JIANGLIU_TWO_MEDIUMID_ISO_MUONS = 141,
        JIANGLIU_TWO_LOOSEID_ISO_MUONS = 142,
    } CUT_ID;

    TString AsString(int id) {

        TString prefix = (id < 0) ? "FAIL " : "";

        if (abs(id) == MET_TRIGGER)                                     return prefix + "MET_TRIGGER";
        if (abs(id) == FCH)                                             return prefix + "FCH";
        if (abs(id) == EMFRAC)                                          return prefix + "EMFRAC";
        if (abs(id) == TIMING)                                          return prefix + "TIMING";
        if (abs(id) == MIN_DPHI_MULTIJET)                               return prefix + "MIN_DPHI_MULTIJET";
        if (abs(id) == PRESELECTION)                                    return prefix + "PRESELECTION";
        if (abs(id) == LEPTON_VETO)                                     return prefix + "LEPTON_VETO";
        if (abs(id) == MET250)                                          return prefix + "MET250";
        if (abs(id) == MET150)                                          return prefix + "MET150";
        if (abs(id) == VETO_LARGE_JET)                                  return prefix + "VETO_LARGE_JET";
        if (abs(id) == GEQ_ONE_LARGE_JET)                               return prefix + "GEQ_ONE_LARGE_JET";
        if (abs(id) == TWO_CENTRAL_JETS)                                return prefix + "TWO_CENTRAL_JETS";
        if (abs(id) == DPHI_FATJET_MET)                                 return prefix + "DPHI_FATJET_MET";
        if (abs(id) == DPHI_DIJET_MET)                                  return prefix + "DPHI_DIJET_MET";
        if (abs(id) == DPHI_DIJET)                                      return prefix + "DPHI_DIJET";
        if (abs(id) == VETO_ASSOCIATED_TRACKBJET)                       return prefix + "VETO_ASSOCIATED_TRACKBJET";
        if (abs(id) == ONE_ASSOCIATED_TRACKBJET)                        return prefix + "ONE_ASSOCIATED_TRACKBJET";
        if (abs(id) == TWO_ASSOCIATED_TRACKBJET)                        return prefix + "TWO_ASSOCIATED_TRACKBJET";
        if (abs(id) == ONE_OR_TWO_ASSOCIATED_TRACKBJET)                 return prefix + "ONE_OR_TWO_ASSOCIATED_TRACKBJET";
        if (abs(id) == LEQ_TWO_ASSOCIATED_TRACKBJET)                    return prefix + "LEQ_TWO_ASSOCIATED_TRACKBJET";
        if (abs(id) == VETO_SEPARATED_TRACKBJET)                        return prefix + "VETO_SEPARATED_TRACKBJET";
        if (abs(id) == VETO_CENTRAL_BJET)                               return prefix + "VETO_CENTRAL_BJET";
        if (abs(id) == ONE_CENTRAL_BJET)                                return prefix + "ONE_CENTRAL_BJET";
        if (abs(id) == TWO_CENTRAL_BJET)                                return prefix + "TWO_CENTRAL_BJET";
        if (abs(id) == ONE_OR_TWO_CENTRAL_BJET)                         return prefix + "ONE_OR_TWO_CENTRAL_BJET";
        if (abs(id) == LEQ_TWO_CENTRAL_BJET)                            return prefix + "LEQ_TWO_CENTRAL_BJET";
        if (abs(id) == VTAG_80_D2)                                      return prefix + "VTAG_80_D2";
        if (abs(id) == VTAG_50_D2)                                      return prefix + "VTAG_50_D2";
        if (abs(id) == VTAG_80_MASS)                                    return prefix + "VTAG_80_MASS";
        if (abs(id) == VTAG_50_MASS)                                    return prefix + "VTAG_50_MASS";
        if (abs(id) == VTAG_80_NTRKS)                                   return prefix + "VTAG_80_NTRKS";
        if (abs(id) == VTAG_50_NTRKS)                                   return prefix + "VTAG_50_NTRKS";
        if (abs(id) == FAIL_80MERGED)                                   return prefix + "FAIL_80MERGED";
        if (abs(id) == FAIL_50MERGED)                                   return prefix + "FAIL_50MERGED";
        if (abs(id) == DIJET_DELTAR)                                    return prefix + "DIJET_DELTAR";
        if (abs(id) == DIJET_MASS)                                      return prefix + "DIJET_MASS";
        if (abs(id) == LEAD_CENTRAL_JET_PT45)                           return prefix + "LEAD_CENTRAL_JET_PT45";
        if (abs(id) == SUMPT)                                           return prefix + "SUMPT";
        if (abs(id) == MET_TRACK)                                       return prefix + "MET_TRACK";
        if (abs(id) == TEST)                                            return prefix + "TEST";
        if (abs(id) == C2)                                              return prefix + "C2";
        if (abs(id) == C2_HARD)                                         return prefix + "C2_HARD";
        if (abs(id) == PRESELECTION_NOMUON)                             return prefix + "PRESELECTION_NOMUON";
        if (abs(id) == ONE_TIGHT_ISO_MUON)                              return prefix + "ONE_TIGHT_ISO_MUON";
        if (abs(id) == MET_NOMUON_MASS)                                 return prefix + "MET_NOMUON_MASS";
        if (abs(id) == MET_NOMUON150)                                   return prefix + "MET_NOMUON150";
        if (abs(id) == SUBLEAD_CENTRAL_JET_PT20)                        return prefix + "SUBLEAD_CENTRAL_JET_PT20";
        if (abs(id) == GEQ_ONE_CENTRAL_BJET)                            return prefix + "GEQ_ONE_CENTRAL_BJET";
        if (abs(id) == DPHI_DIJET_MET_NOMUON)                           return prefix + "DPHI_DIJET_MET_NOMUON";
        if (abs(id) == MIN_DPHI_MULTIJET_NOMUON)                        return prefix + "MIN_DPHI_MULTIJET_NOMUON";
        if (abs(id) == MET_TRACK_NOMUON)                                return prefix + "MET_TRACK_NOMUON";
        if (abs(id) == NO_CUT)                                          return prefix + "NO_CUT";
        if (abs(id) == MET_SIG8)                                        return prefix + "MET_SIG8";
        if (abs(id) == MET_SIG10)                                       return prefix + "MET_SIG10";
        if (abs(id) == MET_SIG12)                                       return prefix + "MET_SIG12";
        if (abs(id) == MET_SIG14)                                       return prefix + "MET_SIG14";
        if (abs(id) == TRIGGER_HLT_xe70_mht)                            return prefix + "TRIGGER_HLT_xe70_mht";
        if (abs(id) == TRIGGER_HLT_xe90_mht_L1XE50)                     return prefix + "TRIGGER_HLT_xe90_mht_L1XE50";
        if (abs(id) == TRIGGER_HLT_xe100_mht_L1XE50)                    return prefix + "TRIGGER_HLT_xe100_mht_L1XE50";
        if (abs(id) == TRIGGER_HLT_xe110_mht_L1XE50)                    return prefix + "TRIGGER_HLT_xe110_mht_L1XE50";
        if (abs(id) == TRIGGER_HLT_xe110_pufit_L1XE55)                  return prefix + "TRIGGER_HLT_xe110_pufit_L1XE55";
        if (abs(id) == TRIGGER_HLT_xe110_pufit_L1XE50)                  return prefix + "TRIGGER_HLT_xe110_pufit_L1XE50";
        if (abs(id) == TRIGGER_HLT_xe110_pufit_xe70_L1XE50)             return prefix + "TRIGGER_HLT_xe110_pufit_xe70_L1XE50";
        if (abs(id) == TRIGGER_HLT_xe110_pufit_xe65_L1XE50)             return prefix + "TRIGGER_HLT_xe110_pufit_xe65_L1XE50";
        if (abs(id) == MUNU_MT)                                         return prefix + "MUNU_MT";
        if (abs(id) == IS_2015)                                         return prefix + "IS_2015";
        if (abs(id) == IS_2016)                                         return prefix + "IS_2016";
        if (abs(id) == IS_2016_A)                                       return prefix + "IS_2016_A";
        if (abs(id) == IS_2016_B)                                       return prefix + "IS_2016_B";
        if (abs(id) == IS_2016_C)                                       return prefix + "IS_2016_C";
        if (abs(id) == IS_2017)                                         return prefix + "IS_2017";
        if (abs(id) == IS_2017_A)                                       return prefix + "IS_2017_A";
        if (abs(id) == IS_2017_B)                                       return prefix + "IS_2017_B";
        if (abs(id) == IS_2018)                                         return prefix + "IS_2018";
        if (abs(id) == IS_2018_A)                                       return prefix + "IS_2018_A";
        if (abs(id) == IS_2018_B)                                       return prefix + "IS_2018_B";
        if (abs(id) == TWO_LOOSE_ONE_MEDIUM_MUON)                       return prefix + "TWO_LOOSE_ONE_MEDIUM_MUON";
        if (abs(id) == DIMUON_MASS)                                     return prefix + "DIMUON_MASS";
        if (abs(id) == MET_NOMUON250)                                   return prefix + "MET_NOMUON250";
        if (abs(id) == DPHI_FATJET_MET_NOMUON)                          return prefix + "DPHI_FATJET_MET_NOMUON";
        if (abs(id) == GEQ_ONE_ASSOCIATED_TRACKBJET)                    return prefix + "GEQ_ONE_ASSOCIATED_TRACKBJET";
        if (abs(id) == ACTUALMU_LEQ20)                                  return prefix + "ACTUALMU_LEQ20";
        if (abs(id) == ACTUALMU_GEQ20_LEQ40)                            return prefix + "ACTUALMU_GEQ20_LEQ40";
        if (abs(id) == ACTUALMU_GEQ40)                                  return prefix + "ACTUALMU_GEQ40";
        if (abs(id) == AVERAGEMU_LEQ20)                                 return prefix + "AVERAGEMU_LEQ20";
        if (abs(id) == AVERAGEMU_GEQ20_LEQ40)                           return prefix + "AVERAGEMU_GEQ20_LEQ40";
        if (abs(id) == AVERAGEMU_GEQ40)                                 return prefix + "AVERAGEMU_GEQ40";
        if (abs(id) == PRESELECTION_NOELECTRON)                         return prefix + "PRESELECTION_NOELECTRON";
        if (abs(id) == MIN_DPHI_MULTIJET_NOELECTRON)                    return prefix + "MIN_DPHI_MULTIJET_NOELECTRON";
        if (abs(id) == MET_TRACK_NOELECTRON)                            return prefix + "MET_TRACK_NOELECTRON";
        if (abs(id) == ONE_TIGHT_ISO_ELECTRON)                          return prefix + "ONE_TIGHT_ISO_ELECTRON";
        if (abs(id) == EL_TRIGGER)                                      return prefix + "EL_TRIGGER";
        if (abs(id) == ENU_MT)                                          return prefix + "ENU_MT";
        if (abs(id) == MET_NOELECTRON150)                               return prefix + "MET_NOELECTRON150";
        if (abs(id) == MET_NOELECTRON250)                               return prefix + "MET_NOELECTRON250";
        if (abs(id) == DPHI_DIJET_MET_NOELECTRON)                       return prefix + "DPHI_DIJET_MET_NOELECTRON";
        if (abs(id) == TWO_LOOSE_ONE_MEDIUM_ELECTRON)                   return prefix + "TWO_LOOSE_ONE_MEDIUM_ELECTRON";
        if (abs(id) == DIELECTRON_MASS)                                 return prefix + "DIELECTRON_MASS";
        if (abs(id) == DPHI_FATJET_MET_NOELECTRON)                      return prefix + "DPHI_FATJET_MET_NOELECTRON";
        if (abs(id) == TRIGGER_HLT_e24_lhmedium_L1EM20VH)               return prefix + "TRIGGER_HLT_e24_lhmedium_L1EM20VH";
        if (abs(id) == TRIGGER_HLT_e60_lhmedium)                        return prefix + "TRIGGER_HLT_e60_lhmedium";
        if (abs(id) == TRIGGER_HLT_e120_lhloose)                        return prefix + "TRIGGER_HLT_e120_lhloose";
        if (abs(id) == TRIGGER_HLT_e26_lhtight_nod0_ivarloose)          return prefix + "TRIGGER_HLT_e26_lhtight_nod0_ivarloose";
        if (abs(id) == TRIGGER_HLT_e60_lhmedium_nod0)                   return prefix + "TRIGGER_HLT_e60_lhmedium_nod0";
        if (abs(id) == TRIGGER_HLT_e140_lhloose_nod0)                   return prefix + "TRIGGER_HLT_e140_lhloose_nod0";
        if (abs(id) == PRESELECTION_NOELECTRON_USEMETTRIGGER)           return prefix + "PRESELECTION_NOELECTRON_USEMETTRIGGER";
        if (abs(id) == ONE_MEDIUM_ID_TIGHT_ISO_ELECTRON)                return prefix + "ONE_MEDIUM_ID_TIGHT_ISO_ELECTRON";
        if (abs(id) == MET70)                                           return prefix + "MET70";
        if (abs(id) == POOR_METSIG5)                                    return prefix + "POOR_METSIG5";
        if (abs(id) == JIANGLIU_PRESELECTION)                           return prefix + "JIANGLIU_PRESELECTION";
        if (abs(id) == JIANGLIU_ELECTRON_ID)                            return prefix + "JIANGLIU_ELECTRON_ID";
        if (abs(id) == JIANGLIU_DPHI_DIJET_MET_NOELECTRON)              return prefix + "JIANGLIU_DPHI_DIJET_MET_NOELECTRON";
        if (abs(id) == JET_CLEANING)                                    return prefix + "JET_CLEANING";
        if (abs(id) == JIANGLIU_ELECTRON_ID_WP)                         return prefix + "JIANGLIU_ELECTRON_ID_WP";
        if (abs(id) == JIANGLIU_ELECTRON_ID_ISO)                        return prefix + "JIANGLIU_ELECTRON_ID_ISO";
        if (abs(id) == MUON_VETO)                                       return prefix + "MUON_VETO";
        if (abs(id) == PH_VETO)                                         return prefix + "PH_VETO";
        if (abs(id) == TAU_VETO)                                        return prefix + "TAU_VETO";
        if (abs(id) == RUN_A)                                           return prefix + "RUN_A";
        if (abs(id) == RUN_B)                                           return prefix + "RUN_B";
        if (abs(id) == RUN_C)                                           return prefix + "RUN_C";
        if (abs(id) == RUN_D)                                           return prefix + "RUN_D";
        if (abs(id) == RUN_E)                                           return prefix + "RUN_E";
        if (abs(id) == RUN_F)                                           return prefix + "RUN_F";
        if (abs(id) == RUN_G)                                           return prefix + "RUN_G";
        if (abs(id) == RUN_H)                                           return prefix + "RUN_H";
        if (abs(id) == RUN_I)                                           return prefix + "RUN_I";
        if (abs(id) == RUN_J)                                           return prefix + "RUN_J";
        if (abs(id) == RUN_K)                                           return prefix + "RUN_K";
        if (abs(id) == RUN_L)                                           return prefix + "RUN_L";
        if (abs(id) == RUN_M)                                           return prefix + "RUN_M";
        if (abs(id) == RUN_N)                                           return prefix + "RUN_N";
        if (abs(id) == EL_TRIGGER_INCLUSIVE)                            return prefix + "EL_TRIGGER_INCLUSIVE";
        if (abs(id) == PRESELECTION_NOELECTRON_INCLUSIVE_EL_TRIGGER)    return prefix + "PRESELECTION_NOELECTRON_INCLUSIVE_EL_TRIGGER";
        if (abs(id) == PRESELECTION_NOELECTRON_WITH_JET_CLEANING)       return prefix + "PRESELECTION_NOELECTRON_WITH_JET_CLEANING";
        if (abs(id) == ALL_EL_ISO)                                      return prefix + "ALL_EL_ISO";
        if (abs(id) == EL_ISO_PTVARCONE_NONZERO)                        return prefix + "EL_ISO_PTVARCONE_NONZERO";
        if (abs(id) == JIANGLIU_TWO_TIGHTID_ISO_MUONS)                  return prefix + "JIANGLIU_TWO_TIGHTID_ISO_MUONS";
        if (abs(id) == JIANGLIU_PRESELECTION_NOMUON)                    return prefix + "JIANGLIU_PRESELECTION_NOMUON";
        if (abs(id) == MET_TRIGGER_FROM_MJ_NOTE)                        return prefix + "MET_TRIGGER_FROM_MJ_NOTE";
        if (abs(id) == JIANGLIU_TWO_MEDIUMID_ISO_MUONS)                 return prefix + "JIANGLIU_TWO_MEDIUMID_ISO_MUONS";
        if (abs(id) == JIANGLIU_TWO_LOOSEID_ISO_MUONS)                  return prefix + "JIANGLIU_TWO_LOOSEID_ISO_MUONS";

        coutError("CUTS::AsString -- CUT_ID " + to_string(id) + " not implemented");
        return "";
    }

};

#endif
