//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri May  1 11:04:59 2020 by ROOT version 6.20/04
// from TTree ClusterTree/
// found on file: group.phys-exotics.leadImage_312427.group.phys-exotics.DAOD_EXOT27_minitrees.root/group.phys-exotics.20874012._000001.minitrees.root
//////////////////////////////////////////////////////////

#ifndef ClusterTree_h
#define ClusterTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"

class ClusterTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           run;
   ULong64_t       event;
   Int_t           last;
   Int_t           year;
   Float_t         met_tst_sig;
   Float_t         xSec_SUSY;
   Float_t         k_factor;
   Float_t         filter_eff;
   Int_t           isSR;
   Int_t           isCR1mubveto;
   Int_t           isCR1mubtag;
   Int_t           isCR1ebveto;
   Int_t           isCR1ebtag;
   Int_t           isCR2e;
   Int_t           isCR2mu;
   Int_t           isCR1ph;
   Int_t           n_tau_baseline;
   Float_t         mconly_weight;
   Float_t         syst_weight;
   Float_t         pu_weight;
   Float_t         btag_weight;
   Float_t         jvt_weight;
   Float_t         truth_V_bare_pt;
   Float_t         truth_V_dressed_pt;
   Float_t         truth_V_simple_pt;
   Float_t         munu_mT;
   Float_t         enu_mT;
   Float_t         mumu_m;
   Float_t         ee_m;
   Float_t         dPhiLCTopoJetMet;
   Float_t         dPhiTCCJetMet;
   Float_t         m_LCTopoJet;
   Float_t         m_TCCJet;
   Int_t           n_jet_central;
   Float_t         dPhiDijetMet;
   Float_t         dPhiDijet;
   Float_t         dRDijet;
   Float_t         DijetSumPt;
   Float_t         TrijetSumPt;
   Float_t         DijetMass;
   Int_t           n_trackjet;
   Int_t           n_bcentralJet;
   Int_t           n_btrackJet;
   Int_t           MJ_passDeltaPhi;
   Int_t           n_trackLCTopoAssociatedBjet;
   Int_t           n_trackLCTopoAssociatedNotBjet;
   Int_t           n_trackLCTopoSeparatedBjet;
   Int_t           n_trackLCTopoSeparatedNotBjet;
   Int_t           n_trackTCCAssociatedBjet;
   Int_t           n_trackTCCAssociatedNotBjet;
   Int_t           n_trackTCCSeparatedBjet;
   Int_t           n_trackTCCSeparatedNotBjet;
   Float_t         ptV_1Muon_pt;
   Float_t         ptV_1Muon_eta;
   Float_t         ptV_1Muon_phi;
   Float_t         ptV_2Lepton_pt;
   Float_t         ptV_2Lepton_eta;
   Float_t         ptV_2Lepton_phi;
   Int_t           n_jet;
   Int_t           n_jet_preor;
   Int_t           n_tau_preor;
   Int_t           n_mu_preor;
   Int_t           n_el_preor;
   Int_t           n_ph_preor;
   Int_t           n_bjet;
   Int_t           n_el;
   Int_t           n_el_baseline;
   Int_t           n_mu_baseline;
   Int_t           n_mu_baseline_bad;
   Int_t           n_allmu_bad;
   Int_t           n_tau;
   Int_t           n_mu;
   vector<float>   *mconly_weights;
   Float_t         jvt_all_weight;
   Int_t           n_smallJet;
   Int_t           n_truthFatJet;
   Int_t           n_LCTopoJet;
   Int_t           n_TCCJet;
   Int_t           n_TARJet;
   Int_t           n_TruthTARJet;
   Int_t           n_tau_truth;
   Int_t           n_truthTop;
   Float_t         averageIntPerXing;
   Float_t         actualIntPerXing;
   Float_t         corAverageIntPerXing;
   Float_t         corActualIntPerXing;
   Int_t           n_vx;
   Float_t         pu_hash;
   Float_t         allmu_tot_SF;
   Int_t           trigger_matched_electron;
   Int_t           trigger_matched_muon;
   Int_t           trigger_HLT_e120_lhloose;
   Int_t           trigger_HLT_e140_lhloose_nod0;
   Int_t           trigger_HLT_e24_lhmedium_L1EM20VH;
   Int_t           trigger_HLT_e24_lhtight_nod0_ivarloose;
   Int_t           trigger_HLT_e26_lhtight_nod0_ivarloose;
   Int_t           trigger_HLT_e60_lhmedium;
   Int_t           trigger_HLT_e60_lhmedium_nod0;
   Int_t           trigger_HLT_g140_loose;
   Int_t           trigger_HLT_xe100_mht_L1XE50;
   Int_t           trigger_HLT_xe110_mht_L1XE50;
   Int_t           trigger_HLT_xe130_mht_L1XE50;
   Int_t           trigger_HLT_xe70;
   Int_t           trigger_HLT_xe80_tc_lcw_L1XE50;
   Int_t           trigger_HLT_xe90_mht_L1XE50;
   Int_t           trigger_pass;
   Int_t           trigger_matched_HLT_e60_lhmedium;
   Int_t           trigger_matched_HLT_e120_lhloose;
   Int_t           trigger_matched_HLT_e24_lhmedium_L1EM18VH;
   Int_t           trigger_matched_HLT_e24_lhmedium_L1EM20VH;
   Int_t           lbn;
   Int_t           bcid;
   Float_t         pdf_x1;
   Float_t         pdf_x2;
   Float_t         pdf_pdf1;
   Float_t         pdf_pdf2;
   Float_t         pdf_scale;
   Int_t           flag_bib;
   Int_t           flag_bib_raw;
   Int_t           flag_sct;
   Int_t           flag_core;
   Int_t           trigger_HLT_2e17_loose;
   Int_t           trigger_HLT_3j175;
   Int_t           trigger_HLT_4j85;
   Int_t           trigger_HLT_e120_lhloose_nod0;
   Int_t           trigger_HLT_e24_lhmedium_L1EM18VH;
   Int_t           trigger_HLT_e24_lhmedium_iloose_L1EM20VH;
   Int_t           trigger_HLT_e24_lhtight_iloose;
   Int_t           trigger_HLT_e28_tight_iloose;
   Int_t           trigger_HLT_g120_loose;
   Int_t           trigger_HLT_g160_loose;
   Int_t           trigger_HLT_g300_etcut;
   Int_t           trigger_HLT_ht700_L1J100;
   Int_t           trigger_HLT_ht850_L1J100;
   Int_t           trigger_HLT_j30_xe10_razor100;
   Int_t           trigger_HLT_j30_xe10_razor170;
   Int_t           trigger_HLT_j30_xe10_razor185;
   Int_t           trigger_HLT_j30_xe10_razor195;
   Int_t           trigger_HLT_j30_xe60_razor100;
   Int_t           trigger_HLT_j30_xe60_razor170;
   Int_t           trigger_HLT_j30_xe60_razor185;
   Int_t           trigger_HLT_j30_xe60_razor195;
   Int_t           trigger_HLT_j360;
   Int_t           trigger_HLT_j380;
   Int_t           trigger_HLT_mu24_imedium;
   Int_t           trigger_HLT_mu60_0eta105_msonly;
   Int_t           trigger_HLT_xe100;
   Int_t           trigger_HLT_xe100_mht;
   Int_t           trigger_HLT_xe100_pueta;
   Int_t           trigger_HLT_xe100_pufit;
   Int_t           trigger_HLT_xe100_pufit_L1XE50;
   Int_t           trigger_HLT_xe100_pufit_L1XE55;
   Int_t           trigger_HLT_xe100_tc_em;
   Int_t           trigger_HLT_xe100_tc_lcw;
   Int_t           trigger_HLT_xe100_tc_lcw_L1XE50;
   Int_t           trigger_HLT_xe100_tc_lcw_L1XE60;
   Int_t           trigger_HLT_xe110_pufit_L1XE50;
   Int_t           trigger_HLT_xe110_pufit_L1XE55;
   Int_t           trigger_HLT_xe110_pufit_xe65_L1XE50;
   Int_t           trigger_HLT_xe110_pufit_xe70_L1XE50;
   Int_t           trigger_HLT_xe120_pufit_L1XE50;
   Int_t           trigger_HLT_xe70_pueta;
   Int_t           trigger_HLT_xe70_pufit;
   Int_t           trigger_HLT_xe70_tc_em;
   Int_t           trigger_HLT_xe70_tc_lcw;
   Int_t           trigger_HLT_xe80;
   Int_t           trigger_HLT_xe80_pueta;
   Int_t           trigger_HLT_xe90_pufit_L1XE50;
   Int_t           trigger_HLT_xe90_tc_lcw_L1XE50;
   Int_t           trigger_L1_XE50;
   Int_t           trigger_L1_XE70;
   Int_t           trigger_L2_2J15_XE55;
   Int_t           trigger_ht700_L1J75;
   Int_t           trigger_ht850_L1J75;
   Int_t           hfor;
   Int_t           n_ph;
   Int_t           n_ph_tight;
   Int_t           n_ph_baseline;
   Int_t           n_ph_baseline_tight;
   Int_t           pdf_id1;
   Int_t           pdf_id2;
   Int_t           bb_decision;
   Int_t           n_jet_truth;
   Float_t         truth_V_bare_eta;
   Float_t         truth_V_bare_phi;
   Float_t         truth_V_bare_m;
   Float_t         truth_V_dressed_eta;
   Float_t         truth_V_dressed_phi;
   Float_t         truth_V_dressed_m;
   Float_t         truth_V_simple_eta;
   Float_t         truth_V_simple_phi;
   Float_t         truth_V_simple_m;
   Float_t         met_eleterm_et;
   Float_t         met_eleterm_phi;
   Float_t         met_eleterm_etx;
   Float_t         met_eleterm_ety;
   Float_t         met_eleterm_sumet;
   Float_t         met_jetterm_et;
   Float_t         met_jetterm_phi;
   Float_t         met_jetterm_etx;
   Float_t         met_jetterm_ety;
   Float_t         met_jetterm_sumet;
   Float_t         met_muonterm_et;
   Float_t         met_muonterm_phi;
   Float_t         met_muonterm_etx;
   Float_t         met_muonterm_ety;
   Float_t         met_muonterm_sumet;
   Float_t         met_muonterm_tst_et;
   Float_t         met_muonterm_tst_phi;
   Float_t         met_muonterm_tst_etx;
   Float_t         met_muonterm_tst_ety;
   Float_t         met_muonterm_tst_sumet;
   Float_t         met_noelectron_tst_et;
   Float_t         met_noelectron_tst_phi;
   Float_t         met_noelectron_tst_etx;
   Float_t         met_noelectron_tst_ety;
   Float_t         met_noelectron_tst_sumet;
   Float_t         met_nomuon_tst_et;
   Float_t         met_nomuon_tst_phi;
   Float_t         met_nomuon_tst_etx;
   Float_t         met_nomuon_tst_ety;
   Float_t         met_nomuon_tst_sumet;
   Float_t         met_nophoton_tst_et;
   Float_t         met_nophoton_tst_phi;
   Float_t         met_nophoton_tst_etx;
   Float_t         met_nophoton_tst_ety;
   Float_t         met_nophoton_tst_sumet;
   Float_t         met_phterm_et;
   Float_t         met_phterm_phi;
   Float_t         met_phterm_etx;
   Float_t         met_phterm_ety;
   Float_t         met_phterm_sumet;
   Float_t         met_softerm_tst_et;
   Float_t         met_softerm_tst_phi;
   Float_t         met_softerm_tst_etx;
   Float_t         met_softerm_tst_ety;
   Float_t         met_softerm_tst_sumet;
   Float_t         met_track_et;
   Float_t         met_track_phi;
   Float_t         met_track_etx;
   Float_t         met_track_ety;
   Float_t         met_track_sumet;
   Float_t         met_truth_et;
   Float_t         met_truth_phi;
   Float_t         met_truth_etx;
   Float_t         met_truth_ety;
   Float_t         met_truth_sumet;
   Float_t         met_tst_et;
   Float_t         met_tst_phi;
   Float_t         met_tst_etx;
   Float_t         met_tst_ety;
   Float_t         met_tst_sumet;
   vector<float>   *mu_pt;
   vector<double>  *mu_SF;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<double>  *mu_SF_iso;
   vector<float>   *mu_m;
   vector<float>   *mu_charge;
   vector<float>   *mu_id_pt;
   vector<float>   *mu_id_eta;
   vector<float>   *mu_id_phi;
   vector<float>   *mu_id_m;
   vector<float>   *mu_me_pt;
   vector<float>   *mu_me_eta;
   vector<float>   *mu_me_phi;
   vector<float>   *mu_me_m;
   vector<float>   *mu_ptcone20;
   vector<float>   *mu_ptvarcone20;
   vector<float>   *mu_etcone20;
   vector<float>   *mu_topoetcone20;
   vector<float>   *mu_ptcone30;
   vector<float>   *mu_ptvarcone30;
   vector<float>   *mu_ptvarcone30_TightTTVA_pt1000;
   vector<float>   *mu_etcone30;
   vector<float>   *mu_topoetcone30;
   vector<float>   *mu_ptcone40;
   vector<float>   *mu_ptvarcone40;
   vector<float>   *mu_etcone40;
   vector<float>   *mu_topoetcone40;
   vector<int>     *mu_author;
   vector<int>     *mu_quality;
   vector<int>     *mu_isSA;
   vector<float>   *mu_met_nomuon_dphi;
   vector<float>   *mu_met_wmuon_dphi;
   vector<int>     *mu_truth_type;
   vector<int>     *mu_truth_origin;
   vector<float>   *mu_baseline_pt;
   vector<double>  *mu_baseline_SF;
   vector<float>   *mu_baseline_eta;
   vector<float>   *mu_baseline_phi;
   vector<bool>    *mu_baseline_isLooseID;
   vector<bool>    *mu_baseline_isMediumID;
   vector<bool>    *mu_baseline_isTightID;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_BADMUON_SYS__1down;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_BADMUON_SYS__1up;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_ISO_STAT__1down;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_ISO_STAT__1up;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_ISO_SYS__1down;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_ISO_SYS__1up;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_RECO_STAT__1down;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_RECO_STAT__1up;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_RECO_SYS__1down;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_RECO_SYS__1up;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_TTVA_STAT__1down;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_TTVA_STAT__1up;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_TTVA_SYS__1down;
   vector<double>  *mu_baseline_SF_syst_MUON_EFF_TTVA_SYS__1up;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_phi;
   vector<double>  *el_SF;
   vector<double>  *el_SF_iso;
   vector<double>  *el_SF_trigger;
   vector<double>  *el_eff_trigger;
   vector<float>   *el_m;
   vector<float>   *el_charge;
   vector<float>   *el_id_pt;
   vector<float>   *el_id_eta;
   vector<float>   *el_id_phi;
   vector<float>   *el_id_m;
   vector<float>   *el_cl_pt;
   vector<float>   *el_cl_eta;
   vector<float>   *el_cl_etaBE2;
   vector<float>   *el_cl_phi;
   vector<float>   *el_cl_m;
   vector<float>   *el_ptcone20;
   vector<float>   *el_ptvarcone20;
   vector<float>   *el_ptvarcone20_TightTTVA_pt1000;
   vector<float>   *el_etcone20;
   vector<float>   *el_topoetcone20;
   vector<float>   *el_ptcone30;
   vector<float>   *el_ptvarcone30;
   vector<float>   *el_etcone30;
   vector<float>   *el_topoetcone30;
   vector<float>   *el_ptcone40;
   vector<float>   *el_ptvarcone40;
   vector<float>   *el_etcone40;
   vector<float>   *el_topoetcone40;
   vector<int>     *el_author;
   vector<int>     *el_isConv;
   vector<float>   *el_truth_pt;
   vector<float>   *el_truth_eta;
   vector<float>   *el_truth_phi;
   vector<int>     *el_truth_status;
   vector<int>     *el_truth_type;
   vector<int>     *el_truth_origin;
   vector<float>   *el_met_nomuon_dphi;
   vector<float>   *el_met_wmuon_dphi;
   vector<float>   *el_met_noelectron_dphi;
   vector<float>   *el_baseline_pt;
   vector<double>  *el_baseline_SF;
   vector<float>   *el_baseline_eta;
   vector<float>   *el_baseline_phi;
   vector<bool>    *el_baseline_isLooseID;
   vector<bool>    *el_baseline_isMediumID;
   vector<bool>    *el_baseline_isTightID;
   vector<double>  *el_baseline_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   vector<double>  *el_baseline_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   vector<double>  *el_baseline_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   vector<double>  *el_baseline_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   vector<double>  *el_baseline_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   vector<double>  *el_baseline_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_m;
   vector<float>   *jet_fmax;
   vector<float>   *jet_fch;
   vector<float>   *jet_MV2c10_discriminant;
   vector<float>   *jet_MV2c20_discriminant;
   vector<int>     *jet_isbjet;
   vector<int>     *jet_PartonTruthLabelID;
   vector<int>     *jet_ConeTruthLabelID;
   vector<float>   *jet_met_nomuon_dphi;
   vector<float>   *jet_met_wmuon_dphi;
   vector<float>   *jet_met_noelectron_dphi;
   vector<float>   *jet_met_nophoton_dphi;
   vector<float>   *jet_weight;
   vector<float>   *jet_raw_pt;
   vector<float>   *jet_raw_eta;
   vector<float>   *jet_raw_phi;
   vector<float>   *jet_raw_m;
   vector<float>   *jet_timing;
   vector<float>   *jet_emfrac;
   vector<float>   *jet_hecf;
   vector<float>   *jet_hecq;
   vector<float>   *jet_larq;
   vector<float>   *jet_avglarq;
   vector<float>   *jet_negE;
   vector<float>   *jet_lambda;
   vector<float>   *jet_lambda2;
   vector<float>   *jet_jvtxf;
   vector<int>     *jet_fmaxi;
   vector<int>     *jet_isbjet_loose;
   vector<float>   *jet_jvt;
   vector<float>   *jet_fjvt;
   vector<int>     *jet_cleaning;
   vector<float>   *jet_TruthLabelDeltaR_B;
   vector<float>   *jet_TruthLabelDeltaR_C;
   vector<float>   *jet_TruthLabelDeltaR_T;
   vector<int>     *jet_DFCommonJets_QGTagger_NTracks;
   vector<float>   *jet_DFCommonJets_QGTagger_TracksWidth;
   vector<float>   *jet_DFCommonJets_QGTagger_TracksC1;
   vector<float>   *trackjet_pt;
   vector<float>   *trackjet_eta;
   vector<float>   *trackjet_phi;
   vector<float>   *trackjet_m;
   vector<float>   *trackjet_fmax;
   vector<float>   *trackjet_fch;
   vector<float>   *trackjet_MV2c10_discriminant;
   vector<float>   *trackjet_MV2c20_discriminant;
   vector<int>     *trackjet_isbjet;
   vector<int>     *trackjet_PartonTruthLabelID;
   vector<int>     *trackjet_ConeTruthLabelID;
   vector<float>   *trackjet_met_nomuon_dphi;
   vector<float>   *trackjet_met_wmuon_dphi;
   vector<float>   *trackjet_met_noelectron_dphi;
   vector<float>   *trackjet_met_nophoton_dphi;
   vector<float>   *trackjet_weight;
   vector<float>   *trackjet_raw_pt;
   vector<float>   *trackjet_raw_eta;
   vector<float>   *trackjet_raw_phi;
   vector<float>   *trackjet_raw_m;
   vector<float>   *trackjet_timing;
   vector<float>   *trackjet_emfrac;
   vector<float>   *trackjet_hecf;
   vector<float>   *trackjet_hecq;
   vector<float>   *trackjet_larq;
   vector<float>   *trackjet_avglarq;
   vector<float>   *trackjet_negE;
   vector<float>   *trackjet_lambda;
   vector<float>   *trackjet_lambda2;
   vector<float>   *trackjet_jvtxf;
   vector<int>     *trackjet_fmaxi;
   vector<int>     *trackjet_isbjet_loose;
   vector<float>   *trackjet_jvt;
   vector<float>   *trackjet_fjvt;
   vector<int>     *trackjet_cleaning;
   vector<float>   *trackjet_TruthLabelDeltaR_B;
   vector<float>   *trackjet_TruthLabelDeltaR_C;
   vector<float>   *trackjet_TruthLabelDeltaR_T;
   vector<int>     *trackjet_DFCommonJets_QGTagger_NTracks;
   vector<float>   *trackjet_DFCommonJets_QGTagger_TracksWidth;
   vector<float>   *trackjet_DFCommonJets_QGTagger_TracksC1;
   vector<float>   *LCTopoJet_pt;
   vector<float>   *LCTopoJet_eta;
   vector<float>   *LCTopoJet_phi;
   vector<float>   *LCTopoJet_m;
   vector<float>   *LCTopoJet_tau21;
   vector<float>   *LCTopoJet_D2;
   vector<float>   *LCTopoJet_C2;
   Int_t           LCTopoJet_ntrk;
   vector<int>     *LCTopoJet_nConstit;
   vector<int>     *LCTopoJet_passD2_W50;
   vector<int>     *LCTopoJet_passD2_Z50;
   vector<int>     *LCTopoJet_passD2_W80;
   vector<int>     *LCTopoJet_passD2_Z80;
   vector<int>     *LCTopoJet_passMass_W50;
   vector<int>     *LCTopoJet_passMass_Z50;
   vector<int>     *LCTopoJet_passMass_W80;
   vector<int>     *LCTopoJet_passMass_Z80;
   vector<float>   *LCTopoJet_cutD2_W50;
   vector<float>   *LCTopoJet_cutD2_Z50;
   vector<float>   *LCTopoJet_cutD2_W80;
   vector<float>   *LCTopoJet_cutD2_Z80;
   vector<float>   *LCTopoJet_cutMlow_W50;
   vector<float>   *LCTopoJet_cutMlow_Z50;
   vector<float>   *LCTopoJet_cutMlow_W80;
   vector<float>   *LCTopoJet_cutMlow_Z80;
   vector<float>   *LCTopoJet_cutMhigh_W50;
   vector<float>   *LCTopoJet_cutMhigh_Z50;
   vector<float>   *LCTopoJet_cutMhigh_W80;
   vector<float>   *LCTopoJet_cutMhigh_Z80;
   vector<float>   *LCTopoJet_weight;
   vector<float>   *TCCJet_pt;
   vector<float>   *TCCJet_eta;
   vector<float>   *TCCJet_phi;
   vector<float>   *TCCJet_m;
   vector<float>   *TCCJet_tau21;
   vector<float>   *TCCJet_D2;
   vector<float>   *TCCJet_C2;
   Int_t           TCCJet_ntrk;
   vector<int>     *TCCJet_nConstit;
   vector<int>     *TCCJet_passD2_W50;
   vector<int>     *TCCJet_passD2_Z50;
   vector<int>     *TCCJet_passD2_W80;
   vector<int>     *TCCJet_passD2_Z80;
   vector<int>     *TCCJet_passMass_W50;
   vector<int>     *TCCJet_passMass_Z50;
   vector<int>     *TCCJet_passMass_W80;
   vector<int>     *TCCJet_passMass_Z80;
   vector<float>   *TCCJet_cutD2_W50;
   vector<float>   *TCCJet_cutD2_Z50;
   vector<float>   *TCCJet_cutD2_W80;
   vector<float>   *TCCJet_cutD2_Z80;
   vector<float>   *TCCJet_cutMlow_W50;
   vector<float>   *TCCJet_cutMlow_Z50;
   vector<float>   *TCCJet_cutMlow_W80;
   vector<float>   *TCCJet_cutMlow_Z80;
   vector<float>   *TCCJet_cutMhigh_W50;
   vector<float>   *TCCJet_cutMhigh_Z50;
   vector<float>   *TCCJet_cutMhigh_W80;
   vector<float>   *TCCJet_cutMhigh_Z80;
   vector<float>   *TCCJet_lead_clusters_pt;
   vector<float>   *TCCJet_lead_clusters_eta;
   vector<float>   *TCCJet_lead_clusters_phi;
   vector<float>   *TCCJet_lead_clusters_m;
   vector<float>   *TCCJet_weight;
   vector<float>   *ph_pt;
   vector<float>   *ph_eta;
   vector<float>   *ph_phi;
   vector<float>   *ph_truth_pt;
   vector<double>  *ph_SF;
   vector<double>  *ph_SF_iso;
   vector<int>     *ph_isotool_pass_fixedcuttight;
   vector<float>   *ph_m;
   vector<float>   *ph_ptcone20;
   vector<float>   *ph_ptvarcone20;
   vector<float>   *ph_etcone20;
   vector<float>   *ph_topoetcone20;
   vector<float>   *ph_ptcone30;
   vector<float>   *ph_ptvarcone30;
   vector<float>   *ph_etcone30;
   vector<float>   *ph_topoetcone30;
   vector<float>   *ph_ptcone40;
   vector<float>   *ph_ptvarcone40;
   vector<float>   *ph_etcone40;
   vector<float>   *ph_topoetcone40;
   vector<int>     *ph_isTight;
   vector<int>     *ph_isEM;
   vector<unsigned int> *ph_OQ;
   vector<int>     *ph_author;
   vector<int>     *ph_isConv;
   vector<float>   *ph_truth_eta;
   vector<float>   *ph_truth_phi;
   vector<int>     *ph_truth_status;
   vector<int>     *ph_truth_type;
   vector<int>     *ph_truth_origin;
   vector<float>   *ph_met_nomuon_dphi;
   vector<float>   *ph_met_wmuon_dphi;
   vector<float>   *ph_met_nophoton_dphi;
   vector<float>   *ph_baseline_pt;
   vector<double>  *ph_baseline_SF;
   vector<float>   *ph_baseline_eta;
   vector<float>   *ph_baseline_phi;
   vector<double>  *ph_baseline_SF_syst_PH_EFF_ID_Uncertainty__1down;
   vector<double>  *ph_baseline_SF_syst_PH_EFF_ID_Uncertainty__1up;
   vector<double>  *ph_baseline_SF_syst_PH_EFF_ISO_Uncertainty__1down;
   vector<double>  *ph_baseline_SF_syst_PH_EFF_ISO_Uncertainty__1up;

   // List of branches
   TBranch        *b_run;   //!
   TBranch        *b_event;   //!
   TBranch        *b_last;   //!
   TBranch        *b_year;   //!
   TBranch        *b_met_tst_sig;   //!
   TBranch        *b_xSec_SUSY;   //!
   TBranch        *b_k_factor;   //!
   TBranch        *b_filter_eff;   //!
   TBranch        *b_isSR;   //!
   TBranch        *b_isCR1mubveto;   //!
   TBranch        *b_isCR1mubtag;   //!
   TBranch        *b_isCR1ebveto;   //!
   TBranch        *b_isCR1ebtag;   //!
   TBranch        *b_isCR2e;   //!
   TBranch        *b_isCR2mu;   //!
   TBranch        *b_isCR1ph;   //!
   TBranch        *b_n_tau_baseline;   //!
   TBranch        *b_mconly_weight;   //!
   TBranch        *b_syst_weight;   //!
   TBranch        *b_pu_weight;   //!
   TBranch        *b_btag_weight;   //!
   TBranch        *b_jvt_weight;   //!
   TBranch        *b_truth_V_bare_pt;   //!
   TBranch        *b_truth_V_dressed_pt;   //!
   TBranch        *b_truth_V_simple_pt;   //!
   TBranch        *b_munu_mT;   //!
   TBranch        *b_enu_mT;   //!
   TBranch        *b_mumu_m;   //!
   TBranch        *b_ee_m;   //!
   TBranch        *b_dPhiLCTopoJetMet;   //!
   TBranch        *b_dPhiTCCJetMet;   //!
   TBranch        *b_m_LCTopoJet;   //!
   TBranch        *b_m_TCCJet;   //!
   TBranch        *b_n_jet_central;   //!
   TBranch        *b_dPhiDijetMet;   //!
   TBranch        *b_dPhiDijet;   //!
   TBranch        *b_dRDijet;   //!
   TBranch        *b_DijetSumPt;   //!
   TBranch        *b_TrijetSumPt;   //!
   TBranch        *b_DijetMass;   //!
   TBranch        *b_n_trackjet;   //!
   TBranch        *b_n_bcentralJet;   //!
   TBranch        *b_n_btrackJet;   //!
   TBranch        *b_MJ_passDeltaPhi;   //!
   TBranch        *b_n_trackLCTopoAssociatedBjet;   //!
   TBranch        *b_n_trackLCTopoAssociatedNotBjet;   //!
   TBranch        *b_n_trackLCTopoSeparatedBjet;   //!
   TBranch        *b_n_trackLCTopoSeparatedNotBjet;   //!
   TBranch        *b_n_trackTCCAssociatedBjet;   //!
   TBranch        *b_n_trackTCCAssociatedNotBjet;   //!
   TBranch        *b_n_trackTCCSeparatedBjet;   //!
   TBranch        *b_n_trackTCCSeparatedNotBjet;   //!
   TBranch        *b_ptV_1Muon_pt;   //!
   TBranch        *b_ptV_1Muon_eta;   //!
   TBranch        *b_ptV_1Muon_phi;   //!
   TBranch        *b_ptV_2Lepton_pt;   //!
   TBranch        *b_ptV_2Lepton_eta;   //!
   TBranch        *b_ptV_2Lepton_phi;   //!
   TBranch        *b_n_jet;   //!
   TBranch        *b_n_jet_preor;   //!
   TBranch        *b_n_tau_preor;   //!
   TBranch        *b_n_mu_preor;   //!
   TBranch        *b_n_el_preor;   //!
   TBranch        *b_n_ph_preor;   //!
   TBranch        *b_n_bjet;   //!
   TBranch        *b_n_el;   //!
   TBranch        *b_n_el_baseline;   //!
   TBranch        *b_n_mu_baseline;   //!
   TBranch        *b_n_mu_baseline_bad;   //!
   TBranch        *b_n_allmu_bad;   //!
   TBranch        *b_n_tau;   //!
   TBranch        *b_n_mu;   //!
   TBranch        *b_mconly_weights;   //!
   TBranch        *b_jvt_all_weight;   //!
   TBranch        *b_n_smallJet;   //!
   TBranch        *b_n_truthFatJet;   //!
   TBranch        *b_n_LCTopoJet;   //!
   TBranch        *b_n_TCCJet;   //!
   TBranch        *b_n_TARJet;   //!
   TBranch        *b_n_TruthTARJet;   //!
   TBranch        *b_n_tau_truth;   //!
   TBranch        *b_n_truthTop;   //!
   TBranch        *b_averageIntPerXing;   //!
   TBranch        *b_actualIntPerXing;   //!
   TBranch        *b_corAverageIntPerXing;   //!
   TBranch        *b_corActualIntPerXing;   //!
   TBranch        *b_n_vx;   //!
   TBranch        *b_pu_hash;   //!
   TBranch        *b_allmu_tot_SF;   //!
   TBranch        *b_trigger_matched_electron;   //!
   TBranch        *b_trigger_matched_muon;   //!
   TBranch        *b_trigger_HLT_e120_lhloose;   //!
   TBranch        *b_trigger_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_trigger_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_trigger_HLT_e24_lhtight_nod0_ivarloose;   //!
   TBranch        *b_trigger_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_trigger_HLT_e60_lhmedium;   //!
   TBranch        *b_trigger_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_trigger_HLT_g140_loose;   //!
   TBranch        *b_trigger_HLT_xe100_mht_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe110_mht_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe130_mht_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe70;   //!
   TBranch        *b_trigger_HLT_xe80_tc_lcw_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe90_mht_L1XE50;   //!
   TBranch        *b_trigger_pass;   //!
   TBranch        *b_trigger_matched_HLT_e60_lhmedium;   //!
   TBranch        *b_trigger_matched_HLT_e120_lhloose;   //!
   TBranch        *b_trigger_matched_HLT_e24_lhmedium_L1EM18VH;   //!
   TBranch        *b_trigger_matched_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_pdf_x1;   //!
   TBranch        *b_pdf_x2;   //!
   TBranch        *b_pdf_pdf1;   //!
   TBranch        *b_pdf_pdf2;   //!
   TBranch        *b_pdf_scale;   //!
   TBranch        *b_flag_bib;   //!
   TBranch        *b_flag_bib_raw;   //!
   TBranch        *b_flag_sct;   //!
   TBranch        *b_flag_core;   //!
   TBranch        *b_trigger_HLT_2e17_loose;   //!
   TBranch        *b_trigger_HLT_3j175;   //!
   TBranch        *b_trigger_HLT_4j85;   //!
   TBranch        *b_trigger_HLT_e120_lhloose_nod0;   //!
   TBranch        *b_trigger_HLT_e24_lhmedium_L1EM18VH;   //!
   TBranch        *b_trigger_HLT_e24_lhmedium_iloose_L1EM20VH;   //!
   TBranch        *b_trigger_HLT_e24_lhtight_iloose;   //!
   TBranch        *b_trigger_HLT_e28_tight_iloose;   //!
   TBranch        *b_trigger_HLT_g120_loose;   //!
   TBranch        *b_trigger_HLT_g160_loose;   //!
   TBranch        *b_trigger_HLT_g300_etcut;   //!
   TBranch        *b_trigger_HLT_ht700_L1J100;   //!
   TBranch        *b_trigger_HLT_ht850_L1J100;   //!
   TBranch        *b_trigger_HLT_j30_xe10_razor100;   //!
   TBranch        *b_trigger_HLT_j30_xe10_razor170;   //!
   TBranch        *b_trigger_HLT_j30_xe10_razor185;   //!
   TBranch        *b_trigger_HLT_j30_xe10_razor195;   //!
   TBranch        *b_trigger_HLT_j30_xe60_razor100;   //!
   TBranch        *b_trigger_HLT_j30_xe60_razor170;   //!
   TBranch        *b_trigger_HLT_j30_xe60_razor185;   //!
   TBranch        *b_trigger_HLT_j30_xe60_razor195;   //!
   TBranch        *b_trigger_HLT_j360;   //!
   TBranch        *b_trigger_HLT_j380;   //!
   TBranch        *b_trigger_HLT_mu24_imedium;   //!
   TBranch        *b_trigger_HLT_mu60_0eta105_msonly;   //!
   TBranch        *b_trigger_HLT_xe100;   //!
   TBranch        *b_trigger_HLT_xe100_mht;   //!
   TBranch        *b_trigger_HLT_xe100_pueta;   //!
   TBranch        *b_trigger_HLT_xe100_pufit;   //!
   TBranch        *b_trigger_HLT_xe100_pufit_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe100_pufit_L1XE55;   //!
   TBranch        *b_trigger_HLT_xe100_tc_em;   //!
   TBranch        *b_trigger_HLT_xe100_tc_lcw;   //!
   TBranch        *b_trigger_HLT_xe100_tc_lcw_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe100_tc_lcw_L1XE60;   //!
   TBranch        *b_trigger_HLT_xe110_pufit_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe110_pufit_L1XE55;   //!
   TBranch        *b_trigger_HLT_xe110_pufit_xe65_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe110_pufit_xe70_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe120_pufit_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe70_pueta;   //!
   TBranch        *b_trigger_HLT_xe70_pufit;   //!
   TBranch        *b_trigger_HLT_xe70_tc_em;   //!
   TBranch        *b_trigger_HLT_xe70_tc_lcw;   //!
   TBranch        *b_trigger_HLT_xe80;   //!
   TBranch        *b_trigger_HLT_xe80_pueta;   //!
   TBranch        *b_trigger_HLT_xe90_pufit_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe90_tc_lcw_L1XE50;   //!
   TBranch        *b_trigger_L1_XE50;   //!
   TBranch        *b_trigger_L1_XE70;   //!
   TBranch        *b_trigger_L2_2J15_XE55;   //!
   TBranch        *b_trigger_ht700_L1J75;   //!
   TBranch        *b_trigger_ht850_L1J75;   //!
   TBranch        *b_hfor;   //!
   TBranch        *b_n_ph;   //!
   TBranch        *b_n_ph_tight;   //!
   TBranch        *b_n_ph_baseline;   //!
   TBranch        *b_n_ph_baseline_tight;   //!
   TBranch        *b_pdf_id1;   //!
   TBranch        *b_pdf_id2;   //!
   TBranch        *b_bb_decision;   //!
   TBranch        *b_n_jet_truth;   //!
   TBranch        *b_truth_V_bare_eta;   //!
   TBranch        *b_truth_V_bare_phi;   //!
   TBranch        *b_truth_V_bare_m;   //!
   TBranch        *b_truth_V_dressed_eta;   //!
   TBranch        *b_truth_V_dressed_phi;   //!
   TBranch        *b_truth_V_dressed_m;   //!
   TBranch        *b_truth_V_simple_eta;   //!
   TBranch        *b_truth_V_simple_phi;   //!
   TBranch        *b_truth_V_simple_m;   //!
   TBranch        *b_met_eleterm_et;   //!
   TBranch        *b_met_eleterm_phi;   //!
   TBranch        *b_met_eleterm_etx;   //!
   TBranch        *b_met_eleterm_ety;   //!
   TBranch        *b_met_eleterm_sumet;   //!
   TBranch        *b_met_jetterm_et;   //!
   TBranch        *b_met_jetterm_phi;   //!
   TBranch        *b_met_jetterm_etx;   //!
   TBranch        *b_met_jetterm_ety;   //!
   TBranch        *b_met_jetterm_sumet;   //!
   TBranch        *b_met_muonterm_et;   //!
   TBranch        *b_met_muonterm_phi;   //!
   TBranch        *b_met_muonterm_etx;   //!
   TBranch        *b_met_muonterm_ety;   //!
   TBranch        *b_met_muonterm_sumet;   //!
   TBranch        *b_met_muonterm_tst_et;   //!
   TBranch        *b_met_muonterm_tst_phi;   //!
   TBranch        *b_met_muonterm_tst_etx;   //!
   TBranch        *b_met_muonterm_tst_ety;   //!
   TBranch        *b_met_muonterm_tst_sumet;   //!
   TBranch        *b_met_noelectron_tst_et;   //!
   TBranch        *b_met_noelectron_tst_phi;   //!
   TBranch        *b_met_noelectron_tst_etx;   //!
   TBranch        *b_met_noelectron_tst_ety;   //!
   TBranch        *b_met_noelectron_tst_sumet;   //!
   TBranch        *b_met_nomuon_tst_et;   //!
   TBranch        *b_met_nomuon_tst_phi;   //!
   TBranch        *b_met_nomuon_tst_etx;   //!
   TBranch        *b_met_nomuon_tst_ety;   //!
   TBranch        *b_met_nomuon_tst_sumet;   //!
   TBranch        *b_met_nophoton_tst_et;   //!
   TBranch        *b_met_nophoton_tst_phi;   //!
   TBranch        *b_met_nophoton_tst_etx;   //!
   TBranch        *b_met_nophoton_tst_ety;   //!
   TBranch        *b_met_nophoton_tst_sumet;   //!
   TBranch        *b_met_phterm_et;   //!
   TBranch        *b_met_phterm_phi;   //!
   TBranch        *b_met_phterm_etx;   //!
   TBranch        *b_met_phterm_ety;   //!
   TBranch        *b_met_phterm_sumet;   //!
   TBranch        *b_met_softerm_tst_et;   //!
   TBranch        *b_met_softerm_tst_phi;   //!
   TBranch        *b_met_softerm_tst_etx;   //!
   TBranch        *b_met_softerm_tst_ety;   //!
   TBranch        *b_met_softerm_tst_sumet;   //!
   TBranch        *b_met_track_et;   //!
   TBranch        *b_met_track_phi;   //!
   TBranch        *b_met_track_etx;   //!
   TBranch        *b_met_track_ety;   //!
   TBranch        *b_met_track_sumet;   //!
   TBranch        *b_met_truth_et;   //!
   TBranch        *b_met_truth_phi;   //!
   TBranch        *b_met_truth_etx;   //!
   TBranch        *b_met_truth_ety;   //!
   TBranch        *b_met_truth_sumet;   //!
   TBranch        *b_met_tst_et;   //!
   TBranch        *b_met_tst_phi;   //!
   TBranch        *b_met_tst_etx;   //!
   TBranch        *b_met_tst_ety;   //!
   TBranch        *b_met_tst_sumet;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_SF;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_SF_iso;   //!
   TBranch        *b_mu_m;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_id_pt;   //!
   TBranch        *b_mu_id_eta;   //!
   TBranch        *b_mu_id_phi;   //!
   TBranch        *b_mu_id_m;   //!
   TBranch        *b_mu_me_pt;   //!
   TBranch        *b_mu_me_eta;   //!
   TBranch        *b_mu_me_phi;   //!
   TBranch        *b_mu_me_m;   //!
   TBranch        *b_mu_ptcone20;   //!
   TBranch        *b_mu_ptvarcone20;   //!
   TBranch        *b_mu_etcone20;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_ptcone30;   //!
   TBranch        *b_mu_ptvarcone30;   //!
   TBranch        *b_mu_ptvarcone30_TightTTVA_pt1000;   //!
   TBranch        *b_mu_etcone30;   //!
   TBranch        *b_mu_topoetcone30;   //!
   TBranch        *b_mu_ptcone40;   //!
   TBranch        *b_mu_ptvarcone40;   //!
   TBranch        *b_mu_etcone40;   //!
   TBranch        *b_mu_topoetcone40;   //!
   TBranch        *b_mu_author;   //!
   TBranch        *b_mu_quality;   //!
   TBranch        *b_mu_isSA;   //!
   TBranch        *b_mu_met_nomuon_dphi;   //!
   TBranch        *b_mu_met_wmuon_dphi;   //!
   TBranch        *b_mu_truth_type;   //!
   TBranch        *b_mu_truth_origin;   //!
   TBranch        *b_mu_baseline_pt;   //!
   TBranch        *b_mu_baseline_SF;   //!
   TBranch        *b_mu_baseline_eta;   //!
   TBranch        *b_mu_baseline_phi;   //!
   TBranch        *b_mu_baseline_isLooseID;   //!
   TBranch        *b_mu_baseline_isMediumID;   //!
   TBranch        *b_mu_baseline_isTightID;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_BADMUON_SYS__1down;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_BADMUON_SYS__1up;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_ISO_STAT__1down;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_ISO_STAT__1up;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_ISO_SYS__1down;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_ISO_SYS__1up;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_RECO_STAT__1down;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_RECO_STAT__1up;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_RECO_SYS__1down;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_RECO_SYS__1up;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_TTVA_STAT__1down;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_TTVA_STAT__1up;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_TTVA_SYS__1down;   //!
   TBranch        *b_mu_baseline_SF_syst_MUON_EFF_TTVA_SYS__1up;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_SF;   //!
   TBranch        *b_el_SF_iso;   //!
   TBranch        *b_el_SF_trigger;   //!
   TBranch        *b_el_eff_trigger;   //!
   TBranch        *b_el_m;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_id_pt;   //!
   TBranch        *b_el_id_eta;   //!
   TBranch        *b_el_id_phi;   //!
   TBranch        *b_el_id_m;   //!
   TBranch        *b_el_cl_pt;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_cl_etaBE2;   //!
   TBranch        *b_el_cl_phi;   //!
   TBranch        *b_el_cl_m;   //!
   TBranch        *b_el_ptcone20;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_ptvarcone20_TightTTVA_pt1000;   //!
   TBranch        *b_el_etcone20;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptcone30;   //!
   TBranch        *b_el_ptvarcone30;   //!
   TBranch        *b_el_etcone30;   //!
   TBranch        *b_el_topoetcone30;   //!
   TBranch        *b_el_ptcone40;   //!
   TBranch        *b_el_ptvarcone40;   //!
   TBranch        *b_el_etcone40;   //!
   TBranch        *b_el_topoetcone40;   //!
   TBranch        *b_el_author;   //!
   TBranch        *b_el_isConv;   //!
   TBranch        *b_el_truth_pt;   //!
   TBranch        *b_el_truth_eta;   //!
   TBranch        *b_el_truth_phi;   //!
   TBranch        *b_el_truth_status;   //!
   TBranch        *b_el_truth_type;   //!
   TBranch        *b_el_truth_origin;   //!
   TBranch        *b_el_met_nomuon_dphi;   //!
   TBranch        *b_el_met_wmuon_dphi;   //!
   TBranch        *b_el_met_noelectron_dphi;   //!
   TBranch        *b_el_baseline_pt;   //!
   TBranch        *b_el_baseline_SF;   //!
   TBranch        *b_el_baseline_eta;   //!
   TBranch        *b_el_baseline_phi;   //!
   TBranch        *b_el_baseline_isLooseID;   //!
   TBranch        *b_el_baseline_isMediumID;   //!
   TBranch        *b_el_baseline_isTightID;   //!
   TBranch        *b_el_baseline_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_el_baseline_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_el_baseline_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_el_baseline_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_el_baseline_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_el_baseline_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_m;   //!
   TBranch        *b_jet_fmax;   //!
   TBranch        *b_jet_fch;   //!
   TBranch        *b_jet_MV2c10_discriminant;   //!
   TBranch        *b_jet_MV2c20_discriminant;   //!
   TBranch        *b_jet_isbjet;   //!
   TBranch        *b_jet_PartonTruthLabelID;   //!
   TBranch        *b_jet_ConeTruthLabelID;   //!
   TBranch        *b_jet_met_nomuon_dphi;   //!
   TBranch        *b_jet_met_wmuon_dphi;   //!
   TBranch        *b_jet_met_noelectron_dphi;   //!
   TBranch        *b_jet_met_nophoton_dphi;   //!
   TBranch        *b_jet_weight;   //!
   TBranch        *b_jet_raw_pt;   //!
   TBranch        *b_jet_raw_eta;   //!
   TBranch        *b_jet_raw_phi;   //!
   TBranch        *b_jet_raw_m;   //!
   TBranch        *b_jet_timing;   //!
   TBranch        *b_jet_emfrac;   //!
   TBranch        *b_jet_hecf;   //!
   TBranch        *b_jet_hecq;   //!
   TBranch        *b_jet_larq;   //!
   TBranch        *b_jet_avglarq;   //!
   TBranch        *b_jet_negE;   //!
   TBranch        *b_jet_lambda;   //!
   TBranch        *b_jet_lambda2;   //!
   TBranch        *b_jet_jvtxf;   //!
   TBranch        *b_jet_fmaxi;   //!
   TBranch        *b_jet_isbjet_loose;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_fjvt;   //!
   TBranch        *b_jet_cleaning;   //!
   TBranch        *b_jet_TruthLabelDeltaR_B;   //!
   TBranch        *b_jet_TruthLabelDeltaR_C;   //!
   TBranch        *b_jet_TruthLabelDeltaR_T;   //!
   TBranch        *b_jet_DFCommonJets_QGTagger_NTracks;   //!
   TBranch        *b_jet_DFCommonJets_QGTagger_TracksWidth;   //!
   TBranch        *b_jet_DFCommonJets_QGTagger_TracksC1;   //!
   TBranch        *b_trackjet_pt;   //!
   TBranch        *b_trackjet_eta;   //!
   TBranch        *b_trackjet_phi;   //!
   TBranch        *b_trackjet_m;   //!
   TBranch        *b_trackjet_fmax;   //!
   TBranch        *b_trackjet_fch;   //!
   TBranch        *b_trackjet_MV2c10_discriminant;   //!
   TBranch        *b_trackjet_MV2c20_discriminant;   //!
   TBranch        *b_trackjet_isbjet;   //!
   TBranch        *b_trackjet_PartonTruthLabelID;   //!
   TBranch        *b_trackjet_ConeTruthLabelID;   //!
   TBranch        *b_trackjet_met_nomuon_dphi;   //!
   TBranch        *b_trackjet_met_wmuon_dphi;   //!
   TBranch        *b_trackjet_met_noelectron_dphi;   //!
   TBranch        *b_trackjet_met_nophoton_dphi;   //!
   TBranch        *b_trackjet_weight;   //!
   TBranch        *b_trackjet_raw_pt;   //!
   TBranch        *b_trackjet_raw_eta;   //!
   TBranch        *b_trackjet_raw_phi;   //!
   TBranch        *b_trackjet_raw_m;   //!
   TBranch        *b_trackjet_timing;   //!
   TBranch        *b_trackjet_emfrac;   //!
   TBranch        *b_trackjet_hecf;   //!
   TBranch        *b_trackjet_hecq;   //!
   TBranch        *b_trackjet_larq;   //!
   TBranch        *b_trackjet_avglarq;   //!
   TBranch        *b_trackjet_negE;   //!
   TBranch        *b_trackjet_lambda;   //!
   TBranch        *b_trackjet_lambda2;   //!
   TBranch        *b_trackjet_jvtxf;   //!
   TBranch        *b_trackjet_fmaxi;   //!
   TBranch        *b_trackjet_isbjet_loose;   //!
   TBranch        *b_trackjet_jvt;   //!
   TBranch        *b_trackjet_fjvt;   //!
   TBranch        *b_trackjet_cleaning;   //!
   TBranch        *b_trackjet_TruthLabelDeltaR_B;   //!
   TBranch        *b_trackjet_TruthLabelDeltaR_C;   //!
   TBranch        *b_trackjet_TruthLabelDeltaR_T;   //!
   TBranch        *b_trackjet_DFCommonJets_QGTagger_NTracks;   //!
   TBranch        *b_trackjet_DFCommonJets_QGTagger_TracksWidth;   //!
   TBranch        *b_trackjet_DFCommonJets_QGTagger_TracksC1;   //!
   TBranch        *b_LCTopoJet_pt;   //!
   TBranch        *b_LCTopoJet_eta;   //!
   TBranch        *b_LCTopoJet_phi;   //!
   TBranch        *b_LCTopoJet_m;   //!
   TBranch        *b_LCTopoJet_tau21;   //!
   TBranch        *b_LCTopoJet_D2;   //!
   TBranch        *b_LCTopoJet_C2;   //!
   TBranch        *b_LCTopoJet_ntrk;   //!
   TBranch        *b_LCTopoJet_nConstit;   //!
   TBranch        *b_LCTopoJet_passD2_W50;   //!
   TBranch        *b_LCTopoJet_passD2_Z50;   //!
   TBranch        *b_LCTopoJet_passD2_W80;   //!
   TBranch        *b_LCTopoJet_passD2_Z80;   //!
   TBranch        *b_LCTopoJet_passMass_W50;   //!
   TBranch        *b_LCTopoJet_passMass_Z50;   //!
   TBranch        *b_LCTopoJet_passMass_W80;   //!
   TBranch        *b_LCTopoJet_passMass_Z80;   //!
   TBranch        *b_LCTopoJet_cutD2_W50;   //!
   TBranch        *b_LCTopoJet_cutD2_Z50;   //!
   TBranch        *b_LCTopoJet_cutD2_W80;   //!
   TBranch        *b_LCTopoJet_cutD2_Z80;   //!
   TBranch        *b_LCTopoJet_cutMlow_W50;   //!
   TBranch        *b_LCTopoJet_cutMlow_Z50;   //!
   TBranch        *b_LCTopoJet_cutMlow_W80;   //!
   TBranch        *b_LCTopoJet_cutMlow_Z80;   //!
   TBranch        *b_LCTopoJet_cutMhigh_W50;   //!
   TBranch        *b_LCTopoJet_cutMhigh_Z50;   //!
   TBranch        *b_LCTopoJet_cutMhigh_W80;   //!
   TBranch        *b_LCTopoJet_cutMhigh_Z80;   //!
   TBranch        *b_LCTopoJet_weight;   //!
   TBranch        *b_TCCJet_pt;   //!
   TBranch        *b_TCCJet_eta;   //!
   TBranch        *b_TCCJet_phi;   //!
   TBranch        *b_TCCJet_m;   //!
   TBranch        *b_TCCJet_tau21;   //!
   TBranch        *b_TCCJet_D2;   //!
   TBranch        *b_TCCJet_C2;   //!
   TBranch        *b_TCCJet_ntrk;   //!
   TBranch        *b_TCCJet_nConstit;   //!
   TBranch        *b_TCCJet_passD2_W50;   //!
   TBranch        *b_TCCJet_passD2_Z50;   //!
   TBranch        *b_TCCJet_passD2_W80;   //!
   TBranch        *b_TCCJet_passD2_Z80;   //!
   TBranch        *b_TCCJet_passMass_W50;   //!
   TBranch        *b_TCCJet_passMass_Z50;   //!
   TBranch        *b_TCCJet_passMass_W80;   //!
   TBranch        *b_TCCJet_passMass_Z80;   //!
   TBranch        *b_TCCJet_cutD2_W50;   //!
   TBranch        *b_TCCJet_cutD2_Z50;   //!
   TBranch        *b_TCCJet_cutD2_W80;   //!
   TBranch        *b_TCCJet_cutD2_Z80;   //!
   TBranch        *b_TCCJet_cutMlow_W50;   //!
   TBranch        *b_TCCJet_cutMlow_Z50;   //!
   TBranch        *b_TCCJet_cutMlow_W80;   //!
   TBranch        *b_TCCJet_cutMlow_Z80;   //!
   TBranch        *b_TCCJet_cutMhigh_W50;   //!
   TBranch        *b_TCCJet_cutMhigh_Z50;   //!
   TBranch        *b_TCCJet_cutMhigh_W80;   //!
   TBranch        *b_TCCJet_cutMhigh_Z80;   //!
   TBranch        *b_TCCJet_lead_clusters_pt;   //!
   TBranch        *b_TCCJet_lead_clusters_eta;   //!
   TBranch        *b_TCCJet_lead_clusters_phi;   //!
   TBranch        *b_TCCJet_lead_clusters_m;   //!
   TBranch        *b_TCCJet_weight;   //!
   TBranch        *b_ph_pt;   //!
   TBranch        *b_ph_eta;   //!
   TBranch        *b_ph_phi;   //!
   TBranch        *b_ph_truth_pt;   //!
   TBranch        *b_ph_SF;   //!
   TBranch        *b_ph_SF_iso;   //!
   TBranch        *b_ph_isotool_pass_fixedcuttight;   //!
   TBranch        *b_ph_m;   //!
   TBranch        *b_ph_ptcone20;   //!
   TBranch        *b_ph_ptvarcone20;   //!
   TBranch        *b_ph_etcone20;   //!
   TBranch        *b_ph_topoetcone20;   //!
   TBranch        *b_ph_ptcone30;   //!
   TBranch        *b_ph_ptvarcone30;   //!
   TBranch        *b_ph_etcone30;   //!
   TBranch        *b_ph_topoetcone30;   //!
   TBranch        *b_ph_ptcone40;   //!
   TBranch        *b_ph_ptvarcone40;   //!
   TBranch        *b_ph_etcone40;   //!
   TBranch        *b_ph_topoetcone40;   //!
   TBranch        *b_ph_isTight;   //!
   TBranch        *b_ph_isEM;   //!
   TBranch        *b_ph_OQ;   //!
   TBranch        *b_ph_author;   //!
   TBranch        *b_ph_isConv;   //!
   TBranch        *b_ph_truth_eta;   //!
   TBranch        *b_ph_truth_phi;   //!
   TBranch        *b_ph_truth_status;   //!
   TBranch        *b_ph_truth_type;   //!
   TBranch        *b_ph_truth_origin;   //!
   TBranch        *b_ph_met_nomuon_dphi;   //!
   TBranch        *b_ph_met_wmuon_dphi;   //!
   TBranch        *b_ph_met_nophoton_dphi;   //!
   TBranch        *b_ph_baseline_pt;   //!
   TBranch        *b_ph_baseline_SF;   //!
   TBranch        *b_ph_baseline_eta;   //!
   TBranch        *b_ph_baseline_phi;   //!
   TBranch        *b_ph_baseline_SF_syst_PH_EFF_ID_Uncertainty__1down;   //!
   TBranch        *b_ph_baseline_SF_syst_PH_EFF_ID_Uncertainty__1up;   //!
   TBranch        *b_ph_baseline_SF_syst_PH_EFF_ISO_Uncertainty__1down;   //!
   TBranch        *b_ph_baseline_SF_syst_PH_EFF_ISO_Uncertainty__1up;   //!

   ClusterTree(TString path);
   virtual ~ClusterTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef ClusterTree_cxx
ClusterTree::ClusterTree(TString path) : fChain(0) 
{
   TTree * tree;
   TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(path);
   if (!f || !f->IsOpen()) {
      f = new TFile(path);
   }
   f->GetObject("nominal",tree);

   Init(tree);
}

ClusterTree::~ClusterTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ClusterTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ClusterTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ClusterTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mconly_weights = 0;
   mu_pt = 0;
   mu_SF = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_SF_iso = 0;
   mu_m = 0;
   mu_charge = 0;
   mu_id_pt = 0;
   mu_id_eta = 0;
   mu_id_phi = 0;
   mu_id_m = 0;
   mu_me_pt = 0;
   mu_me_eta = 0;
   mu_me_phi = 0;
   mu_me_m = 0;
   mu_ptcone20 = 0;
   mu_ptvarcone20 = 0;
   mu_etcone20 = 0;
   mu_topoetcone20 = 0;
   mu_ptcone30 = 0;
   mu_ptvarcone30 = 0;
   mu_ptvarcone30_TightTTVA_pt1000 = 0;
   mu_etcone30 = 0;
   mu_topoetcone30 = 0;
   mu_ptcone40 = 0;
   mu_ptvarcone40 = 0;
   mu_etcone40 = 0;
   mu_topoetcone40 = 0;
   mu_author = 0;
   mu_quality = 0;
   mu_isSA = 0;
   mu_met_nomuon_dphi = 0;
   mu_met_wmuon_dphi = 0;
   mu_truth_type = 0;
   mu_truth_origin = 0;
   mu_baseline_pt = 0;
   mu_baseline_SF = 0;
   mu_baseline_eta = 0;
   mu_baseline_phi = 0;
   mu_baseline_isLooseID = 0;
   mu_baseline_isMediumID = 0;
   mu_baseline_isTightID = 0;
   mu_baseline_SF_syst_MUON_EFF_BADMUON_SYS__1down = 0;
   mu_baseline_SF_syst_MUON_EFF_BADMUON_SYS__1up = 0;
   mu_baseline_SF_syst_MUON_EFF_ISO_STAT__1down = 0;
   mu_baseline_SF_syst_MUON_EFF_ISO_STAT__1up = 0;
   mu_baseline_SF_syst_MUON_EFF_ISO_SYS__1down = 0;
   mu_baseline_SF_syst_MUON_EFF_ISO_SYS__1up = 0;
   mu_baseline_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down = 0;
   mu_baseline_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up = 0;
   mu_baseline_SF_syst_MUON_EFF_RECO_STAT__1down = 0;
   mu_baseline_SF_syst_MUON_EFF_RECO_STAT__1up = 0;
   mu_baseline_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down = 0;
   mu_baseline_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up = 0;
   mu_baseline_SF_syst_MUON_EFF_RECO_SYS__1down = 0;
   mu_baseline_SF_syst_MUON_EFF_RECO_SYS__1up = 0;
   mu_baseline_SF_syst_MUON_EFF_TTVA_STAT__1down = 0;
   mu_baseline_SF_syst_MUON_EFF_TTVA_STAT__1up = 0;
   mu_baseline_SF_syst_MUON_EFF_TTVA_SYS__1down = 0;
   mu_baseline_SF_syst_MUON_EFF_TTVA_SYS__1up = 0;
   el_pt = 0;
   el_eta = 0;
   el_phi = 0;
   el_SF = 0;
   el_SF_iso = 0;
   el_SF_trigger = 0;
   el_eff_trigger = 0;
   el_m = 0;
   el_charge = 0;
   el_id_pt = 0;
   el_id_eta = 0;
   el_id_phi = 0;
   el_id_m = 0;
   el_cl_pt = 0;
   el_cl_eta = 0;
   el_cl_etaBE2 = 0;
   el_cl_phi = 0;
   el_cl_m = 0;
   el_ptcone20 = 0;
   el_ptvarcone20 = 0;
   el_ptvarcone20_TightTTVA_pt1000 = 0;
   el_etcone20 = 0;
   el_topoetcone20 = 0;
   el_ptcone30 = 0;
   el_ptvarcone30 = 0;
   el_etcone30 = 0;
   el_topoetcone30 = 0;
   el_ptcone40 = 0;
   el_ptvarcone40 = 0;
   el_etcone40 = 0;
   el_topoetcone40 = 0;
   el_author = 0;
   el_isConv = 0;
   el_truth_pt = 0;
   el_truth_eta = 0;
   el_truth_phi = 0;
   el_truth_status = 0;
   el_truth_type = 0;
   el_truth_origin = 0;
   el_met_nomuon_dphi = 0;
   el_met_wmuon_dphi = 0;
   el_met_noelectron_dphi = 0;
   el_baseline_pt = 0;
   el_baseline_SF = 0;
   el_baseline_eta = 0;
   el_baseline_phi = 0;
   el_baseline_isLooseID = 0;
   el_baseline_isMediumID = 0;
   el_baseline_isTightID = 0;
   el_baseline_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   el_baseline_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   el_baseline_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   el_baseline_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   el_baseline_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   el_baseline_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_m = 0;
   jet_fmax = 0;
   jet_fch = 0;
   jet_MV2c10_discriminant = 0;
   jet_MV2c20_discriminant = 0;
   jet_isbjet = 0;
   jet_PartonTruthLabelID = 0;
   jet_ConeTruthLabelID = 0;
   jet_met_nomuon_dphi = 0;
   jet_met_wmuon_dphi = 0;
   jet_met_noelectron_dphi = 0;
   jet_met_nophoton_dphi = 0;
   jet_weight = 0;
   jet_raw_pt = 0;
   jet_raw_eta = 0;
   jet_raw_phi = 0;
   jet_raw_m = 0;
   jet_timing = 0;
   jet_emfrac = 0;
   jet_hecf = 0;
   jet_hecq = 0;
   jet_larq = 0;
   jet_avglarq = 0;
   jet_negE = 0;
   jet_lambda = 0;
   jet_lambda2 = 0;
   jet_jvtxf = 0;
   jet_fmaxi = 0;
   jet_isbjet_loose = 0;
   jet_jvt = 0;
   jet_fjvt = 0;
   jet_cleaning = 0;
   jet_TruthLabelDeltaR_B = 0;
   jet_TruthLabelDeltaR_C = 0;
   jet_TruthLabelDeltaR_T = 0;
   jet_DFCommonJets_QGTagger_NTracks = 0;
   jet_DFCommonJets_QGTagger_TracksWidth = 0;
   jet_DFCommonJets_QGTagger_TracksC1 = 0;
   trackjet_pt = 0;
   trackjet_eta = 0;
   trackjet_phi = 0;
   trackjet_m = 0;
   trackjet_fmax = 0;
   trackjet_fch = 0;
   trackjet_MV2c10_discriminant = 0;
   trackjet_MV2c20_discriminant = 0;
   trackjet_isbjet = 0;
   trackjet_PartonTruthLabelID = 0;
   trackjet_ConeTruthLabelID = 0;
   trackjet_met_nomuon_dphi = 0;
   trackjet_met_wmuon_dphi = 0;
   trackjet_met_noelectron_dphi = 0;
   trackjet_met_nophoton_dphi = 0;
   trackjet_weight = 0;
   trackjet_raw_pt = 0;
   trackjet_raw_eta = 0;
   trackjet_raw_phi = 0;
   trackjet_raw_m = 0;
   trackjet_timing = 0;
   trackjet_emfrac = 0;
   trackjet_hecf = 0;
   trackjet_hecq = 0;
   trackjet_larq = 0;
   trackjet_avglarq = 0;
   trackjet_negE = 0;
   trackjet_lambda = 0;
   trackjet_lambda2 = 0;
   trackjet_jvtxf = 0;
   trackjet_fmaxi = 0;
   trackjet_isbjet_loose = 0;
   trackjet_jvt = 0;
   trackjet_fjvt = 0;
   trackjet_cleaning = 0;
   trackjet_TruthLabelDeltaR_B = 0;
   trackjet_TruthLabelDeltaR_C = 0;
   trackjet_TruthLabelDeltaR_T = 0;
   trackjet_DFCommonJets_QGTagger_NTracks = 0;
   trackjet_DFCommonJets_QGTagger_TracksWidth = 0;
   trackjet_DFCommonJets_QGTagger_TracksC1 = 0;
   LCTopoJet_pt = 0;
   LCTopoJet_eta = 0;
   LCTopoJet_phi = 0;
   LCTopoJet_m = 0;
   LCTopoJet_tau21 = 0;
   LCTopoJet_D2 = 0;
   LCTopoJet_C2 = 0;
   LCTopoJet_nConstit = 0;
   LCTopoJet_passD2_W50 = 0;
   LCTopoJet_passD2_Z50 = 0;
   LCTopoJet_passD2_W80 = 0;
   LCTopoJet_passD2_Z80 = 0;
   LCTopoJet_passMass_W50 = 0;
   LCTopoJet_passMass_Z50 = 0;
   LCTopoJet_passMass_W80 = 0;
   LCTopoJet_passMass_Z80 = 0;
   LCTopoJet_cutD2_W50 = 0;
   LCTopoJet_cutD2_Z50 = 0;
   LCTopoJet_cutD2_W80 = 0;
   LCTopoJet_cutD2_Z80 = 0;
   LCTopoJet_cutMlow_W50 = 0;
   LCTopoJet_cutMlow_Z50 = 0;
   LCTopoJet_cutMlow_W80 = 0;
   LCTopoJet_cutMlow_Z80 = 0;
   LCTopoJet_cutMhigh_W50 = 0;
   LCTopoJet_cutMhigh_Z50 = 0;
   LCTopoJet_cutMhigh_W80 = 0;
   LCTopoJet_cutMhigh_Z80 = 0;
   LCTopoJet_weight = 0;
   TCCJet_pt = 0;
   TCCJet_eta = 0;
   TCCJet_phi = 0;
   TCCJet_m = 0;
   TCCJet_tau21 = 0;
   TCCJet_D2 = 0;
   TCCJet_C2 = 0;
   TCCJet_nConstit = 0;
   TCCJet_passD2_W50 = 0;
   TCCJet_passD2_Z50 = 0;
   TCCJet_passD2_W80 = 0;
   TCCJet_passD2_Z80 = 0;
   TCCJet_passMass_W50 = 0;
   TCCJet_passMass_Z50 = 0;
   TCCJet_passMass_W80 = 0;
   TCCJet_passMass_Z80 = 0;
   TCCJet_cutD2_W50 = 0;
   TCCJet_cutD2_Z50 = 0;
   TCCJet_cutD2_W80 = 0;
   TCCJet_cutD2_Z80 = 0;
   TCCJet_cutMlow_W50 = 0;
   TCCJet_cutMlow_Z50 = 0;
   TCCJet_cutMlow_W80 = 0;
   TCCJet_cutMlow_Z80 = 0;
   TCCJet_cutMhigh_W50 = 0;
   TCCJet_cutMhigh_Z50 = 0;
   TCCJet_cutMhigh_W80 = 0;
   TCCJet_cutMhigh_Z80 = 0;
   TCCJet_lead_clusters_pt = 0;
   TCCJet_lead_clusters_eta = 0;
   TCCJet_lead_clusters_phi = 0;
   TCCJet_lead_clusters_m = 0;
   TCCJet_weight = 0;
   ph_pt = 0;
   ph_eta = 0;
   ph_phi = 0;
   ph_truth_pt = 0;
   ph_SF = 0;
   ph_SF_iso = 0;
   ph_isotool_pass_fixedcuttight = 0;
   ph_m = 0;
   ph_ptcone20 = 0;
   ph_ptvarcone20 = 0;
   ph_etcone20 = 0;
   ph_topoetcone20 = 0;
   ph_ptcone30 = 0;
   ph_ptvarcone30 = 0;
   ph_etcone30 = 0;
   ph_topoetcone30 = 0;
   ph_ptcone40 = 0;
   ph_ptvarcone40 = 0;
   ph_etcone40 = 0;
   ph_topoetcone40 = 0;
   ph_isTight = 0;
   ph_isEM = 0;
   ph_OQ = 0;
   ph_author = 0;
   ph_isConv = 0;
   ph_truth_eta = 0;
   ph_truth_phi = 0;
   ph_truth_status = 0;
   ph_truth_type = 0;
   ph_truth_origin = 0;
   ph_met_nomuon_dphi = 0;
   ph_met_wmuon_dphi = 0;
   ph_met_nophoton_dphi = 0;
   ph_baseline_pt = 0;
   ph_baseline_SF = 0;
   ph_baseline_eta = 0;
   ph_baseline_phi = 0;
   ph_baseline_SF_syst_PH_EFF_ID_Uncertainty__1down = 0;
   ph_baseline_SF_syst_PH_EFF_ID_Uncertainty__1up = 0;
   ph_baseline_SF_syst_PH_EFF_ISO_Uncertainty__1down = 0;
   ph_baseline_SF_syst_PH_EFF_ISO_Uncertainty__1up = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("run", &run, &b_run);
   fChain->SetBranchAddress("event", &event, &b_event);
   fChain->SetBranchAddress("last", &last, &b_last);
   fChain->SetBranchAddress("year", &year, &b_year);
   fChain->SetBranchAddress("met_tst_sig", &met_tst_sig, &b_met_tst_sig);
   fChain->SetBranchAddress("xSec_SUSY", &xSec_SUSY, &b_xSec_SUSY);
   fChain->SetBranchAddress("k_factor", &k_factor, &b_k_factor);
   fChain->SetBranchAddress("filter_eff", &filter_eff, &b_filter_eff);
   fChain->SetBranchAddress("isSR", &isSR, &b_isSR);
   fChain->SetBranchAddress("isCR1mubveto", &isCR1mubveto, &b_isCR1mubveto);
   fChain->SetBranchAddress("isCR1mubtag", &isCR1mubtag, &b_isCR1mubtag);
   fChain->SetBranchAddress("isCR1ebveto", &isCR1ebveto, &b_isCR1ebveto);
   fChain->SetBranchAddress("isCR1ebtag", &isCR1ebtag, &b_isCR1ebtag);
   fChain->SetBranchAddress("isCR2e", &isCR2e, &b_isCR2e);
   fChain->SetBranchAddress("isCR2mu", &isCR2mu, &b_isCR2mu);
   fChain->SetBranchAddress("isCR1ph", &isCR1ph, &b_isCR1ph);
   fChain->SetBranchAddress("n_tau_baseline", &n_tau_baseline, &b_n_tau_baseline);
   fChain->SetBranchAddress("mconly_weight", &mconly_weight, &b_mconly_weight);
   fChain->SetBranchAddress("syst_weight", &syst_weight, &b_syst_weight);
   fChain->SetBranchAddress("pu_weight", &pu_weight, &b_pu_weight);
   fChain->SetBranchAddress("btag_weight", &btag_weight, &b_btag_weight);
   fChain->SetBranchAddress("jvt_weight", &jvt_weight, &b_jvt_weight);
   fChain->SetBranchAddress("truth_V_bare_pt", &truth_V_bare_pt, &b_truth_V_bare_pt);
   fChain->SetBranchAddress("truth_V_dressed_pt", &truth_V_dressed_pt, &b_truth_V_dressed_pt);
   fChain->SetBranchAddress("truth_V_simple_pt", &truth_V_simple_pt, &b_truth_V_simple_pt);
   fChain->SetBranchAddress("munu_mT", &munu_mT, &b_munu_mT);
   fChain->SetBranchAddress("enu_mT", &enu_mT, &b_enu_mT);
   fChain->SetBranchAddress("mumu_m", &mumu_m, &b_mumu_m);
   fChain->SetBranchAddress("ee_m", &ee_m, &b_ee_m);
   fChain->SetBranchAddress("dPhiLCTopoJetMet", &dPhiLCTopoJetMet, &b_dPhiLCTopoJetMet);
   fChain->SetBranchAddress("dPhiTCCJetMet", &dPhiTCCJetMet, &b_dPhiTCCJetMet);
   fChain->SetBranchAddress("m_LCTopoJet", &m_LCTopoJet, &b_m_LCTopoJet);
   fChain->SetBranchAddress("m_TCCJet", &m_TCCJet, &b_m_TCCJet);
   fChain->SetBranchAddress("n_jet_central", &n_jet_central, &b_n_jet_central);
   fChain->SetBranchAddress("dPhiDijetMet", &dPhiDijetMet, &b_dPhiDijetMet);
   fChain->SetBranchAddress("dPhiDijet", &dPhiDijet, &b_dPhiDijet);
   fChain->SetBranchAddress("dRDijet", &dRDijet, &b_dRDijet);
   fChain->SetBranchAddress("DijetSumPt", &DijetSumPt, &b_DijetSumPt);
   fChain->SetBranchAddress("TrijetSumPt", &TrijetSumPt, &b_TrijetSumPt);
   fChain->SetBranchAddress("DijetMass", &DijetMass, &b_DijetMass);
   fChain->SetBranchAddress("n_trackjet", &n_trackjet, &b_n_trackjet);
   fChain->SetBranchAddress("n_bcentralJet", &n_bcentralJet, &b_n_bcentralJet);
   fChain->SetBranchAddress("n_btrackJet", &n_btrackJet, &b_n_btrackJet);
   fChain->SetBranchAddress("MJ_passDeltaPhi", &MJ_passDeltaPhi, &b_MJ_passDeltaPhi);
   fChain->SetBranchAddress("n_trackLCTopoAssociatedBjet", &n_trackLCTopoAssociatedBjet, &b_n_trackLCTopoAssociatedBjet);
   fChain->SetBranchAddress("n_trackLCTopoAssociatedNotBjet", &n_trackLCTopoAssociatedNotBjet, &b_n_trackLCTopoAssociatedNotBjet);
   fChain->SetBranchAddress("n_trackLCTopoSeparatedBjet", &n_trackLCTopoSeparatedBjet, &b_n_trackLCTopoSeparatedBjet);
   fChain->SetBranchAddress("n_trackLCTopoSeparatedNotBjet", &n_trackLCTopoSeparatedNotBjet, &b_n_trackLCTopoSeparatedNotBjet);
   fChain->SetBranchAddress("n_trackTCCAssociatedBjet", &n_trackTCCAssociatedBjet, &b_n_trackTCCAssociatedBjet);
   fChain->SetBranchAddress("n_trackTCCAssociatedNotBjet", &n_trackTCCAssociatedNotBjet, &b_n_trackTCCAssociatedNotBjet);
   fChain->SetBranchAddress("n_trackTCCSeparatedBjet", &n_trackTCCSeparatedBjet, &b_n_trackTCCSeparatedBjet);
   fChain->SetBranchAddress("n_trackTCCSeparatedNotBjet", &n_trackTCCSeparatedNotBjet, &b_n_trackTCCSeparatedNotBjet);
   fChain->SetBranchAddress("ptV_1Muon_pt", &ptV_1Muon_pt, &b_ptV_1Muon_pt);
   fChain->SetBranchAddress("ptV_1Muon_eta", &ptV_1Muon_eta, &b_ptV_1Muon_eta);
   fChain->SetBranchAddress("ptV_1Muon_phi", &ptV_1Muon_phi, &b_ptV_1Muon_phi);
   fChain->SetBranchAddress("ptV_2Lepton_pt", &ptV_2Lepton_pt, &b_ptV_2Lepton_pt);
   fChain->SetBranchAddress("ptV_2Lepton_eta", &ptV_2Lepton_eta, &b_ptV_2Lepton_eta);
   fChain->SetBranchAddress("ptV_2Lepton_phi", &ptV_2Lepton_phi, &b_ptV_2Lepton_phi);
   fChain->SetBranchAddress("n_jet", &n_jet, &b_n_jet);
   fChain->SetBranchAddress("n_jet_preor", &n_jet_preor, &b_n_jet_preor);
   fChain->SetBranchAddress("n_tau_preor", &n_tau_preor, &b_n_tau_preor);
   fChain->SetBranchAddress("n_mu_preor", &n_mu_preor, &b_n_mu_preor);
   fChain->SetBranchAddress("n_el_preor", &n_el_preor, &b_n_el_preor);
   fChain->SetBranchAddress("n_ph_preor", &n_ph_preor, &b_n_ph_preor);
   fChain->SetBranchAddress("n_bjet", &n_bjet, &b_n_bjet);
   fChain->SetBranchAddress("n_el", &n_el, &b_n_el);
   fChain->SetBranchAddress("n_el_baseline", &n_el_baseline, &b_n_el_baseline);
   fChain->SetBranchAddress("n_mu_baseline", &n_mu_baseline, &b_n_mu_baseline);
   fChain->SetBranchAddress("n_mu_baseline_bad", &n_mu_baseline_bad, &b_n_mu_baseline_bad);
   fChain->SetBranchAddress("n_allmu_bad", &n_allmu_bad, &b_n_allmu_bad);
   fChain->SetBranchAddress("n_tau", &n_tau, &b_n_tau);
   fChain->SetBranchAddress("n_mu", &n_mu, &b_n_mu);
   fChain->SetBranchAddress("mconly_weights", &mconly_weights, &b_mconly_weights);
   fChain->SetBranchAddress("jvt_all_weight", &jvt_all_weight, &b_jvt_all_weight);
   fChain->SetBranchAddress("n_smallJet", &n_smallJet, &b_n_smallJet);
   fChain->SetBranchAddress("n_truthFatJet", &n_truthFatJet, &b_n_truthFatJet);
   fChain->SetBranchAddress("n_LCTopoJet", &n_LCTopoJet, &b_n_LCTopoJet);
   fChain->SetBranchAddress("n_TCCJet", &n_TCCJet, &b_n_TCCJet);
   fChain->SetBranchAddress("n_TARJet", &n_TARJet, &b_n_TARJet);
   fChain->SetBranchAddress("n_TruthTARJet", &n_TruthTARJet, &b_n_TruthTARJet);
   fChain->SetBranchAddress("n_tau_truth", &n_tau_truth, &b_n_tau_truth);
   fChain->SetBranchAddress("n_truthTop", &n_truthTop, &b_n_truthTop);
   fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
   fChain->SetBranchAddress("actualIntPerXing", &actualIntPerXing, &b_actualIntPerXing);
   fChain->SetBranchAddress("corAverageIntPerXing", &corAverageIntPerXing, &b_corAverageIntPerXing);
   fChain->SetBranchAddress("corActualIntPerXing", &corActualIntPerXing, &b_corActualIntPerXing);
   fChain->SetBranchAddress("n_vx", &n_vx, &b_n_vx);
   fChain->SetBranchAddress("pu_hash", &pu_hash, &b_pu_hash);
   fChain->SetBranchAddress("allmu_tot_SF", &allmu_tot_SF, &b_allmu_tot_SF);
   fChain->SetBranchAddress("trigger_matched_electron", &trigger_matched_electron, &b_trigger_matched_electron);
   fChain->SetBranchAddress("trigger_matched_muon", &trigger_matched_muon, &b_trigger_matched_muon);
   fChain->SetBranchAddress("trigger_HLT_e120_lhloose", &trigger_HLT_e120_lhloose, &b_trigger_HLT_e120_lhloose);
   fChain->SetBranchAddress("trigger_HLT_e140_lhloose_nod0", &trigger_HLT_e140_lhloose_nod0, &b_trigger_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("trigger_HLT_e24_lhmedium_L1EM20VH", &trigger_HLT_e24_lhmedium_L1EM20VH, &b_trigger_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("trigger_HLT_e24_lhtight_nod0_ivarloose", &trigger_HLT_e24_lhtight_nod0_ivarloose, &b_trigger_HLT_e24_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("trigger_HLT_e26_lhtight_nod0_ivarloose", &trigger_HLT_e26_lhtight_nod0_ivarloose, &b_trigger_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("trigger_HLT_e60_lhmedium", &trigger_HLT_e60_lhmedium, &b_trigger_HLT_e60_lhmedium);
   fChain->SetBranchAddress("trigger_HLT_e60_lhmedium_nod0", &trigger_HLT_e60_lhmedium_nod0, &b_trigger_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("trigger_HLT_g140_loose", &trigger_HLT_g140_loose, &b_trigger_HLT_g140_loose);
   fChain->SetBranchAddress("trigger_HLT_xe100_mht_L1XE50", &trigger_HLT_xe100_mht_L1XE50, &b_trigger_HLT_xe100_mht_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe110_mht_L1XE50", &trigger_HLT_xe110_mht_L1XE50, &b_trigger_HLT_xe110_mht_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe130_mht_L1XE50", &trigger_HLT_xe130_mht_L1XE50, &b_trigger_HLT_xe130_mht_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe70", &trigger_HLT_xe70, &b_trigger_HLT_xe70);
   fChain->SetBranchAddress("trigger_HLT_xe80_tc_lcw_L1XE50", &trigger_HLT_xe80_tc_lcw_L1XE50, &b_trigger_HLT_xe80_tc_lcw_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe90_mht_L1XE50", &trigger_HLT_xe90_mht_L1XE50, &b_trigger_HLT_xe90_mht_L1XE50);
   fChain->SetBranchAddress("trigger_pass", &trigger_pass, &b_trigger_pass);
   fChain->SetBranchAddress("trigger_matched_HLT_e60_lhmedium", &trigger_matched_HLT_e60_lhmedium, &b_trigger_matched_HLT_e60_lhmedium);
   fChain->SetBranchAddress("trigger_matched_HLT_e120_lhloose", &trigger_matched_HLT_e120_lhloose, &b_trigger_matched_HLT_e120_lhloose);
   fChain->SetBranchAddress("trigger_matched_HLT_e24_lhmedium_L1EM18VH", &trigger_matched_HLT_e24_lhmedium_L1EM18VH, &b_trigger_matched_HLT_e24_lhmedium_L1EM18VH);
   fChain->SetBranchAddress("trigger_matched_HLT_e24_lhmedium_L1EM20VH", &trigger_matched_HLT_e24_lhmedium_L1EM20VH, &b_trigger_matched_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("pdf_x1", &pdf_x1, &b_pdf_x1);
   fChain->SetBranchAddress("pdf_x2", &pdf_x2, &b_pdf_x2);
   fChain->SetBranchAddress("pdf_pdf1", &pdf_pdf1, &b_pdf_pdf1);
   fChain->SetBranchAddress("pdf_pdf2", &pdf_pdf2, &b_pdf_pdf2);
   fChain->SetBranchAddress("pdf_scale", &pdf_scale, &b_pdf_scale);
   fChain->SetBranchAddress("flag_bib", &flag_bib, &b_flag_bib);
   fChain->SetBranchAddress("flag_bib_raw", &flag_bib_raw, &b_flag_bib_raw);
   fChain->SetBranchAddress("flag_sct", &flag_sct, &b_flag_sct);
   fChain->SetBranchAddress("flag_core", &flag_core, &b_flag_core);
   fChain->SetBranchAddress("trigger_HLT_2e17_loose", &trigger_HLT_2e17_loose, &b_trigger_HLT_2e17_loose);
   fChain->SetBranchAddress("trigger_HLT_3j175", &trigger_HLT_3j175, &b_trigger_HLT_3j175);
   fChain->SetBranchAddress("trigger_HLT_4j85", &trigger_HLT_4j85, &b_trigger_HLT_4j85);
   fChain->SetBranchAddress("trigger_HLT_e120_lhloose_nod0", &trigger_HLT_e120_lhloose_nod0, &b_trigger_HLT_e120_lhloose_nod0);
   fChain->SetBranchAddress("trigger_HLT_e24_lhmedium_L1EM18VH", &trigger_HLT_e24_lhmedium_L1EM18VH, &b_trigger_HLT_e24_lhmedium_L1EM18VH);
   fChain->SetBranchAddress("trigger_HLT_e24_lhmedium_iloose_L1EM20VH", &trigger_HLT_e24_lhmedium_iloose_L1EM20VH, &b_trigger_HLT_e24_lhmedium_iloose_L1EM20VH);
   fChain->SetBranchAddress("trigger_HLT_e24_lhtight_iloose", &trigger_HLT_e24_lhtight_iloose, &b_trigger_HLT_e24_lhtight_iloose);
   fChain->SetBranchAddress("trigger_HLT_e28_tight_iloose", &trigger_HLT_e28_tight_iloose, &b_trigger_HLT_e28_tight_iloose);
   fChain->SetBranchAddress("trigger_HLT_g120_loose", &trigger_HLT_g120_loose, &b_trigger_HLT_g120_loose);
   fChain->SetBranchAddress("trigger_HLT_g160_loose", &trigger_HLT_g160_loose, &b_trigger_HLT_g160_loose);
   fChain->SetBranchAddress("trigger_HLT_g300_etcut", &trigger_HLT_g300_etcut, &b_trigger_HLT_g300_etcut);
   fChain->SetBranchAddress("trigger_HLT_ht700_L1J100", &trigger_HLT_ht700_L1J100, &b_trigger_HLT_ht700_L1J100);
   fChain->SetBranchAddress("trigger_HLT_ht850_L1J100", &trigger_HLT_ht850_L1J100, &b_trigger_HLT_ht850_L1J100);
   fChain->SetBranchAddress("trigger_HLT_j30_xe10_razor100", &trigger_HLT_j30_xe10_razor100, &b_trigger_HLT_j30_xe10_razor100);
   fChain->SetBranchAddress("trigger_HLT_j30_xe10_razor170", &trigger_HLT_j30_xe10_razor170, &b_trigger_HLT_j30_xe10_razor170);
   fChain->SetBranchAddress("trigger_HLT_j30_xe10_razor185", &trigger_HLT_j30_xe10_razor185, &b_trigger_HLT_j30_xe10_razor185);
   fChain->SetBranchAddress("trigger_HLT_j30_xe10_razor195", &trigger_HLT_j30_xe10_razor195, &b_trigger_HLT_j30_xe10_razor195);
   fChain->SetBranchAddress("trigger_HLT_j30_xe60_razor100", &trigger_HLT_j30_xe60_razor100, &b_trigger_HLT_j30_xe60_razor100);
   fChain->SetBranchAddress("trigger_HLT_j30_xe60_razor170", &trigger_HLT_j30_xe60_razor170, &b_trigger_HLT_j30_xe60_razor170);
   fChain->SetBranchAddress("trigger_HLT_j30_xe60_razor185", &trigger_HLT_j30_xe60_razor185, &b_trigger_HLT_j30_xe60_razor185);
   fChain->SetBranchAddress("trigger_HLT_j30_xe60_razor195", &trigger_HLT_j30_xe60_razor195, &b_trigger_HLT_j30_xe60_razor195);
   fChain->SetBranchAddress("trigger_HLT_j360", &trigger_HLT_j360, &b_trigger_HLT_j360);
   fChain->SetBranchAddress("trigger_HLT_j380", &trigger_HLT_j380, &b_trigger_HLT_j380);
   fChain->SetBranchAddress("trigger_HLT_mu24_imedium", &trigger_HLT_mu24_imedium, &b_trigger_HLT_mu24_imedium);
   fChain->SetBranchAddress("trigger_HLT_mu60_0eta105_msonly", &trigger_HLT_mu60_0eta105_msonly, &b_trigger_HLT_mu60_0eta105_msonly);
   fChain->SetBranchAddress("trigger_HLT_xe100", &trigger_HLT_xe100, &b_trigger_HLT_xe100);
   fChain->SetBranchAddress("trigger_HLT_xe100_mht", &trigger_HLT_xe100_mht, &b_trigger_HLT_xe100_mht);
   fChain->SetBranchAddress("trigger_HLT_xe100_pueta", &trigger_HLT_xe100_pueta, &b_trigger_HLT_xe100_pueta);
   fChain->SetBranchAddress("trigger_HLT_xe100_pufit", &trigger_HLT_xe100_pufit, &b_trigger_HLT_xe100_pufit);
   fChain->SetBranchAddress("trigger_HLT_xe100_pufit_L1XE50", &trigger_HLT_xe100_pufit_L1XE50, &b_trigger_HLT_xe100_pufit_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe100_pufit_L1XE55", &trigger_HLT_xe100_pufit_L1XE55, &b_trigger_HLT_xe100_pufit_L1XE55);
   fChain->SetBranchAddress("trigger_HLT_xe100_tc_em", &trigger_HLT_xe100_tc_em, &b_trigger_HLT_xe100_tc_em);
   fChain->SetBranchAddress("trigger_HLT_xe100_tc_lcw", &trigger_HLT_xe100_tc_lcw, &b_trigger_HLT_xe100_tc_lcw);
   fChain->SetBranchAddress("trigger_HLT_xe100_tc_lcw_L1XE50", &trigger_HLT_xe100_tc_lcw_L1XE50, &b_trigger_HLT_xe100_tc_lcw_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe100_tc_lcw_L1XE60", &trigger_HLT_xe100_tc_lcw_L1XE60, &b_trigger_HLT_xe100_tc_lcw_L1XE60);
   fChain->SetBranchAddress("trigger_HLT_xe110_pufit_L1XE50", &trigger_HLT_xe110_pufit_L1XE50, &b_trigger_HLT_xe110_pufit_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe110_pufit_L1XE55", &trigger_HLT_xe110_pufit_L1XE55, &b_trigger_HLT_xe110_pufit_L1XE55);
   fChain->SetBranchAddress("trigger_HLT_xe110_pufit_xe65_L1XE50", &trigger_HLT_xe110_pufit_xe65_L1XE50, &b_trigger_HLT_xe110_pufit_xe65_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe110_pufit_xe70_L1XE50", &trigger_HLT_xe110_pufit_xe70_L1XE50, &b_trigger_HLT_xe110_pufit_xe70_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe120_pufit_L1XE50", &trigger_HLT_xe120_pufit_L1XE50, &b_trigger_HLT_xe120_pufit_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe70_pueta", &trigger_HLT_xe70_pueta, &b_trigger_HLT_xe70_pueta);
   fChain->SetBranchAddress("trigger_HLT_xe70_pufit", &trigger_HLT_xe70_pufit, &b_trigger_HLT_xe70_pufit);
   fChain->SetBranchAddress("trigger_HLT_xe70_tc_em", &trigger_HLT_xe70_tc_em, &b_trigger_HLT_xe70_tc_em);
   fChain->SetBranchAddress("trigger_HLT_xe70_tc_lcw", &trigger_HLT_xe70_tc_lcw, &b_trigger_HLT_xe70_tc_lcw);
   fChain->SetBranchAddress("trigger_HLT_xe80", &trigger_HLT_xe80, &b_trigger_HLT_xe80);
   fChain->SetBranchAddress("trigger_HLT_xe80_pueta", &trigger_HLT_xe80_pueta, &b_trigger_HLT_xe80_pueta);
   fChain->SetBranchAddress("trigger_HLT_xe90_pufit_L1XE50", &trigger_HLT_xe90_pufit_L1XE50, &b_trigger_HLT_xe90_pufit_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe90_tc_lcw_L1XE50", &trigger_HLT_xe90_tc_lcw_L1XE50, &b_trigger_HLT_xe90_tc_lcw_L1XE50);
   fChain->SetBranchAddress("trigger_L1_XE50", &trigger_L1_XE50, &b_trigger_L1_XE50);
   fChain->SetBranchAddress("trigger_L1_XE70", &trigger_L1_XE70, &b_trigger_L1_XE70);
   fChain->SetBranchAddress("trigger_L2_2J15_XE55", &trigger_L2_2J15_XE55, &b_trigger_L2_2J15_XE55);
   fChain->SetBranchAddress("trigger_ht700_L1J75", &trigger_ht700_L1J75, &b_trigger_ht700_L1J75);
   fChain->SetBranchAddress("trigger_ht850_L1J75", &trigger_ht850_L1J75, &b_trigger_ht850_L1J75);
   fChain->SetBranchAddress("hfor", &hfor, &b_hfor);
   fChain->SetBranchAddress("n_ph", &n_ph, &b_n_ph);
   fChain->SetBranchAddress("n_ph_tight", &n_ph_tight, &b_n_ph_tight);
   fChain->SetBranchAddress("n_ph_baseline", &n_ph_baseline, &b_n_ph_baseline);
   fChain->SetBranchAddress("n_ph_baseline_tight", &n_ph_baseline_tight, &b_n_ph_baseline_tight);
   fChain->SetBranchAddress("pdf_id1", &pdf_id1, &b_pdf_id1);
   fChain->SetBranchAddress("pdf_id2", &pdf_id2, &b_pdf_id2);
   fChain->SetBranchAddress("bb_decision", &bb_decision, &b_bb_decision);
   fChain->SetBranchAddress("n_jet_truth", &n_jet_truth, &b_n_jet_truth);
   fChain->SetBranchAddress("truth_V_bare_eta", &truth_V_bare_eta, &b_truth_V_bare_eta);
   fChain->SetBranchAddress("truth_V_bare_phi", &truth_V_bare_phi, &b_truth_V_bare_phi);
   fChain->SetBranchAddress("truth_V_bare_m", &truth_V_bare_m, &b_truth_V_bare_m);
   fChain->SetBranchAddress("truth_V_dressed_eta", &truth_V_dressed_eta, &b_truth_V_dressed_eta);
   fChain->SetBranchAddress("truth_V_dressed_phi", &truth_V_dressed_phi, &b_truth_V_dressed_phi);
   fChain->SetBranchAddress("truth_V_dressed_m", &truth_V_dressed_m, &b_truth_V_dressed_m);
   fChain->SetBranchAddress("truth_V_simple_eta", &truth_V_simple_eta, &b_truth_V_simple_eta);
   fChain->SetBranchAddress("truth_V_simple_phi", &truth_V_simple_phi, &b_truth_V_simple_phi);
   fChain->SetBranchAddress("truth_V_simple_m", &truth_V_simple_m, &b_truth_V_simple_m);
   fChain->SetBranchAddress("met_eleterm_et", &met_eleterm_et, &b_met_eleterm_et);
   fChain->SetBranchAddress("met_eleterm_phi", &met_eleterm_phi, &b_met_eleterm_phi);
   fChain->SetBranchAddress("met_eleterm_etx", &met_eleterm_etx, &b_met_eleterm_etx);
   fChain->SetBranchAddress("met_eleterm_ety", &met_eleterm_ety, &b_met_eleterm_ety);
   fChain->SetBranchAddress("met_eleterm_sumet", &met_eleterm_sumet, &b_met_eleterm_sumet);
   fChain->SetBranchAddress("met_jetterm_et", &met_jetterm_et, &b_met_jetterm_et);
   fChain->SetBranchAddress("met_jetterm_phi", &met_jetterm_phi, &b_met_jetterm_phi);
   fChain->SetBranchAddress("met_jetterm_etx", &met_jetterm_etx, &b_met_jetterm_etx);
   fChain->SetBranchAddress("met_jetterm_ety", &met_jetterm_ety, &b_met_jetterm_ety);
   fChain->SetBranchAddress("met_jetterm_sumet", &met_jetterm_sumet, &b_met_jetterm_sumet);
   fChain->SetBranchAddress("met_muonterm_et", &met_muonterm_et, &b_met_muonterm_et);
   fChain->SetBranchAddress("met_muonterm_phi", &met_muonterm_phi, &b_met_muonterm_phi);
   fChain->SetBranchAddress("met_muonterm_etx", &met_muonterm_etx, &b_met_muonterm_etx);
   fChain->SetBranchAddress("met_muonterm_ety", &met_muonterm_ety, &b_met_muonterm_ety);
   fChain->SetBranchAddress("met_muonterm_sumet", &met_muonterm_sumet, &b_met_muonterm_sumet);
   fChain->SetBranchAddress("met_muonterm_tst_et", &met_muonterm_tst_et, &b_met_muonterm_tst_et);
   fChain->SetBranchAddress("met_muonterm_tst_phi", &met_muonterm_tst_phi, &b_met_muonterm_tst_phi);
   fChain->SetBranchAddress("met_muonterm_tst_etx", &met_muonterm_tst_etx, &b_met_muonterm_tst_etx);
   fChain->SetBranchAddress("met_muonterm_tst_ety", &met_muonterm_tst_ety, &b_met_muonterm_tst_ety);
   fChain->SetBranchAddress("met_muonterm_tst_sumet", &met_muonterm_tst_sumet, &b_met_muonterm_tst_sumet);
   fChain->SetBranchAddress("met_noelectron_tst_et", &met_noelectron_tst_et, &b_met_noelectron_tst_et);
   fChain->SetBranchAddress("met_noelectron_tst_phi", &met_noelectron_tst_phi, &b_met_noelectron_tst_phi);
   fChain->SetBranchAddress("met_noelectron_tst_etx", &met_noelectron_tst_etx, &b_met_noelectron_tst_etx);
   fChain->SetBranchAddress("met_noelectron_tst_ety", &met_noelectron_tst_ety, &b_met_noelectron_tst_ety);
   fChain->SetBranchAddress("met_noelectron_tst_sumet", &met_noelectron_tst_sumet, &b_met_noelectron_tst_sumet);
   fChain->SetBranchAddress("met_nomuon_tst_et", &met_nomuon_tst_et, &b_met_nomuon_tst_et);
   fChain->SetBranchAddress("met_nomuon_tst_phi", &met_nomuon_tst_phi, &b_met_nomuon_tst_phi);
   fChain->SetBranchAddress("met_nomuon_tst_etx", &met_nomuon_tst_etx, &b_met_nomuon_tst_etx);
   fChain->SetBranchAddress("met_nomuon_tst_ety", &met_nomuon_tst_ety, &b_met_nomuon_tst_ety);
   fChain->SetBranchAddress("met_nomuon_tst_sumet", &met_nomuon_tst_sumet, &b_met_nomuon_tst_sumet);
   fChain->SetBranchAddress("met_nophoton_tst_et", &met_nophoton_tst_et, &b_met_nophoton_tst_et);
   fChain->SetBranchAddress("met_nophoton_tst_phi", &met_nophoton_tst_phi, &b_met_nophoton_tst_phi);
   fChain->SetBranchAddress("met_nophoton_tst_etx", &met_nophoton_tst_etx, &b_met_nophoton_tst_etx);
   fChain->SetBranchAddress("met_nophoton_tst_ety", &met_nophoton_tst_ety, &b_met_nophoton_tst_ety);
   fChain->SetBranchAddress("met_nophoton_tst_sumet", &met_nophoton_tst_sumet, &b_met_nophoton_tst_sumet);
   fChain->SetBranchAddress("met_phterm_et", &met_phterm_et, &b_met_phterm_et);
   fChain->SetBranchAddress("met_phterm_phi", &met_phterm_phi, &b_met_phterm_phi);
   fChain->SetBranchAddress("met_phterm_etx", &met_phterm_etx, &b_met_phterm_etx);
   fChain->SetBranchAddress("met_phterm_ety", &met_phterm_ety, &b_met_phterm_ety);
   fChain->SetBranchAddress("met_phterm_sumet", &met_phterm_sumet, &b_met_phterm_sumet);
   fChain->SetBranchAddress("met_softerm_tst_et", &met_softerm_tst_et, &b_met_softerm_tst_et);
   fChain->SetBranchAddress("met_softerm_tst_phi", &met_softerm_tst_phi, &b_met_softerm_tst_phi);
   fChain->SetBranchAddress("met_softerm_tst_etx", &met_softerm_tst_etx, &b_met_softerm_tst_etx);
   fChain->SetBranchAddress("met_softerm_tst_ety", &met_softerm_tst_ety, &b_met_softerm_tst_ety);
   fChain->SetBranchAddress("met_softerm_tst_sumet", &met_softerm_tst_sumet, &b_met_softerm_tst_sumet);
   fChain->SetBranchAddress("met_track_et", &met_track_et, &b_met_track_et);
   fChain->SetBranchAddress("met_track_phi", &met_track_phi, &b_met_track_phi);
   fChain->SetBranchAddress("met_track_etx", &met_track_etx, &b_met_track_etx);
   fChain->SetBranchAddress("met_track_ety", &met_track_ety, &b_met_track_ety);
   fChain->SetBranchAddress("met_track_sumet", &met_track_sumet, &b_met_track_sumet);
   fChain->SetBranchAddress("met_truth_et", &met_truth_et, &b_met_truth_et);
   fChain->SetBranchAddress("met_truth_phi", &met_truth_phi, &b_met_truth_phi);
   fChain->SetBranchAddress("met_truth_etx", &met_truth_etx, &b_met_truth_etx);
   fChain->SetBranchAddress("met_truth_ety", &met_truth_ety, &b_met_truth_ety);
   fChain->SetBranchAddress("met_truth_sumet", &met_truth_sumet, &b_met_truth_sumet);
   fChain->SetBranchAddress("met_tst_et", &met_tst_et, &b_met_tst_et);
   fChain->SetBranchAddress("met_tst_phi", &met_tst_phi, &b_met_tst_phi);
   fChain->SetBranchAddress("met_tst_etx", &met_tst_etx, &b_met_tst_etx);
   fChain->SetBranchAddress("met_tst_ety", &met_tst_ety, &b_met_tst_ety);
   fChain->SetBranchAddress("met_tst_sumet", &met_tst_sumet, &b_met_tst_sumet);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_SF", &mu_SF, &b_mu_SF);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_SF_iso", &mu_SF_iso, &b_mu_SF_iso);
   fChain->SetBranchAddress("mu_m", &mu_m, &b_mu_m);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_id_pt", &mu_id_pt, &b_mu_id_pt);
   fChain->SetBranchAddress("mu_id_eta", &mu_id_eta, &b_mu_id_eta);
   fChain->SetBranchAddress("mu_id_phi", &mu_id_phi, &b_mu_id_phi);
   fChain->SetBranchAddress("mu_id_m", &mu_id_m, &b_mu_id_m);
   fChain->SetBranchAddress("mu_me_pt", &mu_me_pt, &b_mu_me_pt);
   fChain->SetBranchAddress("mu_me_eta", &mu_me_eta, &b_mu_me_eta);
   fChain->SetBranchAddress("mu_me_phi", &mu_me_phi, &b_mu_me_phi);
   fChain->SetBranchAddress("mu_me_m", &mu_me_m, &b_mu_me_m);
   fChain->SetBranchAddress("mu_ptcone20", &mu_ptcone20, &b_mu_ptcone20);
   fChain->SetBranchAddress("mu_ptvarcone20", &mu_ptvarcone20, &b_mu_ptvarcone20);
   fChain->SetBranchAddress("mu_etcone20", &mu_etcone20, &b_mu_etcone20);
   fChain->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
   fChain->SetBranchAddress("mu_ptcone30", &mu_ptcone30, &b_mu_ptcone30);
   fChain->SetBranchAddress("mu_ptvarcone30", &mu_ptvarcone30, &b_mu_ptvarcone30);
   fChain->SetBranchAddress("mu_ptvarcone30_TightTTVA_pt1000", &mu_ptvarcone30_TightTTVA_pt1000, &b_mu_ptvarcone30_TightTTVA_pt1000);
   fChain->SetBranchAddress("mu_etcone30", &mu_etcone30, &b_mu_etcone30);
   fChain->SetBranchAddress("mu_topoetcone30", &mu_topoetcone30, &b_mu_topoetcone30);
   fChain->SetBranchAddress("mu_ptcone40", &mu_ptcone40, &b_mu_ptcone40);
   fChain->SetBranchAddress("mu_ptvarcone40", &mu_ptvarcone40, &b_mu_ptvarcone40);
   fChain->SetBranchAddress("mu_etcone40", &mu_etcone40, &b_mu_etcone40);
   fChain->SetBranchAddress("mu_topoetcone40", &mu_topoetcone40, &b_mu_topoetcone40);
   fChain->SetBranchAddress("mu_author", &mu_author, &b_mu_author);
   fChain->SetBranchAddress("mu_quality", &mu_quality, &b_mu_quality);
   fChain->SetBranchAddress("mu_isSA", &mu_isSA, &b_mu_isSA);
   fChain->SetBranchAddress("mu_met_nomuon_dphi", &mu_met_nomuon_dphi, &b_mu_met_nomuon_dphi);
   fChain->SetBranchAddress("mu_met_wmuon_dphi", &mu_met_wmuon_dphi, &b_mu_met_wmuon_dphi);
   fChain->SetBranchAddress("mu_truth_type", &mu_truth_type, &b_mu_truth_type);
   fChain->SetBranchAddress("mu_truth_origin", &mu_truth_origin, &b_mu_truth_origin);
   fChain->SetBranchAddress("mu_baseline_pt", &mu_baseline_pt, &b_mu_baseline_pt);
   fChain->SetBranchAddress("mu_baseline_SF", &mu_baseline_SF, &b_mu_baseline_SF);
   fChain->SetBranchAddress("mu_baseline_eta", &mu_baseline_eta, &b_mu_baseline_eta);
   fChain->SetBranchAddress("mu_baseline_phi", &mu_baseline_phi, &b_mu_baseline_phi);
   fChain->SetBranchAddress("mu_baseline_isLooseID", &mu_baseline_isLooseID, &b_mu_baseline_isLooseID);
   fChain->SetBranchAddress("mu_baseline_isMediumID", &mu_baseline_isMediumID, &b_mu_baseline_isMediumID);
   fChain->SetBranchAddress("mu_baseline_isTightID", &mu_baseline_isTightID, &b_mu_baseline_isTightID);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_BADMUON_SYS__1down", &mu_baseline_SF_syst_MUON_EFF_BADMUON_SYS__1down, &b_mu_baseline_SF_syst_MUON_EFF_BADMUON_SYS__1down);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_BADMUON_SYS__1up", &mu_baseline_SF_syst_MUON_EFF_BADMUON_SYS__1up, &b_mu_baseline_SF_syst_MUON_EFF_BADMUON_SYS__1up);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_ISO_STAT__1down", &mu_baseline_SF_syst_MUON_EFF_ISO_STAT__1down, &b_mu_baseline_SF_syst_MUON_EFF_ISO_STAT__1down);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_ISO_STAT__1up", &mu_baseline_SF_syst_MUON_EFF_ISO_STAT__1up, &b_mu_baseline_SF_syst_MUON_EFF_ISO_STAT__1up);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_ISO_SYS__1down", &mu_baseline_SF_syst_MUON_EFF_ISO_SYS__1down, &b_mu_baseline_SF_syst_MUON_EFF_ISO_SYS__1down);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_ISO_SYS__1up", &mu_baseline_SF_syst_MUON_EFF_ISO_SYS__1up, &b_mu_baseline_SF_syst_MUON_EFF_ISO_SYS__1up);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down", &mu_baseline_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down, &b_mu_baseline_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up", &mu_baseline_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up, &b_mu_baseline_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_RECO_STAT__1down", &mu_baseline_SF_syst_MUON_EFF_RECO_STAT__1down, &b_mu_baseline_SF_syst_MUON_EFF_RECO_STAT__1down);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_RECO_STAT__1up", &mu_baseline_SF_syst_MUON_EFF_RECO_STAT__1up, &b_mu_baseline_SF_syst_MUON_EFF_RECO_STAT__1up);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down", &mu_baseline_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down, &b_mu_baseline_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up", &mu_baseline_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up, &b_mu_baseline_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_RECO_SYS__1down", &mu_baseline_SF_syst_MUON_EFF_RECO_SYS__1down, &b_mu_baseline_SF_syst_MUON_EFF_RECO_SYS__1down);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_RECO_SYS__1up", &mu_baseline_SF_syst_MUON_EFF_RECO_SYS__1up, &b_mu_baseline_SF_syst_MUON_EFF_RECO_SYS__1up);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_TTVA_STAT__1down", &mu_baseline_SF_syst_MUON_EFF_TTVA_STAT__1down, &b_mu_baseline_SF_syst_MUON_EFF_TTVA_STAT__1down);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_TTVA_STAT__1up", &mu_baseline_SF_syst_MUON_EFF_TTVA_STAT__1up, &b_mu_baseline_SF_syst_MUON_EFF_TTVA_STAT__1up);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_TTVA_SYS__1down", &mu_baseline_SF_syst_MUON_EFF_TTVA_SYS__1down, &b_mu_baseline_SF_syst_MUON_EFF_TTVA_SYS__1down);
   fChain->SetBranchAddress("mu_baseline_SF_syst_MUON_EFF_TTVA_SYS__1up", &mu_baseline_SF_syst_MUON_EFF_TTVA_SYS__1up, &b_mu_baseline_SF_syst_MUON_EFF_TTVA_SYS__1up);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_SF", &el_SF, &b_el_SF);
   fChain->SetBranchAddress("el_SF_iso", &el_SF_iso, &b_el_SF_iso);
   fChain->SetBranchAddress("el_SF_trigger", &el_SF_trigger, &b_el_SF_trigger);
   fChain->SetBranchAddress("el_eff_trigger", &el_eff_trigger, &b_el_eff_trigger);
   fChain->SetBranchAddress("el_m", &el_m, &b_el_m);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_id_pt", &el_id_pt, &b_el_id_pt);
   fChain->SetBranchAddress("el_id_eta", &el_id_eta, &b_el_id_eta);
   fChain->SetBranchAddress("el_id_phi", &el_id_phi, &b_el_id_phi);
   fChain->SetBranchAddress("el_id_m", &el_id_m, &b_el_id_m);
   fChain->SetBranchAddress("el_cl_pt", &el_cl_pt, &b_el_cl_pt);
   fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
   fChain->SetBranchAddress("el_cl_etaBE2", &el_cl_etaBE2, &b_el_cl_etaBE2);
   fChain->SetBranchAddress("el_cl_phi", &el_cl_phi, &b_el_cl_phi);
   fChain->SetBranchAddress("el_cl_m", &el_cl_m, &b_el_cl_m);
   fChain->SetBranchAddress("el_ptcone20", &el_ptcone20, &b_el_ptcone20);
   fChain->SetBranchAddress("el_ptvarcone20", &el_ptvarcone20, &b_el_ptvarcone20);
   fChain->SetBranchAddress("el_ptvarcone20_TightTTVA_pt1000", &el_ptvarcone20_TightTTVA_pt1000, &b_el_ptvarcone20_TightTTVA_pt1000);
   fChain->SetBranchAddress("el_etcone20", &el_etcone20, &b_el_etcone20);
   fChain->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
   fChain->SetBranchAddress("el_ptcone30", &el_ptcone30, &b_el_ptcone30);
   fChain->SetBranchAddress("el_ptvarcone30", &el_ptvarcone30, &b_el_ptvarcone30);
   fChain->SetBranchAddress("el_etcone30", &el_etcone30, &b_el_etcone30);
   fChain->SetBranchAddress("el_topoetcone30", &el_topoetcone30, &b_el_topoetcone30);
   fChain->SetBranchAddress("el_ptcone40", &el_ptcone40, &b_el_ptcone40);
   fChain->SetBranchAddress("el_ptvarcone40", &el_ptvarcone40, &b_el_ptvarcone40);
   fChain->SetBranchAddress("el_etcone40", &el_etcone40, &b_el_etcone40);
   fChain->SetBranchAddress("el_topoetcone40", &el_topoetcone40, &b_el_topoetcone40);
   fChain->SetBranchAddress("el_author", &el_author, &b_el_author);
   fChain->SetBranchAddress("el_isConv", &el_isConv, &b_el_isConv);
   fChain->SetBranchAddress("el_truth_pt", &el_truth_pt, &b_el_truth_pt);
   fChain->SetBranchAddress("el_truth_eta", &el_truth_eta, &b_el_truth_eta);
   fChain->SetBranchAddress("el_truth_phi", &el_truth_phi, &b_el_truth_phi);
   fChain->SetBranchAddress("el_truth_status", &el_truth_status, &b_el_truth_status);
   fChain->SetBranchAddress("el_truth_type", &el_truth_type, &b_el_truth_type);
   fChain->SetBranchAddress("el_truth_origin", &el_truth_origin, &b_el_truth_origin);
   fChain->SetBranchAddress("el_met_nomuon_dphi", &el_met_nomuon_dphi, &b_el_met_nomuon_dphi);
   fChain->SetBranchAddress("el_met_wmuon_dphi", &el_met_wmuon_dphi, &b_el_met_wmuon_dphi);
   fChain->SetBranchAddress("el_met_noelectron_dphi", &el_met_noelectron_dphi, &b_el_met_noelectron_dphi);
   fChain->SetBranchAddress("el_baseline_pt", &el_baseline_pt, &b_el_baseline_pt);
   fChain->SetBranchAddress("el_baseline_SF", &el_baseline_SF, &b_el_baseline_SF);
   fChain->SetBranchAddress("el_baseline_eta", &el_baseline_eta, &b_el_baseline_eta);
   fChain->SetBranchAddress("el_baseline_phi", &el_baseline_phi, &b_el_baseline_phi);
   fChain->SetBranchAddress("el_baseline_isLooseID", &el_baseline_isLooseID, &b_el_baseline_isLooseID);
   fChain->SetBranchAddress("el_baseline_isMediumID", &el_baseline_isMediumID, &b_el_baseline_isMediumID);
   fChain->SetBranchAddress("el_baseline_isTightID", &el_baseline_isTightID, &b_el_baseline_isTightID);
   fChain->SetBranchAddress("el_baseline_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &el_baseline_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_el_baseline_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("el_baseline_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &el_baseline_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_el_baseline_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("el_baseline_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &el_baseline_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_el_baseline_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("el_baseline_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &el_baseline_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_el_baseline_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("el_baseline_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &el_baseline_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_el_baseline_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("el_baseline_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &el_baseline_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_el_baseline_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_m", &jet_m, &b_jet_m);
   fChain->SetBranchAddress("jet_fmax", &jet_fmax, &b_jet_fmax);
   fChain->SetBranchAddress("jet_fch", &jet_fch, &b_jet_fch);
   fChain->SetBranchAddress("jet_MV2c10_discriminant", &jet_MV2c10_discriminant, &b_jet_MV2c10_discriminant);
   fChain->SetBranchAddress("jet_MV2c20_discriminant", &jet_MV2c20_discriminant, &b_jet_MV2c20_discriminant);
   fChain->SetBranchAddress("jet_isbjet", &jet_isbjet, &b_jet_isbjet);
   fChain->SetBranchAddress("jet_PartonTruthLabelID", &jet_PartonTruthLabelID, &b_jet_PartonTruthLabelID);
   fChain->SetBranchAddress("jet_ConeTruthLabelID", &jet_ConeTruthLabelID, &b_jet_ConeTruthLabelID);
   fChain->SetBranchAddress("jet_met_nomuon_dphi", &jet_met_nomuon_dphi, &b_jet_met_nomuon_dphi);
   fChain->SetBranchAddress("jet_met_wmuon_dphi", &jet_met_wmuon_dphi, &b_jet_met_wmuon_dphi);
   fChain->SetBranchAddress("jet_met_noelectron_dphi", &jet_met_noelectron_dphi, &b_jet_met_noelectron_dphi);
   fChain->SetBranchAddress("jet_met_nophoton_dphi", &jet_met_nophoton_dphi, &b_jet_met_nophoton_dphi);
   fChain->SetBranchAddress("jet_weight", &jet_weight, &b_jet_weight);
   fChain->SetBranchAddress("jet_raw_pt", &jet_raw_pt, &b_jet_raw_pt);
   fChain->SetBranchAddress("jet_raw_eta", &jet_raw_eta, &b_jet_raw_eta);
   fChain->SetBranchAddress("jet_raw_phi", &jet_raw_phi, &b_jet_raw_phi);
   fChain->SetBranchAddress("jet_raw_m", &jet_raw_m, &b_jet_raw_m);
   fChain->SetBranchAddress("jet_timing", &jet_timing, &b_jet_timing);
   fChain->SetBranchAddress("jet_emfrac", &jet_emfrac, &b_jet_emfrac);
   fChain->SetBranchAddress("jet_hecf", &jet_hecf, &b_jet_hecf);
   fChain->SetBranchAddress("jet_hecq", &jet_hecq, &b_jet_hecq);
   fChain->SetBranchAddress("jet_larq", &jet_larq, &b_jet_larq);
   fChain->SetBranchAddress("jet_avglarq", &jet_avglarq, &b_jet_avglarq);
   fChain->SetBranchAddress("jet_negE", &jet_negE, &b_jet_negE);
   fChain->SetBranchAddress("jet_lambda", &jet_lambda, &b_jet_lambda);
   fChain->SetBranchAddress("jet_lambda2", &jet_lambda2, &b_jet_lambda2);
   fChain->SetBranchAddress("jet_jvtxf", &jet_jvtxf, &b_jet_jvtxf);
   fChain->SetBranchAddress("jet_fmaxi", &jet_fmaxi, &b_jet_fmaxi);
   fChain->SetBranchAddress("jet_isbjet_loose", &jet_isbjet_loose, &b_jet_isbjet_loose);
   fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   fChain->SetBranchAddress("jet_fjvt", &jet_fjvt, &b_jet_fjvt);
   fChain->SetBranchAddress("jet_cleaning", &jet_cleaning, &b_jet_cleaning);
   fChain->SetBranchAddress("jet_TruthLabelDeltaR_B", &jet_TruthLabelDeltaR_B, &b_jet_TruthLabelDeltaR_B);
   fChain->SetBranchAddress("jet_TruthLabelDeltaR_C", &jet_TruthLabelDeltaR_C, &b_jet_TruthLabelDeltaR_C);
   fChain->SetBranchAddress("jet_TruthLabelDeltaR_T", &jet_TruthLabelDeltaR_T, &b_jet_TruthLabelDeltaR_T);
   fChain->SetBranchAddress("jet_DFCommonJets_QGTagger_NTracks", &jet_DFCommonJets_QGTagger_NTracks, &b_jet_DFCommonJets_QGTagger_NTracks);
   fChain->SetBranchAddress("jet_DFCommonJets_QGTagger_TracksWidth", &jet_DFCommonJets_QGTagger_TracksWidth, &b_jet_DFCommonJets_QGTagger_TracksWidth);
   fChain->SetBranchAddress("jet_DFCommonJets_QGTagger_TracksC1", &jet_DFCommonJets_QGTagger_TracksC1, &b_jet_DFCommonJets_QGTagger_TracksC1);
   fChain->SetBranchAddress("trackjet_pt", &trackjet_pt, &b_trackjet_pt);
   fChain->SetBranchAddress("trackjet_eta", &trackjet_eta, &b_trackjet_eta);
   fChain->SetBranchAddress("trackjet_phi", &trackjet_phi, &b_trackjet_phi);
   fChain->SetBranchAddress("trackjet_m", &trackjet_m, &b_trackjet_m);
   fChain->SetBranchAddress("trackjet_fmax", &trackjet_fmax, &b_trackjet_fmax);
   fChain->SetBranchAddress("trackjet_fch", &trackjet_fch, &b_trackjet_fch);
   fChain->SetBranchAddress("trackjet_MV2c10_discriminant", &trackjet_MV2c10_discriminant, &b_trackjet_MV2c10_discriminant);
   fChain->SetBranchAddress("trackjet_MV2c20_discriminant", &trackjet_MV2c20_discriminant, &b_trackjet_MV2c20_discriminant);
   fChain->SetBranchAddress("trackjet_isbjet", &trackjet_isbjet, &b_trackjet_isbjet);
   fChain->SetBranchAddress("trackjet_PartonTruthLabelID", &trackjet_PartonTruthLabelID, &b_trackjet_PartonTruthLabelID);
   fChain->SetBranchAddress("trackjet_ConeTruthLabelID", &trackjet_ConeTruthLabelID, &b_trackjet_ConeTruthLabelID);
   fChain->SetBranchAddress("trackjet_met_nomuon_dphi", &trackjet_met_nomuon_dphi, &b_trackjet_met_nomuon_dphi);
   fChain->SetBranchAddress("trackjet_met_wmuon_dphi", &trackjet_met_wmuon_dphi, &b_trackjet_met_wmuon_dphi);
   fChain->SetBranchAddress("trackjet_met_noelectron_dphi", &trackjet_met_noelectron_dphi, &b_trackjet_met_noelectron_dphi);
   fChain->SetBranchAddress("trackjet_met_nophoton_dphi", &trackjet_met_nophoton_dphi, &b_trackjet_met_nophoton_dphi);
   fChain->SetBranchAddress("trackjet_weight", &trackjet_weight, &b_trackjet_weight);
   fChain->SetBranchAddress("trackjet_raw_pt", &trackjet_raw_pt, &b_trackjet_raw_pt);
   fChain->SetBranchAddress("trackjet_raw_eta", &trackjet_raw_eta, &b_trackjet_raw_eta);
   fChain->SetBranchAddress("trackjet_raw_phi", &trackjet_raw_phi, &b_trackjet_raw_phi);
   fChain->SetBranchAddress("trackjet_raw_m", &trackjet_raw_m, &b_trackjet_raw_m);
   fChain->SetBranchAddress("trackjet_timing", &trackjet_timing, &b_trackjet_timing);
   fChain->SetBranchAddress("trackjet_emfrac", &trackjet_emfrac, &b_trackjet_emfrac);
   fChain->SetBranchAddress("trackjet_hecf", &trackjet_hecf, &b_trackjet_hecf);
   fChain->SetBranchAddress("trackjet_hecq", &trackjet_hecq, &b_trackjet_hecq);
   fChain->SetBranchAddress("trackjet_larq", &trackjet_larq, &b_trackjet_larq);
   fChain->SetBranchAddress("trackjet_avglarq", &trackjet_avglarq, &b_trackjet_avglarq);
   fChain->SetBranchAddress("trackjet_negE", &trackjet_negE, &b_trackjet_negE);
   fChain->SetBranchAddress("trackjet_lambda", &trackjet_lambda, &b_trackjet_lambda);
   fChain->SetBranchAddress("trackjet_lambda2", &trackjet_lambda2, &b_trackjet_lambda2);
   fChain->SetBranchAddress("trackjet_jvtxf", &trackjet_jvtxf, &b_trackjet_jvtxf);
   fChain->SetBranchAddress("trackjet_fmaxi", &trackjet_fmaxi, &b_trackjet_fmaxi);
   fChain->SetBranchAddress("trackjet_isbjet_loose", &trackjet_isbjet_loose, &b_trackjet_isbjet_loose);
   fChain->SetBranchAddress("trackjet_jvt", &trackjet_jvt, &b_trackjet_jvt);
   fChain->SetBranchAddress("trackjet_fjvt", &trackjet_fjvt, &b_trackjet_fjvt);
   fChain->SetBranchAddress("trackjet_cleaning", &trackjet_cleaning, &b_trackjet_cleaning);
   fChain->SetBranchAddress("trackjet_TruthLabelDeltaR_B", &trackjet_TruthLabelDeltaR_B, &b_trackjet_TruthLabelDeltaR_B);
   fChain->SetBranchAddress("trackjet_TruthLabelDeltaR_C", &trackjet_TruthLabelDeltaR_C, &b_trackjet_TruthLabelDeltaR_C);
   fChain->SetBranchAddress("trackjet_TruthLabelDeltaR_T", &trackjet_TruthLabelDeltaR_T, &b_trackjet_TruthLabelDeltaR_T);
   fChain->SetBranchAddress("trackjet_DFCommonJets_QGTagger_NTracks", &trackjet_DFCommonJets_QGTagger_NTracks, &b_trackjet_DFCommonJets_QGTagger_NTracks);
   fChain->SetBranchAddress("trackjet_DFCommonJets_QGTagger_TracksWidth", &trackjet_DFCommonJets_QGTagger_TracksWidth, &b_trackjet_DFCommonJets_QGTagger_TracksWidth);
   fChain->SetBranchAddress("trackjet_DFCommonJets_QGTagger_TracksC1", &trackjet_DFCommonJets_QGTagger_TracksC1, &b_trackjet_DFCommonJets_QGTagger_TracksC1);
   fChain->SetBranchAddress("LCTopoJet_pt", &LCTopoJet_pt, &b_LCTopoJet_pt);
   fChain->SetBranchAddress("LCTopoJet_eta", &LCTopoJet_eta, &b_LCTopoJet_eta);
   fChain->SetBranchAddress("LCTopoJet_phi", &LCTopoJet_phi, &b_LCTopoJet_phi);
   fChain->SetBranchAddress("LCTopoJet_m", &LCTopoJet_m, &b_LCTopoJet_m);
   fChain->SetBranchAddress("LCTopoJet_tau21", &LCTopoJet_tau21, &b_LCTopoJet_tau21);
   fChain->SetBranchAddress("LCTopoJet_D2", &LCTopoJet_D2, &b_LCTopoJet_D2);
   fChain->SetBranchAddress("LCTopoJet_C2", &LCTopoJet_C2, &b_LCTopoJet_C2);
   fChain->SetBranchAddress("LCTopoJet_ntrk", &LCTopoJet_ntrk, &b_LCTopoJet_ntrk);
   fChain->SetBranchAddress("LCTopoJet_nConstit", &LCTopoJet_nConstit, &b_LCTopoJet_nConstit);
   fChain->SetBranchAddress("LCTopoJet_passD2_W50", &LCTopoJet_passD2_W50, &b_LCTopoJet_passD2_W50);
   fChain->SetBranchAddress("LCTopoJet_passD2_Z50", &LCTopoJet_passD2_Z50, &b_LCTopoJet_passD2_Z50);
   fChain->SetBranchAddress("LCTopoJet_passD2_W80", &LCTopoJet_passD2_W80, &b_LCTopoJet_passD2_W80);
   fChain->SetBranchAddress("LCTopoJet_passD2_Z80", &LCTopoJet_passD2_Z80, &b_LCTopoJet_passD2_Z80);
   fChain->SetBranchAddress("LCTopoJet_passMass_W50", &LCTopoJet_passMass_W50, &b_LCTopoJet_passMass_W50);
   fChain->SetBranchAddress("LCTopoJet_passMass_Z50", &LCTopoJet_passMass_Z50, &b_LCTopoJet_passMass_Z50);
   fChain->SetBranchAddress("LCTopoJet_passMass_W80", &LCTopoJet_passMass_W80, &b_LCTopoJet_passMass_W80);
   fChain->SetBranchAddress("LCTopoJet_passMass_Z80", &LCTopoJet_passMass_Z80, &b_LCTopoJet_passMass_Z80);
   fChain->SetBranchAddress("LCTopoJet_cutD2_W50", &LCTopoJet_cutD2_W50, &b_LCTopoJet_cutD2_W50);
   fChain->SetBranchAddress("LCTopoJet_cutD2_Z50", &LCTopoJet_cutD2_Z50, &b_LCTopoJet_cutD2_Z50);
   fChain->SetBranchAddress("LCTopoJet_cutD2_W80", &LCTopoJet_cutD2_W80, &b_LCTopoJet_cutD2_W80);
   fChain->SetBranchAddress("LCTopoJet_cutD2_Z80", &LCTopoJet_cutD2_Z80, &b_LCTopoJet_cutD2_Z80);
   fChain->SetBranchAddress("LCTopoJet_cutMlow_W50", &LCTopoJet_cutMlow_W50, &b_LCTopoJet_cutMlow_W50);
   fChain->SetBranchAddress("LCTopoJet_cutMlow_Z50", &LCTopoJet_cutMlow_Z50, &b_LCTopoJet_cutMlow_Z50);
   fChain->SetBranchAddress("LCTopoJet_cutMlow_W80", &LCTopoJet_cutMlow_W80, &b_LCTopoJet_cutMlow_W80);
   fChain->SetBranchAddress("LCTopoJet_cutMlow_Z80", &LCTopoJet_cutMlow_Z80, &b_LCTopoJet_cutMlow_Z80);
   fChain->SetBranchAddress("LCTopoJet_cutMhigh_W50", &LCTopoJet_cutMhigh_W50, &b_LCTopoJet_cutMhigh_W50);
   fChain->SetBranchAddress("LCTopoJet_cutMhigh_Z50", &LCTopoJet_cutMhigh_Z50, &b_LCTopoJet_cutMhigh_Z50);
   fChain->SetBranchAddress("LCTopoJet_cutMhigh_W80", &LCTopoJet_cutMhigh_W80, &b_LCTopoJet_cutMhigh_W80);
   fChain->SetBranchAddress("LCTopoJet_cutMhigh_Z80", &LCTopoJet_cutMhigh_Z80, &b_LCTopoJet_cutMhigh_Z80);
   fChain->SetBranchAddress("LCTopoJet_weight", &LCTopoJet_weight, &b_LCTopoJet_weight);
   fChain->SetBranchAddress("TCCJet_pt", &TCCJet_pt, &b_TCCJet_pt);
   fChain->SetBranchAddress("TCCJet_eta", &TCCJet_eta, &b_TCCJet_eta);
   fChain->SetBranchAddress("TCCJet_phi", &TCCJet_phi, &b_TCCJet_phi);
   fChain->SetBranchAddress("TCCJet_m", &TCCJet_m, &b_TCCJet_m);
   fChain->SetBranchAddress("TCCJet_tau21", &TCCJet_tau21, &b_TCCJet_tau21);
   fChain->SetBranchAddress("TCCJet_D2", &TCCJet_D2, &b_TCCJet_D2);
   fChain->SetBranchAddress("TCCJet_C2", &TCCJet_C2, &b_TCCJet_C2);
   fChain->SetBranchAddress("TCCJet_ntrk", &TCCJet_ntrk, &b_TCCJet_ntrk);
   fChain->SetBranchAddress("TCCJet_nConstit", &TCCJet_nConstit, &b_TCCJet_nConstit);
   fChain->SetBranchAddress("TCCJet_passD2_W50", &TCCJet_passD2_W50, &b_TCCJet_passD2_W50);
   fChain->SetBranchAddress("TCCJet_passD2_Z50", &TCCJet_passD2_Z50, &b_TCCJet_passD2_Z50);
   fChain->SetBranchAddress("TCCJet_passD2_W80", &TCCJet_passD2_W80, &b_TCCJet_passD2_W80);
   fChain->SetBranchAddress("TCCJet_passD2_Z80", &TCCJet_passD2_Z80, &b_TCCJet_passD2_Z80);
   fChain->SetBranchAddress("TCCJet_passMass_W50", &TCCJet_passMass_W50, &b_TCCJet_passMass_W50);
   fChain->SetBranchAddress("TCCJet_passMass_Z50", &TCCJet_passMass_Z50, &b_TCCJet_passMass_Z50);
   fChain->SetBranchAddress("TCCJet_passMass_W80", &TCCJet_passMass_W80, &b_TCCJet_passMass_W80);
   fChain->SetBranchAddress("TCCJet_passMass_Z80", &TCCJet_passMass_Z80, &b_TCCJet_passMass_Z80);
   fChain->SetBranchAddress("TCCJet_cutD2_W50", &TCCJet_cutD2_W50, &b_TCCJet_cutD2_W50);
   fChain->SetBranchAddress("TCCJet_cutD2_Z50", &TCCJet_cutD2_Z50, &b_TCCJet_cutD2_Z50);
   fChain->SetBranchAddress("TCCJet_cutD2_W80", &TCCJet_cutD2_W80, &b_TCCJet_cutD2_W80);
   fChain->SetBranchAddress("TCCJet_cutD2_Z80", &TCCJet_cutD2_Z80, &b_TCCJet_cutD2_Z80);
   fChain->SetBranchAddress("TCCJet_cutMlow_W50", &TCCJet_cutMlow_W50, &b_TCCJet_cutMlow_W50);
   fChain->SetBranchAddress("TCCJet_cutMlow_Z50", &TCCJet_cutMlow_Z50, &b_TCCJet_cutMlow_Z50);
   fChain->SetBranchAddress("TCCJet_cutMlow_W80", &TCCJet_cutMlow_W80, &b_TCCJet_cutMlow_W80);
   fChain->SetBranchAddress("TCCJet_cutMlow_Z80", &TCCJet_cutMlow_Z80, &b_TCCJet_cutMlow_Z80);
   fChain->SetBranchAddress("TCCJet_cutMhigh_W50", &TCCJet_cutMhigh_W50, &b_TCCJet_cutMhigh_W50);
   fChain->SetBranchAddress("TCCJet_cutMhigh_Z50", &TCCJet_cutMhigh_Z50, &b_TCCJet_cutMhigh_Z50);
   fChain->SetBranchAddress("TCCJet_cutMhigh_W80", &TCCJet_cutMhigh_W80, &b_TCCJet_cutMhigh_W80);
   fChain->SetBranchAddress("TCCJet_cutMhigh_Z80", &TCCJet_cutMhigh_Z80, &b_TCCJet_cutMhigh_Z80);
   fChain->SetBranchAddress("TCCJet_lead_clusters_pt", &TCCJet_lead_clusters_pt, &b_TCCJet_lead_clusters_pt);
   fChain->SetBranchAddress("TCCJet_lead_clusters_eta", &TCCJet_lead_clusters_eta, &b_TCCJet_lead_clusters_eta);
   fChain->SetBranchAddress("TCCJet_lead_clusters_phi", &TCCJet_lead_clusters_phi, &b_TCCJet_lead_clusters_phi);
   fChain->SetBranchAddress("TCCJet_lead_clusters_m", &TCCJet_lead_clusters_m, &b_TCCJet_lead_clusters_m);
   fChain->SetBranchAddress("TCCJet_weight", &TCCJet_weight, &b_TCCJet_weight);
   fChain->SetBranchAddress("ph_pt", &ph_pt, &b_ph_pt);
   fChain->SetBranchAddress("ph_eta", &ph_eta, &b_ph_eta);
   fChain->SetBranchAddress("ph_phi", &ph_phi, &b_ph_phi);
   fChain->SetBranchAddress("ph_truth_pt", &ph_truth_pt, &b_ph_truth_pt);
   fChain->SetBranchAddress("ph_SF", &ph_SF, &b_ph_SF);
   fChain->SetBranchAddress("ph_SF_iso", &ph_SF_iso, &b_ph_SF_iso);
   fChain->SetBranchAddress("ph_isotool_pass_fixedcuttight", &ph_isotool_pass_fixedcuttight, &b_ph_isotool_pass_fixedcuttight);
   fChain->SetBranchAddress("ph_m", &ph_m, &b_ph_m);
   fChain->SetBranchAddress("ph_ptcone20", &ph_ptcone20, &b_ph_ptcone20);
   fChain->SetBranchAddress("ph_ptvarcone20", &ph_ptvarcone20, &b_ph_ptvarcone20);
   fChain->SetBranchAddress("ph_etcone20", &ph_etcone20, &b_ph_etcone20);
   fChain->SetBranchAddress("ph_topoetcone20", &ph_topoetcone20, &b_ph_topoetcone20);
   fChain->SetBranchAddress("ph_ptcone30", &ph_ptcone30, &b_ph_ptcone30);
   fChain->SetBranchAddress("ph_ptvarcone30", &ph_ptvarcone30, &b_ph_ptvarcone30);
   fChain->SetBranchAddress("ph_etcone30", &ph_etcone30, &b_ph_etcone30);
   fChain->SetBranchAddress("ph_topoetcone30", &ph_topoetcone30, &b_ph_topoetcone30);
   fChain->SetBranchAddress("ph_ptcone40", &ph_ptcone40, &b_ph_ptcone40);
   fChain->SetBranchAddress("ph_ptvarcone40", &ph_ptvarcone40, &b_ph_ptvarcone40);
   fChain->SetBranchAddress("ph_etcone40", &ph_etcone40, &b_ph_etcone40);
   fChain->SetBranchAddress("ph_topoetcone40", &ph_topoetcone40, &b_ph_topoetcone40);
   fChain->SetBranchAddress("ph_isTight", &ph_isTight, &b_ph_isTight);
   fChain->SetBranchAddress("ph_isEM", &ph_isEM, &b_ph_isEM);
   fChain->SetBranchAddress("ph_OQ", &ph_OQ, &b_ph_OQ);
   fChain->SetBranchAddress("ph_author", &ph_author, &b_ph_author);
   fChain->SetBranchAddress("ph_isConv", &ph_isConv, &b_ph_isConv);
   fChain->SetBranchAddress("ph_truth_eta", &ph_truth_eta, &b_ph_truth_eta);
   fChain->SetBranchAddress("ph_truth_phi", &ph_truth_phi, &b_ph_truth_phi);
   fChain->SetBranchAddress("ph_truth_status", &ph_truth_status, &b_ph_truth_status);
   fChain->SetBranchAddress("ph_truth_type", &ph_truth_type, &b_ph_truth_type);
   fChain->SetBranchAddress("ph_truth_origin", &ph_truth_origin, &b_ph_truth_origin);
   fChain->SetBranchAddress("ph_met_nomuon_dphi", &ph_met_nomuon_dphi, &b_ph_met_nomuon_dphi);
   fChain->SetBranchAddress("ph_met_wmuon_dphi", &ph_met_wmuon_dphi, &b_ph_met_wmuon_dphi);
   fChain->SetBranchAddress("ph_met_nophoton_dphi", &ph_met_nophoton_dphi, &b_ph_met_nophoton_dphi);
   fChain->SetBranchAddress("ph_baseline_pt", &ph_baseline_pt, &b_ph_baseline_pt);
   fChain->SetBranchAddress("ph_baseline_SF", &ph_baseline_SF, &b_ph_baseline_SF);
   fChain->SetBranchAddress("ph_baseline_eta", &ph_baseline_eta, &b_ph_baseline_eta);
   fChain->SetBranchAddress("ph_baseline_phi", &ph_baseline_phi, &b_ph_baseline_phi);
   fChain->SetBranchAddress("ph_baseline_SF_syst_PH_EFF_ID_Uncertainty__1down", &ph_baseline_SF_syst_PH_EFF_ID_Uncertainty__1down, &b_ph_baseline_SF_syst_PH_EFF_ID_Uncertainty__1down);
   fChain->SetBranchAddress("ph_baseline_SF_syst_PH_EFF_ID_Uncertainty__1up", &ph_baseline_SF_syst_PH_EFF_ID_Uncertainty__1up, &b_ph_baseline_SF_syst_PH_EFF_ID_Uncertainty__1up);
   fChain->SetBranchAddress("ph_baseline_SF_syst_PH_EFF_ISO_Uncertainty__1down", &ph_baseline_SF_syst_PH_EFF_ISO_Uncertainty__1down, &b_ph_baseline_SF_syst_PH_EFF_ISO_Uncertainty__1down);
   fChain->SetBranchAddress("ph_baseline_SF_syst_PH_EFF_ISO_Uncertainty__1up", &ph_baseline_SF_syst_PH_EFF_ISO_Uncertainty__1up, &b_ph_baseline_SF_syst_PH_EFF_ISO_Uncertainty__1up);
   Notify();
}

Bool_t ClusterTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ClusterTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ClusterTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ClusterTree_cxx
