#ifndef PlotList_h
#define PlotList_h

#include <TString.h>
#include <map>
#include "methods.h"
#include "Cuts.h"

class PlotList {

    public:

    //Map from treeVariable to {n_bin,low_bin,high_bin}
    //The key TString must be implemented in PlotTools::getBranchValue and PlotList::getBranchQuery
    static map<TString,vector<Double_t>> map_binning() {
        return {

                                                // {"met_tst_et__from0_to1200",{14,0,1200}},
                                                // {"met_tst_et__from0_to800",{80,0,800}},
                                                // {"met_tst_et__from100_to800",{80,100,800}},
                                                // {"met_tst_et__from0_to500",{50,0,500}},
                                                // {"met_tst_et__from100_to500",{50,100,500}},
                                                {"met_tst_et__from100_to150",{100,100,150}},
                                                {"met_tst_et__from100_to1200",{120,100,1200}},
                                                {"met_tst_et__from100_to1200_rebin",{24,100,1200}},
                                                // {"met_tst_et__from200_to1200",{12,200,1200}},
                                                {"met_tst_et__from150_to1000",{12,150,1000}},
                                                {"met_tst_et__from250_to1200",{12,250,1200}},
                                                {"met_nomuon_tst_et__from250_to1200",{12,250,1200}},
                                                {"met_nomuon_tst_et__from150_to1000",{12,150,1000}},
                                                {"met_nomuon_tst_et__from0_to1000",{50,0,1000}},
                                                {"met_noelectron_tst_et__from250_to1200",{12,250,1200}},
                                                {"met_noelectron_tst_et__from150_to1000",{12,150,1000}},
                                                // {"metNoLooseMuon__from250_to1200",{12,250,1200}},
                                                // {"met_tst_et__from200_to2000",{20,200,2000}},
                                                {"lead_jet_pt__from150_to1200",{12,150,1200}},
                                                // {"lead_jet_pt__from150_to2000",{12,150,2000}},
                                                {"lead_jet_pt__from0_to1200",{12,0,1200}},
                                                {"DijetMass",{20,0,200}},
                                                {"TCCJetMass",{20,0,200}},
                                                {"LCTopoJetMass",{20,0,200}},

                                                // {"lead_jet_eta",{20,-3.5,3.5}},
                                                // {"lead_jet_phi",{20,-3.5,3.5}},
                                                // {"lead_el_pt__from0_to500",{50,0,500}},
                                                // {"lead_mu_pt__from0_to500",{50,0,500}},
                                                // {"lead_tau_pt__from0_to200",{20,0,200}},
                                                {"lead_el_eta",{20,-3.5,3.5}},
                                                // {"lead_el_phi",{20,-3.5,3.5}},
                                                // {"lead_mu_eta",{20,-3.5,3.5}},
                                                // {"lead_mu_phi",{20,-3.5,3.5}},
                                                {"n_jet",{10,0,10}},
                                                {"n_TCCJet",{10,0,10}},
                                                {"n_LCTopoJet",{10,0,10}},
                                                {"n_el",{10,0,10}},
                                                {"n_mu",{10,0,10}},
                                                {"n_tau",{10,0,10}},
                                                
                                                {"dPhiTCCJetMet",{35,0,3.5}},
                                                {"dPhiLCTopoJetMet",{35,0,3.5}},
                                                {"dPhiDijetMet",{35,0,3.5}},
                                                {"dPhiDijet",{35,0,3.5}},
                                                {"dRDijet",{70,0,7}},

                                                {"TCCD2",{50,0,10}},
                                                {"LCTopoD2",{50,0,10}},
                                                {"TCCC2",{50,0,0.4}},
                                                {"LCTopoC2",{50,0,0.4}},
                                                {"TCCtau21",{50,0,1}},
                                                {"LCTopotau21",{50,0,1}},
                                                {"TCCNtrks",{50,0,50}},
                                                {"LCTopoNtrks",{50,0,50}},

                                                {"n_trackTCCAssociatedBjet",{10,0,10}},
                                                {"n_trackLCTopoAssociatedBjet",{10,0,10}},
                                                {"n_trackTCCSeparatedBjet",{10,0,10}},
                                                {"n_trackLCTopoSeparatedBjet",{10,0,10}},
                                                {"n_bcentralJet",{10,0,10}},

                                                {"met_tst_sig",{40,0,20}},

                                                // {"pu_weight__fromm10_to10",{20,-10,10}},
                                                // {"pu_weight__fromm50_to50",{100,-50,50}},
                                                // {"pu_weight__fromm100_to100",{200,-100,100}},
                                                // {"pu_weight__fromm1000_to1000",{2000,-1000,1000}},

                                                {"lead_jet_emfrac",{50,0,1}},
                                                {"fchRatio",{100,0,1}},
                                                {"mindPhiJetMet",{35,0,3.5}},
                                                {"poorMetSignCut",{40,0,20}},
                                                {"el_ptvarcone20_TightTTVA_pt1000_ratio_pt",{50,0,0.1}},
                                                {"el_topoetcone20_ratio_pt",{50,0,0.1}},
                                                {"mu_ptcone20_ratio_pt",{50,0,1}},

                                                {"corActualIntPerXing",{80,0,80}},
                                                {"corAverageIntPerXing",{80,0,80}},
                                                {"actualIntPerXing",{80,0,80}},
                                                {"averageIntPerXing",{80,0,80}},
                                                {"n_vx",{80,0,80}},

                                                {"DiElectronMass",{150,0,150}},
                                                {"DiMuonMass",{150,0,150}},
                                                {"DiLeptonMass",{150,0,150}},

                                                {"munu_mT",{120,0,120}},
                                                {"enu_mT",{120,0,120}},

                                                {"year",{4,2015,2019}},

                                                // {"checkJiangliu_multijet_nomuon_cut",{100,-10,10}},
                                                // {"checkJiangliu_met_track_nomuon_pt",{100,-10,10}},
                                                // {"checkJiangliu_met_track_nomuon_eta",{100,-10,10}},
                                                // {"checkJiangliu_met_track_nomuon_phi",{100,-10,10}},
                                                // {"checkJiangliu_met_track_nomuon_dphi",{100,-10,10}},


        };
    };  

    static map<TString,vector<Double_t>> map_avgHist_binning() {
        return {

                                                {"jetPT_vs_MET",{14,150,1200}},
                                                {"elSF_vs_MET",{30,150,1200}},
                                                {"muSF_vs_MET",{30,150,1200}},
                                                {"elSF_vs_NoElMET",{30,150,1200}},
        };
    }

    static map<TString,vector<TString>> map_avgHist_variables() {
        return {

                                                {"jetPT_vs_MET",{"met_tst_et","lead_jet_pt"}},
                                                {"elSF_vs_MET",{"met_tst_et","lead_el_SF"}},
                                                {"muSF_vs_MET",{"met_tst_et","lead_mu_SF"}},
                                                {"elSF_vs_NoElMET",{"met_noelectron_tst_et","lead_el_SF"}},
        };
    }

    static map<TString,TString> map_xLabel() {
            return {
                                        {"met_tst_et","MET"},
                                        {"met_nomuon_tst_et","MET+muon"},
                                        {"met_noelectron_tst_et","MET+electron"},
                                        {"metNoLooseMuon","MET+#mu_{loose}"},
                                        {"lead_jet_pt","p_{T}^{j_{1}}"},
                                        {"DijetMass","m_{j_{1}j_{2}}"},
                                        {"TCCJetMass","m_{J}^{TCC}"},
                                        {"LCTopoJetMass","m_{J}^{LCTopo}"},
                                        {"lead_jet_eta","#eta^{j_{1}}"},
                                        {"lead_jet_phi","#phi^{j_{1}}"},
                                        {"lead_el_pt","p_{T}^{e_{1}}"},
                                        {"lead_mu_pt","p_{T}^{#mu_{1}}"},
                                        {"lead_tau_pt","p_{T}^{#tau_{1}}"},
                                        {"lead_el_eta","#eta^{e_{1}}"},
                                        {"lead_el_phi","#phi^{e_{1}}"},
                                        {"lead_mu_eta","#eta^{#mu_{1}}"},
                                        {"lead_mu_phi","#phi^{#mu_{1}}"},
                                        {"n_jet","Number of 0.4 jets"},
                                        {"n_TCCJet","Number of large TCC jets"},
                                        {"n_LCTopoJet","Number of large LCTopo jets"},
                                        {"n_el","Number of electrons"},
                                        {"n_mu","Number of muons"},
                                        {"n_tau","Number of taus"},
                                        {"dPhiTCCJetMet","#Delta#phi(J^{TCC},MET)"},
                                        {"dPhiLCTopoJetMet","#Delta#phi(J^{LCTopo},MET)"},
                                        {"dPhiDijetMet","#Delta#phi(j_{1}j_{2},MET)"},
                                        {"dPhiDijet","#Delta#phi(j_{1},j_{2})"},
                                        {"dRDijet","#Delta R(j_{1},j_{2})"},
                                        {"TCCD2","D_{2}^{TCC}"},
                                        {"LCTopoD2","D_{2}^{LCTopo}"},
                                        {"TCCC2","C_{2}^{TCC}"},
                                        {"LCTopoC2","C_{2}^{LCTopo}"},
                                        {"TCCtau21","#tau_{21}^{TCC}"},
                                        {"LCTopotau21","#tau_{21}^{LCTopo}"},
                                        {"TCCNtrks","N_{trk}^{TCC}"},
                                        {"LCTopoNtrks","N_{trk}^{LCTopo}"},
                                        {"pu_weight","w_{pu}"},
                                        {"evntWeight","w_{evnt}"},
                                        {"n_trackTCCAssociatedBjet","Number of bjet #in J^{TCC}"},
                                        {"n_trackLCTopoAssociatedBjet","Number of bjet #in J^{LCTopo}"},
                                        {"n_trackTCCSeparatedBjet","Number of bjet #notin J^{TCC}"},
                                        {"n_trackLCTopoSeparatedBjet","Number of bjet #notin J^{LCTopo}"},
                                        {"n_bcentralJet","Number of bjet^{c}"},
                                        {"met_tst_sig","MET significance"},
                                        {"lead_jet_emfrac","jet_{0}^{emfrac}"},
                                        {"fchRatio","jet_{0}^{fch} / jet_{0}^{fmax}"},
                                        {"mindPhiJetMet","min[#Delta#phi(j_{i},MET)]"},
                                        {"poorMetSignCut","MET / #sqrt{H_{T}}"},
                                        {"el_ptvarcone20_TightTTVA_pt1000_ratio_pt","el_{0}^{ptvarcone20} / el_{0}^{pT}"},
                                        {"el_topoetcone20_ratio_pt","el_{0}^{topoetcone20} / el_{0}^{pT}"},
                                        {"mu_ptcone20_ratio_pt","mu_{0}^{ptcone20} / mu_{0}^{pT}"},

                                        {"intPerXing","#mu"},
                                        {"CorIntPerXing","#mu"},

                                        {"corActualIntPerXing","Actual #mu_{corr}"},
                                        {"corAverageIntPerXing","Avg #mu_{corr}"},
                                        {"actualIntPerXing","Actual #mu"},
                                        {"averageIntPerXing","Avg #mu"},
                                        {"n_vx","Number of vertices"},

                                        {"DiElectronMass","m_{ee}"},
                                        {"DiMuonMass","m_{#mu#mu}"},
                                        {"DiLeptonMass","m_{ll}"},
                                        {"munu_mT","m_{#mu#nu}"},
                                        {"enu_mT","m_{e#nu}"},

                                        {"year","year"},

                                        {"lead_el_SF","el SF"},
                                        {"lead_mu_SF","#mu SF"},
        };
    };

    static map<TString,TString> map_xUnit() {
        return {
                                        {"met_tst_et","GeV"},
                                        {"met_nomuon_tst_et","GeV"},
                                        {"met_noelectron_tst_et","GeV"},
                                        {"metNoLooseMuon","GeV"},
                                        {"lead_jet_pt","GeV"},
                                        {"DijetMass","GeV"},
                                        {"TCCJetMass","GeV"},
                                        {"LCTopoJetMass","GeV"},
                                        {"lead_el_pt","GeV"},
                                        {"lead_mu_pt","GeV"},
                                        {"lead_tau_pt","GeV"},
                                        {"poorMetSignCut","GeV^{1/2}"},
                                        {"DiElectronMass","GeV"},
                                        {"DiMuonMass","GeV"},
                                        {"DiLeptonMass","GeV"},
                                        {"munu_mT","GeV"},
                                        {"enu_mT","GeV"},
        };
    };

    static TString getBranchQuery(TString name) {
        if      (name.Contains("met_tst_et"))                               return "met_tst_et / 1000";
        else if (name.Contains("met_nomuon_tst_et"))                        return "met_nomuon_tst_et / 1000";
        else if (name.Contains("met_noelectron_tst_et"))                        return "met_noelectron_tst_et / 1000";
        else if (name.Contains("lead_jet_pt"))                              return "jet_pt[0] / 1000.";
        else if (name.Contains("DijetMass"))                                return "DijetMass / 1000.";
        else if (name.Contains("TCCJetMass"))                               return "m_TCCJet / 1000.";
        else if (name.Contains("LCTopoJetMass"))                            return "m_LCTopoJet / 1000.";
        else if (name.Contains("lead_jet_eta"))                             return "jet_eta[0]";
        else if (name.Contains("lead_jet_phi"))                             return "jet_phi[0]";
        else if (name.Contains("lead_el_pt"))                               return "el_pt[0] / 1000.";
        else if (name.Contains("lead_mu_pt"))                               return "mu_pt[0] / 1000.";
        else if (name.Contains("lead_tau_pt"))                              return "tau_pt[0] / 1000.";
        else if (name.Contains("lead_el_eta"))                              return "el_eta[0]";
        else if (name.Contains("lead_el_phi"))                              return "el_phi[0]";
        else if (name.Contains("lead_mu_eta"))                              return "mu_eta[0]";
        else if (name.Contains("lead_mu_phi"))                              return "mu_phi[0]";
        else if (name.Contains("n_jet"))                                    return "n_jet";
        else if (name.Contains("n_TCCJet"))                                 return "n_TCCJet";
        else if (name.Contains("n_LCTopoJet"))                              return "n_LCTopoJet";
        else if (name.Contains("n_el"))                                     return "n_el";
        else if (name.Contains("n_mu"))                                     return "n_mu";
        else if (name.Contains("n_tau"))                                    return "n_tau";
        else if (name.Contains("dPhiTCCJetMet"))                            return "dPhiTCCJetMet";
        else if (name.Contains("dPhiLCTopoJetMet"))                         return "dPhiLCTopoJetMet";
        else if (name.Contains("dPhiDijetMet"))                             return "dPhiDijetMet";
        else if (name.Contains("dPhiDijet"))                                return "dPhiDijet";
        else if (name.Contains("dRDijet"))                                  return "dRDijet";
        else if (name.Contains("pu_weight"))                                return "pu_weight";
        else if (name.Contains("evntWeight"))                               return "weight";
        else if (name.Contains("TCCD2"))                                    return "TCCJet_D2[0]";
        else if (name.Contains("LCTopoD2"))                                 return "LCTopoJet_D2[0]";
        else if (name.Contains("TCCC2"))                                    return "TCCJet_C2[0]";
        else if (name.Contains("LCTopoC2"))                                 return "LCTopoJet_C2[0]";
        else if (name.Contains("TCCtau21"))                                 return "TCCJet_tau21[0]";
        else if (name.Contains("LCTopotau21"))                              return "LCTopoJet_tau21[0]";
        else if (name.Contains("TCCNtrks"))                                 return "TCCJet_ntrk";
        else if (name.Contains("LCTopoNtrks"))                              return "LCTopoJet_ntrk";
        else if (name.Contains("n_trackTCCAssociatedBjet"))                 return "n_trackTCCAssociatedBjet";
        else if (name.Contains("n_trackLCTopoAssociatedBjet"))              return "n_trackLCTopoAssociatedBjet";
        else if (name.Contains("n_trackTCCSeparatedBjet"))                  return "n_trackTCCSeparatedBjet";
        else if (name.Contains("n_trackLCTopoSeparatedBjet"))               return "n_trackLCTopoSeparatedBjet";
        else if (name.Contains("n_bcentralJet"))                            return "n_bcentralJet";
        else if (name.Contains("met_tst_sig"))                              return "met_tst_sig";
        else if (name.Contains("lead_jet_emfrac"))                          return "jet_emfrac[0]";
        else if (name.Contains("fchRatio"))                                 return "jet_fch[0] / jet_fmax[0]";
        else if (name.Contains("el_ptvarcone20_TightTTVA_pt1000_ratio_pt")) return "el_ptvarcone20_TightTTVA_pt1000[0] / el_pt[0]";
        else if (name.Contains("el_topoetcone20_ratio_pt"))                 return "el_topoetcone20[0] / el_pt[0]";
        else if (name.Contains("mu_ptcone20_ratio_pt"))                     return "mu_ptcone20[0] / mu_pt[0]";
        else if (name.Contains("corActualIntPerXing"))                      return "corActualIntPerXing";
        else if (name.Contains("corAverageIntPerXing"))                     return "corAverageIntPerXing";
        else if (name.Contains("actualIntPerXing"))                         return "actualIntPerXing";
        else if (name.Contains("averageIntPerXing"))                        return "averageIntPerXing";
        else if (name.Contains("n_vx"))                                     return "n_vx";
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        else coutError("The variable " + name + " is not implemented in PlotList::getBranchQuery");
        return "";
    }


    static TString getXLabel(TString name) {
        TString xLabel = "";
        vector<TString> variables;
        for (auto kv_label : map_xLabel()) variables.push_back(kv_label.first);
        sort(variables.begin(),variables.end(), [] (const TString & first, const TString & second) {return first.Length() < second.Length();});

        if (map_avgHist_binning().count(name) == 0) {
            for (int i_label = variables.size() - 1 ; i_label >= 0 ; i_label--) {
                TString var_name = variables.at(i_label);
                if (name.Contains(var_name)) {
                    xLabel = map_xLabel()[var_name]; 
                    break;
                }
            }
        } else {
            if (map_xLabel().count(map_avgHist_variables()[name].at(0)) > 0) xLabel =map_xLabel()[map_avgHist_variables()[name].at(0)];
        }

        if (xLabel == "") coutError("No xLabel found for " + name);
        return xLabel;
    }

    static TString getXUnit(TString name, bool toAppend = true) {
        TString xUnit = "";
        for (auto kv_label : map_xUnit()) {
            TString var_name = kv_label.first;
            if (name.Contains(var_name)) {
                xUnit = map_xUnit()[var_name]; 
                if (toAppend) xUnit = " [" + xUnit + "]";
                break;
            }
        }
        return xUnit;
    }

    static TString getXTitle(TString name) {
        TString xTitle = getXLabel(name) + getXUnit(name, true);
        return xTitle;
    }

    static string makeVariablesDict() {
        stringstream variables_dict;
        variables_dict << "var_dict = {" << endl;

        for (auto kv : map_binning()) {

            TString var_name = kv.first;
            vector<Double_t> var_binning = kv.second;
            TString var_label = getXLabel(var_name);
            TString var_unit = getXUnit(var_name,false);

            variables_dict << "\t\t'" << var_name << "':['" << getBranchQuery(var_name) << "'," << var_binning.at(0) << "," << var_binning.at(1) << "," << var_binning.at(2) << ",'" << var_label << "','" << var_unit << "']," << endl;

        }
        variables_dict << "}" << endl;

        return variables_dict.str();
    }

    static TString getVariableForSelection(TString N1_selection) {
        if (N1_selection.Contains("dPhiFatJetMet") && N1_selection.Contains("useTCC"))        return "dPhiTCCJetMet";
        else if (N1_selection.Contains("dPhiFatJetMet"))                                   return "dPhiLCTopoJetMet";
        else if (N1_selection.Contains("dPhiDijetMet"))                                    return "dPhiDijetMet";
        else if (N1_selection.Contains("dPhiDiCentralJets"))                               return "dPhiDijet";
        else if (N1_selection.Contains("CutMassFatJet") && N1_selection.Contains("useTCC"))   return "TCCJetMass";
        else if (N1_selection.Contains("CutMassFatJet"))                                   return "LCTopoJetMass";
        else if (N1_selection.Contains("VTagD2FatJet") && N1_selection.Contains("useTCC"))    return "TCCD2";
        else if (N1_selection.Contains("VTagD2FatJet"))                                    return "LCTopoD2";
        else if (N1_selection.Contains("VTag80D2FatJet") && N1_selection.Contains("useTCC"))  return "TCCD2";
        else if (N1_selection.Contains("VTag80D2FatJet"))                                  return "LCTopoD2";
        else if (N1_selection.Contains("VTag50D2FatJet") && N1_selection.Contains("useTCC"))  return "TCCD2";
        else if (N1_selection.Contains("VTag50D2FatJet"))                                  return "LCTopoD2";
        else if (N1_selection.Contains("VTagC2FatJet") && N1_selection.Contains("useTCC"))    return "TCCC2";
        else if (N1_selection.Contains("VTagC2FatJet"))                                    return "LCTopoC2";
        else if (N1_selection.Contains("VTagtau21FatJet") && N1_selection.Contains("useTCC")) return "TCCtau21";
        else if (N1_selection.Contains("VTagtau21FatJet"))                                 return "LCTopotau21";
        else if (N1_selection.Contains("met_tst_sig"))                                     return "met_tst_sig";
        else if (N1_selection.Contains("met"))                                             return "met_tst_et__from100_to1200";
        else if (N1_selection.Contains("LCTopoNtrks"))                                     return "LCTopoNtrks";
        else if (N1_selection.Contains("TCCNtrks"))                                        return "TCCNtrks";

        coutError("PlotList::getVariableForSelection -- No variable defined for the selection " + N1_selection);

        return "";
    }

    static vector<int> getCutForSelection(TString N1_selection) {
        if (N1_selection.Contains("dPhiFatJetMet"))              return {CUTS::DPHI_FATJET_MET};
        else if (N1_selection.Contains("dPhiDijetMet"))          return {CUTS::DPHI_DIJET_MET};
        else if (N1_selection.Contains("dPhiDiCentralJets"))     return {CUTS::DPHI_DIJET};
        else if (N1_selection.Contains("CutMassFatJet"))         return {CUTS::VTAG_80_MASS,CUTS::VTAG_50_MASS};
        else if (N1_selection.Contains("VTagD2FatJet"))          return {CUTS::VTAG_80_D2,CUTS::VTAG_50_D2};
        else if (N1_selection.Contains("VTag80D2FatJet"))        return {CUTS::VTAG_80_D2};
        else if (N1_selection.Contains("VTag50D2FatJet"))        return {CUTS::VTAG_50_D2};
        else if (N1_selection.Contains("met250"))                return {CUTS::MET250,CUTS::MET150};
        else if (N1_selection.Contains("met150"))                return {CUTS::MET250,CUTS::MET150};
        else if (N1_selection.Contains("LCTopoNtrks"))           return {CUTS::VTAG_80_NTRKS,CUTS::VTAG_50_NTRKS};
        else if (N1_selection.Contains("TCCNtrks"))              return {CUTS::VTAG_80_NTRKS,CUTS::VTAG_50_NTRKS};


        return {};
    }

    static bool validForN1Plot(TString N1_selection, TString selection) {
        if (N1_selection.Contains("met_tst_sig") || N1_selection.Contains("LCTopoNtrks") || N1_selection.Contains("TCCNtrks") || N1_selection.Contains("VTagC2") || N1_selection.Contains("VTagtau21")) return true;

        if (selection.Contains("monoV_merged")) {
            if (N1_selection.Contains("dPhiFatJetMet")) return true;
            if (N1_selection.Contains("VTagD2FatJet")) return true;
            if (N1_selection.Contains("VTag80D2FatJet")) return true;
            if (N1_selection.Contains("VTag50D2FatJet")) return true;
            if (N1_selection.Contains("VTagC2FatJet")) return true;
            if (N1_selection.Contains("VTagtau21FatJet")) return true;
            if (N1_selection.Contains("met250")) return true;
            if (N1_selection.Contains("LCTopoNtrks")) return true;
            if (N1_selection.Contains("TCCNtrks")) return true;
            if (N1_selection.Contains("CutMassFatJet")) return true;
        }

        if (selection.Contains("monoV_resolved")) {
            if (N1_selection.Contains("dPhiDijetMet")) return true;
            if (N1_selection.Contains("dPhiDiCentralJets")) return true;
            if (N1_selection.Contains("met150")) return true;
        }

        return false;

    }

};

#endif