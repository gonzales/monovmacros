#ifndef ProgressBar_h
#define ProgressBar_h

class ProgressBar {

    public:

        int barWidth;
        int prevProgress;

        ProgressBar(int barWidth);

        virtual void print(float progress, TString message);

};


ProgressBar::ProgressBar(int bw) {
    barWidth = bw;
    prevProgress = -1;
}

#endif