#!/bin/bash

config_file=../conf/configuration_Feb2020_full.conf

for d in $(cat ../torun.txt); do

	N=$(($(ps ux | grep "root -l -b -q ExtractPlotsReader.C" | wc -l)-1))
	
	while [ $N -gt 1 ]; do
		sleep 10
		N=$(($(ps ux | grep "root -l -b -q ExtractPlotsReader.C" | wc -l)-1))
	done
	echo "Launching ${d}"

	root -l -b -q "ExtractPlotsReader.C(\"${config_file}\",{\"${d}\"},\"\",{},1)" >& tmp/LOG/PlottingReader/${d}.log &
	sleep 10
done

