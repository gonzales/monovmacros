#ifndef MergeJob_h
#define MergeJob_h

#include <map>
#include <TNamed.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <string>
#include <sstream>
#include "methods.h"

using namespace std;

class MergeJob : public TNamed {
    private:
    TString channel_name;
    map<UInt_t, Double_t> map_dsid_toMCWeight_mc16a;
    map<UInt_t, Double_t> map_dsid_toMCWeight_mc16d;
    map<UInt_t, Double_t> map_dsid_toMCWeight_mc16e;
    map<UInt_t, UInt_t> map_dsid_sumRawEvents;
    TString out_fileName;
    TString out_folder;
    TString out_plots_folder;
    TString out_tables_folder;
    TString out_tree_name;
    vector<TString> selections;
    vector<TString> N1_selections;
    TString trim_selection;
    TString label;
    bool isData;
    bool useProof;
    bool isMerging;
    bool isPlotting;
    bool isCutflow;
    bool doTables;
    bool doN1Plots;
    bool readingMergedFile;
    bool doTurnOnPlots;
    bool checkRepeatedEvents;
    int skimLevel;

    public:
    MergeJob(const char *s = "mergeJob") : TNamed(s,s), channel_name("EMPTY"), out_fileName(""), out_folder(""), out_plots_folder(""), out_tables_folder(""), out_tree_name(""), trim_selection("EMPTY"), isData(kFALSE), useProof(kTRUE), isMerging(kTRUE), isPlotting(kFALSE), isCutflow(kFALSE), doTables(kFALSE), doN1Plots(kFALSE), readingMergedFile(kFALSE), label(""), doTurnOnPlots(kFALSE), checkRepeatedEvents(kFALSE), skimLevel(0) {}

    virtual ~MergeJob() {}

    void SetChannelName(TString _channel_name) {
        channel_name = _channel_name;
    }

    TString GetChannelName() {
        return channel_name;
    }

    void SetLabel(TString _label) {
        label = _label;
    }

    TString GetLabel() {
        return label;
    }

    void SetOutputFileName(TString _out_fileName) {
        out_fileName = _out_fileName;
    }

    TString GetOutputFile() {
        return out_fileName;
    }

    void SetOutputFolder(TString _out_folder) {
        makeDirectory(_out_folder);
        out_folder = _out_folder;
    }

    TString GetOutputFolder() {
        return out_folder;
    }

    void SetOutputPlotsFolder(TString _out_plots_folder) {
        out_plots_folder = _out_plots_folder;
    }

    TString GetOutputsPlotFolder() {
        return out_plots_folder;
    }

    void SetOutputTablesFolder(TString _out_tables_folder) {
        out_tables_folder = _out_tables_folder;
    }

    TString GetOutputTablesFolder() {
        return out_tables_folder;
    }

    void SetOutputTreeName(TString _out_tree_name) {
        out_tree_name = _out_tree_name;
    }

    TString GetOutputTreeName() {
        return out_tree_name;
    }

    void SetSelections(vector<TString> _selections) {
        selections = _selections;
    }

    vector<TString> GetSelections(){
        return selections;
    }

    void SetN1Selections(vector<TString> _N1_selections) {
        N1_selections = _N1_selections;
    }

    vector<TString> GetN1Selections(){
        return N1_selections;
    }

    void SetTrimSelection(TString _trim_selection) {
        trim_selection = _trim_selection;
    }

    TString GetTrimSelection() {
        return trim_selection;
    }

    void SetIsData(bool _isData) {
        isData = _isData;
    }

    bool GetIsData() {
        return isData;
    }

    void SetUseProof(bool _useProof) {
        useProof = _useProof;
    }

    bool GetUsePROOF() {
        return useProof;
    }

    void SetIsMerging(bool _isMerging) {
        isMerging = _isMerging;
    }

    bool GetIsMerging() {
        return isMerging;
    }
    
    void SetIsPlotting(bool _isPlotting) {
        isPlotting = _isPlotting;
    }

    bool GetIsPlotting() {
        return isPlotting;
    }

    void SetIsCutflow(bool _isCutflow) {
        isCutflow = _isCutflow;
    }

    bool GetIsCutflow() {
        return isCutflow;
    }

    void SetDoTables(bool _doTables) {
        doTables = _doTables;
    }

    bool GetDoTables() {
        return doTables;
    }

    void SetDoN1Plots(bool _doN1Plots) {
        doN1Plots = _doN1Plots;
    }

    bool GetDoN1Plots() {
        return doN1Plots;
    }

    void SetReadingMergedFile(bool _readingMergedFile) {
        readingMergedFile = _readingMergedFile;
    }

    bool GetReadingMergedFile() {
        return readingMergedFile;
    }

    void AddMCWeight(UInt_t dsid, Double_t mc_weight, int campaign_index) {
        if (campaign_index == 0 && map_dsid_toMCWeight_mc16a.count(dsid) == 0) map_dsid_toMCWeight_mc16a[dsid] = 0;
        if (campaign_index == 1 && map_dsid_toMCWeight_mc16d.count(dsid) == 0) map_dsid_toMCWeight_mc16d[dsid] = 0;
        if (campaign_index == 2 && map_dsid_toMCWeight_mc16e.count(dsid) == 0) map_dsid_toMCWeight_mc16e[dsid] = 0;

        if (campaign_index == 0) map_dsid_toMCWeight_mc16a[dsid] += mc_weight;
        if (campaign_index == 1) map_dsid_toMCWeight_mc16d[dsid] += mc_weight;
        if (campaign_index == 2) map_dsid_toMCWeight_mc16e[dsid] += mc_weight;
    }

    Float_t GetMCWeight(int dsid, int campaign_index) {
        if (campaign_index == 0 && map_dsid_toMCWeight_mc16a.count(dsid) == 0) {coutError("MergeJob::GetMCWeight  No MCWeights defined for " + to_string(dsid)); return 1;}
        if (campaign_index == 1 && map_dsid_toMCWeight_mc16d.count(dsid) == 0) {coutError("MergeJob::GetMCWeight  No MCWeights defined for " + to_string(dsid)); return 1;}
        if (campaign_index == 2 && map_dsid_toMCWeight_mc16e.count(dsid) == 0) {coutError("MergeJob::GetMCWeight  No MCWeights defined for " + to_string(dsid)); return 1;}

        if (campaign_index == 0) return map_dsid_toMCWeight_mc16a[dsid];
        if (campaign_index == 1) return map_dsid_toMCWeight_mc16d[dsid];
        if (campaign_index == 2) return map_dsid_toMCWeight_mc16e[dsid];

        coutError("GetMCWeight -- Not a valid campaign index " + to_string(campaign_index));
        return 1;
    }

    void AddRawEvents(UInt_t dsid, UInt_t rawEvnts) {
        if (map_dsid_sumRawEvents.count(dsid) == 0) map_dsid_sumRawEvents[dsid] = 0;
        map_dsid_sumRawEvents[dsid] += rawEvnts;
    }

    UInt_t GetRawEvents(UInt_t dsid) {
        return map_dsid_sumRawEvents[dsid];
    }

    void ResetRawEvents() {
        for (auto kv : map_dsid_sumRawEvents) map_dsid_sumRawEvents[kv.first] = 0;
    }

    void SetDoTurnOnPlots(bool _doTurnOnPlots) {
        doTurnOnPlots = _doTurnOnPlots;
    }

    bool GetDoTurnOnPlots() {
        return doTurnOnPlots;
    }

    void SetCheckRepeatedEvents(bool _checkRepeatedEvents) {
        checkRepeatedEvents = _checkRepeatedEvents;
    }

    bool GetCheckRepeatedEvents() {
        return checkRepeatedEvents;
    }

    void SetSkimLevel(int _skimLevel) {
        skimLevel = _skimLevel;
    }

    int GetSkimLevel() {
        return skimLevel;
    }

    ClassDef(MergeJob, 1)

};

#endif
