#ifndef methods_h
#define methods_h

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <sys/stat.h>
#include <TMath.h>
#include <TSystemDirectory.h>
#include <TSystemFile.h>
#include <TPRegexp.h>
#include <TLegend.h>
#include <THStack.h>
#include <TGraph.h>
#include <TLine.h>
#include <TArrow.h>
#include <TObjString.h>
#include <TColor.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TSystem.h>
#include <TString.h>
#include <TDSet.h>
#include <TProof.h>
#include <numeric>      // std::iota
#include <algorithm>    // std::sort, std::stable_sort

using namespace std;

Double_t GeV = 1000;
double DegToRad = TMath::Pi() / 180.;
int GB = 1000;

/* Print error in color red */
void coutError(TString error) {
    std::cout << "\033[1;31m" << error << "\033[0m" << std::endl;
}

/* Returns as a string the output of a shell command */
string getCMDoutput(TString command) {
    string result, file;
    FILE* pipe{popen(command, "r")};
    char buffer[512];

    while(fgets(buffer, sizeof(buffer), pipe) != NULL)
    {
        file = buffer;
        result += file.substr(0, file.size() - 1);
    }

    pclose(pipe);
    return result;
}

int getNFiles(TString path) {
    TString command = "ls -l " + path + " | wc -l";
    int n = stoi(getCMDoutput(command.Data())) -1;
    return n;
}

/* Returns as a vector of strings a list of the folders in a given path */
vector<TString> getFilesList(TString path) {
    TSystemDirectory dir(path,path);
    TList * files = dir.GetListOfFiles();

    TSystemFile * file;
    TIter nxt(files);

    vector<TString> output;

    while ((file=(TSystemFile*)nxt())) {
        TString fileName = file->GetName();
        if (fileName == "." || fileName == "..") continue;
        output.push_back(fileName);
    }

    return output;
}

/* Retrieves the DSID for a given sample name */
int getDSID(TString job_folder) {
    TPRegexp re("mc16_13TeV\\.(\\d\\d\\d\\d\\d\\d)");
    if (job_folder.Contains("leadImage")) {
        TPRegexp re2("leadImage_(\\d\\d\\d\\d\\d\\d)");
        re = re2;
    }

    TObjArray *sub = re.MatchS(job_folder);

    if (sub->Last() == nullptr) {
        coutError("Unable to extract DSID from " + job_folder);
        return 0;
    } else {
        TString dsid = ((TObjString *)sub->At(1))->GetString();
        return dsid.Atoi();
    }

    return 0;
}

int makeDataDSID(TString job_folder) {
    TPRegexp re("data(\\d\\d)_13TeV");
    TPRegexp re2("period([A-Z])");
    TObjArray *sub = re.MatchS(job_folder);
    TObjArray *sub2 = re2.MatchS(job_folder);

    if (sub->Last() == nullptr) {
        coutError("Unable to make data DSID from " + job_folder);
        return 0;
    } else {
        TString year = ((TObjString *)sub->At(1))->GetString();
        TString period = ((TObjString *)sub2->At(1))->GetString();
        int period_number = (int)period[0];
        TString concat = year + to_string(period_number);
        return concat.Atoi();
    }

    return 0;
}

/* Creates a folder */
void makeDirectory(TString dir) {
    TString command = "mkdir -p " + dir;
    system(command);
}

/* Checks if a file exists */
bool fileExists(const TString name) {
  struct stat buffer;   
  return (stat (name, &buffer) == 0); 
}

/* Get value from AMI. Needs "lsetup pyami" before running */
double getAMIValue(string info, string dataset) {
    makeDirectory("AMIDatasets");
    if (!fileExists("AMIDatasets/dataset_info_" + dataset + ".txt")) {
        TString command = "ami show dataset info " + dataset +" > AMIDatasets/dataset_info_" + dataset + ".txt";
        system(command);
    }
    string query =  "awk \'/"+ info +" /{print $3}\' AMIDatasets/dataset_info_" + dataset + ".txt";
    string result = getCMDoutput(query);
    double value = 1.;
    if (result.size() == 0 || result == "NULL") {
        std::cout << "\033[1;31mThe value for " << info << " was not found. Using 1 instead!!!\033[0m" << std::endl; 
        result = "1";
    }
    value = stod(result);
    return value;
}

bool eventRemovedFromALPsMerging(int dsid, Float_t met_truth_et) {
    if ((dsid == 312410 || dsid == 312416) && (met_truth_et < 400*GeV || met_truth_et > 500*GeV)) return true; // 150 GeV
    if ((dsid == 312411 || dsid == 312417) && (met_truth_et < 500*GeV || met_truth_et > 650*GeV)) return true; // 250 GeV
    if ((dsid == 312412 || dsid == 312418) && (met_truth_et < 650*GeV || met_truth_et > 850*GeV)) return true; // 400 GeV
    if ((dsid == 312413 || dsid == 312419) && (met_truth_et < 850*GeV || met_truth_et > 1050*GeV)) return true; // 600 GeV
    if ((dsid == 312414 || dsid == 312420) && (met_truth_et < 1050*GeV || met_truth_et > 1250*GeV )) return true; // 800 GeV
    if ((dsid == 312415 || dsid == 312421) && (met_truth_et < 1250*GeV)) return true; // 1000 GeV

    return false;
}

template <typename T>
vector<size_t> sort_indexes(const vector<T> &v) {

  vector<size_t> idx(v.size());
  iota(idx.begin(), idx.end(), 0);

  stable_sort(idx.begin(), idx.end(),
       [&v](size_t i1, size_t i2) {return v[i1] < v[i2];});

  return idx;
}

template <typename T>
void sortTwoLists(vector<long double> & list_base, vector<T> & other_list){

    vector<long double> sorted_1;
    vector<T> sorted_2;

    for (auto i: sort_indexes(list_base)) {
        sorted_1.push_back(list_base[i]);
        sorted_2.push_back(other_list[i]);
    }

    // for (unsigned int i = 0 ; i < list_base.size() ; i++) {
    //     sorted_1.push_back(list_base.at(i));
    // }

    // stable_sort(sorted_1.begin(), sorted_1.end()); 

    // for (unsigned int i = 0 ; i < sorted_1.size() ; i++) {
    //     for (unsigned int j = 0 ; j < sorted_1.size() ; j++) {
    //         if (sorted_1.at(i) == list_base.at(j)) {
    //             sorted_2.push_back(other_list.at(j));
    //             break;
    //         }
    //     }
    // }

    list_base = sorted_1;
    other_list = sorted_2;

}

template <typename T>
int indexOf(T value, vector<T> v) {
    if (!contains(value,v)) return -1;
    return find(v.begin(),v.end(),value) - v.begin();
}

template <typename T>
bool contains(T value, vector<T> v) {
    return find(v.begin(),v.end(),value) < v.end();
}

// bool contains(TString value, vector<TString> v) {
//     return find(v.begin(),v.end(),value) < v.end();
// }

double *HSVtoRGB(double h)
{
    float fR = 0., fG = 0., fB = 0.;
    float fV = 1., fS = 1.;
    double *rgb = new double[3];
    float fC = fV * fS; // Chroma
    float fHPrime = fmod(h / 60.0, 6);
    float fX = fC * (1 - fabs(fmod(fHPrime, 2) - 1));
    float fM = fV - fC;

    if (0 <= fHPrime && fHPrime < 1)
    {
        fR = fC;
        fG = fX;
        fB = 0;
    }
    else if (1 <= fHPrime && fHPrime < 2)
    {
        fR = fX;
        fG = fC;
        fB = 0;
    }
    else if (2 <= fHPrime && fHPrime < 3)
    {
        fR = 0;
        fG = fC;
        fB = fX;
    }
    else if (3 <= fHPrime && fHPrime < 4)
    {
        fR = 0;
        fG = fX;
        fB = fC;
    }
    else if (4 <= fHPrime && fHPrime < 5)
    {
        fR = fX;
        fG = 0;
        fB = fC;
    }
    else if (5 <= fHPrime && fHPrime < 6)
    {
        fR = fC;
        fG = 0;
        fB = fX;
    }
    else
    {
        fR = 0;
        fG = 0;
        fB = 0;
    }

    rgb[0] = fR + fM;
    rgb[1] = fG + fM;
    rgb[2] = fB + fM;

    rgb[1] = rgb[0]>0.75 && rgb[1]>0.75 ? rgb[1] - 0.25 : rgb[1];  // Avoid some yellow colors

    return rgb;
}

// Returns a list of N colors from Red to Blue
vector<int> getColors(int N, int direction = 1) {
    if (N == 0) return {kBlue};
    vector<int> colors;
    if (direction == 1) {
        for (int j = 0 ; j < N + 1 ; j++){
            double hue = 240. * (double)j / (double)N;
            double *rgb = HSVtoRGB(hue);
            int ci = TColor::GetFreeColorIndex();
            TColor *c = new TColor(ci, rgb[0], rgb[1], rgb[2]);
            colors.push_back(ci);
        }
    } else {
        for (int j = N ; j >= 0 ; j--){
            double hue = 240. * (double)j / (double)N;
            double *rgb = HSVtoRGB(hue);
            int ci = TColor::GetFreeColorIndex();
            TColor *c = new TColor(ci, rgb[0], rgb[1], rgb[2]);
            colors.push_back(ci);
        }
    }
    return colors;
}

string to_string_with_precision(double num, const int n = 6)
{
    ostringstream out;
    out.precision(n);
    out << fixed << num;
    return out.str();
}

vector<TString> getTokens(TString line, TString delim)
{
   vector<TString> vtokens;
   TObjArray* tokens = TString(line).Tokenize(delim); //delimiters
   if (tokens->GetEntriesFast()) {
      TIter iString(tokens);
      TObjString* os = 0;
      while ((os = (TObjString*)iString())) {
         vtokens.push_back(os->GetString().Data());
      }
   }
   delete tokens;

   return vtokens;
}

Double_t DeltaPhi(double phi1, double phi2) {
    double dphi = fabs(phi1 - phi2);
    if (dphi > TMath::Pi()) dphi = 2*TMath::Pi() - dphi;
    return dphi;
}

Double_t DeltaR(double eta1, double eta2, double phi1, double phi2) {
    double deltaEta = eta1 - eta2;
    double deltaPhi = DeltaPhi(phi1, phi2);
    double deltaR = TMath::Sqrt(deltaEta*deltaEta + deltaPhi*deltaPhi);
    return deltaR;
}

// Produce a LaTEX table
void writeTable(TString fileName, TString caption, vector<vector<TString>> headers, vector<vector<TString>> rows)
{
    ofstream outfile(fileName + ".tex");


    outfile << "\\begin{tabular}{";
    for(auto header : headers.at(0)) {
        if (header == "|") outfile << "|";
        else {
            if (header.Contains("multicolumn")) {
                int c_head = stoi(header((TRegexp)"[0-9]+"));
                for (int i_c_head = 0 ; i_c_head < c_head ; i_c_head++) outfile << "l";
            } else {
                outfile << "l";
            }
        }
    }
    outfile << "}" << std::endl;
    
    outfile << "    \\toprule" << std::endl;

    outfile << "    ";
    for (vector<TString> headers_row : headers) {
        for (unsigned int i = 0 ; i < headers_row.size() ; i++) {
            if (headers_row[i] == "|") continue;
            if (!headers_row[i].Contains("$")) headers_row[i].ReplaceAll("_","\\_");
            outfile << " " << headers_row[i] << " ";
            if (i < headers_row.size() - 1) outfile << "&";
            else outfile << " \\\\" << std::endl;
        }
    }
    
    outfile << "    \\midrule" << std::endl;

    for (unsigned int i = 0 ; i < rows.size() ; i++) {
        outfile << "    ";
        for (unsigned int j = 0; j < rows[i].size() ; j++) {
            rows[i][j].ReplaceAll("#","\\");
            if (!rows[i][j].Contains("$")) {
                rows[i][j].ReplaceAll("_","\\_");
                rows[i][j].ReplaceAll("\\leq","$\\leq$");
                rows[i][j].ReplaceAll("<","$<$");
                rows[i][j].ReplaceAll(">","$>$");
            }
            outfile << " " << rows[i][j] << " ";
            if (j < rows[i].size() - 1) outfile << "&";
            else if (rows[i][j] != "\\midrule") outfile << "\\\\" << std::endl;
            else outfile << std::endl;
        }
    }

    outfile << "    \\toprule  " << std::endl;
    outfile << "\\end{tabular}" << std::endl;
    outfile << std::endl;
    outfile << std::endl;

    std::cout << "Table written in " << fileName + ".tex" << std::endl;
}

void writeTable(TString fileName, TString caption, vector<TString> headers, vector<vector<TString>> rows) {
    writeTable(fileName, caption, (vector<vector<TString>>){headers}, rows);
}

Double_t get_file_size(TString filePath) // path to file
{
    FILE *p_file = NULL;
    p_file = fopen(filePath.Data(),"rb");
    fseek(p_file,0,SEEK_END);
    Double_t size = ftell(p_file);
    fclose(p_file);
    return size*1e-6; // In MB
}

Double_t get_folder_size(TString folderPath)
{
    vector<TString> fileList = getFilesList(folderPath);
    Double_t size = 0;
    for (TString filePath : fileList) size += get_file_size(folderPath + "/" + filePath);
    return size; // In MB
}

int getSizeThreshold(Double_t expected_size) {
    Double_t sizeThreshold = 50*GB; 
    if (expected_size > 50*GB) sizeThreshold = ceil(expected_size / 3.)+1*GB;
    if (expected_size > 100*GB) sizeThreshold = 25*GB;
    return sizeThreshold;
}

void haddFiles(TString outputFile, TString files_for_hadd) {
    TString command = "hadd -f " + outputFile + files_for_hadd;
    gSystem->Exec(command);
}

// Hadding files from subpartsFolder.
// If the size of the subparts folder is greater than 100GB, hadd into 25GB files
// If the size of the subparts folder is between 50GB and 100GB, hadd into two files with size not greater than 50GB files
// Otherwise, hadd all files
void haddFiles(TString subpartsFolder, TString output_folder, TString channel_name) 
{
    
    vector<TString> fileList = getFilesList(subpartsFolder);
    Double_t folderSize = get_folder_size(subpartsFolder);
    std::cout << "The subparts folder has size " << folderSize/GB << " GB" << std::endl;

    Double_t sizeThreshold = getSizeThreshold(folderSize); 

    std::cout << "Setting a size threshold of " << sizeThreshold/GB << "GB" << std::endl;

    TString files_for_hadd = "";
    Double_t currentSize = 0;
    int haddedFiles = 0;
    for (unsigned int i_file = 0 ; i_file < fileList.size() ; i_file++) {
        TString subFile = fileList.at(i_file);
        Double_t fileSize = get_file_size(subpartsFolder + "/" + subFile);
        if (currentSize + fileSize > sizeThreshold || i_file == fileList.size() - 1) {
            haddedFiles++;
            if (i_file == fileList.size() - 1) files_for_hadd += " " + subpartsFolder + "/" + subFile; // adding to hadd list in case we are at the end of the fileList
            TString haddedFileName = output_folder + "/" + channel_name; // Preparing hadd file name
            if (i_file != fileList.size() - 1) haddedFileName += "_p" + to_string(haddedFiles); // adding p#{number} to the file name in case we are going to hadd multiple times
            else if (haddedFiles > 1) haddedFileName += "_p" + to_string(haddedFiles); 
            haddedFileName += ".root",
            std::cout << "-------------------------------------------------------" << std::endl;
            std::cout << "Hadding..." << std::endl;
            TString command = "hadd -f " + haddedFileName + files_for_hadd;
            gSystem->Exec(command);
            currentSize = fileSize;
            files_for_hadd = "";
        } else {
            currentSize += fileSize;
            files_for_hadd += " " + subpartsFolder + "/" + subFile;
        }
    } 
}

Int_t Get2DBin(TH2D * hist, double x, double y) {
    TAxis *xaxis = hist->GetXaxis();
    TAxis *yaxis = hist->GetYaxis();
    Int_t binx = xaxis->FindBin(x);
    Int_t biny = yaxis->FindBin(y);
    return hist->GetBin(binx,biny);
}


string makeChannelsPathDict(TString dictName, vector<TString> channels, vector<TString> paths_to_root_folder) {

    stringstream channel_paths_definition;
    channel_paths_definition << dictName << " = {" << std::endl;
    for (TString path_to_root_folder : paths_to_root_folder) {
        vector<TString> mergedFiles = getFilesList(path_to_root_folder);

        for (TString channel_name : channels) {
            vector<TString> channel_paths;

            for (TString mergedFile : mergedFiles) {
                if (!mergedFile.Contains(channel_name)) continue;
                if (!mergedFile.Contains(".root")) continue;
                channel_paths.push_back(path_to_root_folder + "/" + mergedFile);
            }

            if (channel_paths.size() > 0) {
                channel_paths_definition << "\t\t'" << channel_name << "':[";
                for (TString path_to_root : channel_paths) channel_paths_definition << "'" << path_to_root << "',";
                channel_paths_definition << "]," << std::endl;
            }

        } // Loop channels
    }
    channel_paths_definition << "}" << std::endl;

    return channel_paths_definition.str();
}

int getYear(int year, int run) {

    if ((year == 0 && run >= 276262 && run <= 284484) || year == 2015) return 2015; // data2015
    if ((year == 0 && run >  284484 && run <= 311481) || year == 2016) return 2016; // data2016
    if ((year == 0 && run >= 325713 && run <= 341649) || year == 2017) return 2017; // data2017
    if ((year == 0 && run >= 348885 && run <= 364292) || year == 2018) return 2018; // data2018

    coutError("The year could not be extracted!");
    return -1;

}

double GetMedian(TH1D * hist) {
    double q[1];
    double probs[1] = {0.5};
    hist->GetQuantiles(1,q,probs);
    return q[0];
}

double GetCampaignLumi(TString campaign) {
    double lumi = -1;
    if (campaign == "mc16a") lumi = 36.2077058326;
    if (campaign == "mc16d") lumi = 44.30722199232813;
    if (campaign == "mc16e") lumi = 58.45026027800002;
    if (campaign == "full")  lumi = 139.0;
    return lumi;
}

template <typename T>
pair<T, T> GetMeanErr(vector<T> v) {
    if (v.size() == 0) return make_pair(0,0);

    T sum = accumulate(v.begin(), v.end(), 0.0);
    T mean = sum / v.size();

    T sq_sum = 0;
    for (auto val : v) {
        sq_sum += (val - mean)*(val - mean);
    }
    T err = sqrt(sq_sum/(v.size() - 1))/sqrt(v.size());

    mean = !isnan(mean) ? mean : -999;
    err = !isnan(err) ? err : -999;

    return make_pair(mean,err);
}

#endif
