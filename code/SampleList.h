#ifndef SampleList_h
#define SampleList_h

#include <TString.h>
#include <map>
#include "methods.h"


class SampleList {

    public:

    static map<TString,vector<int>> MC_samples() {
        return  {
                                                {"diboson",{345043, 345044, 345045, 363355, 363356, 363357, 363358, 363359, 363360, 363489}},
                                                {"VH",{345053, 345054, 345055, 345056, 345057, 345058}},
                                                {"jetjet",{361020, 361021, 361022, 361023, 361024, 361025, 361026, 361027, 361028, 361029, 361030, 361031, 361032}},
                                                {"Zmumu",{364100, 364101, 364102, 364103, 364104, 364105, 364106, 364107, 364108, 364109, 364110, 364111, 364112, 364113}},
                                                {"Zee",{364114, 364115, 364116, 364117, 364118, 364119, 364120, 364121, 364122, 364123, 364124, 364125, 364126, 364127}},
                                                {"Ztautau",{364128, 364129, 364130, 364131, 364132, 364133, 364134, 364135, 364136, 364137, 364138, 364139, 364140, 364141}},
                                                {"Znunu",{364142, 364143, 364144, 364145, 364146, 364147, 364148, 364149, 364150, 364151, 364152, 364153, 364154, 364155}},
                                                {"Wmunu",{364156, 364157, 364158, 364159, 364160, 364161, 364162, 364163, 364164, 364165, 364166, 364167, 364168, 364169}},
                                                {"Wenu",{364170, 364171, 364172, 364173, 364174, 364175, 364176, 364177, 364178, 364179, 364180, 364181, 364182, 364183}},
                                                {"Wtaunu",{364184, 364185, 364186, 364187, 364188, 364189, 364190, 364191, 364192, 364193, 364194, 364195, 364196, 364197}},
                                                {"ggVV",{364302, 364303, 364304, 364305}},
                                                // {"SinglePhoton",{364543, 364544, 364545, 364546, 364547}},
                                                {"Znunu_PTV",{364222, 364223, 366010, 366011, 366012, 366013, 366014, 366015, 366016, 366017, 366019, 366020, 366021, 366022, 366023, 366024, 366025, 366026, 366028, 366029, 366030, 366031, 366032, 366033, 366034, 366035}},
                                                {"ttbar",{410470, 410471, 410472}},
                                                {"ttbar_wFilter",{345935, 407345, 407346, 407347}},
                                                {"singletop",{410644, 410645}},
                                                {"Wt_DR",{410646, 410647}},
                                                {"tchan",{410658, 410659}},
                                                {"ttV",{410157, 410155}},
                                                {"ttH",{346344}}
        };
    };

    static map<TString,vector<int>> ALPs_samples() {
        return  {
                                            {"ALPs_axW",{312410, 312411, 312412, 312413, 312414, 312415}},
                                            {"ALPs_axZ",{312416, 312417, 312418, 312419, 312420, 312421}},
        };
    };

    static map<TString,vector<int>> invH_samples() {

        return  {
                                            {"invH",{345596,346588,346600,346605,346606,346607,346632,346633,346634}},
        };
    };
    

    static map<TString,vector<int>> DM_samples() {
            return  {
                                            {"DM_Whad_DM1_MM500",{312422}},
                                            {"DM_Whad_DM1_MM1000",{312423}},
                                            {"DM_Whad_DM1_MM1500",{312424}},
                                            {"DM_Zhad_DM1_MM500",{312425}},
                                            {"DM_Zhad_DM1_MM1000",{312426}},
                                            {"DM_Zhad_DM1_MM1500",{312427}},
        };
    };

    static map<int,string> map_dsids() {
            return  {
                                    //diboson
                                    {345043,"mc16_13TeV.345043.Sherpa_221_NNPDF30NNLO_ZbbZvv.deriv.DAOD_EXOT27.e6470_s3126_r9364_p3990"}, // <-all
                                    {345044,"mc16_13TeV.345044.Sherpa_221_NNPDF30NNLO_ZbbZll.deriv.DAOD_EXOT27.e6470_s3126_r9364_p3990"},
                                    {345045,"mc16_13TeV.345045.Sherpa_221_NNPDF30NNLO_WlvZbb.deriv.DAOD_EXOT27.e6470_s3126_r9364_p3990"},
                                    {363355,"mc16_13TeV.363355.Sherpa_221_NNPDF30NNLO_ZqqZvv.deriv.DAOD_EXOT27.e5525_s3126_r9364_p3990"},
                                    {363356,"mc16_13TeV.363356.Sherpa_221_NNPDF30NNLO_ZqqZll.deriv.DAOD_EXOT27.e5525_s3126_r9364_p3990"},
                                    {363357,"mc16_13TeV.363357.Sherpa_221_NNPDF30NNLO_WqqZvv.deriv.DAOD_EXOT27.e5525_s3126_r9364_p3990"},
                                    {363358,"mc16_13TeV.363358.Sherpa_221_NNPDF30NNLO_WqqZll.deriv.DAOD_EXOT27.e5525_s3126_r9364_p3990"},
                                    {363359,"mc16_13TeV.363359.Sherpa_221_NNPDF30NNLO_WpqqWmlv.deriv.DAOD_EXOT27.e5583_s3126_r9364_p3990"},
                                    {363360,"mc16_13TeV.363360.Sherpa_221_NNPDF30NNLO_WplvWmqq.deriv.DAOD_EXOT27.e5983_s3126_r9364_p3990"},
                                    {363489,"mc16_13TeV.363489.Sherpa_221_NNPDF30NNLO_WlvZqq.deriv.DAOD_EXOT27.e5525_s3126_r9364_p3990"},
                                    //VH
                                    {345053,"mc16_13TeV.345053.PowhegPythia8EvtGen_NNPDF3_AZNLO_WmH125J_MINLO_lvbb_VpT.deriv.DAOD_EXOT27.e5706_s3126_r9364_p3990"}, // <-all
                                    {345054,"mc16_13TeV.345054.PowhegPythia8EvtGen_NNPDF3_AZNLO_WpH125J_MINLO_lvbb_VpT.deriv.DAOD_EXOT27.e5706_s3126_r9364_p3990"},
                                    {345055,"mc16_13TeV.345055.PowhegPythia8EvtGen_NNPDF3_AZNLO_ZH125J_MINLO_llbb_VpT.deriv.DAOD_EXOT27.e5706_s3126_r9364_p3990"},
                                    {345056,"mc16_13TeV.345056.PowhegPythia8EvtGen_NNPDF3_AZNLO_ZH125J_MINLO_vvbb_VpT.deriv.DAOD_EXOT27.e5706_s3126_r9364_p3990"},
                                    {345057,"mc16_13TeV.345057.PowhegPythia8EvtGen_NNPDF3_AZNLO_ggZH125_llbb.deriv.DAOD_EXOT27.e5706_s3126_r9364_p3990"},
                                    {345058,"mc16_13TeV.345058.PowhegPythia8EvtGen_NNPDF3_AZNLO_ggZH125_vvbb.deriv.DAOD_EXOT27.e6004_s3126_r9364_p3990"},
                                    //jetjet
                                    {361020,"mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.deriv.DAOD_EXOT27.e3569_s3126_r9364_p3990"},
                                    {361021,"mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.deriv.DAOD_EXOT27.e3569_s3126_r9364_p3990"},
                                    {361022,"mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.deriv.DAOD_EXOT27.e3668_s3126_r9364_p3990"},
                                    {361023,"mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.deriv.DAOD_EXOT27.e3668_s3126_r9364_p3990"},
                                    {361024,"mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.deriv.DAOD_EXOT27.e3668_s3126_r9364_p3990"},
                                    {361025,"mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.deriv.DAOD_EXOT27.e3668_s3126_r9364_p3990"},
                                    {361026,"mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.deriv.DAOD_EXOT27.e3569_s3126_r9364_p3990"},
                                    {361027,"mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.deriv.DAOD_EXOT27.e3668_s3126_r9364_p3990"},
                                    {361028,"mc16_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.deriv.DAOD_EXOT27.e3569_s3126_r9364_p3990"},
                                    {361029,"mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.deriv.DAOD_EXOT27.e3569_s3126_r9364_p3990"},
                                    {361030,"mc16_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.deriv.DAOD_EXOT27.e3569_s3126_r9364_p3990"},
                                    {361031,"mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.deriv.DAOD_EXOT27.e3569_s3126_r9364_p3990"},
                                    {361032,"mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.deriv.DAOD_EXOT27.e3668_s3126_r9364_p3990"},
                                    //Zmumu
                                    {364100,"mc16_13TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT27.e5271_s3126_r9364_p3990"}, // <--
                                    {364101,"mc16_13TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT27.e5271_s3126_r9364_p3990"}, // <--
                                    {364102,"mc16_13TeV.364102.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT27.e5271_s3126_r9364_p3990"},
                                    {364103,"mc16_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT27.e5271_s3126_r9364_p3990"},
                                    {364104,"mc16_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT27.e5271_s3126_r9364_p3990"},
                                    {364105,"mc16_13TeV.364105.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT27.e5271_s3126_r9364_p3990"},
                                    {364106,"mc16_13TeV.364106.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT27.e5271_s3126_r9364_p3990"},
                                    {364107,"mc16_13TeV.364107.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_EXOT27.e5271_s3126_r9364_p3990"},
                                    {364108,"mc16_13TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT27.e5271_s3126_r9364_p3990"},
                                    {364109,"mc16_13TeV.364109.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT27.e5271_s3126_r9364_p3990"},
                                    {364110,"mc16_13TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT27.e5271_s3126_r9364_p3990"},
                                    {364111,"mc16_13TeV.364111.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_BFilter.deriv.DAOD_EXOT27.e5271_s3126_r9364_p3990"},
                                    {364112,"mc16_13TeV.364112.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000.deriv.DAOD_EXOT27.e5271_s3126_r9364_p3990"},
                                    {364113,"mc16_13TeV.364113.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT27.e5271_s3126_r9364_p3990"},
                                    //Zee
                                    {364114,"mc16_13TeV.364114.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT27.e5299_s3126_r9364_p3990"},
                                    {364115,"mc16_13TeV.364115.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT27.e5299_s3126_r9364_p3990"},
                                    {364116,"mc16_13TeV.364116.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT27.e5299_s3126_r9364_p3990"},
                                    {364117,"mc16_13TeV.364117.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT27.e5299_s3126_r9364_p3990"},
                                    {364118,"mc16_13TeV.364118.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT27.e5299_s3126_r9364_p3990"},
                                    {364119,"mc16_13TeV.364119.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT27.e5299_s3126_r9364_p3990"},
                                    {364120,"mc16_13TeV.364120.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT27.e5299_s3126_r9364_p3990"},
                                    {364121,"mc16_13TeV.364121.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_EXOT27.e5299_s3126_r9364_p3990"},
                                    {364122,"mc16_13TeV.364122.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT27.e5299_s3126_r9364_p3990"},
                                    {364123,"mc16_13TeV.364123.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT27.e5299_s3126_r9364_p3990"},
                                    {364124,"mc16_13TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT27.e5299_s3126_r9364_p3990"},
                                    {364125,"mc16_13TeV.364125.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_BFilter.deriv.DAOD_EXOT27.e5299_s3126_r9364_p3990"},
                                    {364126,"mc16_13TeV.364126.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV500_1000.deriv.DAOD_EXOT27.e5299_s3126_r9364_p3990"},
                                    {364127,"mc16_13TeV.364127.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT27.e5299_s3126_r9364_p3990"},
                                    //Ztautau
                                    {364128,"mc16_13TeV.364128.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT27.e5307_s3126_r9364_p3990"},
                                    {364129,"mc16_13TeV.364129.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT27.e5307_s3126_r9364_p3990"},
                                    {364130,"mc16_13TeV.364130.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT27.e5307_s3126_r9364_p3990"},
                                    {364131,"mc16_13TeV.364131.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT27.e5307_s3126_r9364_p3990"},
                                    {364132,"mc16_13TeV.364132.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT27.e5307_s3126_r9364_p3990"},
                                    {364133,"mc16_13TeV.364133.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT27.e5307_s3126_r9364_p3990"},
                                    {364134,"mc16_13TeV.364134.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT27.e5307_s3126_r9364_p3990"},
                                    {364135,"mc16_13TeV.364135.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_EXOT27.e5307_s3126_r9364_p3990"},
                                    {364136,"mc16_13TeV.364136.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT27.e5307_s3126_r9364_p3990"},
                                    {364137,"mc16_13TeV.364137.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT27.e5307_s3126_r9364_p3990"},
                                    {364138,"mc16_13TeV.364138.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT27.e5313_s3126_r9364_p3990"},
                                    {364139,"mc16_13TeV.364139.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_BFilter.deriv.DAOD_EXOT27.e5313_s3126_r9364_p3990"},
                                    {364140,"mc16_13TeV.364140.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV500_1000.deriv.DAOD_EXOT27.e5307_s3126_r9364_p3990"},
                                    {364141,"mc16_13TeV.364141.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT27.e5307_s3126_r9364_p3990"},
                                    //Znunu
                                    {364142,"mc16_13TeV.364142.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT27.e5308_s3126_r9364_p3990"},
                                    {364143,"mc16_13TeV.364143.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT27.e5308_s3126_r9364_p3990"},
                                    {364144,"mc16_13TeV.364144.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT27.e5308_s3126_r9364_p3990"},
                                    {364145,"mc16_13TeV.364145.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT27.e5308_s3126_r9364_p3990"},
                                    {364146,"mc16_13TeV.364146.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT27.e5308_s3126_r9364_p3990"},
                                    {364147,"mc16_13TeV.364147.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT27.e5308_s3126_r9364_p3990"},
                                    {364148,"mc16_13TeV.364148.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT27.e5308_s3126_r9364_p3990"},
                                    {364149,"mc16_13TeV.364149.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_EXOT27.e5308_s3126_r9364_p3990"},
                                    {364150,"mc16_13TeV.364150.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT27.e5308_s3126_r9364_p3990"},
                                    {364151,"mc16_13TeV.364151.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT27.e5308_s3126_r9364_p3990"},
                                    {364152,"mc16_13TeV.364152.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT27.e5308_s3126_r9364_p3990"},
                                    {364153,"mc16_13TeV.364153.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV280_500_BFilter.deriv.DAOD_EXOT27.e5308_s3126_r9364_p3990"},
                                    {364154,"mc16_13TeV.364154.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV500_1000.deriv.DAOD_EXOT27.e5308_s3126_r9364_p3990"},
                                    {364155,"mc16_13TeV.364155.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT27.e5308_s3126_r9364_p3990"},
                                    //Wmunu
                                    {364156,"mc16_13TeV.364156.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364157,"mc16_13TeV.364157.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364158,"mc16_13TeV.364158.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364159,"mc16_13TeV.364159.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364160,"mc16_13TeV.364160.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364161,"mc16_13TeV.364161.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364162,"mc16_13TeV.364162.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364163,"mc16_13TeV.364163.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364164,"mc16_13TeV.364164.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364165,"mc16_13TeV.364165.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364166,"mc16_13TeV.364166.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364167,"mc16_13TeV.364167.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_BFilter.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364168,"mc16_13TeV.364168.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV500_1000.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364169,"mc16_13TeV.364169.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    //Wenu
                                    {364170,"mc16_13TeV.364170.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364171,"mc16_13TeV.364171.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364172,"mc16_13TeV.364172.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364173,"mc16_13TeV.364173.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364174,"mc16_13TeV.364174.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364175,"mc16_13TeV.364175.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364176,"mc16_13TeV.364176.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364177,"mc16_13TeV.364177.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364178,"mc16_13TeV.364178.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364179,"mc16_13TeV.364179.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364180,"mc16_13TeV.364180.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364181,"mc16_13TeV.364181.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_BFilter.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364182,"mc16_13TeV.364182.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV500_1000.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364183,"mc16_13TeV.364183.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    //Wtaunu
                                    {364184,"mc16_13TeV.364184.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364185,"mc16_13TeV.364185.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364186,"mc16_13TeV.364186.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_BFilter.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364187,"mc16_13TeV.364187.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364188,"mc16_13TeV.364188.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364189,"mc16_13TeV.364189.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_BFilter.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364190,"mc16_13TeV.364190.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364191,"mc16_13TeV.364191.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364192,"mc16_13TeV.364192.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_BFilter.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364193,"mc16_13TeV.364193.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364194,"mc16_13TeV.364194.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364195,"mc16_13TeV.364195.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_BFilter.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364196,"mc16_13TeV.364196.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV500_1000.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    {364197,"mc16_13TeV.364197.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV1000_E_CMS.deriv.DAOD_EXOT27.e5340_s3126_r9364_p3990"},
                                    //ggVV
                                    {364302,"mc16_13TeV.364302.Sherpa_222_NNPDF30NNLO_ggZllZqq.deriv.DAOD_EXOT27.e6273_s3126_r9364_p3990"},
                                    {364303,"mc16_13TeV.364303.Sherpa_222_NNPDF30NNLO_ggZvvZqq.deriv.DAOD_EXOT27.e6273_s3126_r9364_p3990"},
                                    {364304,"mc16_13TeV.364304.Sherpa_222_NNPDF30NNLO_ggWmlvWpqq.deriv.DAOD_EXOT27.e6273_s3126_r9364_p3990"},
                                    {364305,"mc16_13TeV.364305.Sherpa_222_NNPDF30NNLO_ggWplvWmqq.deriv.DAOD_EXOT27.e6273_s3126_r9364_p3990"},
                                    //SinglePhoton
                                    {364543,"mc16_13TeV.364543.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_70_140.deriv.DAOD_EXOT27.e5938_s3126_r9364_p3990"},
                                    {364544,"mc16_13TeV.364544.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_140_280.deriv.DAOD_EXOT27.e5938_s3126_r9364_p3990"},
                                    {364545,"mc16_13TeV.364545.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_280_500.deriv.DAOD_EXOT27.e5938_s3126_r9364_p3990"},
                                    {364546,"mc16_13TeV.364546.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_500_1000.deriv.DAOD_EXOT27.e5938_s3126_r9364_p3990"},
                                    {364547,"mc16_13TeV.364547.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_1000_E_CMS.deriv.DAOD_EXOT27.e6068_s3126_r9364_p3990"},
                                    //Znunu_PTV
                                    {364222,"mc16_13TeV.364222.Sherpa_221_NNPDF30NNLO_Znunu_PTV500_1000.deriv.DAOD_EXOT27.e5626_s3126_r9364_p3990"},
                                    {364223,"mc16_13TeV.364223.Sherpa_221_NNPDF30NNLO_Znunu_PTV1000_E_CMS.deriv.DAOD_EXOT27.e5626_s3126_r9364_p3990"},
                                    {366010,"mc16_13TeV.366010.Sh_221_NN30NNLO_Znunu_PTV70_100_BFilter.deriv.DAOD_EXOT27.e6695_s3126_r9364_p3990"},
                                    {366011,"mc16_13TeV.366011.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ0_500_BFilter.deriv.DAOD_EXOT27.e6695_s3126_r9364_p3990"},
                                    {366012,"mc16_13TeV.366012.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ500_1000_BFilter.deriv.DAOD_EXOT27.e6695_s3126_r9364_p3990"},
                                    {366013,"mc16_13TeV.366013.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ1000_E_CMS_BFilter.deriv.DAOD_EXOT27.e6695_s3126_r9364_p3990"},
                                    {366014,"mc16_13TeV.366014.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ0_500_BFilter.deriv.DAOD_EXOT27.e6695_s3126_r9364_p3990"},
                                    {366015,"mc16_13TeV.366015.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ500_1000_BFilter.deriv.DAOD_EXOT27.e6695_s3126_r9364_p3990"},
                                    {366016,"mc16_13TeV.366016.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ1000_E_CMS_BFilter.deriv.DAOD_EXOT27.e6695_s3126_r9364_p3990"},
                                    {366017,"mc16_13TeV.366017.Sh_221_NN30NNLO_Znunu_PTV280_500_BFilter.deriv.DAOD_EXOT27.e6695_s3126_r9364_p3990"},
                                    {366019,"mc16_13TeV.366019.Sh_221_NN30NNLO_Znunu_PTV70_100_CFilterBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    {366020,"mc16_13TeV.366020.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ0_500_CFilterBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    {366021,"mc16_13TeV.366021.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ500_1000_CFilterBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    {366022,"mc16_13TeV.366022.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ1000_E_CMS_CFilterBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    {366023,"mc16_13TeV.366023.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ0_500_CFilterBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    {366024,"mc16_13TeV.366024.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ500_1000_CFilterBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    {366025,"mc16_13TeV.366025.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ1000_E_CMS_CFilterBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    {366026,"mc16_13TeV.366026.Sh_221_NN30NNLO_Znunu_PTV280_500_CFilterBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    {366028,"mc16_13TeV.366028.Sh_221_NN30NNLO_Znunu_PTV70_100_CVetoBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    {366029,"mc16_13TeV.366029.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ0_500_CVetoBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    {366030,"mc16_13TeV.366030.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ500_1000_CVetoBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    {366031,"mc16_13TeV.366031.Sh_221_NN30NNLO_Znunu_PTV100_140_MJJ1000_E_CMS_CVetoBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    {366032,"mc16_13TeV.366032.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ0_500_CVetoBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    {366033,"mc16_13TeV.366033.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ500_1000_CVetoBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    {366034,"mc16_13TeV.366034.Sh_221_NN30NNLO_Znunu_PTV140_280_MJJ1000_E_CMS_CVetoBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    {366035,"mc16_13TeV.366035.Sh_221_NN30NNLO_Znunu_PTV280_500_CVetoBVeto.deriv.DAOD_EXOT27.e7033_s3126_r9364_p3990"},
                                    //ttbar
                                    {410470,"mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_EXOT27.e6337_s3126_r9364_p3990"},
                                    {410471,"mc16_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_EXOT27.e6337_s3126_r9364_p3840"},
                                    {410472,"mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_EXOT27.e6348_s3126_r9364_p3990"},
                                    //ttbar_wFilter
                                    {345935,"mc16_13TeV.345935.PhPy8EG_A14_ttbarMET100_200_hdamp258p75_nonallhad.deriv.DAOD_EXOT27.e6620_s3126_r9364_p3990"}, // <--
                                    {407345,"mc16_13TeV.407345.PhPy8EG_A14_ttbarMET200_300_hdamp258p75_nonallhad.deriv.DAOD_EXOT27.e6414_s3126_r9364_p3990"},
                                    {407346,"mc16_13TeV.407346.PhPy8EG_A14_ttbarMET300_400_hdamp258p75_nonallhad.deriv.DAOD_EXOT27.e6414_s3126_r9364_p3990"},
                                    {407347,"mc16_13TeV.407347.PhPy8EG_A14_ttbarMET400_hdamp258p75_nonallhad.deriv.DAOD_EXOT27.e6414_s3126_r9364_p3990"},
                                    //singletop
                                    {410644,"mc16_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_EXOT27.e6527_s3126_r9364_p3990"},
                                    {410645,"mc16_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_EXOT27.e6527_s3126_r9364_p3990"},
                                    //Wt_DR
                                    {410646,"mc16_13TeV.410646.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_top.deriv.DAOD_EXOT27.e6552_s3126_r9364_p3990"},
                                    {410647,"mc16_13TeV.410647.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_antitop.deriv.DAOD_EXOT27.e6552_s3126_r9364_p3990"},
                                    //tchan
                                    {410658,"mc16_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_EXOT27.e6671_s3126_r9364_p3990"},
                                    {410659,"mc16_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_EXOT27.e6671_s3126_r9364_p3990"},
                                    //ttV
                                    {410157,"mc16_13TeV.410157.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttZqq.deriv.DAOD_EXOT27.e5070_s3126_r9364_p3990"},
                                    {410155,"mc16_13TeV.410155.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttW.deriv.DAOD_EXOT27.e5070_s3126_r9364_p3990"},
                                    //ttH
                                    {346344,"mc16_13TeV.346344.PhPy8EG_A14NNPDF23_NNPDF30ME_ttH125_semilep.deriv.DAOD_EXOT27.e7148_s3126_r9364_p3990"},
                                    //ALPs_axW
                                    {312410,"mc16_13TeV.312410.MGPy8EG_A14NNPDF23LO_axW_Ma1_CWtil1_fa1000_MET150.recon.AOD.e7681_s3126_r9364"},
                                    {312411,"mc16_13TeV.312411.MGPy8EG_A14NNPDF23LO_axW_Ma1_CWtil1_fa1000_MET250.recon.AOD.e7681_s3126_r9364"},
                                    {312412,"mc16_13TeV.312412.MGPy8EG_A14NNPDF23LO_axW_Ma1_CWtil1_fa1000_MET400.recon.AOD.e7681_s3126_r9364"},
                                    {312413,"mc16_13TeV.312413.MGPy8EG_A14NNPDF23LO_axW_Ma1_CWtil1_fa1000_MET600.recon.AOD.e7681_s3126_r9364"},
                                    {312414,"mc16_13TeV.312414.MGPy8EG_A14NNPDF23LO_axW_Ma1_CWtil1_fa1000_MET800.recon.AOD.e7681_s3126_r9364"},
                                    {312415,"mc16_13TeV.312415.MGPy8EG_A14NNPDF23LO_axW_Ma1_CWtil1_fa1000_MET1000.recon.AOD.e7681_s3126_r9364"},
                                    //ALPs_axZ
                                    {312416,"mc16_13TeV.312416.MGPy8EG_A14NNPDF23LO_axZ_Ma1_CWtil1_fa1000_MET150.recon.AOD.e7681_s3126_r9364"},
                                    {312417,"mc16_13TeV.312417.MGPy8EG_A14NNPDF23LO_axZ_Ma1_CWtil1_fa1000_MET250.recon.AOD.e7681_s3126_r9364"},
                                    {312418,"mc16_13TeV.312418.MGPy8EG_A14NNPDF23LO_axZ_Ma1_CWtil1_fa1000_MET400.recon.AOD.e7681_s3126_r9364"},
                                    {312419,"mc16_13TeV.312419.MGPy8EG_A14NNPDF23LO_axZ_Ma1_CWtil1_fa1000_MET600.recon.AOD.e7681_s3126_r9364"},
                                    {312420,"mc16_13TeV.312420.MGPy8EG_A14NNPDF23LO_axZ_Ma1_CWtil1_fa1000_MET800.recon.AOD.e7681_s3126_r9364"},
                                    {312421,"mc16_13TeV.312421.MGPy8EG_A14NNPDF23LO_axZ_Ma1_CWtil1_fa1000_MET1000.recon.AOD.e7681_s3126_r9364"},
                                    //invH
                                    {345596,"mc16_13TeV.345596.PowhegPythia8EvtGen_NNPDF3_AZNLO_ggZH125_Zinc_HZZinv.deriv.DAOD_EXOT5.e6025_s3126_r9364_p3712"},
                                    {346588,"mc16_13TeV.346588.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4nu_MET75.deriv.DAOD_EXOT5.e7628_s3126_r9364_p3712"},
                                    {346600,"mc16_13TeV.346600.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_ZZ4nu_MET75.deriv.DAOD_EXOT5.e7613_s3126_r9364_p3712"},
                                    {346605,"mc16_13TeV.346605.PowhegPythia8EvtGen_NNPDF30_AZNLO_WpH125J_WinclHinv_MET75.deriv.DAOD_EXOT5.e7641_s3126_r9364_p3712"},
                                    {346606,"mc16_13TeV.346606.PowhegPythia8EvtGen_NNPDF30_AZNLO_WmH125J_WinclHinv_MET75.deriv.DAOD_EXOT5.e7641_s3126_r9364_p3712"},
                                    {346607,"mc16_13TeV.346607.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_ZinclHinv_MET75.deriv.DAOD_EXOT5.e7641_s3126_r9364_p3712"},
                                    {346632,"mc16_13TeV.346632.PowhegPy8EG_A14NNPDF23_NNPDF30ME_ttH125_ZZ4nu_semilep.deriv.DAOD_EXOT5.e7667_s3126_r9364_p3895"},
                                    {346633,"mc16_13TeV.346633.PowhegPy8EG_A14NNPDF23_NNPDF30ME_ttH125_ZZ4nu_allhad.deriv.DAOD_EXOT5.e7629_s3126_r9364_p3895"},
                                    {346634,"mc16_13TeV.346634.PowhegPy8EG_A14NNPDF23_NNPDF30ME_ttH125_ZZ4nu_dilep.deriv.DAOD_EXOT5.e7629_s3126_r9364_p3895"},
                                    //DM_Whad_DM1_MM500
                                    {312422,"mc16_13TeV.312422.aMcAtNloPy8EG_N30NLO_A14N23LO_dmA_Whad_DM1_MM500_MET50.deriv.DAOD_EXOT27.e7691_s3126_r9364_p3975"},
                                    //DM_Whad_DM1_MM1000
                                    {312423,"mc16_13TeV.312423.aMcAtNloPy8EG_N30NLO_A14N23LO_dmA_Whad_DM1_MM1000_MET50.deriv.DAOD_EXOT27.e7691_s3126_r9364_p3975"},
                                    //DM_Whad_DM1_MM1500
                                    {312424,"mc16_13TeV.312424.aMcAtNloPy8EG_N30NLO_A14N23LO_dmA_Whad_DM1_MM1500_MET50.deriv.DAOD_EXOT27.e7691_s3126_r9364_p3975"},
                                    //DM_Zhad_DM1_MM500
                                    {312425,"mc16_13TeV.312425.aMcAtNloPy8EG_N30NLO_A14N23LO_dmA_Zhad_DM1_MM500_MET50.deriv.DAOD_EXOT27.e7691_s3126_r9364_p3975"},
                                    //DM_Zhad_DM1_MM1000
                                    {312426,"mc16_13TeV.312426.aMcAtNloPy8EG_N30NLO_A14N23LO_dmA_Zhad_DM1_MM1000_MET50.deriv.DAOD_EXOT27.e7691_s3126_r9364_p3975"},
                                    //DM_Zhad_DM1_MM1500
                                    {312427,"mc16_13TeV.312427.aMcAtNloPy8EG_N30NLO_A14N23LO_dmA_Zhad_DM1_MM1500_MET50.deriv.DAOD_EXOT27.e7691_s3126_r9364_p3975"},
                                    //unused
                                    {308071,"mc16_13TeV.308071.PowhegPythia8EvtGen_CT10_AZNLO_WpH125J_MINLO_jj_ZZinv.deriv.DAOD_EXOT27.e5831_s3126_r9364_p3714"},
                                    {308072,"mc16_13TeV.308072.PowhegPythia8EvtGen_CT10_AZNLO_ZH125J_MINLO_jj_ZZinv.deriv.DAOD_EXOT27.e5831_s3126_r9364_p3714"},
                                    {308276,"mc16_13TeV.308276.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_ZZ4nu_MET125.deriv.DAOD_EXOT27.e6126_s3126_r9364_p3714"},
                                    {308284,"mc16_13TeV.308284.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_ZZ4nu_MET75.deriv.DAOD_EXOT27.e6126_s3126_r9364_p3714"},
        };
    };

    static vector<int> getDSIDs(TString name) {
        if      (MC_samples().count(name) != 0) return MC_samples()[name];
        else if (ALPs_samples().count(name) != 0) return ALPs_samples()[name];
        else if (invH_samples().count(name) != 0) return invH_samples()[name];
        else if (DM_samples().count(name) != 0) return DM_samples()[name];

        coutError("SampleList::getDSIDs -- No list of DSIDs was found for " + name );
        return {};
    };

    static TString getDSIDLabel(Int_t dsid) {
        //diboson
        if (dsid == 345043) return "ZbbZvv";
        if (dsid == 345044) return "ZbbZll";
        if (dsid == 345045) return "WlvZbb";
        if (dsid == 363355) return "ZqqZvv";
        if (dsid == 363356) return "ZqqZll";
        if (dsid == 363357) return "WqqZvv";
        if (dsid == 363358) return "WqqZll";
        if (dsid == 363359) return "WpqqWmlv";
        if (dsid == 363360) return "WplvWmqq";
        if (dsid == 363489) return "WlvZqq";
        //VH
        if (dsid == 345053) return "WmH125J_MINLO_lvbb_VpT";
        if (dsid == 345054) return "WpH125J_MINLO_lvbb_VpT";
        if (dsid == 345055) return "ZH125J_MINLO_llbb_VpT";
        if (dsid == 345056) return "ZH125J_MINLO_vvbb_VpT";
        if (dsid == 345057) return "ggZH125_llbb";
        if (dsid == 345058) return "ggZH125_vvbb";
        //jetjet
        if (dsid == 361020) return "jetjet_JZ0W";
        if (dsid == 361021) return "jetjet_JZ1W";
        if (dsid == 361022) return "jetjet_JZ2W";
        if (dsid == 361023) return "jetjet_JZ3W";
        if (dsid == 361024) return "jetjet_JZ4W";
        if (dsid == 361025) return "jetjet_JZ5W";
        if (dsid == 361026) return "jetjet_JZ6W";
        if (dsid == 361027) return "jetjet_JZ7W";
        if (dsid == 361028) return "jetjet_JZ8W";
        if (dsid == 361029) return "jetjet_JZ9W";
        if (dsid == 361030) return "jetjet_JZ10W";
        if (dsid == 361031) return "jetjet_JZ11W";
        if (dsid == 361032) return "jetjet_JZ12W";
        //Zmumu
        if (dsid == 364100) return "Zmumu_MAXHTPTV0_70_CVetoBVeto";
        if (dsid == 364101) return "Zmumu_MAXHTPTV0_70_CFilterBVeto";
        if (dsid == 364102) return "Zmumu_MAXHTPTV0_70_BFilter";
        if (dsid == 364103) return "Zmumu_MAXHTPTV70_140_CVetoBVeto";
        if (dsid == 364104) return "Zmumu_MAXHTPTV70_140_CFilterBVeto";
        if (dsid == 364105) return "Zmumu_MAXHTPTV70_140_BFilter";
        if (dsid == 364106) return "Zmumu_MAXHTPTV140_280_CVetoBVeto";
        if (dsid == 364107) return "Zmumu_MAXHTPTV140_280_CFilterBVeto";
        if (dsid == 364108) return "Zmumu_MAXHTPTV140_280_BFilter";
        if (dsid == 364109) return "Zmumu_MAXHTPTV280_500_CVetoBVeto";
        if (dsid == 364110) return "Zmumu_MAXHTPTV280_500_CFilterBVeto";
        if (dsid == 364111) return "Zmumu_MAXHTPTV280_500_BFilter";
        if (dsid == 364112) return "Zmumu_MAXHTPTV500_1000";
        if (dsid == 364113) return "Zmumu_MAXHTPTV1000_E_CMS";
        //Zee
        if (dsid == 364114) return "Zee_MAXHTPTV0_70_CVetoBVeto";
        if (dsid == 364115) return "Zee_MAXHTPTV0_70_CFilterBVeto";
        if (dsid == 364116) return "Zee_MAXHTPTV0_70_BFilter";
        if (dsid == 364117) return "Zee_MAXHTPTV70_140_CVetoBVeto";
        if (dsid == 364118) return "Zee_MAXHTPTV70_140_CFilterBVeto";
        if (dsid == 364119) return "Zee_MAXHTPTV70_140_BFilter";
        if (dsid == 364120) return "Zee_MAXHTPTV140_280_CVetoBVeto";
        if (dsid == 364121) return "Zee_MAXHTPTV140_280_CFilterBVeto";
        if (dsid == 364122) return "Zee_MAXHTPTV140_280_BFilter";
        if (dsid == 364123) return "Zee_MAXHTPTV280_500_CVetoBVeto";
        if (dsid == 364124) return "Zee_MAXHTPTV280_500_CFilterBVeto";
        if (dsid == 364125) return "Zee_MAXHTPTV280_500_BFilter";
        if (dsid == 364126) return "Zee_MAXHTPTV500_1000";
        if (dsid == 364127) return "Zee_MAXHTPTV1000_E_CMS";
        //Ztautau
        if (dsid == 364128) return "Ztautau_MAXHTPTV0_70_CVetoBVeto";
        if (dsid == 364129) return "Ztautau_MAXHTPTV0_70_CFilterBVeto";
        if (dsid == 364130) return "Ztautau_MAXHTPTV0_70_BFilter";
        if (dsid == 364131) return "Ztautau_MAXHTPTV70_140_CVetoBVeto";
        if (dsid == 364132) return "Ztautau_MAXHTPTV70_140_CFilterBVeto";
        if (dsid == 364133) return "Ztautau_MAXHTPTV70_140_BFilter";
        if (dsid == 364134) return "Ztautau_MAXHTPTV140_280_CVetoBVeto";
        if (dsid == 364135) return "Ztautau_MAXHTPTV140_280_CFilterBVeto";
        if (dsid == 364136) return "Ztautau_MAXHTPTV140_280_BFilter";
        if (dsid == 364137) return "Ztautau_MAXHTPTV280_500_CVetoBVeto";
        if (dsid == 364138) return "Ztautau_MAXHTPTV280_500_CFilterBVeto";
        if (dsid == 364139) return "Ztautau_MAXHTPTV280_500_BFilter";
        if (dsid == 364140) return "Ztautau_MAXHTPTV500_1000";
        if (dsid == 364141) return "Ztautau_MAXHTPTV1000_E_CMS";
        //Znunu
        if (dsid == 364142) return "Znunu_MAXHTPTV0_70_CVetoBVeto";
        if (dsid == 364143) return "Znunu_MAXHTPTV0_70_CFilterBVeto";
        if (dsid == 364144) return "Znunu_MAXHTPTV0_70_BFilter";
        if (dsid == 364145) return "Znunu_MAXHTPTV70_140_CVetoBVeto";
        if (dsid == 364146) return "Znunu_MAXHTPTV70_140_CFilterBVeto";
        if (dsid == 364147) return "Znunu_MAXHTPTV70_140_BFilter";
        if (dsid == 364148) return "Znunu_MAXHTPTV140_280_CVetoBVeto";
        if (dsid == 364149) return "Znunu_MAXHTPTV140_280_CFilterBVeto";
        if (dsid == 364150) return "Znunu_MAXHTPTV140_280_BFilter";
        if (dsid == 364151) return "Znunu_MAXHTPTV280_500_CVetoBVeto";
        if (dsid == 364152) return "Znunu_MAXHTPTV280_500_CFilterBVeto";
        if (dsid == 364153) return "Znunu_MAXHTPTV280_500_BFilter";
        if (dsid == 364154) return "Znunu_MAXHTPTV500_1000";
        if (dsid == 364155) return "Znunu_MAXHTPTV1000_E_CMS";
        //Wmunu
        if (dsid == 364156) return "Wmunu_MAXHTPTV0_70_CVetoBVeto";
        if (dsid == 364157) return "Wmunu_MAXHTPTV0_70_CFilterBVeto";
        if (dsid == 364158) return "Wmunu_MAXHTPTV0_70_BFilter";
        if (dsid == 364159) return "Wmunu_MAXHTPTV70_140_CVetoBVeto";
        if (dsid == 364160) return "Wmunu_MAXHTPTV70_140_CFilterBVeto";
        if (dsid == 364161) return "Wmunu_MAXHTPTV70_140_BFilter";
        if (dsid == 364162) return "Wmunu_MAXHTPTV140_280_CVetoBVeto";
        if (dsid == 364163) return "Wmunu_MAXHTPTV140_280_CFilterBVeto";
        if (dsid == 364164) return "Wmunu_MAXHTPTV140_280_BFilter";
        if (dsid == 364165) return "Wmunu_MAXHTPTV280_500_CVetoBVeto";
        if (dsid == 364166) return "Wmunu_MAXHTPTV280_500_CFilterBVeto";
        if (dsid == 364167) return "Wmunu_MAXHTPTV280_500_BFilter";
        if (dsid == 364168) return "Wmunu_MAXHTPTV500_1000";
        if (dsid == 364169) return "Wmunu_MAXHTPTV1000_E_CMS";
        //Wenu
        if (dsid == 364170) return "Wenu_MAXHTPTV0_70_CVetoBVeto";
        if (dsid == 364171) return "Wenu_MAXHTPTV0_70_CFilterBVeto";
        if (dsid == 364172) return "Wenu_MAXHTPTV0_70_BFilter";
        if (dsid == 364173) return "Wenu_MAXHTPTV70_140_CVetoBVeto";
        if (dsid == 364174) return "Wenu_MAXHTPTV70_140_CFilterBVeto";
        if (dsid == 364175) return "Wenu_MAXHTPTV70_140_BFilter";
        if (dsid == 364176) return "Wenu_MAXHTPTV140_280_CVetoBVeto";
        if (dsid == 364177) return "Wenu_MAXHTPTV140_280_CFilterBVeto";
        if (dsid == 364178) return "Wenu_MAXHTPTV140_280_BFilter";
        if (dsid == 364179) return "Wenu_MAXHTPTV280_500_CVetoBVeto";
        if (dsid == 364180) return "Wenu_MAXHTPTV280_500_CFilterBVeto";
        if (dsid == 364181) return "Wenu_MAXHTPTV280_500_BFilter";
        if (dsid == 364182) return "Wenu_MAXHTPTV500_1000";
        if (dsid == 364183) return "Wenu_MAXHTPTV1000_E_CMS";
        //Wtaunu
        if (dsid == 364184) return "Wtaunu_MAXHTPTV0_70_CVetoBVeto";
        if (dsid == 364185) return "Wtaunu_MAXHTPTV0_70_CFilterBVeto";
        if (dsid == 364186) return "Wtaunu_MAXHTPTV0_70_BFilter";
        if (dsid == 364187) return "Wtaunu_MAXHTPTV70_140_CVetoBVeto";
        if (dsid == 364188) return "Wtaunu_MAXHTPTV70_140_CFilterBVeto";
        if (dsid == 364189) return "Wtaunu_MAXHTPTV70_140_BFilter";
        if (dsid == 364190) return "Wtaunu_MAXHTPTV140_280_CVetoBVeto";
        if (dsid == 364191) return "Wtaunu_MAXHTPTV140_280_CFilterBVeto";
        if (dsid == 364192) return "Wtaunu_MAXHTPTV140_280_BFilter";
        if (dsid == 364193) return "Wtaunu_MAXHTPTV280_500_CVetoBVeto";
        if (dsid == 364194) return "Wtaunu_MAXHTPTV280_500_CFilterBVeto";
        if (dsid == 364195) return "Wtaunu_MAXHTPTV280_500_BFilter";
        if (dsid == 364196) return "Wtaunu_MAXHTPTV500_1000";
        if (dsid == 364197) return "Wtaunu_MAXHTPTV1000_E_CMS";
        //ggVV
        if (dsid == 364302) return "ggZllZqq";
        if (dsid == 364303) return "ggZvvZqq";
        if (dsid == 364304) return "ggWmlvWpqq";
        if (dsid == 364305) return "ggWplvWmqq";
        //SinglePhoton
        if (dsid == 364543) return "SinglePhoton_pty_70_140";
        if (dsid == 364544) return "SinglePhoton_pty_140_280";
        if (dsid == 364545) return "SinglePhoton_pty_280_500";
        if (dsid == 364546) return "SinglePhoton_pty_500_1000";
        if (dsid == 364547) return "SinglePhoton_pty_1000_E_CMS";
        //Znunu_PTV
        if (dsid == 364222) return "Znunu_PTV500_1000";
        if (dsid == 364223) return "Znunu_PTV1000_E_CMS";
        if (dsid == 366010) return "Znunu_PTV70_100_BFilter";
        if (dsid == 366011) return "Znunu_PTV100_140_MJJ0_500_BFilter";
        if (dsid == 366012) return "Znunu_PTV100_140_MJJ500_1000_BFilter";
        if (dsid == 366013) return "Znunu_PTV100_140_MJJ1000_E_CMS_BFilter";
        if (dsid == 366014) return "Znunu_PTV140_280_MJJ0_500_BFilter";
        if (dsid == 366015) return "Znunu_PTV140_280_MJJ500_1000_BFilter";
        if (dsid == 366016) return "Znunu_PTV140_280_MJJ1000_E_CMS_BFilter";
        if (dsid == 366017) return "Znunu_PTV280_500_BFilter";
        if (dsid == 366019) return "Znunu_PTV70_100_CFilterBVeto";
        if (dsid == 366020) return "Znunu_PTV100_140_MJJ0_500_CFilterBVeto";
        if (dsid == 366021) return "Znunu_PTV100_140_MJJ500_1000_CFilterBVeto";
        if (dsid == 366022) return "Znunu_PTV100_140_MJJ1000_E_CMS_CFilterBVeto";
        if (dsid == 366023) return "Znunu_PTV140_280_MJJ0_500_CFilterBVeto";
        if (dsid == 366024) return "Znunu_PTV140_280_MJJ500_1000_CFilterBVeto";
        if (dsid == 366025) return "Znunu_PTV140_280_MJJ1000_E_CMS_CFilterBVeto";
        if (dsid == 366026) return "Znunu_PTV280_500_CFilterBVeto";
        if (dsid == 366028) return "Znunu_PTV70_100_CVetoBVeto";
        if (dsid == 366029) return "Znunu_PTV100_140_MJJ0_500_CVetoBVeto";
        if (dsid == 366030) return "Znunu_PTV100_140_MJJ500_1000_CVetoBVeto";
        if (dsid == 366031) return "Znunu_PTV100_140_MJJ1000_E_CMS_CVetoBVeto";
        if (dsid == 366032) return "Znunu_PTV140_280_MJJ0_500_CVetoBVeto";
        if (dsid == 366033) return "Znunu_PTV140_280_MJJ500_1000_CVetoBVeto";
        if (dsid == 366034) return "Znunu_PTV140_280_MJJ1000_E_CMS_CVetoBVeto";
        if (dsid == 366035) return "Znunu_PTV280_500_CVetoBVeto";
        //ttbar
        if (dsid == 410470) return "ttbar_hdamp258p75_nonallhad";
        if (dsid == 410471) return "ttbar_hdamp258p75_allhad";
        if (dsid == 410472) return "ttbar_hdamp258p75_dil";
        //ttbar_wFilter
        if (dsid == 345935) return "ttbarMET100_200_hdamp258p75_nonallhad";
        if (dsid == 407345) return "ttbarMET200_300_hdamp258p75_nonallhad";
        if (dsid == 407346) return "ttbarMET300_400_hdamp258p75_nonallhad";
        if (dsid == 407347) return "ttbarMET400_hdamp258p75_nonallhad";
        //singletop
        if (dsid == 410644) return "singletop_schan_lept_top";
        if (dsid == 410645) return "singletop_schan_lept_antitop";
        //Wt_DR
        if (dsid == 410646) return "Wt_DR_inclusive_top";
        if (dsid == 410647) return "Wt_DR_inclusive_antitop";
        //tchan
        if (dsid == 410658) return "tchan_BW50_lept_top";
        if (dsid == 410659) return "tchan_BW50_lept_antitop";
        //ttV
        if (dsid == 410157) return "ttZqq";
        if (dsid == 410155) return "ttW";
        //ttH
        if (dsid == 346344) return "ttH125_semilep";
        //ALPs_axW
        if (dsid == 312410) return "axW_Ma1_CWtil1_fa1000_MET150";
        if (dsid == 312411) return "axW_Ma1_CWtil1_fa1000_MET250";
        if (dsid == 312412) return "axW_Ma1_CWtil1_fa1000_MET400";
        if (dsid == 312413) return "axW_Ma1_CWtil1_fa1000_MET600";
        if (dsid == 312414) return "axW_Ma1_CWtil1_fa1000_MET800";
        if (dsid == 312415) return "axW_Ma1_CWtil1_fa1000_MET1000";
        //ALPs_axZ
        if (dsid == 312416) return "axZ_Ma1_CWtil1_fa1000_MET150";
        if (dsid == 312417) return "axZ_Ma1_CWtil1_fa1000_MET250";
        if (dsid == 312418) return "axZ_Ma1_CWtil1_fa1000_MET400";
        if (dsid == 312419) return "axZ_Ma1_CWtil1_fa1000_MET600";
        if (dsid == 312420) return "axZ_Ma1_CWtil1_fa1000_MET800";
        if (dsid == 312421) return "axZ_Ma1_CWtil1_fa1000_MET1000";
        //invH
        if (dsid == 345596) return "ggZH125_Zinc_HZZinv";
        if (dsid == 346588) return "ggH125_ZZ4nu_MET75";
        if (dsid == 346600) return "VBFH125_ZZ4nu_MET75";
        if (dsid == 346605) return "WpH125J_WinclHinv_MET75";
        if (dsid == 346606) return "WmH125J_WinclHinv_MET75";
        if (dsid == 346607) return "ZH125J_ZinclHinv_MET75";
        if (dsid == 346632) return "ttH125_ZZ4nu_semilep";
        if (dsid == 346633) return "ttH125_ZZ4nu_allhad";
        if (dsid == 346634) return "ttH125_ZZ4nu_dilep";
        //DM_Whad_DM1_MM500
        if (dsid == 312422) return "dmA_Whad_DM1_MM500_MET50";
        //DM_Whad_DM1_MM1000
        if (dsid == 312423) return "dmA_Whad_DM1_MM1000_MET50";
        //DM_Whad_DM1_MM1500
        if (dsid == 312424) return "dmA_Whad_DM1_MM1500_MET50";
        //DM_Zhad_DM1_MM500
        if (dsid == 312425) return "dmA_Zhad_DM1_MM500_MET50";
        //DM_Zhad_DM1_MM1000
        if (dsid == 312426) return "dmA_Zhad_DM1_MM1000_MET50";
        //DM_Zhad_DM1_MM1500
        if (dsid == 312427) return "dmA_Zhad_DM1_MM1500_MET50";
        //unused
        if (dsid == 308071) return "WpH125J_MINLO_jj_ZZinv";
        if (dsid == 308072) return "ZH125J_MINLO_jj_ZZinv";
        if (dsid == 308276) return "VBFH125_ZZ4nu_MET125";
        if (dsid == 308284) return "ggH125_ZZ4nu_MET75";

        return to_string(dsid);
    }

    static TString getPrettyName(Int_t dsid) {
        //invH
        if      (dsid == 346600) return "VBFH";
        else if (dsid == 346605) return "WpH";
        else if (dsid == 346606) return "WmH";
        else if (dsid == 346607) return "ZH";
        else if (dsid == 346588) return "ggH";
        else if (dsid == 345596) return "ggZH";
        else if (dsid == 346632) return "ttH";
        else if (dsid == 346633) return "ttH";
        else if (dsid == 346634) return "ttH";
        //ALPs
        else if (dsid == 312410) return "MET150";
        else if (dsid == 312411) return "MET250";
        else if (dsid == 312412) return "MET400";
        else if (dsid == 312413) return "MET600";
        else if (dsid == 312414) return "MET800";
        else if (dsid == 312415) return "MET1000";
        else if (dsid == 312416) return "MET150";
        else if (dsid == 312417) return "MET250";
        else if (dsid == 312418) return "MET400";
        else if (dsid == 312419) return "MET600";
        else if (dsid == 312420) return "MET800";
        else if (dsid == 312421) return "MET1000";

        // coutError("SampleList::getPrettyName -- No pretty name for " + to_string(dsid));
        return to_string(dsid);
    };

    static TString getLabelForChannel(TString channel_name) {
        if (channel_name == "DM_Whad_DM1_MM500")    return "Whad DM1 MM500";
        if (channel_name == "DM_Whad_DM1_MM1000")   return "Whad DM1 MM1000";
        if (channel_name == "DM_Whad_DM1_MM1500")   return "Whad DM1 MM1500";
        if (channel_name == "DM_Zhad_DM1_MM500")    return "Zhad DM1 MM500";
        if (channel_name == "DM_Zhad_DM1_MM1000")   return "Zhad DM1 MM1000";
        if (channel_name == "DM_Zhad_DM1_MM1500")   return "Zhad DM1 MM1500";
        if (channel_name == "invH_DSID346600")      return "VBFH";
        if (channel_name == "invH_DSID346605")      return "WpH";
        if (channel_name == "invH_DSID346606")      return "WmH";
        if (channel_name == "invH_DSID346607")      return "ZH";
        if (channel_name == "invH_DSID346588")      return "ggH";
        if (channel_name == "invH_DSID345596")      return "ggZH";
        if (channel_name == "invH_DSID346632")      return "ttHinv";
        if (channel_name == "invH_DSID346633")      return "ttHinv";
        if (channel_name == "invH_DSID346634")      return "ttHinv";
        return channel_name;
    };

    static int getColorForChannel(TString channel_label, int default_color) {
        if (channel_label == "VBFH")    return kAzure+1;
        if (channel_label == "WpH")     return kRed;
        if (channel_label == "WmH")     return kRed+1;
        if (channel_label == "ZH")      return kRed+2;
        if (channel_label == "ggH")     return kBlack;
        if (channel_label == "ggZH")    return kViolet;
        if (channel_label == "ttHinv")  return kGreen-9;

        return default_color;
    }

    static vector<TString> getChannelGroup(TString channelGroup) {
        vector<TString> channels;
        if (channelGroup.Contains("BKG_nottbarFilter_nojetjet")) channels = {
                                                    "diboson",
                                                    "ggVV",
                                                    "VH",
                                                    "Wenu",
                                                    "Wmunu",
                                                    "Wtaunu",
                                                    "Zee",
                                                    "Zmumu",
                                                    "Znunu_PTV",
                                                    "Ztautau",
                                                    "ttbar",
                                                    "singletop",
                                                    "tchan",
                                                    "ttV",
                                                    "ttH",
                                                    "Wt_DR"};
        else if (channelGroup.Contains("BKG_noBadYear")) channels = {
                                                    "ggVV",
                                                    "Wenu",
                                                    "Wmunu",
                                                    "Wtaunu",
                                                    "Zee",
                                                    "Zmumu",
                                                    "Znunu_PTV",
                                                    "Ztautau",
                                                    "ttbar",
                                                    "singletop",
                                                    "tchan",
                                                    "ttV",
                                                    "ttH",
                                                    "Wt_DR"};
        else if (channelGroup.Contains("BKG_diboson_nojetjet")) channels = {
                                                    "ggVV",
                                                    "VH",
                                                    "Wenu",
                                                    "Wmunu",
                                                    "Wtaunu",
                                                    "Zee",
                                                    "Zmumu",
                                                    "Znunu_PTV",
                                                    "Ztautau",
                                                    "ttbar",
                                                    "ttbar_wFilter",
                                                    "singletop",
                                                    "tchan",
                                                    "ttV",
                                                    "ttH",
                                                    "Wt_DR"};
        else if (channelGroup.Contains("BKG_nottbar_nojetjet")) channels = {
                                                    "diboson",
                                                    "ggVV",
                                                    "VH",
                                                    "Wenu",
                                                    "Wmunu",
                                                    "Wtaunu",
                                                    "Zee",
                                                    "Zmumu",
                                                    "Znunu_PTV",
                                                    "Ztautau",
                                                    "ttbar_wFilter",
                                                    "singletop",
                                                    "tchan",
                                                    "ttV",
                                                    "ttH",
                                                    "Wt_DR"};
        else if (channelGroup.Contains("BKG_nojetjet")) channels = {
                                                    "diboson",
                                                    "ggVV",
                                                    "VH",
                                                    "Wenu",
                                                    "Wmunu",
                                                    "Wtaunu",
                                                    "Zee",
                                                    "Zmumu",
                                                    "Znunu_PTV",
                                                    "Ztautau",
                                                    "ttbar",
                                                    "ttbar_wFilter",
                                                    "singletop",
                                                    "tchan",
                                                    "ttV",
                                                    "ttH",
                                                    "Wt_DR"};
        else if (channelGroup.Contains("BKG")) channels = {
                                                    "diboson",
                                                    "ggVV",
                                                    "jetjet",
                                                    "VH",
                                                    "Wenu",
                                                    "Wmunu",
                                                    "Wtaunu",
                                                    "Zee",
                                                    "Zmumu",
                                                    "Znunu_PTV",
                                                    "Ztautau",
                                                    "ttbar",
                                                    "ttbar_wFilter",
                                                    "singletop",
                                                    "tchan",
                                                    "ttV",
                                                    "ttH",
                                                    "Wt_DR"};
        else if (channelGroup.Contains("Vjets")) channels = {
                                                    "Wenu",
                                                    "Wmunu",
                                                    "Wtaunu",
                                                    "Zee",
                                                    "Zmumu",
                                                    "Znunu_PTV",
                                                    "Ztautau"};
        else if (channelGroup.Contains("SIG_ex")) channels = {
                                                    "DM_Whad_DM1_MM500",
                                                    "DM_Whad_DM1_MM1500",
                                                    "invH",
                                                    };
        else if (channelGroup.Contains("SIG")) channels = {"invH",
                                                    "ALPs_axW",
                                                    "ALPs_axZ",
                                                    "DM_Whad_DM1_MM500",
                                                    "DM_Whad_DM1_MM1000",
                                                    "DM_Whad_DM1_MM1500",
                                                    "DM_Zhad_DM1_MM500",
                                                    "DM_Zhad_DM1_MM1000",
                                                    "DM_Zhad_DM1_MM1500"
                                                    };
        else if (channelGroup.Contains("invH_perDSIDs")) channels ={
                                                        "invH_DSID345596",
                                                        "invH_DSID346588",
                                                        "invH_DSID346600",
                                                        "invH_DSID346605",
                                                        "invH_DSID346606",
                                                        "invH_DSID346607",
                                                        "invH_DSID346632",
                                                        "invH_DSID346633",
                                                        "invH_DSID346634",
                                                    };
        else if (channelGroup.Contains("invH_bigg")) channels ={
                                                        "invH_DSID346588",
                                                        "invH_DSID346600",
                                                        "invH_DSID346605",
                                                        "invH_DSID346606",
                                                        "invH_DSID346607",
                                                    };
        else if (channelGroup.Contains("JDM")) channels ={
                                                        "invH",
                                                        "invH_DSID346605",
                                                        "invH_DSID346606",
                                                        "invH_DSID346607",
                                                        "DM_Whad_DM1_MM500",
                                                    };
        else if (channelGroup.Contains("VH_DM")) channels ={
                                                        "invH_DSID346605",
                                                        "invH_DSID346606",
                                                        "invH_DSID346607",
                                                        "DM_Whad_DM1_MM500",
                                                        "DM_Zhad_DM1_MM500",
                                                    };
        else if (channelGroup.Contains("invH_contribution")) channels ={
                                                        "invH",
                                                        "invH_DSID345596",
                                                        "invH_DSID346588",
                                                        "invH_DSID346600",
                                                        "invH_DSID346605",
                                                        "invH_DSID346606",
                                                        "invH_DSID346607",
                                                        "invH_DSID346632",
                                                        "invH_DSID346633",
                                                        "invH_DSID346634",
                                                    };
        else if (channelGroup.Contains("invH_monoW")) channels = {
                                                        "invH",
                                                        "DM_Whad_DM1_MM500",
                                                        "DM_Whad_DM1_MM1000",
                                                        "DM_Whad_DM1_MM1500",
                                                    };
        else if (channelGroup.Contains("monoW")) channels = {
                                                        "ALPs_axW",
                                                        "DM_Whad_DM1_MM500",
                                                        "DM_Whad_DM1_MM1000",
                                                        "DM_Whad_DM1_MM1500",
                                                    };
        else if (channelGroup.Contains("monoZ")) channels = {
                                                        "ALPs_axZ",
                                                        "DM_Zhad_DM1_MM500",
                                                        "DM_Zhad_DM1_MM1000",
                                                        "DM_Zhad_DM1_MM1500",
                                                    };
        else if (channelGroup.Contains("invH_compare_DMWhad")) channels = {
                                                        "invH",
                                                        "DM_Whad_DM1_MM500",
                                                    };
        else if (channelGroup.Contains("INVH")) channels = {"invH"};
        else if (channelGroup.Contains("None")) channels = {};
        else channels = {channelGroup};

        return channels;

    };


};

#endif
