#include "methods.h"
#include "SampleList.h"
#include "SelectionTool.h"
#include "PlotUtils.h"

void makeDataStudyPlot(TH1D * hist, vector<TString> labels, TString xLabel, TString yLabel, TString plotName, TLine * line, double yRange1, double yRange2, vector<TString> strs, vector<double> x_str, vector<double> y_str);
double GetYield(TString directory, TString channel, TString selection, TString runPeriod);

void DateStudy(TString config_file, TString bkg_group = "BKG_nojetjet") {

    TStopwatch timer; timer.Start();
    gStyle->SetOptStat(0);

    //////////////////////
    //Parsing of the parameters
    TEnv env; 
    if (env.ReadFile(config_file, kEnvAll)) {
        coutError("Unable to read configuration file from PathResolverFindCalibFile of input " + config_file);
        // return;
    }
    TString output_folder = env.GetValue("output_folder","EMPTY"); if (output_folder == "EMPTY") {coutError("No output_folder in the configuration file"); return;}
    TString input_folder_tag = env.GetValue("input_folder_tag","EMPTY"); if (input_folder_tag == "EMPTY") {coutError("No input_folder_tag in the configuration file"); return;}
    vector<TString> selections = getTokens(env.GetValue("plot_selections",""), " "); if (selections.size() == 0) {coutError("No selections defined in the config file or given!"); return;}

    makeDirectory(output_folder);
   
    //////////////////////
    // Prepare vectors
    vector<TString> channels = SampleList::getChannelGroup(bkg_group);
    vector<TString> runPeriods = 
                                    {
                                        "_runCutA",
                                        "_runCutB",
                                        "_runCutC",
                                        "_runCutD",
                                        "_runCutE",
                                        "_runCutF",
                                        "_runCutG",
                                        "_runCutH",
                                        "_runCutI",
                                        "_runCutJ",
                                        "_runCutK",
                                        "_runCutL",
                                        "_runCutM",
                                        "_runCutN",
                                    };
    vector<TString> labels = 
                                    {
                                        "276262-284484",
                                        "297730-303560",
                                        "303638-307394",
                                        "307454-311481",
                                        "325713-331742",
                                        "331772-335177",
                                        "335222-338183",
                                        "338220-340453",
                                        "348885-350682",
                                        "350749-355273",
                                        "355529-358215",
                                        "358233-359735",
                                        "359766-363129",
                                        "363198-364292",
                                    };
    vector<TString> campaignForPeriod = 
                                    {
                                        "mc16a",
                                        "mc16a",
                                        "mc16a",
                                        "mc16a",
                                        "mc16d",
                                        "mc16d",
                                        "mc16d",
                                        "mc16d",
                                        "mc16e",
                                        "mc16e",
                                        "mc16e",
                                        "mc16e",
                                        "mc16e",
                                        "mc16e",
                                    };
    vector<double> lumi_perPeriod =  // pb-1
                                    {
                                        3219.552576,
                                        10055.55458,
                                        10312.551661,
                                        12627.333133,
                                        10337.751992,
                                        10234.377365,
                                        10716.97509,
                                        13018.084831,
                                        10689.292403,
                                        10426.385533,
                                        10338.389651,
                                        10398.55203,
                                        10824.615958,
                                        5773.02507,
                                    };

    //////////////////////
    // PROCESSING

    for (TString selection :selections) {
        vector<double> data_yields;
        vector<double> MC_yields;
        vector<double> ratio_yields;
        for (int i_period = 0 ; i_period < runPeriods.size() ; i_period++) {
            TString runPeriod = runPeriods.at(i_period);
            TString campaign = campaignForPeriod.at(i_period);
            double period_lumi = lumi_perPeriod.at(i_period) / 1000;
            data_yields.push_back(0);
            MC_yields.push_back(0);
            for (TString channel : channels) {
                double lumi = GetCampaignLumi(campaign);
                double mc_yield = GetYield("/nfs/pic.es/user/s/sgonzalez/scratch2/MonoV/monovmacros/Plots/plots_Feb2020Prod_" + campaign + input_folder_tag + "/",channel,selection,"");
                cout << "    " << channel << " " << mc_yield << endl;
                MC_yields.at(i_period) += period_lumi * mc_yield / lumi;
            }
            if (campaign == "mc16a") {
                data_yields.at(i_period) += GetYield("/nfs/pic.es/user/s/sgonzalez/scratch2/MonoV/monovmacros/Plots/plots_Feb2020Prod_mc16a" + input_folder_tag + "/","data15",selection,runPeriod);
                data_yields.at(i_period) += GetYield("/nfs/pic.es/user/s/sgonzalez/scratch2/MonoV/monovmacros/Plots/plots_Feb2020Prod_mc16a" + input_folder_tag + "/","data16",selection,runPeriod);
            }
            if (campaign == "mc16d") {
                data_yields.at(i_period) += GetYield("/nfs/pic.es/user/s/sgonzalez/scratch2/MonoV/monovmacros/Plots/plots_Feb2020Prod_mc16d" + input_folder_tag + "/","data17",selection,runPeriod);
            }
            if (campaign == "mc16e") {
                data_yields.at(i_period) += GetYield("/nfs/pic.es/user/s/sgonzalez/scratch2/MonoV/monovmacros/Plots/plots_Feb2020Prod_mc16e" + input_folder_tag + "/","data18",selection,runPeriod);
            }
            cout << runPeriod << " " << data_yields.at(i_period) << " / " << MC_yields.at(i_period) << " = " << data_yields.at(i_period)/MC_yields.at(i_period) << endl;
            ratio_yields.push_back(data_yields.at(i_period)/MC_yields.at(i_period));
        }

        //////////////////////
        // PREPARE HIST

        TH1D * hist_data = new TH1D("","",runPeriods.size(),0,runPeriods.size());
        TH1D * hist_MC = new TH1D("","",runPeriods.size(),0,runPeriods.size());
        for (int i_period = 0 ; i_period < runPeriods.size() ; i_period++) {
            hist_data->SetBinContent(i_period+1,data_yields.at(i_period));
            hist_MC->SetBinContent(i_period+1,MC_yields.at(i_period));
        }
        TH1D * hist_ratio = (TH1D*)hist_data->Clone();
        hist_ratio->Sumw2();
        hist_ratio->Divide(hist_MC);
        TLine * line = new TLine(0,GetMeanErr(ratio_yields).first,runPeriods.size(),GetMeanErr(ratio_yields).first);
        makeDataStudyPlot(hist_ratio, labels, "Run Number", "Data/MC", output_folder + "/plot_" + selection, line, 0, 2, {SelectionTool::getSelectionLabel(selection)}, {0.13}, {0.87});
    }

}

void makeDataStudyPlot(TH1D * hist, vector<TString> labels, TString xLabel, TString yLabel, TString plotName, TLine * line = 0, double yRange1 = -1, double yRange2 = -1, vector<TString> strs = {}, vector<double> x_str = {}, vector<double> y_str = {}) {

    TCanvas * c = PlotUtils::getDefaultCanvas(false);
    c->SetBottomMargin(0.15);

    // Drawing
    hist->Draw("hist e");
    for (unsigned int i = 0 ; i < labels.size() ; i++) {
        hist->GetXaxis()->SetBinLabel(hist->GetXaxis()->FindBin(i), labels.at(i));
    }

    // Style
    hist->GetXaxis()->SetTitle(xLabel);
    hist->GetXaxis()->SetTitleOffset(1.75);
    hist->GetYaxis()->SetTitle(yLabel);
    if (yRange1 != yRange2) hist->GetYaxis()->SetRangeUser(yRange1,yRange2);

    // Line
    if (line != 0) {
        line->SetLineStyle(2);
        line->SetLineWidth(2);
        line->Draw("same");
    }

    // TLatex
    TLatex latex; latex.SetNDC(); latex.SetTextSize(0.040); latex.SetTextAlign(13); latex.SetTextColor(kBlack);
    for (unsigned int i_str = 0 ; i_str < strs.size() ; i_str++) {
        latex.DrawLatex(x_str.at(i_str), y_str.at(i_str), strs.at(i_str));
    }

    c->Print(plotName + ".png");
    c->Print(plotName + ".eps");

    delete c;
}

double GetYield(TString directory, TString channel, TString selection, TString runPeriod) {
    TString fileName = directory + "/" + channel + "/CutflowPlot/histograms_" + channel + ".root";
    if (!fileExists(fileName)) {coutError("File does not exist: " + fileName); return -999;}

    // cout << "Opening " << fileName << endl;
    TFile * file = new TFile(fileName);
    TString histName = channel + "_sumw_" + selection + runPeriod;
    if (!file->GetListOfKeys()->Contains(histName)) {coutError("Couldn't find " + histName); return -999;}

    TH1D * hist = (TH1D*)file->Get(histName);
    double yield = hist->GetBinContent(hist->GetNbinsX());

    file->Close();
    delete file;

    return yield;
}
