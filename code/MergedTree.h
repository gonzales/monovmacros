//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jan 20 14:54:15 2020 by ROOT version 6.18/04
// from TTree ALPs_axW/
// found on file: ../out/ALPs_axW.root
//////////////////////////////////////////////////////////

#ifndef MergedTree_h
#define MergedTree_h

#include "MiniTree.C"

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"

class MergedTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           run;
   ULong64_t       event;
   Int_t           last;
   Int_t           year;
   Float_t         met_tst_sig;
   Float_t         xSec_SUSY;
   Float_t         k_factor;
   Float_t         filter_eff;
   vector<float>   *el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   vector<float>   *el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   vector<float>   *el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   vector<float>   *el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   vector<float>   *el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   vector<float>   *el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   vector<float>   *el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   vector<float>   *el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   vector<float>   *el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   vector<float>   *el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   vector<float>   *el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   vector<float>   *el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   vector<float>   *mu_SF_syst_MUON_EFF_BADMUON_SYS__1down;
   vector<float>   *mu_SF_syst_MUON_EFF_BADMUON_SYS__1up;
   vector<float>   *mu_SF_syst_MUON_EFF_ISO_STAT__1down;
   vector<float>   *mu_SF_syst_MUON_EFF_ISO_STAT__1up;
   vector<float>   *mu_SF_syst_MUON_EFF_ISO_SYS__1down;
   vector<float>   *mu_SF_syst_MUON_EFF_ISO_SYS__1up;
   vector<float>   *mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down;
   vector<float>   *mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up;
   vector<float>   *mu_SF_syst_MUON_EFF_RECO_STAT__1down;
   vector<float>   *mu_SF_syst_MUON_EFF_RECO_STAT__1up;
   vector<float>   *mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down;
   vector<float>   *mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up;
   vector<float>   *mu_SF_syst_MUON_EFF_RECO_SYS__1down;
   vector<float>   *mu_SF_syst_MUON_EFF_RECO_SYS__1up;
   vector<float>   *mu_SF_syst_MUON_EFF_TTVA_STAT__1down;
   vector<float>   *mu_SF_syst_MUON_EFF_TTVA_STAT__1up;
   vector<float>   *mu_SF_syst_MUON_EFF_TTVA_SYS__1down;
   vector<float>   *mu_SF_syst_MUON_EFF_TTVA_SYS__1up;
   vector<float>   *mu_SF_syst_MUON_EFF_TrigStatUncertainty__1down;
   vector<float>   *mu_SF_syst_MUON_EFF_TrigStatUncertainty__1up;
   vector<float>   *mu_SF_syst_MUON_EFF_TrigSystUncertainty__1down;
   vector<float>   *mu_SF_syst_MUON_EFF_TrigSystUncertainty__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1up;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1down;
   vector<float>   *tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1up;
   vector<float>   *ph_SF_syst_PH_EFF_ID_Uncertainty__1down;
   vector<float>   *ph_SF_syst_PH_EFF_ID_Uncertainty__1up;
   vector<float>   *ph_SF_syst_PH_EFF_ISO_Uncertainty__1down;
   vector<float>   *ph_SF_syst_PH_EFF_ISO_Uncertainty__1up;
   vector<float>   *ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1down;
   vector<float>   *ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1up;
   vector<float>   *jet_SF_syst_JET_JvtEfficiency__1down;
   vector<float>   *jet_SF_syst_JET_JvtEfficiency__1up;
   vector<float>   *pu_weight_syst_PRW_DATASF__1down;
   vector<float>   *pu_weight_syst_PRW_DATASF__1up;
   vector<float>   *btag_weight_syst_FT_EFF_B_systematics__1down;
   vector<float>   *btag_weight_syst_FT_EFF_B_systematics__1up;
   vector<float>   *btag_weight_syst_FT_EFF_C_systematics__1down;
   vector<float>   *btag_weight_syst_FT_EFF_C_systematics__1up;
   vector<float>   *btag_weight_syst_FT_EFF_Light_systematics__1down;
   vector<float>   *btag_weight_syst_FT_EFF_Light_systematics__1up;
   vector<float>   *btag_weight_syst_FT_EFF_extrapolation__1down;
   vector<float>   *btag_weight_syst_FT_EFF_extrapolation__1up;
   vector<float>   *btag_weight_syst_FT_EFF_extrapolation_from_charm__1down;
   vector<float>   *btag_weight_syst_FT_EFF_extrapolation_from_charm__1up;
   vector<float>   *btagloose_weight_syst_FT_EFF_B_systematics__1down;
   vector<float>   *btagloose_weight_syst_FT_EFF_B_systematics__1up;
   vector<float>   *btagloose_weight_syst_FT_EFF_C_systematics__1down;
   vector<float>   *btagloose_weight_syst_FT_EFF_C_systematics__1up;
   vector<float>   *btagloose_weight_syst_FT_EFF_Light_systematics__1down;
   vector<float>   *btagloose_weight_syst_FT_EFF_Light_systematics__1up;
   vector<float>   *btagloose_weight_syst_FT_EFF_extrapolation__1down;
   vector<float>   *btagloose_weight_syst_FT_EFF_extrapolation__1up;
   vector<float>   *btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down;
   vector<float>   *btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up;
   vector<float>   *trkbtag_weight_syst_FT_EFF_B_systematics__1down;
   vector<float>   *trkbtag_weight_syst_FT_EFF_B_systematics__1up;
   vector<float>   *trkbtag_weight_syst_FT_EFF_C_systematics__1down;
   vector<float>   *trkbtag_weight_syst_FT_EFF_C_systematics__1up;
   vector<float>   *trkbtag_weight_syst_FT_EFF_Light_systematics__1down;
   vector<float>   *trkbtag_weight_syst_FT_EFF_Light_systematics__1up;
   vector<float>   *trkbtag_weight_syst_FT_EFF_extrapolation__1down;
   vector<float>   *trkbtag_weight_syst_FT_EFF_extrapolation__1up;
   vector<float>   *trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1down;
   vector<float>   *trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1up;
   vector<float>   *trkbtagloose_weight_syst_FT_EFF_B_systematics__1down;
   vector<float>   *trkbtagloose_weight_syst_FT_EFF_B_systematics__1up;
   vector<float>   *trkbtagloose_weight_syst_FT_EFF_C_systematics__1down;
   vector<float>   *trkbtagloose_weight_syst_FT_EFF_C_systematics__1up;
   vector<float>   *trkbtagloose_weight_syst_FT_EFF_Light_systematics__1down;
   vector<float>   *trkbtagloose_weight_syst_FT_EFF_Light_systematics__1up;
   vector<float>   *trkbtagloose_weight_syst_FT_EFF_extrapolation__1down;
   vector<float>   *trkbtagloose_weight_syst_FT_EFF_extrapolation__1up;
   vector<float>   *trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down;
   vector<float>   *trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up;
   Int_t           n_tau_baseline;
   Float_t         mconly_weight;
   Float_t         pu_weight;
   Float_t         btag_weight;
   Float_t         btagloose_weight;
   Float_t         trkbtag_weight;
   Float_t         trkbtagloose_weight;
   Float_t         jvt_weight;
   Float_t         munu_mT;
   Float_t         enu_mT;
   Float_t         mumu_m;
   Float_t         ee_m;
   Float_t         dPhiLCTopoJetMet;
   Float_t         dPhiLCTopoJetMetNoElectron;
   Float_t         dPhiLCTopoJetMetNoMuon;
   Float_t         dPhiDijetMet;
   Float_t         dPhiDijetMetNoElectron;
   Float_t         dPhiDijetMetNoMuon;
   Float_t         dPhiDijet;
   Float_t         dRDijet;
   Float_t         DijetSumPt;
   Float_t         TrijetSumPt;
   Float_t         DijetMass;
   Int_t           n_trackjet;
   Int_t           n_btrackJet;
   Int_t           n_btrackJet_loose;
   Int_t           n_trackLCTopoAssociatedBjet;
   Int_t           n_trackLCTopoSeparatedBjet;
   Int_t           n_trackLCTopoAssociatedBjetLoose;
   Int_t           n_trackLCTopoSeparatedBjetLoose;
   Int_t           n_jet;
   Int_t           n_bjet;
   Int_t           n_bjet_loose;
   Int_t           n_el;
   Int_t           n_el_baseline;
   Int_t           n_mu_baseline;
   Int_t           n_tau;
   Int_t           n_mu;
   vector<float>   *mconly_weights;
   Int_t           n_truthFatJet;
   Int_t           n_LCTopoJet;
   Float_t         averageIntPerXing;
   Float_t         actualIntPerXing;
   Float_t         corAverageIntPerXing;
   Float_t         corActualIntPerXing;
   Int_t           n_vx;
   Float_t         pu_hash;
   Int_t           trigger_HLT_e120_lhloose;
   Int_t           trigger_HLT_e140_lhloose_nod0;
   Int_t           trigger_HLT_e24_lhmedium_L1EM20VH;
   Int_t           trigger_HLT_e24_lhtight_nod0_ivarloose;
   Int_t           trigger_HLT_e26_lhtight_nod0_ivarloose;
   Int_t           trigger_HLT_e60_lhmedium;
   Int_t           trigger_HLT_e60_lhmedium_nod0;
   Int_t           trigger_HLT_e60_medium;
   Int_t           trigger_HLT_mu20_iloose_L1MU15;
   Int_t           trigger_HLT_mu24_iloose;
   Int_t           trigger_HLT_mu24_ivarmedium;
   Int_t           trigger_HLT_mu26_imedium;
   Int_t           trigger_HLT_mu26_ivarmedium;
   Int_t           trigger_HLT_mu40;
   Int_t           trigger_HLT_mu50;
   Int_t           trigger_HLT_xe100_mht_L1XE50;
   Int_t           trigger_HLT_xe110_mht_L1XE50;
   Int_t           trigger_HLT_xe130_mht_L1XE50;
   Int_t           trigger_HLT_xe70;
   Int_t           trigger_HLT_xe70_mht;
   Int_t           trigger_HLT_xe80_tc_lcw_L1XE50;
   Int_t           trigger_HLT_xe90_mht_L1XE50;
   Int_t           trigger_HLT_xe90_mht_wEFMu_L1XE50;
   Int_t           trigger_HLT_e26_lhtight_nod0;
   Int_t           trigger_HLT_e300_etcut;
   Int_t           trigger_HLT_mu24_iloose_L1MU15;
   Int_t           trigger_HLT_mu24_imedium;
   Int_t           trigger_HLT_mu24_ivarloose;
   Int_t           trigger_HLT_mu24_ivarloose_L1MU15;
   Int_t           trigger_HLT_mu60_0eta105_msonly;
   Int_t           trigger_HLT_xe100_L1XE50;
   Int_t           trigger_HLT_xe100_pufit_L1XE50;
   Int_t           trigger_HLT_xe100_pufit_L1XE55;
   Int_t           trigger_HLT_xe100_tc_em_L1XE50;
   Int_t           trigger_HLT_xe110_pueta_L1XE50;
   Int_t           trigger_HLT_xe110_pufit_L1XE50;
   Int_t           trigger_HLT_xe110_pufit_L1XE55;
   Int_t           trigger_HLT_xe110_pufit_xe65_L1XE50;
   Int_t           trigger_HLT_xe110_pufit_xe70_L1XE50;
   Int_t           trigger_HLT_xe120_pueta;
   Int_t           trigger_HLT_xe120_pufit;
   Int_t           trigger_HLT_xe120_pufit_L1XE50;
   Int_t           trigger_HLT_xe120_tc_lcw_L1XE50;
   Int_t           trigger_HLT_xe70_tc_lcw;
   Int_t           trigger_HLT_xe90_pufit_L1XE50;
   Int_t           trigger_HLT_xe90_tc_lcw_wEFMu_L1XE50;
   Int_t           n_ph;
   Int_t           n_ph_baseline;
   Int_t           n_jet_truth;
   Float_t         met_noelectron_tst_et;
   Float_t         met_noelectron_tst_phi;
   Float_t         met_noelectron_tst_etx;
   Float_t         met_noelectron_tst_ety;
   Float_t         met_nomuon_tst_et;
   Float_t         met_nomuon_tst_phi;
   Float_t         met_nomuon_tst_etx;
   Float_t         met_nomuon_tst_ety;
   Float_t         met_track_et;
   Float_t         met_track_phi;
   Float_t         met_track_etx;
   Float_t         met_track_ety;
   Float_t         met_track_noelectron_et;
   Float_t         met_track_noelectron_phi;
   Float_t         met_track_noelectron_etx;
   Float_t         met_track_noelectron_ety;
   Float_t         met_track_nomuon_et;
   Float_t         met_track_nomuon_phi;
   Float_t         met_track_nomuon_etx;
   Float_t         met_track_nomuon_ety;
   Float_t         met_truth_et;
   Float_t         met_truth_phi;
   Float_t         met_truth_etx;
   Float_t         met_truth_ety;
   Float_t         met_tst_et;
   Float_t         met_tst_phi;
   Float_t         met_tst_etx;
   Float_t         met_tst_ety;
   vector<float>   *mu_pt;
   vector<double>  *mu_SF;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<double>  *mu_SF_iso;
   vector<float>   *mu_m;
   vector<float>   *mu_charge;
   vector<float>   *mu_ptcone20;
   vector<float>   *mu_baseline_pt;
   vector<float>   *mu_baseline_ptcone20;
   vector<double>  *mu_baseline_SF;
   vector<float>   *mu_baseline_eta;
   vector<float>   *mu_baseline_phi;
   vector<bool>    *mu_baseline_isLooseID;
   vector<bool>    *mu_baseline_isMediumID;
   vector<bool>    *mu_baseline_isTightID;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_phi;
   vector<double>  *el_SF;
   vector<double>  *el_SF_iso;
   vector<double>  *el_SF_trigger;
   vector<double>  *el_eff_trigger;
   vector<float>   *el_topoetcone20;
   vector<float>   *el_ptcone20;
   vector<float>   *el_m;
   vector<float>   *el_charge;
   vector<float>   *el_ptvarcone20_TightTTVA_pt1000;
   vector<float>   *el_baseline_pt;
   vector<float>   *el_baseline_topoetcone20;
   vector<float>   *el_baseline_ptcone20;
   vector<double>  *el_baseline_SF;
   vector<float>   *el_baseline_eta;
   vector<float>   *el_baseline_phi;
   vector<bool>    *el_baseline_isLooseID;
   vector<bool>    *el_baseline_isMediumID;
   vector<bool>    *el_baseline_isTightID;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_m;
   vector<float>   *jet_fmax;
   vector<float>   *jet_fch;
   vector<int>     *jet_isbjet;
   vector<int>     *jet_PartonTruthLabelID;
   vector<int>     *jet_ConeTruthLabelID;
   vector<float>   *jet_timing;
   vector<float>   *jet_emfrac;
   vector<float>   *jet_hecf;
   vector<float>   *jet_hecq;
   vector<float>   *jet_larq;
   vector<float>   *jet_avglarq;
   vector<float>   *jet_negE;
   vector<float>   *jet_lambda;
   vector<float>   *jet_lambda2;
   vector<float>   *jet_jvtxf;
   vector<int>     *jet_fmaxi;
   vector<int>     *jet_isbjet_loose;
   vector<float>   *jet_jvt;
   vector<int>     *jet_cleaning;
   vector<int>     *jet_DFCommonJets_QGTagger_NTracks;
   vector<float>   *jet_DFCommonJets_QGTagger_TracksWidth;
   vector<float>   *jet_DFCommonJets_QGTagger_TracksC1;
   vector<float>   *jet_truth_pt;
   vector<float>   *jet_truth_eta;
   vector<float>   *jet_truth_phi;
   vector<float>   *LCTopoJet_pt;
   vector<float>   *LCTopoJet_eta;
   vector<float>   *LCTopoJet_phi;
   vector<float>   *LCTopoJet_m;
   vector<float>   *LCTopoJet_tau21;
   vector<float>   *LCTopoJet_D2;
   vector<float>   *LCTopoJet_C2;
   vector<int>     *LCTopoJet_nTrk;
   vector<int>     *LCTopoJet_nConstit;
   vector<int>     *LCTopoJet_passD2_W50;
   vector<int>     *LCTopoJet_passD2_Z50;
   vector<int>     *LCTopoJet_passD2_W80;
   vector<int>     *LCTopoJet_passD2_Z80;
   vector<int>     *LCTopoJet_passMass_W50;
   vector<int>     *LCTopoJet_passMass_Z50;
   vector<int>     *LCTopoJet_passMass_W80;
   vector<int>     *LCTopoJet_passMass_Z80;
   vector<float>   *TruthFatJet_pt;
   vector<float>   *TruthFatJet_eta;
   vector<float>   *TruthFatJet_phi;
   vector<float>   *TruthFatJet_m;
   vector<float>   *TruthFatJet_tau21;
   vector<float>   *TruthFatJet_D2;
   vector<float>   *TruthFatJet_C2;
   vector<int>     *TruthFatJet_nTrk;
   vector<int>     *TruthFatJet_nConstit;
   Int_t           tau_loose_multiplicity;
   Int_t           tau_medium_multiplicity;
   Int_t           tau_tight_multiplicity;
   Int_t           tau_baseline_loose_multiplicity;
   Int_t           tau_baseline_medium_multiplicity;
   Int_t           tau_baseline_tight_multiplicity;

   Float_t         weight;
   Float_t         mc_weight_sum;
   Float_t         extra_weight;
   Float_t         campaign_lumi;

   // List of branches
   TBranch        *b_run;   //!
   TBranch        *b_event;   //!
   TBranch        *b_last;   //!
   TBranch        *b_year;   //!
   TBranch        *b_met_tst_sig;   //!
   TBranch        *b_xSec_SUSY;   //!
   TBranch        *b_k_factor;   //!
   TBranch        *b_filter_eff;   //!
   TBranch        *b_el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_BADMUON_SYS__1down;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_BADMUON_SYS__1up;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_ISO_STAT__1down;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_ISO_STAT__1up;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_ISO_SYS__1down;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_ISO_SYS__1up;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_RECO_STAT__1down;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_RECO_STAT__1up;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_RECO_SYS__1down;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_RECO_SYS__1up;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_TTVA_STAT__1down;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_TTVA_STAT__1up;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_TTVA_SYS__1down;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_TTVA_SYS__1up;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_mu_SF_syst_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1up;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1down;   //!
   TBranch        *b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1up;   //!
   TBranch        *b_ph_SF_syst_PH_EFF_ID_Uncertainty__1down;   //!
   TBranch        *b_ph_SF_syst_PH_EFF_ID_Uncertainty__1up;   //!
   TBranch        *b_ph_SF_syst_PH_EFF_ISO_Uncertainty__1down;   //!
   TBranch        *b_ph_SF_syst_PH_EFF_ISO_Uncertainty__1up;   //!
   TBranch        *b_ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1down;   //!
   TBranch        *b_ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1up;   //!
   TBranch        *b_jet_SF_syst_JET_JvtEfficiency__1down;   //!
   TBranch        *b_jet_SF_syst_JET_JvtEfficiency__1up;   //!
   TBranch        *b_pu_weight_syst_PRW_DATASF__1down;   //!
   TBranch        *b_pu_weight_syst_PRW_DATASF__1up;   //!
   TBranch        *b_btag_weight_syst_FT_EFF_B_systematics__1down;   //!
   TBranch        *b_btag_weight_syst_FT_EFF_B_systematics__1up;   //!
   TBranch        *b_btag_weight_syst_FT_EFF_C_systematics__1down;   //!
   TBranch        *b_btag_weight_syst_FT_EFF_C_systematics__1up;   //!
   TBranch        *b_btag_weight_syst_FT_EFF_Light_systematics__1down;   //!
   TBranch        *b_btag_weight_syst_FT_EFF_Light_systematics__1up;   //!
   TBranch        *b_btag_weight_syst_FT_EFF_extrapolation__1down;   //!
   TBranch        *b_btag_weight_syst_FT_EFF_extrapolation__1up;   //!
   TBranch        *b_btag_weight_syst_FT_EFF_extrapolation_from_charm__1down;   //!
   TBranch        *b_btag_weight_syst_FT_EFF_extrapolation_from_charm__1up;   //!
   TBranch        *b_btagloose_weight_syst_FT_EFF_B_systematics__1down;   //!
   TBranch        *b_btagloose_weight_syst_FT_EFF_B_systematics__1up;   //!
   TBranch        *b_btagloose_weight_syst_FT_EFF_C_systematics__1down;   //!
   TBranch        *b_btagloose_weight_syst_FT_EFF_C_systematics__1up;   //!
   TBranch        *b_btagloose_weight_syst_FT_EFF_Light_systematics__1down;   //!
   TBranch        *b_btagloose_weight_syst_FT_EFF_Light_systematics__1up;   //!
   TBranch        *b_btagloose_weight_syst_FT_EFF_extrapolation__1down;   //!
   TBranch        *b_btagloose_weight_syst_FT_EFF_extrapolation__1up;   //!
   TBranch        *b_btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down;   //!
   TBranch        *b_btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up;   //!
   TBranch        *b_trkbtag_weight_syst_FT_EFF_B_systematics__1down;   //!
   TBranch        *b_trkbtag_weight_syst_FT_EFF_B_systematics__1up;   //!
   TBranch        *b_trkbtag_weight_syst_FT_EFF_C_systematics__1down;   //!
   TBranch        *b_trkbtag_weight_syst_FT_EFF_C_systematics__1up;   //!
   TBranch        *b_trkbtag_weight_syst_FT_EFF_Light_systematics__1down;   //!
   TBranch        *b_trkbtag_weight_syst_FT_EFF_Light_systematics__1up;   //!
   TBranch        *b_trkbtag_weight_syst_FT_EFF_extrapolation__1down;   //!
   TBranch        *b_trkbtag_weight_syst_FT_EFF_extrapolation__1up;   //!
   TBranch        *b_trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1down;   //!
   TBranch        *b_trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1up;   //!
   TBranch        *b_trkbtagloose_weight_syst_FT_EFF_B_systematics__1down;   //!
   TBranch        *b_trkbtagloose_weight_syst_FT_EFF_B_systematics__1up;   //!
   TBranch        *b_trkbtagloose_weight_syst_FT_EFF_C_systematics__1down;   //!
   TBranch        *b_trkbtagloose_weight_syst_FT_EFF_C_systematics__1up;   //!
   TBranch        *b_trkbtagloose_weight_syst_FT_EFF_Light_systematics__1down;   //!
   TBranch        *b_trkbtagloose_weight_syst_FT_EFF_Light_systematics__1up;   //!
   TBranch        *b_trkbtagloose_weight_syst_FT_EFF_extrapolation__1down;   //!
   TBranch        *b_trkbtagloose_weight_syst_FT_EFF_extrapolation__1up;   //!
   TBranch        *b_trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down;   //!
   TBranch        *b_trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up;   //!
   TBranch        *b_n_tau_baseline;   //!
   TBranch        *b_mconly_weight;   //!
   TBranch        *b_pu_weight;   //!
   TBranch        *b_btag_weight;   //!
   TBranch        *b_btagloose_weight;   //!
   TBranch        *b_trkbtag_weight;   //!
   TBranch        *b_trkbtagloose_weight;   //!
   TBranch        *b_jvt_weight;   //!
   TBranch        *b_munu_mT;   //!
   TBranch        *b_enu_mT;   //!
   TBranch        *b_mumu_m;   //!
   TBranch        *b_ee_m;   //!
   TBranch        *b_dPhiLCTopoJetMet;   //!
   TBranch        *b_dPhiLCTopoJetMetNoElectron;   //!
   TBranch        *b_dPhiLCTopoJetMetNoMuon;   //!
   TBranch        *b_dPhiDijetMet;   //!
   TBranch        *b_dPhiDijetMetNoElectron;   //!
   TBranch        *b_dPhiDijetMetNoMuon;   //!
   TBranch        *b_dPhiDijet;   //!
   TBranch        *b_dRDijet;   //!
   TBranch        *b_DijetSumPt;   //!
   TBranch        *b_TrijetSumPt;   //!
   TBranch        *b_DijetMass;   //!
   TBranch        *b_n_trackjet;   //!
   TBranch        *b_n_btrackJet;   //!
   TBranch        *b_n_btrackJet_loose;   //!
   TBranch        *b_n_trackLCTopoAssociatedBjet;   //!
   TBranch        *b_n_trackLCTopoSeparatedBjet;   //!
   TBranch        *b_n_trackLCTopoAssociatedBjetLoose;   //!
   TBranch        *b_n_trackLCTopoSeparatedBjetLoose;   //!
   TBranch        *b_n_jet;   //!
   TBranch        *b_n_bjet;   //!
   TBranch        *b_n_bjet_loose;   //!
   TBranch        *b_n_el;   //!
   TBranch        *b_n_el_baseline;   //!
   TBranch        *b_n_mu_baseline;   //!
   TBranch        *b_n_tau;   //!
   TBranch        *b_n_mu;   //!
   TBranch        *b_mconly_weights;   //!
   TBranch        *b_n_truthFatJet;   //!
   TBranch        *b_n_LCTopoJet;   //!
   TBranch        *b_averageIntPerXing;   //!
   TBranch        *b_actualIntPerXing;   //!
   TBranch        *b_corAverageIntPerXing;   //!
   TBranch        *b_corActualIntPerXing;   //!
   TBranch        *b_n_vx;   //!
   TBranch        *b_pu_hash;   //!
   TBranch        *b_trigger_HLT_e120_lhloose;   //!
   TBranch        *b_trigger_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_trigger_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_trigger_HLT_e24_lhtight_nod0_ivarloose;   //!
   TBranch        *b_trigger_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_trigger_HLT_e60_lhmedium;   //!
   TBranch        *b_trigger_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_trigger_HLT_e60_medium;   //!
   TBranch        *b_trigger_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_trigger_HLT_mu24_iloose;   //!
   TBranch        *b_trigger_HLT_mu24_ivarmedium;   //!
   TBranch        *b_trigger_HLT_mu26_imedium;   //!
   TBranch        *b_trigger_HLT_mu26_ivarmedium;   //!
   TBranch        *b_trigger_HLT_mu40;   //!
   TBranch        *b_trigger_HLT_mu50;   //!
   TBranch        *b_trigger_HLT_xe100_mht_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe110_mht_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe130_mht_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe70;   //!
   TBranch        *b_trigger_HLT_xe70_mht;   //!
   TBranch        *b_trigger_HLT_xe80_tc_lcw_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe90_mht_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe90_mht_wEFMu_L1XE50;   //!
   TBranch        *b_trigger_HLT_e26_lhtight_nod0;   //!
   TBranch        *b_trigger_HLT_e300_etcut;   //!
   TBranch        *b_trigger_HLT_mu24_iloose_L1MU15;   //!
   TBranch        *b_trigger_HLT_mu24_imedium;   //!
   TBranch        *b_trigger_HLT_mu24_ivarloose;   //!
   TBranch        *b_trigger_HLT_mu24_ivarloose_L1MU15;   //!
   TBranch        *b_trigger_HLT_mu60_0eta105_msonly;   //!
   TBranch        *b_trigger_HLT_xe100_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe100_pufit_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe100_pufit_L1XE55;   //!
   TBranch        *b_trigger_HLT_xe100_tc_em_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe110_pueta_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe110_pufit_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe110_pufit_L1XE55;   //!
   TBranch        *b_trigger_HLT_xe110_pufit_xe65_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe110_pufit_xe70_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe120_pueta;   //!
   TBranch        *b_trigger_HLT_xe120_pufit;   //!
   TBranch        *b_trigger_HLT_xe120_pufit_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe120_tc_lcw_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe70_tc_lcw;   //!
   TBranch        *b_trigger_HLT_xe90_pufit_L1XE50;   //!
   TBranch        *b_trigger_HLT_xe90_tc_lcw_wEFMu_L1XE50;   //!
   TBranch        *b_n_ph;   //!
   TBranch        *b_n_ph_baseline;   //!
   TBranch        *b_n_jet_truth;   //!
   TBranch        *b_met_noelectron_tst_et;   //!
   TBranch        *b_met_noelectron_tst_phi;   //!
   TBranch        *b_met_noelectron_tst_etx;   //!
   TBranch        *b_met_noelectron_tst_ety;   //!
   TBranch        *b_met_nomuon_tst_et;   //!
   TBranch        *b_met_nomuon_tst_phi;   //!
   TBranch        *b_met_nomuon_tst_etx;   //!
   TBranch        *b_met_nomuon_tst_ety;   //!
   TBranch        *b_met_track_et;   //!
   TBranch        *b_met_track_phi;   //!
   TBranch        *b_met_track_etx;   //!
   TBranch        *b_met_track_ety;   //!
   TBranch        *b_met_track_noelectron_et;   //!
   TBranch        *b_met_track_noelectron_phi;   //!
   TBranch        *b_met_track_noelectron_etx;   //!
   TBranch        *b_met_track_noelectron_ety;   //!
   TBranch        *b_met_track_nomuon_et;   //!
   TBranch        *b_met_track_nomuon_phi;   //!
   TBranch        *b_met_track_nomuon_etx;   //!
   TBranch        *b_met_track_nomuon_ety;   //!
   TBranch        *b_met_truth_et;   //!
   TBranch        *b_met_truth_phi;   //!
   TBranch        *b_met_truth_etx;   //!
   TBranch        *b_met_truth_ety;   //!
   TBranch        *b_met_tst_et;   //!
   TBranch        *b_met_tst_phi;   //!
   TBranch        *b_met_tst_etx;   //!
   TBranch        *b_met_tst_ety;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_SF;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_SF_iso;   //!
   TBranch        *b_mu_m;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_ptcone20;   //!
   TBranch        *b_mu_baseline_pt;   //!
   TBranch        *b_mu_baseline_ptcone20;   //!
   TBranch        *b_mu_baseline_SF;   //!
   TBranch        *b_mu_baseline_eta;   //!
   TBranch        *b_mu_baseline_phi;   //!
   TBranch        *b_mu_baseline_isLooseID;   //!
   TBranch        *b_mu_baseline_isMediumID;   //!
   TBranch        *b_mu_baseline_isTightID;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_SF;   //!
   TBranch        *b_el_SF_iso;   //!
   TBranch        *b_el_SF_trigger;   //!
   TBranch        *b_el_eff_trigger;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptcone20;   //!
   TBranch        *b_el_m;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_ptvarcone20_TightTTVA_pt1000;   //!
   TBranch        *b_el_baseline_pt;   //!
   TBranch        *b_el_baseline_topoetcone20;   //!
   TBranch        *b_el_baseline_ptcone20;   //!
   TBranch        *b_el_baseline_SF;   //!
   TBranch        *b_el_baseline_eta;   //!
   TBranch        *b_el_baseline_phi;   //!
   TBranch        *b_el_baseline_isLooseID;   //!
   TBranch        *b_el_baseline_isMediumID;   //!
   TBranch        *b_el_baseline_isTightID;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_m;   //!
   TBranch        *b_jet_fmax;   //!
   TBranch        *b_jet_fch;   //!
   TBranch        *b_jet_isbjet;   //!
   TBranch        *b_jet_PartonTruthLabelID;   //!
   TBranch        *b_jet_ConeTruthLabelID;   //!
   TBranch        *b_jet_timing;   //!
   TBranch        *b_jet_emfrac;   //!
   TBranch        *b_jet_hecf;   //!
   TBranch        *b_jet_hecq;   //!
   TBranch        *b_jet_larq;   //!
   TBranch        *b_jet_avglarq;   //!
   TBranch        *b_jet_negE;   //!
   TBranch        *b_jet_lambda;   //!
   TBranch        *b_jet_lambda2;   //!
   TBranch        *b_jet_jvtxf;   //!
   TBranch        *b_jet_fmaxi;   //!
   TBranch        *b_jet_isbjet_loose;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_cleaning;   //!
   TBranch        *b_jet_DFCommonJets_QGTagger_NTracks;   //!
   TBranch        *b_jet_DFCommonJets_QGTagger_TracksWidth;   //!
   TBranch        *b_jet_DFCommonJets_QGTagger_TracksC1;   //!
   TBranch        *b_jet_truth_pt;   //!
   TBranch        *b_jet_truth_eta;   //!
   TBranch        *b_jet_truth_phi;   //!
   TBranch        *b_LCTopoJet_pt;   //!
   TBranch        *b_LCTopoJet_eta;   //!
   TBranch        *b_LCTopoJet_phi;   //!
   TBranch        *b_LCTopoJet_m;   //!
   TBranch        *b_LCTopoJet_tau21;   //!
   TBranch        *b_LCTopoJet_D2;   //!
   TBranch        *b_LCTopoJet_C2;   //!
   TBranch        *b_LCTopoJet_nTrk;   //!
   TBranch        *b_LCTopoJet_nConstit;   //!
   TBranch        *b_LCTopoJet_passD2_W50;   //!
   TBranch        *b_LCTopoJet_passD2_Z50;   //!
   TBranch        *b_LCTopoJet_passD2_W80;   //!
   TBranch        *b_LCTopoJet_passD2_Z80;   //!
   TBranch        *b_LCTopoJet_passMass_W50;   //!
   TBranch        *b_LCTopoJet_passMass_Z50;   //!
   TBranch        *b_LCTopoJet_passMass_W80;   //!
   TBranch        *b_LCTopoJet_passMass_Z80;   //!
   TBranch        *b_TruthFatJet_pt;   //!
   TBranch        *b_TruthFatJet_eta;   //!
   TBranch        *b_TruthFatJet_phi;   //!
   TBranch        *b_TruthFatJet_m;   //!
   TBranch        *b_TruthFatJet_tau21;   //!
   TBranch        *b_TruthFatJet_D2;   //!
   TBranch        *b_TruthFatJet_C2;   //!
   TBranch        *b_TruthFatJet_nTrk;   //!
   TBranch        *b_TruthFatJet_nConstit;   //!
   TBranch        *b_tau_loose_multiplicity;   //!
   TBranch        *b_tau_medium_multiplicity;   //!
   TBranch        *b_tau_tight_multiplicity;   //!
   TBranch        *b_tau_baseline_loose_multiplicity;   //!
   TBranch        *b_tau_baseline_medium_multiplicity;   //!
   TBranch        *b_tau_baseline_tight_multiplicity;   //!

   TBranch        *b_weight;   //!
   TBranch        *b_mc_weight_sum;   //!
   TBranch        *b_extra_weight;   //!
   TBranch        *b_campaign_lumi; //!

   MergedTree(TString treeName, TString option, bool isData, bool isSyst, int skimLevel);
   MergedTree(MiniTree * minitree, bool doAddAMI);
   MergedTree(TString path, TString treeName);
   virtual ~MergedTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree, bool isData, bool isSyst);
   virtual void     Book(bool isData, bool isSyst, int skimLevel);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

};

MergedTree::MergedTree(TString treeName, TString option, bool isData, bool isSyst, int skimLevel) {
   fChain = new TTree(treeName, option);
   Book(isData, isSyst, skimLevel);
}

MergedTree::MergedTree(TString path, TString treeName) : fChain(0) 
{

   TTree * tree;

   TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(path);
   if (!f || !f->IsOpen()) {
      f = new TFile(path);
   }
   f->GetObject(treeName,tree);

   Init(tree, treeName.Contains("data"), treeName.Contains("syst"));
}

MergedTree::~MergedTree()
{
   if (!fChain) return;
   fChain->GetCurrentFile()->Close();
   delete fChain->GetCurrentFile();
}

Int_t MergedTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t MergedTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void MergedTree::Init(TTree *tree, bool isData, bool isSyst)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down = 0;
   el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up = 0;
   mu_SF_syst_MUON_EFF_BADMUON_SYS__1down = 0;
   mu_SF_syst_MUON_EFF_BADMUON_SYS__1up = 0;
   mu_SF_syst_MUON_EFF_ISO_STAT__1down = 0;
   mu_SF_syst_MUON_EFF_ISO_STAT__1up = 0;
   mu_SF_syst_MUON_EFF_ISO_SYS__1down = 0;
   mu_SF_syst_MUON_EFF_ISO_SYS__1up = 0;
   mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down = 0;
   mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up = 0;
   mu_SF_syst_MUON_EFF_RECO_STAT__1down = 0;
   mu_SF_syst_MUON_EFF_RECO_STAT__1up = 0;
   mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down = 0;
   mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up = 0;
   mu_SF_syst_MUON_EFF_RECO_SYS__1down = 0;
   mu_SF_syst_MUON_EFF_RECO_SYS__1up = 0;
   mu_SF_syst_MUON_EFF_TTVA_STAT__1down = 0;
   mu_SF_syst_MUON_EFF_TTVA_STAT__1up = 0;
   mu_SF_syst_MUON_EFF_TTVA_SYS__1down = 0;
   mu_SF_syst_MUON_EFF_TTVA_SYS__1up = 0;
   mu_SF_syst_MUON_EFF_TrigStatUncertainty__1down = 0;
   mu_SF_syst_MUON_EFF_TrigStatUncertainty__1up = 0;
   mu_SF_syst_MUON_EFF_TrigSystUncertainty__1down = 0;
   mu_SF_syst_MUON_EFF_TrigSystUncertainty__1up = 0;
   tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down = 0;
   tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up = 0;
   tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down = 0;
   tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1up = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1down = 0;
   tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1up = 0;
   ph_SF_syst_PH_EFF_ID_Uncertainty__1down = 0;
   ph_SF_syst_PH_EFF_ID_Uncertainty__1up = 0;
   ph_SF_syst_PH_EFF_ISO_Uncertainty__1down = 0;
   ph_SF_syst_PH_EFF_ISO_Uncertainty__1up = 0;
   ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1down = 0;
   ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1up = 0;
   jet_SF_syst_JET_JvtEfficiency__1down = 0;
   jet_SF_syst_JET_JvtEfficiency__1up = 0;
   pu_weight_syst_PRW_DATASF__1down = 0;
   pu_weight_syst_PRW_DATASF__1up = 0;
   btag_weight_syst_FT_EFF_B_systematics__1down = 0;
   btag_weight_syst_FT_EFF_B_systematics__1up = 0;
   btag_weight_syst_FT_EFF_C_systematics__1down = 0;
   btag_weight_syst_FT_EFF_C_systematics__1up = 0;
   btag_weight_syst_FT_EFF_Light_systematics__1down = 0;
   btag_weight_syst_FT_EFF_Light_systematics__1up = 0;
   btag_weight_syst_FT_EFF_extrapolation__1down = 0;
   btag_weight_syst_FT_EFF_extrapolation__1up = 0;
   btag_weight_syst_FT_EFF_extrapolation_from_charm__1down = 0;
   btag_weight_syst_FT_EFF_extrapolation_from_charm__1up = 0;
   btagloose_weight_syst_FT_EFF_B_systematics__1down = 0;
   btagloose_weight_syst_FT_EFF_B_systematics__1up = 0;
   btagloose_weight_syst_FT_EFF_C_systematics__1down = 0;
   btagloose_weight_syst_FT_EFF_C_systematics__1up = 0;
   btagloose_weight_syst_FT_EFF_Light_systematics__1down = 0;
   btagloose_weight_syst_FT_EFF_Light_systematics__1up = 0;
   btagloose_weight_syst_FT_EFF_extrapolation__1down = 0;
   btagloose_weight_syst_FT_EFF_extrapolation__1up = 0;
   btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down = 0;
   btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up = 0;
   trkbtag_weight_syst_FT_EFF_B_systematics__1down = 0;
   trkbtag_weight_syst_FT_EFF_B_systematics__1up = 0;
   trkbtag_weight_syst_FT_EFF_C_systematics__1down = 0;
   trkbtag_weight_syst_FT_EFF_C_systematics__1up = 0;
   trkbtag_weight_syst_FT_EFF_Light_systematics__1down = 0;
   trkbtag_weight_syst_FT_EFF_Light_systematics__1up = 0;
   trkbtag_weight_syst_FT_EFF_extrapolation__1down = 0;
   trkbtag_weight_syst_FT_EFF_extrapolation__1up = 0;
   trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1down = 0;
   trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1up = 0;
   trkbtagloose_weight_syst_FT_EFF_B_systematics__1down = 0;
   trkbtagloose_weight_syst_FT_EFF_B_systematics__1up = 0;
   trkbtagloose_weight_syst_FT_EFF_C_systematics__1down = 0;
   trkbtagloose_weight_syst_FT_EFF_C_systematics__1up = 0;
   trkbtagloose_weight_syst_FT_EFF_Light_systematics__1down = 0;
   trkbtagloose_weight_syst_FT_EFF_Light_systematics__1up = 0;
   trkbtagloose_weight_syst_FT_EFF_extrapolation__1down = 0;
   trkbtagloose_weight_syst_FT_EFF_extrapolation__1up = 0;
   trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down = 0;
   trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up = 0;
   mconly_weights = 0;
   mu_pt = 0;
   mu_SF = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_SF_iso = 0;
   mu_m = 0;
   mu_charge = 0;
   mu_ptcone20 = 0;
   mu_baseline_pt = 0;
   mu_baseline_ptcone20 = 0;
   mu_baseline_SF = 0;
   mu_baseline_eta = 0;
   mu_baseline_phi = 0;
   mu_baseline_isLooseID = 0;
   mu_baseline_isMediumID = 0;
   mu_baseline_isTightID = 0;
   el_pt = 0;
   el_eta = 0;
   el_phi = 0;
   el_SF = 0;
   el_SF_iso = 0;
   el_SF_trigger = 0;
   el_eff_trigger = 0;
   el_topoetcone20 = 0;
   el_ptcone20 = 0;
   el_m = 0;
   el_charge = 0;
   el_ptvarcone20_TightTTVA_pt1000 = 0;
   el_baseline_pt = 0;
   el_baseline_topoetcone20 = 0;
   el_baseline_ptcone20 = 0;
   el_baseline_SF = 0;
   el_baseline_eta = 0;
   el_baseline_phi = 0;
   el_baseline_isLooseID = 0;
   el_baseline_isMediumID = 0;
   el_baseline_isTightID = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_m = 0;
   jet_fmax = 0;
   jet_fch = 0;
   jet_isbjet = 0;
   jet_PartonTruthLabelID = 0;
   jet_ConeTruthLabelID = 0;
   jet_timing = 0;
   jet_emfrac = 0;
   jet_hecf = 0;
   jet_hecq = 0;
   jet_larq = 0;
   jet_avglarq = 0;
   jet_negE = 0;
   jet_lambda = 0;
   jet_lambda2 = 0;
   jet_jvtxf = 0;
   jet_fmaxi = 0;
   jet_isbjet_loose = 0;
   jet_jvt = 0;
   jet_cleaning = 0;
   jet_DFCommonJets_QGTagger_NTracks = 0;
   jet_DFCommonJets_QGTagger_TracksWidth = 0;
   jet_DFCommonJets_QGTagger_TracksC1 = 0;
   jet_truth_pt = 0;
   jet_truth_eta = 0;
   jet_truth_phi = 0;
   LCTopoJet_pt = 0;
   LCTopoJet_eta = 0;
   LCTopoJet_phi = 0;
   LCTopoJet_m = 0;
   LCTopoJet_tau21 = 0;
   LCTopoJet_D2 = 0;
   LCTopoJet_C2 = 0;
   LCTopoJet_nTrk = 0;
   LCTopoJet_nConstit = 0;
   LCTopoJet_passD2_W50 = 0;
   LCTopoJet_passD2_Z50 = 0;
   LCTopoJet_passD2_W80 = 0;
   LCTopoJet_passD2_Z80 = 0;
   LCTopoJet_passMass_W50 = 0;
   LCTopoJet_passMass_Z50 = 0;
   LCTopoJet_passMass_W80 = 0;
   LCTopoJet_passMass_Z80 = 0;
   TruthFatJet_pt = 0;
   TruthFatJet_eta = 0;
   TruthFatJet_phi = 0;
   TruthFatJet_m = 0;
   TruthFatJet_tau21 = 0;
   TruthFatJet_D2 = 0;
   TruthFatJet_C2 = 0;
   TruthFatJet_nTrk = 0;
   TruthFatJet_nConstit = 0;

   weight = 0;
   mc_weight_sum = 0;
   extra_weight = 0;
   campaign_lumi = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("run", &run, &b_run);
   fChain->SetBranchAddress("event", &event, &b_event);
   fChain->SetBranchAddress("last", &last, &b_last);
   fChain->SetBranchAddress("year", &year, &b_year);
   fChain->SetBranchAddress("met_tst_sig", &met_tst_sig, &b_met_tst_sig);
   if (!isData) {
      fChain->SetBranchAddress("xSec_SUSY", &xSec_SUSY, &b_xSec_SUSY);
      fChain->SetBranchAddress("k_factor", &k_factor, &b_k_factor);
      fChain->SetBranchAddress("filter_eff", &filter_eff, &b_filter_eff);
   }
   if (!isData && !isSyst) {
      fChain->SetBranchAddress("el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down", &el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down);
      fChain->SetBranchAddress("el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up", &el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up);
      fChain->SetBranchAddress("el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
      fChain->SetBranchAddress("el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
      fChain->SetBranchAddress("el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
      fChain->SetBranchAddress("el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
      fChain->SetBranchAddress("el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
      fChain->SetBranchAddress("el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
      fChain->SetBranchAddress("el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down", &el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down);
      fChain->SetBranchAddress("el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", &el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up);
      fChain->SetBranchAddress("el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down", &el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down);
      fChain->SetBranchAddress("el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up", &el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_BADMUON_SYS__1down", &mu_SF_syst_MUON_EFF_BADMUON_SYS__1down, &b_mu_SF_syst_MUON_EFF_BADMUON_SYS__1down);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_BADMUON_SYS__1up", &mu_SF_syst_MUON_EFF_BADMUON_SYS__1up, &b_mu_SF_syst_MUON_EFF_BADMUON_SYS__1up);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_ISO_STAT__1down", &mu_SF_syst_MUON_EFF_ISO_STAT__1down, &b_mu_SF_syst_MUON_EFF_ISO_STAT__1down);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_ISO_STAT__1up", &mu_SF_syst_MUON_EFF_ISO_STAT__1up, &b_mu_SF_syst_MUON_EFF_ISO_STAT__1up);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_ISO_SYS__1down", &mu_SF_syst_MUON_EFF_ISO_SYS__1down, &b_mu_SF_syst_MUON_EFF_ISO_SYS__1down);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_ISO_SYS__1up", &mu_SF_syst_MUON_EFF_ISO_SYS__1up, &b_mu_SF_syst_MUON_EFF_ISO_SYS__1up);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down", &mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down, &b_mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up", &mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up, &b_mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_RECO_STAT__1down", &mu_SF_syst_MUON_EFF_RECO_STAT__1down, &b_mu_SF_syst_MUON_EFF_RECO_STAT__1down);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_RECO_STAT__1up", &mu_SF_syst_MUON_EFF_RECO_STAT__1up, &b_mu_SF_syst_MUON_EFF_RECO_STAT__1up);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down", &mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down, &b_mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up", &mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up, &b_mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_RECO_SYS__1down", &mu_SF_syst_MUON_EFF_RECO_SYS__1down, &b_mu_SF_syst_MUON_EFF_RECO_SYS__1down);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_RECO_SYS__1up", &mu_SF_syst_MUON_EFF_RECO_SYS__1up, &b_mu_SF_syst_MUON_EFF_RECO_SYS__1up);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_TTVA_STAT__1down", &mu_SF_syst_MUON_EFF_TTVA_STAT__1down, &b_mu_SF_syst_MUON_EFF_TTVA_STAT__1down);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_TTVA_STAT__1up", &mu_SF_syst_MUON_EFF_TTVA_STAT__1up, &b_mu_SF_syst_MUON_EFF_TTVA_STAT__1up);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_TTVA_SYS__1down", &mu_SF_syst_MUON_EFF_TTVA_SYS__1down, &b_mu_SF_syst_MUON_EFF_TTVA_SYS__1down);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_TTVA_SYS__1up", &mu_SF_syst_MUON_EFF_TTVA_SYS__1up, &b_mu_SF_syst_MUON_EFF_TTVA_SYS__1up);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_TrigStatUncertainty__1down", &mu_SF_syst_MUON_EFF_TrigStatUncertainty__1down, &b_mu_SF_syst_MUON_EFF_TrigStatUncertainty__1down);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_TrigStatUncertainty__1up", &mu_SF_syst_MUON_EFF_TrigStatUncertainty__1up, &b_mu_SF_syst_MUON_EFF_TrigStatUncertainty__1up);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_TrigSystUncertainty__1down", &mu_SF_syst_MUON_EFF_TrigSystUncertainty__1down, &b_mu_SF_syst_MUON_EFF_TrigSystUncertainty__1down);
      fChain->SetBranchAddress("mu_SF_syst_MUON_EFF_TrigSystUncertainty__1up", &mu_SF_syst_MUON_EFF_TrigSystUncertainty__1up, &b_mu_SF_syst_MUON_EFF_TrigSystUncertainty__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down", &tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down, &b_tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up", &tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up, &b_tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down", &tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down, &b_tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up", &tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up, &b_tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1up);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1down, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1down);
      fChain->SetBranchAddress("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1up, &b_tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1up);
      fChain->SetBranchAddress("ph_SF_syst_PH_EFF_ID_Uncertainty__1down", &ph_SF_syst_PH_EFF_ID_Uncertainty__1down, &b_ph_SF_syst_PH_EFF_ID_Uncertainty__1down);
      fChain->SetBranchAddress("ph_SF_syst_PH_EFF_ID_Uncertainty__1up", &ph_SF_syst_PH_EFF_ID_Uncertainty__1up, &b_ph_SF_syst_PH_EFF_ID_Uncertainty__1up);
      fChain->SetBranchAddress("ph_SF_syst_PH_EFF_ISO_Uncertainty__1down", &ph_SF_syst_PH_EFF_ISO_Uncertainty__1down, &b_ph_SF_syst_PH_EFF_ISO_Uncertainty__1down);
      fChain->SetBranchAddress("ph_SF_syst_PH_EFF_ISO_Uncertainty__1up", &ph_SF_syst_PH_EFF_ISO_Uncertainty__1up, &b_ph_SF_syst_PH_EFF_ISO_Uncertainty__1up);
      fChain->SetBranchAddress("ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1down", &ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1down, &b_ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1down);
      fChain->SetBranchAddress("ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1up", &ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1up, &b_ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1up);
      fChain->SetBranchAddress("jet_SF_syst_JET_JvtEfficiency__1down", &jet_SF_syst_JET_JvtEfficiency__1down, &b_jet_SF_syst_JET_JvtEfficiency__1down);
      fChain->SetBranchAddress("jet_SF_syst_JET_JvtEfficiency__1up", &jet_SF_syst_JET_JvtEfficiency__1up, &b_jet_SF_syst_JET_JvtEfficiency__1up);
      fChain->SetBranchAddress("pu_weight_syst_PRW_DATASF__1down", &pu_weight_syst_PRW_DATASF__1down, &b_pu_weight_syst_PRW_DATASF__1down);
      fChain->SetBranchAddress("pu_weight_syst_PRW_DATASF__1up", &pu_weight_syst_PRW_DATASF__1up, &b_pu_weight_syst_PRW_DATASF__1up);
      fChain->SetBranchAddress("btag_weight_syst_FT_EFF_B_systematics__1down", &btag_weight_syst_FT_EFF_B_systematics__1down, &b_btag_weight_syst_FT_EFF_B_systematics__1down);
      fChain->SetBranchAddress("btag_weight_syst_FT_EFF_B_systematics__1up", &btag_weight_syst_FT_EFF_B_systematics__1up, &b_btag_weight_syst_FT_EFF_B_systematics__1up);
      fChain->SetBranchAddress("btag_weight_syst_FT_EFF_C_systematics__1down", &btag_weight_syst_FT_EFF_C_systematics__1down, &b_btag_weight_syst_FT_EFF_C_systematics__1down);
      fChain->SetBranchAddress("btag_weight_syst_FT_EFF_C_systematics__1up", &btag_weight_syst_FT_EFF_C_systematics__1up, &b_btag_weight_syst_FT_EFF_C_systematics__1up);
      fChain->SetBranchAddress("btag_weight_syst_FT_EFF_Light_systematics__1down", &btag_weight_syst_FT_EFF_Light_systematics__1down, &b_btag_weight_syst_FT_EFF_Light_systematics__1down);
      fChain->SetBranchAddress("btag_weight_syst_FT_EFF_Light_systematics__1up", &btag_weight_syst_FT_EFF_Light_systematics__1up, &b_btag_weight_syst_FT_EFF_Light_systematics__1up);
      fChain->SetBranchAddress("btag_weight_syst_FT_EFF_extrapolation__1down", &btag_weight_syst_FT_EFF_extrapolation__1down, &b_btag_weight_syst_FT_EFF_extrapolation__1down);
      fChain->SetBranchAddress("btag_weight_syst_FT_EFF_extrapolation__1up", &btag_weight_syst_FT_EFF_extrapolation__1up, &b_btag_weight_syst_FT_EFF_extrapolation__1up);
      fChain->SetBranchAddress("btag_weight_syst_FT_EFF_extrapolation_from_charm__1down", &btag_weight_syst_FT_EFF_extrapolation_from_charm__1down, &b_btag_weight_syst_FT_EFF_extrapolation_from_charm__1down);
      fChain->SetBranchAddress("btag_weight_syst_FT_EFF_extrapolation_from_charm__1up", &btag_weight_syst_FT_EFF_extrapolation_from_charm__1up, &b_btag_weight_syst_FT_EFF_extrapolation_from_charm__1up);
      fChain->SetBranchAddress("btagloose_weight_syst_FT_EFF_B_systematics__1down", &btagloose_weight_syst_FT_EFF_B_systematics__1down, &b_btagloose_weight_syst_FT_EFF_B_systematics__1down);
      fChain->SetBranchAddress("btagloose_weight_syst_FT_EFF_B_systematics__1up", &btagloose_weight_syst_FT_EFF_B_systematics__1up, &b_btagloose_weight_syst_FT_EFF_B_systematics__1up);
      fChain->SetBranchAddress("btagloose_weight_syst_FT_EFF_C_systematics__1down", &btagloose_weight_syst_FT_EFF_C_systematics__1down, &b_btagloose_weight_syst_FT_EFF_C_systematics__1down);
      fChain->SetBranchAddress("btagloose_weight_syst_FT_EFF_C_systematics__1up", &btagloose_weight_syst_FT_EFF_C_systematics__1up, &b_btagloose_weight_syst_FT_EFF_C_systematics__1up);
      fChain->SetBranchAddress("btagloose_weight_syst_FT_EFF_Light_systematics__1down", &btagloose_weight_syst_FT_EFF_Light_systematics__1down, &b_btagloose_weight_syst_FT_EFF_Light_systematics__1down);
      fChain->SetBranchAddress("btagloose_weight_syst_FT_EFF_Light_systematics__1up", &btagloose_weight_syst_FT_EFF_Light_systematics__1up, &b_btagloose_weight_syst_FT_EFF_Light_systematics__1up);
      fChain->SetBranchAddress("btagloose_weight_syst_FT_EFF_extrapolation__1down", &btagloose_weight_syst_FT_EFF_extrapolation__1down, &b_btagloose_weight_syst_FT_EFF_extrapolation__1down);
      fChain->SetBranchAddress("btagloose_weight_syst_FT_EFF_extrapolation__1up", &btagloose_weight_syst_FT_EFF_extrapolation__1up, &b_btagloose_weight_syst_FT_EFF_extrapolation__1up);
      fChain->SetBranchAddress("btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down", &btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down, &b_btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down);
      fChain->SetBranchAddress("btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up", &btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up, &b_btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up);
      fChain->SetBranchAddress("trkbtag_weight_syst_FT_EFF_B_systematics__1down", &trkbtag_weight_syst_FT_EFF_B_systematics__1down, &b_trkbtag_weight_syst_FT_EFF_B_systematics__1down);
      fChain->SetBranchAddress("trkbtag_weight_syst_FT_EFF_B_systematics__1up", &trkbtag_weight_syst_FT_EFF_B_systematics__1up, &b_trkbtag_weight_syst_FT_EFF_B_systematics__1up);
      fChain->SetBranchAddress("trkbtag_weight_syst_FT_EFF_C_systematics__1down", &trkbtag_weight_syst_FT_EFF_C_systematics__1down, &b_trkbtag_weight_syst_FT_EFF_C_systematics__1down);
      fChain->SetBranchAddress("trkbtag_weight_syst_FT_EFF_C_systematics__1up", &trkbtag_weight_syst_FT_EFF_C_systematics__1up, &b_trkbtag_weight_syst_FT_EFF_C_systematics__1up);
      fChain->SetBranchAddress("trkbtag_weight_syst_FT_EFF_Light_systematics__1down", &trkbtag_weight_syst_FT_EFF_Light_systematics__1down, &b_trkbtag_weight_syst_FT_EFF_Light_systematics__1down);
      fChain->SetBranchAddress("trkbtag_weight_syst_FT_EFF_Light_systematics__1up", &trkbtag_weight_syst_FT_EFF_Light_systematics__1up, &b_trkbtag_weight_syst_FT_EFF_Light_systematics__1up);
      fChain->SetBranchAddress("trkbtag_weight_syst_FT_EFF_extrapolation__1down", &trkbtag_weight_syst_FT_EFF_extrapolation__1down, &b_trkbtag_weight_syst_FT_EFF_extrapolation__1down);
      fChain->SetBranchAddress("trkbtag_weight_syst_FT_EFF_extrapolation__1up", &trkbtag_weight_syst_FT_EFF_extrapolation__1up, &b_trkbtag_weight_syst_FT_EFF_extrapolation__1up);
      fChain->SetBranchAddress("trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1down", &trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1down, &b_trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1down);
      fChain->SetBranchAddress("trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1up", &trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1up, &b_trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1up);
      fChain->SetBranchAddress("trkbtagloose_weight_syst_FT_EFF_B_systematics__1down", &trkbtagloose_weight_syst_FT_EFF_B_systematics__1down, &b_trkbtagloose_weight_syst_FT_EFF_B_systematics__1down);
      fChain->SetBranchAddress("trkbtagloose_weight_syst_FT_EFF_B_systematics__1up", &trkbtagloose_weight_syst_FT_EFF_B_systematics__1up, &b_trkbtagloose_weight_syst_FT_EFF_B_systematics__1up);
      fChain->SetBranchAddress("trkbtagloose_weight_syst_FT_EFF_C_systematics__1down", &trkbtagloose_weight_syst_FT_EFF_C_systematics__1down, &b_trkbtagloose_weight_syst_FT_EFF_C_systematics__1down);
      fChain->SetBranchAddress("trkbtagloose_weight_syst_FT_EFF_C_systematics__1up", &trkbtagloose_weight_syst_FT_EFF_C_systematics__1up, &b_trkbtagloose_weight_syst_FT_EFF_C_systematics__1up);
      fChain->SetBranchAddress("trkbtagloose_weight_syst_FT_EFF_Light_systematics__1down", &trkbtagloose_weight_syst_FT_EFF_Light_systematics__1down, &b_trkbtagloose_weight_syst_FT_EFF_Light_systematics__1down);
      fChain->SetBranchAddress("trkbtagloose_weight_syst_FT_EFF_Light_systematics__1up", &trkbtagloose_weight_syst_FT_EFF_Light_systematics__1up, &b_trkbtagloose_weight_syst_FT_EFF_Light_systematics__1up);
      fChain->SetBranchAddress("trkbtagloose_weight_syst_FT_EFF_extrapolation__1down", &trkbtagloose_weight_syst_FT_EFF_extrapolation__1down, &b_trkbtagloose_weight_syst_FT_EFF_extrapolation__1down);
      fChain->SetBranchAddress("trkbtagloose_weight_syst_FT_EFF_extrapolation__1up", &trkbtagloose_weight_syst_FT_EFF_extrapolation__1up, &b_trkbtagloose_weight_syst_FT_EFF_extrapolation__1up);
      fChain->SetBranchAddress("trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down", &trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down, &b_trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down);
      fChain->SetBranchAddress("trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up", &trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up, &b_trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up);
   }
   fChain->SetBranchAddress("n_tau_baseline", &n_tau_baseline, &b_n_tau_baseline);
   fChain->SetBranchAddress("mconly_weight", &mconly_weight, &b_mconly_weight);
   fChain->SetBranchAddress("pu_weight", &pu_weight, &b_pu_weight);
   fChain->SetBranchAddress("btag_weight", &btag_weight, &b_btag_weight);
   fChain->SetBranchAddress("btagloose_weight", &btagloose_weight, &b_btagloose_weight);
   fChain->SetBranchAddress("trkbtag_weight", &trkbtag_weight, &b_trkbtag_weight);
   fChain->SetBranchAddress("trkbtagloose_weight", &trkbtagloose_weight, &b_trkbtagloose_weight);
   fChain->SetBranchAddress("jvt_weight", &jvt_weight, &b_jvt_weight);
   fChain->SetBranchAddress("munu_mT", &munu_mT, &b_munu_mT);
   fChain->SetBranchAddress("enu_mT", &enu_mT, &b_enu_mT);
   fChain->SetBranchAddress("mumu_m", &mumu_m, &b_mumu_m);
   fChain->SetBranchAddress("ee_m", &ee_m, &b_ee_m);
   fChain->SetBranchAddress("dPhiLCTopoJetMet", &dPhiLCTopoJetMet, &b_dPhiLCTopoJetMet);
   fChain->SetBranchAddress("dPhiLCTopoJetMetNoElectron", &dPhiLCTopoJetMetNoElectron, &b_dPhiLCTopoJetMetNoElectron);
   fChain->SetBranchAddress("dPhiLCTopoJetMetNoMuon", &dPhiLCTopoJetMetNoMuon, &b_dPhiLCTopoJetMetNoMuon);
   fChain->SetBranchAddress("dPhiDijetMet", &dPhiDijetMet, &b_dPhiDijetMet);
   fChain->SetBranchAddress("dPhiDijetMetNoElectron", &dPhiDijetMetNoElectron, &b_dPhiDijetMetNoElectron);
   fChain->SetBranchAddress("dPhiDijetMetNoMuon", &dPhiDijetMetNoMuon, &b_dPhiDijetMetNoMuon);
   fChain->SetBranchAddress("dPhiDijet", &dPhiDijet, &b_dPhiDijet);
   fChain->SetBranchAddress("dRDijet", &dRDijet, &b_dRDijet);
   fChain->SetBranchAddress("DijetSumPt", &DijetSumPt, &b_DijetSumPt);
   fChain->SetBranchAddress("TrijetSumPt", &TrijetSumPt, &b_TrijetSumPt);
   fChain->SetBranchAddress("DijetMass", &DijetMass, &b_DijetMass);
   fChain->SetBranchAddress("n_trackjet", &n_trackjet, &b_n_trackjet);
   fChain->SetBranchAddress("n_btrackJet", &n_btrackJet, &b_n_btrackJet);
   fChain->SetBranchAddress("n_btrackJet_loose", &n_btrackJet_loose, &b_n_btrackJet_loose);
   fChain->SetBranchAddress("n_trackLCTopoAssociatedBjet", &n_trackLCTopoAssociatedBjet, &b_n_trackLCTopoAssociatedBjet);
   fChain->SetBranchAddress("n_trackLCTopoSeparatedBjet", &n_trackLCTopoSeparatedBjet, &b_n_trackLCTopoSeparatedBjet);
   fChain->SetBranchAddress("n_trackLCTopoAssociatedBjetLoose", &n_trackLCTopoAssociatedBjetLoose, &b_n_trackLCTopoAssociatedBjetLoose);
   fChain->SetBranchAddress("n_trackLCTopoSeparatedBjetLoose", &n_trackLCTopoSeparatedBjetLoose, &b_n_trackLCTopoSeparatedBjetLoose);
   fChain->SetBranchAddress("n_jet", &n_jet, &b_n_jet);
   fChain->SetBranchAddress("n_bjet", &n_bjet, &b_n_bjet);
   fChain->SetBranchAddress("n_bjet_loose", &n_bjet_loose, &b_n_bjet_loose);
   fChain->SetBranchAddress("n_el", &n_el, &b_n_el);
   fChain->SetBranchAddress("n_el_baseline", &n_el_baseline, &b_n_el_baseline);
   fChain->SetBranchAddress("n_mu_baseline", &n_mu_baseline, &b_n_mu_baseline);
   fChain->SetBranchAddress("n_tau", &n_tau, &b_n_tau);
   fChain->SetBranchAddress("n_mu", &n_mu, &b_n_mu);
   fChain->SetBranchAddress("mconly_weights", &mconly_weights, &b_mconly_weights);
   fChain->SetBranchAddress("n_truthFatJet", &n_truthFatJet, &b_n_truthFatJet);
   fChain->SetBranchAddress("n_LCTopoJet", &n_LCTopoJet, &b_n_LCTopoJet);
   fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
   fChain->SetBranchAddress("actualIntPerXing", &actualIntPerXing, &b_actualIntPerXing);
   fChain->SetBranchAddress("corAverageIntPerXing", &corAverageIntPerXing, &b_corAverageIntPerXing);
   fChain->SetBranchAddress("corActualIntPerXing", &corActualIntPerXing, &b_corActualIntPerXing);
   fChain->SetBranchAddress("n_vx", &n_vx, &b_n_vx);
   fChain->SetBranchAddress("pu_hash", &pu_hash, &b_pu_hash);
   fChain->SetBranchAddress("trigger_HLT_e120_lhloose", &trigger_HLT_e120_lhloose, &b_trigger_HLT_e120_lhloose);
   fChain->SetBranchAddress("trigger_HLT_e140_lhloose_nod0", &trigger_HLT_e140_lhloose_nod0, &b_trigger_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("trigger_HLT_e24_lhmedium_L1EM20VH", &trigger_HLT_e24_lhmedium_L1EM20VH, &b_trigger_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("trigger_HLT_e24_lhtight_nod0_ivarloose", &trigger_HLT_e24_lhtight_nod0_ivarloose, &b_trigger_HLT_e24_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("trigger_HLT_e26_lhtight_nod0_ivarloose", &trigger_HLT_e26_lhtight_nod0_ivarloose, &b_trigger_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("trigger_HLT_e60_lhmedium", &trigger_HLT_e60_lhmedium, &b_trigger_HLT_e60_lhmedium);
   fChain->SetBranchAddress("trigger_HLT_e60_lhmedium_nod0", &trigger_HLT_e60_lhmedium_nod0, &b_trigger_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("trigger_HLT_e60_medium", &trigger_HLT_e60_medium, &b_trigger_HLT_e60_medium);
   fChain->SetBranchAddress("trigger_HLT_mu20_iloose_L1MU15", &trigger_HLT_mu20_iloose_L1MU15, &b_trigger_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("trigger_HLT_mu24_iloose", &trigger_HLT_mu24_iloose, &b_trigger_HLT_mu24_iloose);
   fChain->SetBranchAddress("trigger_HLT_mu24_ivarmedium", &trigger_HLT_mu24_ivarmedium, &b_trigger_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("trigger_HLT_mu26_imedium", &trigger_HLT_mu26_imedium, &b_trigger_HLT_mu26_imedium);
   fChain->SetBranchAddress("trigger_HLT_mu26_ivarmedium", &trigger_HLT_mu26_ivarmedium, &b_trigger_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("trigger_HLT_mu40", &trigger_HLT_mu40, &b_trigger_HLT_mu40);
   fChain->SetBranchAddress("trigger_HLT_mu50", &trigger_HLT_mu50, &b_trigger_HLT_mu50);
   fChain->SetBranchAddress("trigger_HLT_xe100_mht_L1XE50", &trigger_HLT_xe100_mht_L1XE50, &b_trigger_HLT_xe100_mht_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe110_mht_L1XE50", &trigger_HLT_xe110_mht_L1XE50, &b_trigger_HLT_xe110_mht_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe130_mht_L1XE50", &trigger_HLT_xe130_mht_L1XE50, &b_trigger_HLT_xe130_mht_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe70", &trigger_HLT_xe70, &b_trigger_HLT_xe70);
   fChain->SetBranchAddress("trigger_HLT_xe70_mht", &trigger_HLT_xe70_mht, &b_trigger_HLT_xe70_mht);
   fChain->SetBranchAddress("trigger_HLT_xe80_tc_lcw_L1XE50", &trigger_HLT_xe80_tc_lcw_L1XE50, &b_trigger_HLT_xe80_tc_lcw_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe90_mht_L1XE50", &trigger_HLT_xe90_mht_L1XE50, &b_trigger_HLT_xe90_mht_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe90_mht_wEFMu_L1XE50", &trigger_HLT_xe90_mht_wEFMu_L1XE50, &b_trigger_HLT_xe90_mht_wEFMu_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_e26_lhtight_nod0", &trigger_HLT_e26_lhtight_nod0, &b_trigger_HLT_e26_lhtight_nod0);
   fChain->SetBranchAddress("trigger_HLT_e300_etcut", &trigger_HLT_e300_etcut, &b_trigger_HLT_e300_etcut);
   fChain->SetBranchAddress("trigger_HLT_mu24_iloose_L1MU15", &trigger_HLT_mu24_iloose_L1MU15, &b_trigger_HLT_mu24_iloose_L1MU15);
   fChain->SetBranchAddress("trigger_HLT_mu24_imedium", &trigger_HLT_mu24_imedium, &b_trigger_HLT_mu24_imedium);
   fChain->SetBranchAddress("trigger_HLT_mu24_ivarloose", &trigger_HLT_mu24_ivarloose, &b_trigger_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("trigger_HLT_mu24_ivarloose_L1MU15", &trigger_HLT_mu24_ivarloose_L1MU15, &b_trigger_HLT_mu24_ivarloose_L1MU15);
   fChain->SetBranchAddress("trigger_HLT_mu60_0eta105_msonly", &trigger_HLT_mu60_0eta105_msonly, &b_trigger_HLT_mu60_0eta105_msonly);
   fChain->SetBranchAddress("trigger_HLT_xe100_L1XE50", &trigger_HLT_xe100_L1XE50, &b_trigger_HLT_xe100_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe100_pufit_L1XE50", &trigger_HLT_xe100_pufit_L1XE50, &b_trigger_HLT_xe100_pufit_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe100_pufit_L1XE55", &trigger_HLT_xe100_pufit_L1XE55, &b_trigger_HLT_xe100_pufit_L1XE55);
   fChain->SetBranchAddress("trigger_HLT_xe100_tc_em_L1XE50", &trigger_HLT_xe100_tc_em_L1XE50, &b_trigger_HLT_xe100_tc_em_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe110_pueta_L1XE50", &trigger_HLT_xe110_pueta_L1XE50, &b_trigger_HLT_xe110_pueta_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe110_pufit_L1XE50", &trigger_HLT_xe110_pufit_L1XE50, &b_trigger_HLT_xe110_pufit_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe110_pufit_L1XE55", &trigger_HLT_xe110_pufit_L1XE55, &b_trigger_HLT_xe110_pufit_L1XE55);
   fChain->SetBranchAddress("trigger_HLT_xe110_pufit_xe65_L1XE50", &trigger_HLT_xe110_pufit_xe65_L1XE50, &b_trigger_HLT_xe110_pufit_xe65_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe110_pufit_xe70_L1XE50", &trigger_HLT_xe110_pufit_xe70_L1XE50, &b_trigger_HLT_xe110_pufit_xe70_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe120_pueta", &trigger_HLT_xe120_pueta, &b_trigger_HLT_xe120_pueta);
   fChain->SetBranchAddress("trigger_HLT_xe120_pufit", &trigger_HLT_xe120_pufit, &b_trigger_HLT_xe120_pufit);
   fChain->SetBranchAddress("trigger_HLT_xe120_pufit_L1XE50", &trigger_HLT_xe120_pufit_L1XE50, &b_trigger_HLT_xe120_pufit_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe120_tc_lcw_L1XE50", &trigger_HLT_xe120_tc_lcw_L1XE50, &b_trigger_HLT_xe120_tc_lcw_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe70_tc_lcw", &trigger_HLT_xe70_tc_lcw, &b_trigger_HLT_xe70_tc_lcw);
   fChain->SetBranchAddress("trigger_HLT_xe90_pufit_L1XE50", &trigger_HLT_xe90_pufit_L1XE50, &b_trigger_HLT_xe90_pufit_L1XE50);
   fChain->SetBranchAddress("trigger_HLT_xe90_tc_lcw_wEFMu_L1XE50", &trigger_HLT_xe90_tc_lcw_wEFMu_L1XE50, &b_trigger_HLT_xe90_tc_lcw_wEFMu_L1XE50);
   fChain->SetBranchAddress("n_ph", &n_ph, &b_n_ph);
   fChain->SetBranchAddress("n_ph_baseline", &n_ph_baseline, &b_n_ph_baseline);
   fChain->SetBranchAddress("n_jet_truth", &n_jet_truth, &b_n_jet_truth);
   fChain->SetBranchAddress("met_noelectron_tst_et", &met_noelectron_tst_et, &b_met_noelectron_tst_et);
   fChain->SetBranchAddress("met_noelectron_tst_phi", &met_noelectron_tst_phi, &b_met_noelectron_tst_phi);
   fChain->SetBranchAddress("met_noelectron_tst_etx", &met_noelectron_tst_etx, &b_met_noelectron_tst_etx);
   fChain->SetBranchAddress("met_noelectron_tst_ety", &met_noelectron_tst_ety, &b_met_noelectron_tst_ety);
   fChain->SetBranchAddress("met_nomuon_tst_et", &met_nomuon_tst_et, &b_met_nomuon_tst_et);
   fChain->SetBranchAddress("met_nomuon_tst_phi", &met_nomuon_tst_phi, &b_met_nomuon_tst_phi);
   fChain->SetBranchAddress("met_nomuon_tst_etx", &met_nomuon_tst_etx, &b_met_nomuon_tst_etx);
   fChain->SetBranchAddress("met_nomuon_tst_ety", &met_nomuon_tst_ety, &b_met_nomuon_tst_ety);
   fChain->SetBranchAddress("met_track_et", &met_track_et, &b_met_track_et);
   fChain->SetBranchAddress("met_track_phi", &met_track_phi, &b_met_track_phi);
   fChain->SetBranchAddress("met_track_etx", &met_track_etx, &b_met_track_etx);
   fChain->SetBranchAddress("met_track_ety", &met_track_ety, &b_met_track_ety);
   fChain->SetBranchAddress("met_track_noelectron_et", &met_track_noelectron_et, &b_met_track_noelectron_et);
   fChain->SetBranchAddress("met_track_noelectron_phi", &met_track_noelectron_phi, &b_met_track_noelectron_phi);
   fChain->SetBranchAddress("met_track_noelectron_etx", &met_track_noelectron_etx, &b_met_track_noelectron_etx);
   fChain->SetBranchAddress("met_track_noelectron_ety", &met_track_noelectron_ety, &b_met_track_noelectron_ety);
   fChain->SetBranchAddress("met_track_nomuon_et", &met_track_nomuon_et, &b_met_track_nomuon_et);
   fChain->SetBranchAddress("met_track_nomuon_phi", &met_track_nomuon_phi, &b_met_track_nomuon_phi);
   fChain->SetBranchAddress("met_track_nomuon_etx", &met_track_nomuon_etx, &b_met_track_nomuon_etx);
   fChain->SetBranchAddress("met_track_nomuon_ety", &met_track_nomuon_ety, &b_met_track_nomuon_ety);
   fChain->SetBranchAddress("met_truth_et", &met_truth_et, &b_met_truth_et);
   fChain->SetBranchAddress("met_truth_phi", &met_truth_phi, &b_met_truth_phi);
   fChain->SetBranchAddress("met_truth_etx", &met_truth_etx, &b_met_truth_etx);
   fChain->SetBranchAddress("met_truth_ety", &met_truth_ety, &b_met_truth_ety);
   fChain->SetBranchAddress("met_tst_et", &met_tst_et, &b_met_tst_et);
   fChain->SetBranchAddress("met_tst_phi", &met_tst_phi, &b_met_tst_phi);
   fChain->SetBranchAddress("met_tst_etx", &met_tst_etx, &b_met_tst_etx);
   fChain->SetBranchAddress("met_tst_ety", &met_tst_ety, &b_met_tst_ety);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_SF", &mu_SF, &b_mu_SF);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_SF_iso", &mu_SF_iso, &b_mu_SF_iso);
   fChain->SetBranchAddress("mu_ptcone20", &mu_ptcone20, &b_mu_ptcone20);
   fChain->SetBranchAddress("mu_m", &mu_m, &b_mu_m);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_baseline_pt", &mu_baseline_pt, &b_mu_baseline_pt);
   fChain->SetBranchAddress("mu_baseline_ptcone20", &mu_baseline_ptcone20, &b_mu_baseline_ptcone20);
   fChain->SetBranchAddress("mu_baseline_SF", &mu_baseline_SF, &b_mu_baseline_SF);
   fChain->SetBranchAddress("mu_baseline_eta", &mu_baseline_eta, &b_mu_baseline_eta);
   fChain->SetBranchAddress("mu_baseline_phi", &mu_baseline_phi, &b_mu_baseline_phi);
   fChain->SetBranchAddress("mu_baseline_isLooseID", &mu_baseline_isLooseID, &b_mu_baseline_isLooseID);
   fChain->SetBranchAddress("mu_baseline_isMediumID", &mu_baseline_isMediumID, &b_mu_baseline_isMediumID);
   fChain->SetBranchAddress("mu_baseline_isTightID", &mu_baseline_isTightID, &b_mu_baseline_isTightID);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_SF", &el_SF, &b_el_SF);
   fChain->SetBranchAddress("el_SF_iso", &el_SF_iso, &b_el_SF_iso);
   fChain->SetBranchAddress("el_SF_trigger", &el_SF_trigger, &b_el_SF_trigger);
   fChain->SetBranchAddress("el_eff_trigger", &el_eff_trigger, &b_el_eff_trigger);
   fChain->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
   fChain->SetBranchAddress("el_ptcone20", &el_ptcone20, &b_el_ptcone20);
   fChain->SetBranchAddress("el_m", &el_m, &b_el_m);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_ptvarcone20_TightTTVA_pt1000", &el_ptvarcone20_TightTTVA_pt1000, &b_el_ptvarcone20_TightTTVA_pt1000);
   fChain->SetBranchAddress("el_baseline_pt", &el_baseline_pt, &b_el_baseline_pt);
   fChain->SetBranchAddress("el_baseline_topoetcone20", &el_baseline_topoetcone20, &b_el_baseline_topoetcone20);
   fChain->SetBranchAddress("el_baseline_ptcone20", &el_baseline_ptcone20, &b_el_baseline_ptcone20);
   fChain->SetBranchAddress("el_baseline_SF", &el_baseline_SF, &b_el_baseline_SF);
   fChain->SetBranchAddress("el_baseline_eta", &el_baseline_eta, &b_el_baseline_eta);
   fChain->SetBranchAddress("el_baseline_phi", &el_baseline_phi, &b_el_baseline_phi);
   fChain->SetBranchAddress("el_baseline_isLooseID", &el_baseline_isLooseID, &b_el_baseline_isLooseID);
   fChain->SetBranchAddress("el_baseline_isMediumID", &el_baseline_isMediumID, &b_el_baseline_isMediumID);
   fChain->SetBranchAddress("el_baseline_isTightID", &el_baseline_isTightID, &b_el_baseline_isTightID);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_m", &jet_m, &b_jet_m);
   fChain->SetBranchAddress("jet_fmax", &jet_fmax, &b_jet_fmax);
   fChain->SetBranchAddress("jet_fch", &jet_fch, &b_jet_fch);
   fChain->SetBranchAddress("jet_isbjet", &jet_isbjet, &b_jet_isbjet);
   fChain->SetBranchAddress("jet_PartonTruthLabelID", &jet_PartonTruthLabelID, &b_jet_PartonTruthLabelID);
   fChain->SetBranchAddress("jet_ConeTruthLabelID", &jet_ConeTruthLabelID, &b_jet_ConeTruthLabelID);
   fChain->SetBranchAddress("jet_timing", &jet_timing, &b_jet_timing);
   fChain->SetBranchAddress("jet_emfrac", &jet_emfrac, &b_jet_emfrac);
   fChain->SetBranchAddress("jet_hecf", &jet_hecf, &b_jet_hecf);
   fChain->SetBranchAddress("jet_hecq", &jet_hecq, &b_jet_hecq);
   fChain->SetBranchAddress("jet_larq", &jet_larq, &b_jet_larq);
   fChain->SetBranchAddress("jet_avglarq", &jet_avglarq, &b_jet_avglarq);
   fChain->SetBranchAddress("jet_negE", &jet_negE, &b_jet_negE);
   fChain->SetBranchAddress("jet_lambda", &jet_lambda, &b_jet_lambda);
   fChain->SetBranchAddress("jet_lambda2", &jet_lambda2, &b_jet_lambda2);
   fChain->SetBranchAddress("jet_jvtxf", &jet_jvtxf, &b_jet_jvtxf);
   fChain->SetBranchAddress("jet_fmaxi", &jet_fmaxi, &b_jet_fmaxi);
   fChain->SetBranchAddress("jet_isbjet_loose", &jet_isbjet_loose, &b_jet_isbjet_loose);
   fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   fChain->SetBranchAddress("jet_cleaning", &jet_cleaning, &b_jet_cleaning);
   fChain->SetBranchAddress("jet_DFCommonJets_QGTagger_NTracks", &jet_DFCommonJets_QGTagger_NTracks, &b_jet_DFCommonJets_QGTagger_NTracks);
   fChain->SetBranchAddress("jet_DFCommonJets_QGTagger_TracksWidth", &jet_DFCommonJets_QGTagger_TracksWidth, &b_jet_DFCommonJets_QGTagger_TracksWidth);
   fChain->SetBranchAddress("jet_DFCommonJets_QGTagger_TracksC1", &jet_DFCommonJets_QGTagger_TracksC1, &b_jet_DFCommonJets_QGTagger_TracksC1);
   fChain->SetBranchAddress("jet_truth_pt", &jet_truth_pt, &b_jet_truth_pt);
   fChain->SetBranchAddress("jet_truth_eta", &jet_truth_eta, &b_jet_truth_eta);
   fChain->SetBranchAddress("jet_truth_phi", &jet_truth_phi, &b_jet_truth_phi);
   fChain->SetBranchAddress("LCTopoJet_pt", &LCTopoJet_pt, &b_LCTopoJet_pt);
   fChain->SetBranchAddress("LCTopoJet_eta", &LCTopoJet_eta, &b_LCTopoJet_eta);
   fChain->SetBranchAddress("LCTopoJet_phi", &LCTopoJet_phi, &b_LCTopoJet_phi);
   fChain->SetBranchAddress("LCTopoJet_m", &LCTopoJet_m, &b_LCTopoJet_m);
   fChain->SetBranchAddress("LCTopoJet_tau21", &LCTopoJet_tau21, &b_LCTopoJet_tau21);
   fChain->SetBranchAddress("LCTopoJet_D2", &LCTopoJet_D2, &b_LCTopoJet_D2);
   fChain->SetBranchAddress("LCTopoJet_C2", &LCTopoJet_C2, &b_LCTopoJet_C2);
   fChain->SetBranchAddress("LCTopoJet_nTrk", &LCTopoJet_nTrk, &b_LCTopoJet_nTrk);
   fChain->SetBranchAddress("LCTopoJet_nConstit", &LCTopoJet_nConstit, &b_LCTopoJet_nConstit);
   fChain->SetBranchAddress("LCTopoJet_passD2_W50", &LCTopoJet_passD2_W50, &b_LCTopoJet_passD2_W50);
   fChain->SetBranchAddress("LCTopoJet_passD2_Z50", &LCTopoJet_passD2_Z50, &b_LCTopoJet_passD2_Z50);
   fChain->SetBranchAddress("LCTopoJet_passD2_W80", &LCTopoJet_passD2_W80, &b_LCTopoJet_passD2_W80);
   fChain->SetBranchAddress("LCTopoJet_passD2_Z80", &LCTopoJet_passD2_Z80, &b_LCTopoJet_passD2_Z80);
   fChain->SetBranchAddress("LCTopoJet_passMass_W50", &LCTopoJet_passMass_W50, &b_LCTopoJet_passMass_W50);
   fChain->SetBranchAddress("LCTopoJet_passMass_Z50", &LCTopoJet_passMass_Z50, &b_LCTopoJet_passMass_Z50);
   fChain->SetBranchAddress("LCTopoJet_passMass_W80", &LCTopoJet_passMass_W80, &b_LCTopoJet_passMass_W80);
   fChain->SetBranchAddress("LCTopoJet_passMass_Z80", &LCTopoJet_passMass_Z80, &b_LCTopoJet_passMass_Z80);
   fChain->SetBranchAddress("TruthFatJet_pt", &TruthFatJet_pt, &b_TruthFatJet_pt);
   fChain->SetBranchAddress("TruthFatJet_eta", &TruthFatJet_eta, &b_TruthFatJet_eta);
   fChain->SetBranchAddress("TruthFatJet_phi", &TruthFatJet_phi, &b_TruthFatJet_phi);
   fChain->SetBranchAddress("TruthFatJet_m", &TruthFatJet_m, &b_TruthFatJet_m);
   fChain->SetBranchAddress("TruthFatJet_tau21", &TruthFatJet_tau21, &b_TruthFatJet_tau21);
   fChain->SetBranchAddress("TruthFatJet_D2", &TruthFatJet_D2, &b_TruthFatJet_D2);
   fChain->SetBranchAddress("TruthFatJet_C2", &TruthFatJet_C2, &b_TruthFatJet_C2);
   fChain->SetBranchAddress("TruthFatJet_nTrk", &TruthFatJet_nTrk, &b_TruthFatJet_nTrk);
   fChain->SetBranchAddress("TruthFatJet_nConstit", &TruthFatJet_nConstit, &b_TruthFatJet_nConstit);
   fChain->SetBranchAddress("tau_loose_multiplicity", &tau_loose_multiplicity, &b_tau_loose_multiplicity);
   fChain->SetBranchAddress("tau_medium_multiplicity", &tau_medium_multiplicity, &b_tau_medium_multiplicity);
   fChain->SetBranchAddress("tau_tight_multiplicity", &tau_tight_multiplicity, &b_tau_tight_multiplicity);
   fChain->SetBranchAddress("tau_baseline_loose_multiplicity", &tau_baseline_loose_multiplicity, &b_tau_baseline_loose_multiplicity);
   fChain->SetBranchAddress("tau_baseline_medium_multiplicity", &tau_baseline_medium_multiplicity, &b_tau_baseline_medium_multiplicity);
   fChain->SetBranchAddress("tau_baseline_tight_multiplicity", &tau_baseline_tight_multiplicity, &b_tau_baseline_tight_multiplicity);

   fChain->SetBranchAddress("weight", &weight, &b_weight);
   fChain->SetBranchAddress("mc_weight_sum", &mc_weight_sum, &b_mc_weight_sum);
   fChain->SetBranchAddress("extra_weight", &extra_weight, &b_extra_weight);
   fChain->SetBranchAddress("campaign_lumi", &campaign_lumi, &b_campaign_lumi);

   Notify();
}


void MergedTree::Book(bool isData, bool isSyst, int skimLevel)
{
   fChain->Branch("run", &run);
   if (skimLevel == 0) fChain->Branch("event", &event);
   if (skimLevel == 0) fChain->Branch("last", &last);
   fChain->Branch("year", &year);
   if (skimLevel == 0) fChain->Branch("met_tst_sig", &met_tst_sig);
   if (!isData) {
      fChain->Branch("xSec_SUSY", &xSec_SUSY);
      fChain->Branch("k_factor", &k_factor);
      fChain->Branch("filter_eff", &filter_eff);
   }
   if (!isData && !isSyst && skimLevel == 0) {
      fChain->Branch("el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down", &el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down);
      fChain->Branch("el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up", &el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up);
      fChain->Branch("el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
      fChain->Branch("el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
      fChain->Branch("el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
      fChain->Branch("el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
      fChain->Branch("el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
      fChain->Branch("el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
      fChain->Branch("el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down", &el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down);
      fChain->Branch("el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", &el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up);
      fChain->Branch("el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down", &el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down);
      fChain->Branch("el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up", &el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up);
      fChain->Branch("mu_SF_syst_MUON_EFF_BADMUON_SYS__1down", &mu_SF_syst_MUON_EFF_BADMUON_SYS__1down);
      fChain->Branch("mu_SF_syst_MUON_EFF_BADMUON_SYS__1up", &mu_SF_syst_MUON_EFF_BADMUON_SYS__1up);
      fChain->Branch("mu_SF_syst_MUON_EFF_ISO_STAT__1down", &mu_SF_syst_MUON_EFF_ISO_STAT__1down);
      fChain->Branch("mu_SF_syst_MUON_EFF_ISO_STAT__1up", &mu_SF_syst_MUON_EFF_ISO_STAT__1up);
      fChain->Branch("mu_SF_syst_MUON_EFF_ISO_SYS__1down", &mu_SF_syst_MUON_EFF_ISO_SYS__1down);
      fChain->Branch("mu_SF_syst_MUON_EFF_ISO_SYS__1up", &mu_SF_syst_MUON_EFF_ISO_SYS__1up);
      fChain->Branch("mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down", &mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down);
      fChain->Branch("mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up", &mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up);
      fChain->Branch("mu_SF_syst_MUON_EFF_RECO_STAT__1down", &mu_SF_syst_MUON_EFF_RECO_STAT__1down);
      fChain->Branch("mu_SF_syst_MUON_EFF_RECO_STAT__1up", &mu_SF_syst_MUON_EFF_RECO_STAT__1up);
      fChain->Branch("mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down", &mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down);
      fChain->Branch("mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up", &mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up);
      fChain->Branch("mu_SF_syst_MUON_EFF_RECO_SYS__1down", &mu_SF_syst_MUON_EFF_RECO_SYS__1down);
      fChain->Branch("mu_SF_syst_MUON_EFF_RECO_SYS__1up", &mu_SF_syst_MUON_EFF_RECO_SYS__1up);
      fChain->Branch("mu_SF_syst_MUON_EFF_TTVA_STAT__1down", &mu_SF_syst_MUON_EFF_TTVA_STAT__1down);
      fChain->Branch("mu_SF_syst_MUON_EFF_TTVA_STAT__1up", &mu_SF_syst_MUON_EFF_TTVA_STAT__1up);
      fChain->Branch("mu_SF_syst_MUON_EFF_TTVA_SYS__1down", &mu_SF_syst_MUON_EFF_TTVA_SYS__1down);
      fChain->Branch("mu_SF_syst_MUON_EFF_TTVA_SYS__1up", &mu_SF_syst_MUON_EFF_TTVA_SYS__1up);
      fChain->Branch("mu_SF_syst_MUON_EFF_TrigStatUncertainty__1down", &mu_SF_syst_MUON_EFF_TrigStatUncertainty__1down);
      fChain->Branch("mu_SF_syst_MUON_EFF_TrigStatUncertainty__1up", &mu_SF_syst_MUON_EFF_TrigStatUncertainty__1up);
      fChain->Branch("mu_SF_syst_MUON_EFF_TrigSystUncertainty__1down", &mu_SF_syst_MUON_EFF_TrigSystUncertainty__1down);
      fChain->Branch("mu_SF_syst_MUON_EFF_TrigSystUncertainty__1up", &mu_SF_syst_MUON_EFF_TrigSystUncertainty__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down", &tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up", &tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down", &tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up", &tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1up);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1down", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1down);
      fChain->Branch("tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1up", &tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1up);
      fChain->Branch("ph_SF_syst_PH_EFF_ID_Uncertainty__1down", &ph_SF_syst_PH_EFF_ID_Uncertainty__1down);
      fChain->Branch("ph_SF_syst_PH_EFF_ID_Uncertainty__1up", &ph_SF_syst_PH_EFF_ID_Uncertainty__1up);
      fChain->Branch("ph_SF_syst_PH_EFF_ISO_Uncertainty__1down", &ph_SF_syst_PH_EFF_ISO_Uncertainty__1down);
      fChain->Branch("ph_SF_syst_PH_EFF_ISO_Uncertainty__1up", &ph_SF_syst_PH_EFF_ISO_Uncertainty__1up);
      fChain->Branch("ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1down", &ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1down);
      fChain->Branch("ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1up", &ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1up);
      fChain->Branch("jet_SF_syst_JET_JvtEfficiency__1down", &jet_SF_syst_JET_JvtEfficiency__1down);
      fChain->Branch("jet_SF_syst_JET_JvtEfficiency__1up", &jet_SF_syst_JET_JvtEfficiency__1up);
      fChain->Branch("pu_weight_syst_PRW_DATASF__1down", &pu_weight_syst_PRW_DATASF__1down);
      fChain->Branch("pu_weight_syst_PRW_DATASF__1up", &pu_weight_syst_PRW_DATASF__1up);
      fChain->Branch("btag_weight_syst_FT_EFF_B_systematics__1down", &btag_weight_syst_FT_EFF_B_systematics__1down);
      fChain->Branch("btag_weight_syst_FT_EFF_B_systematics__1up", &btag_weight_syst_FT_EFF_B_systematics__1up);
      fChain->Branch("btag_weight_syst_FT_EFF_C_systematics__1down", &btag_weight_syst_FT_EFF_C_systematics__1down);
      fChain->Branch("btag_weight_syst_FT_EFF_C_systematics__1up", &btag_weight_syst_FT_EFF_C_systematics__1up);
      fChain->Branch("btag_weight_syst_FT_EFF_Light_systematics__1down", &btag_weight_syst_FT_EFF_Light_systematics__1down);
      fChain->Branch("btag_weight_syst_FT_EFF_Light_systematics__1up", &btag_weight_syst_FT_EFF_Light_systematics__1up);
      fChain->Branch("btag_weight_syst_FT_EFF_extrapolation__1down", &btag_weight_syst_FT_EFF_extrapolation__1down);
      fChain->Branch("btag_weight_syst_FT_EFF_extrapolation__1up", &btag_weight_syst_FT_EFF_extrapolation__1up);
      fChain->Branch("btag_weight_syst_FT_EFF_extrapolation_from_charm__1down", &btag_weight_syst_FT_EFF_extrapolation_from_charm__1down);
      fChain->Branch("btag_weight_syst_FT_EFF_extrapolation_from_charm__1up", &btag_weight_syst_FT_EFF_extrapolation_from_charm__1up);
      fChain->Branch("btagloose_weight_syst_FT_EFF_B_systematics__1down", &btagloose_weight_syst_FT_EFF_B_systematics__1down);
      fChain->Branch("btagloose_weight_syst_FT_EFF_B_systematics__1up", &btagloose_weight_syst_FT_EFF_B_systematics__1up);
      fChain->Branch("btagloose_weight_syst_FT_EFF_C_systematics__1down", &btagloose_weight_syst_FT_EFF_C_systematics__1down);
      fChain->Branch("btagloose_weight_syst_FT_EFF_C_systematics__1up", &btagloose_weight_syst_FT_EFF_C_systematics__1up);
      fChain->Branch("btagloose_weight_syst_FT_EFF_Light_systematics__1down", &btagloose_weight_syst_FT_EFF_Light_systematics__1down);
      fChain->Branch("btagloose_weight_syst_FT_EFF_Light_systematics__1up", &btagloose_weight_syst_FT_EFF_Light_systematics__1up);
      fChain->Branch("btagloose_weight_syst_FT_EFF_extrapolation__1down", &btagloose_weight_syst_FT_EFF_extrapolation__1down);
      fChain->Branch("btagloose_weight_syst_FT_EFF_extrapolation__1up", &btagloose_weight_syst_FT_EFF_extrapolation__1up);
      fChain->Branch("btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down", &btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down);
      fChain->Branch("btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up", &btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up);
      fChain->Branch("trkbtag_weight_syst_FT_EFF_B_systematics__1down", &trkbtag_weight_syst_FT_EFF_B_systematics__1down);
      fChain->Branch("trkbtag_weight_syst_FT_EFF_B_systematics__1up", &trkbtag_weight_syst_FT_EFF_B_systematics__1up);
      fChain->Branch("trkbtag_weight_syst_FT_EFF_C_systematics__1down", &trkbtag_weight_syst_FT_EFF_C_systematics__1down);
      fChain->Branch("trkbtag_weight_syst_FT_EFF_C_systematics__1up", &trkbtag_weight_syst_FT_EFF_C_systematics__1up);
      fChain->Branch("trkbtag_weight_syst_FT_EFF_Light_systematics__1down", &trkbtag_weight_syst_FT_EFF_Light_systematics__1down);
      fChain->Branch("trkbtag_weight_syst_FT_EFF_Light_systematics__1up", &trkbtag_weight_syst_FT_EFF_Light_systematics__1up);
      fChain->Branch("trkbtag_weight_syst_FT_EFF_extrapolation__1down", &trkbtag_weight_syst_FT_EFF_extrapolation__1down);
      fChain->Branch("trkbtag_weight_syst_FT_EFF_extrapolation__1up", &trkbtag_weight_syst_FT_EFF_extrapolation__1up);
      fChain->Branch("trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1down", &trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1down);
      fChain->Branch("trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1up", &trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1up);
      fChain->Branch("trkbtagloose_weight_syst_FT_EFF_B_systematics__1down", &trkbtagloose_weight_syst_FT_EFF_B_systematics__1down);
      fChain->Branch("trkbtagloose_weight_syst_FT_EFF_B_systematics__1up", &trkbtagloose_weight_syst_FT_EFF_B_systematics__1up);
      fChain->Branch("trkbtagloose_weight_syst_FT_EFF_C_systematics__1down", &trkbtagloose_weight_syst_FT_EFF_C_systematics__1down);
      fChain->Branch("trkbtagloose_weight_syst_FT_EFF_C_systematics__1up", &trkbtagloose_weight_syst_FT_EFF_C_systematics__1up);
      fChain->Branch("trkbtagloose_weight_syst_FT_EFF_Light_systematics__1down", &trkbtagloose_weight_syst_FT_EFF_Light_systematics__1down);
      fChain->Branch("trkbtagloose_weight_syst_FT_EFF_Light_systematics__1up", &trkbtagloose_weight_syst_FT_EFF_Light_systematics__1up);
      fChain->Branch("trkbtagloose_weight_syst_FT_EFF_extrapolation__1down", &trkbtagloose_weight_syst_FT_EFF_extrapolation__1down);
      fChain->Branch("trkbtagloose_weight_syst_FT_EFF_extrapolation__1up", &trkbtagloose_weight_syst_FT_EFF_extrapolation__1up);
      fChain->Branch("trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down", &trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down);
      fChain->Branch("trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up", &trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up);
   }
   fChain->Branch("n_tau_baseline", &n_tau_baseline);
   fChain->Branch("mconly_weight", &mconly_weight);
   fChain->Branch("pu_weight", &pu_weight);
   fChain->Branch("btag_weight", &btag_weight);
   fChain->Branch("btagloose_weight", &btagloose_weight);
   fChain->Branch("trkbtag_weight", &trkbtag_weight);
   fChain->Branch("trkbtagloose_weight", &trkbtagloose_weight);
   fChain->Branch("jvt_weight", &jvt_weight);
   if (skimLevel == 0) fChain->Branch("munu_mT", &munu_mT);
   if (skimLevel == 0) fChain->Branch("enu_mT", &enu_mT);
   if (skimLevel == 0) fChain->Branch("mumu_m", &mumu_m);
   if (skimLevel == 0) fChain->Branch("ee_m", &ee_m);
   fChain->Branch("dPhiLCTopoJetMet", &dPhiLCTopoJetMet);
   fChain->Branch("dPhiLCTopoJetMetNoElectron", &dPhiLCTopoJetMetNoElectron);
   fChain->Branch("dPhiLCTopoJetMetNoMuon", &dPhiLCTopoJetMetNoMuon);
   fChain->Branch("dPhiDijetMet", &dPhiDijetMet);
   fChain->Branch("dPhiDijetMetNoElectron", &dPhiDijetMetNoElectron);
   fChain->Branch("dPhiDijetMetNoMuon", &dPhiDijetMetNoMuon);
   fChain->Branch("dPhiDijet", &dPhiDijet);
   fChain->Branch("dRDijet", &dRDijet);
   fChain->Branch("DijetSumPt", &DijetSumPt);
   fChain->Branch("TrijetSumPt", &TrijetSumPt);
   fChain->Branch("DijetMass", &DijetMass);
   fChain->Branch("n_trackjet", &n_trackjet);
   fChain->Branch("n_btrackJet", &n_btrackJet);
   fChain->Branch("n_btrackJet_loose", &n_btrackJet_loose);
   fChain->Branch("n_trackLCTopoAssociatedBjet", &n_trackLCTopoAssociatedBjet);
   fChain->Branch("n_trackLCTopoSeparatedBjet", &n_trackLCTopoSeparatedBjet);
   fChain->Branch("n_trackLCTopoAssociatedBjetLoose", &n_trackLCTopoAssociatedBjetLoose);
   fChain->Branch("n_trackLCTopoSeparatedBjetLoose", &n_trackLCTopoSeparatedBjetLoose);
   fChain->Branch("n_jet", &n_jet);
   fChain->Branch("n_bjet", &n_bjet);
   fChain->Branch("n_bjet_loose", &n_bjet_loose);
   fChain->Branch("n_el", &n_el);
   fChain->Branch("n_el_baseline", &n_el_baseline);
   fChain->Branch("n_mu_baseline", &n_mu_baseline);
   fChain->Branch("n_tau", &n_tau);
   fChain->Branch("n_mu", &n_mu);
   if (skimLevel == 0) fChain->Branch("mconly_weights", &mconly_weights);
   if (skimLevel == 0) fChain->Branch("n_truthFatJet", &n_truthFatJet);
   fChain->Branch("n_LCTopoJet", &n_LCTopoJet);
   if (skimLevel == 0) fChain->Branch("averageIntPerXing", &averageIntPerXing);
   if (skimLevel == 0) fChain->Branch("actualIntPerXing", &actualIntPerXing);
   if (skimLevel == 0) fChain->Branch("corAverageIntPerXing", &corAverageIntPerXing);
   if (skimLevel == 0) fChain->Branch("corActualIntPerXing", &corActualIntPerXing);
   if (skimLevel == 0) fChain->Branch("n_vx", &n_vx);
   if (skimLevel == 0) fChain->Branch("pu_hash", &pu_hash);
   fChain->Branch("trigger_HLT_e120_lhloose", &trigger_HLT_e120_lhloose);
   fChain->Branch("trigger_HLT_e140_lhloose_nod0", &trigger_HLT_e140_lhloose_nod0);
   fChain->Branch("trigger_HLT_e24_lhmedium_L1EM20VH", &trigger_HLT_e24_lhmedium_L1EM20VH);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_e24_lhtight_nod0_ivarloose", &trigger_HLT_e24_lhtight_nod0_ivarloose);
   fChain->Branch("trigger_HLT_e26_lhtight_nod0_ivarloose", &trigger_HLT_e26_lhtight_nod0_ivarloose);
   fChain->Branch("trigger_HLT_e60_lhmedium", &trigger_HLT_e60_lhmedium);
   fChain->Branch("trigger_HLT_e60_lhmedium_nod0", &trigger_HLT_e60_lhmedium_nod0);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_e60_medium", &trigger_HLT_e60_medium);
   fChain->Branch("trigger_HLT_mu20_iloose_L1MU15", &trigger_HLT_mu20_iloose_L1MU15);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_mu24_iloose", &trigger_HLT_mu24_iloose);
   fChain->Branch("trigger_HLT_mu24_ivarmedium", &trigger_HLT_mu24_ivarmedium);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_mu26_imedium", &trigger_HLT_mu26_imedium);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_mu26_ivarmedium", &trigger_HLT_mu26_ivarmedium);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_mu40", &trigger_HLT_mu40);
   fChain->Branch("trigger_HLT_mu50", &trigger_HLT_mu50);
   fChain->Branch("trigger_HLT_xe100_mht_L1XE50", &trigger_HLT_xe100_mht_L1XE50);
   fChain->Branch("trigger_HLT_xe110_mht_L1XE50", &trigger_HLT_xe110_mht_L1XE50);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe130_mht_L1XE50", &trigger_HLT_xe130_mht_L1XE50);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe70", &trigger_HLT_xe70);
   fChain->Branch("trigger_HLT_xe70_mht", &trigger_HLT_xe70_mht);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe80_tc_lcw_L1XE50", &trigger_HLT_xe80_tc_lcw_L1XE50);
   fChain->Branch("trigger_HLT_xe90_mht_L1XE50", &trigger_HLT_xe90_mht_L1XE50);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe90_mht_wEFMu_L1XE50", &trigger_HLT_xe90_mht_wEFMu_L1XE50);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_e26_lhtight_nod0", &trigger_HLT_e26_lhtight_nod0);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_e300_etcut", &trigger_HLT_e300_etcut);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_mu24_iloose_L1MU15", &trigger_HLT_mu24_iloose_L1MU15);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_mu24_imedium", &trigger_HLT_mu24_imedium);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_mu24_ivarloose", &trigger_HLT_mu24_ivarloose);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_mu24_ivarloose_L1MU15", &trigger_HLT_mu24_ivarloose_L1MU15);
   fChain->Branch("trigger_HLT_mu60_0eta105_msonly", &trigger_HLT_mu60_0eta105_msonly);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe100_L1XE50", &trigger_HLT_xe100_L1XE50);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe100_pufit_L1XE50", &trigger_HLT_xe100_pufit_L1XE50);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe100_pufit_L1XE55", &trigger_HLT_xe100_pufit_L1XE55);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe100_tc_em_L1XE50", &trigger_HLT_xe100_tc_em_L1XE50);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe110_pueta_L1XE50", &trigger_HLT_xe110_pueta_L1XE50);
   fChain->Branch("trigger_HLT_xe110_pufit_L1XE50", &trigger_HLT_xe110_pufit_L1XE50);
   fChain->Branch("trigger_HLT_xe110_pufit_L1XE55", &trigger_HLT_xe110_pufit_L1XE55);
   fChain->Branch("trigger_HLT_xe110_pufit_xe65_L1XE50", &trigger_HLT_xe110_pufit_xe65_L1XE50);
   fChain->Branch("trigger_HLT_xe110_pufit_xe70_L1XE50", &trigger_HLT_xe110_pufit_xe70_L1XE50);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe120_pueta", &trigger_HLT_xe120_pueta);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe120_pufit", &trigger_HLT_xe120_pufit);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe120_pufit_L1XE50", &trigger_HLT_xe120_pufit_L1XE50);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe120_tc_lcw_L1XE50", &trigger_HLT_xe120_tc_lcw_L1XE50);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe70_tc_lcw", &trigger_HLT_xe70_tc_lcw);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe90_pufit_L1XE50", &trigger_HLT_xe90_pufit_L1XE50);
   if (skimLevel == 0) fChain->Branch("trigger_HLT_xe90_tc_lcw_wEFMu_L1XE50", &trigger_HLT_xe90_tc_lcw_wEFMu_L1XE50);
   if (skimLevel == 0) fChain->Branch("n_ph", &n_ph);
   fChain->Branch("n_ph_baseline", &n_ph_baseline);
   if (skimLevel == 0) fChain->Branch("n_jet_truth", &n_jet_truth);
   fChain->Branch("met_noelectron_tst_et", &met_noelectron_tst_et);
   fChain->Branch("met_noelectron_tst_phi", &met_noelectron_tst_phi);
   if (skimLevel == 0) fChain->Branch("met_noelectron_tst_etx", &met_noelectron_tst_etx);
   if (skimLevel == 0) fChain->Branch("met_noelectron_tst_ety", &met_noelectron_tst_ety);
   fChain->Branch("met_nomuon_tst_et", &met_nomuon_tst_et);
   fChain->Branch("met_nomuon_tst_phi", &met_nomuon_tst_phi);
   if (skimLevel == 0) fChain->Branch("met_nomuon_tst_etx", &met_nomuon_tst_etx);
   if (skimLevel == 0) fChain->Branch("met_nomuon_tst_ety", &met_nomuon_tst_ety);
   fChain->Branch("met_track_et", &met_track_et);
   fChain->Branch("met_track_phi", &met_track_phi);
   if (skimLevel == 0) fChain->Branch("met_track_etx", &met_track_etx);
   if (skimLevel == 0) fChain->Branch("met_track_ety", &met_track_ety);
   fChain->Branch("met_track_noelectron_et", &met_track_noelectron_et);
   fChain->Branch("met_track_noelectron_phi", &met_track_noelectron_phi);
   if (skimLevel == 0) fChain->Branch("met_track_noelectron_etx", &met_track_noelectron_etx);
   if (skimLevel == 0) fChain->Branch("met_track_noelectron_ety", &met_track_noelectron_ety);
   fChain->Branch("met_track_nomuon_et", &met_track_nomuon_et);
   fChain->Branch("met_track_nomuon_phi", &met_track_nomuon_phi);
   if (skimLevel == 0) fChain->Branch("met_track_nomuon_etx", &met_track_nomuon_etx);
   if (skimLevel == 0) fChain->Branch("met_track_nomuon_ety", &met_track_nomuon_ety);
   if (skimLevel == 0) fChain->Branch("met_truth_et", &met_truth_et);
   fChain->Branch("met_truth_phi", &met_truth_phi);
   if (skimLevel == 0) fChain->Branch("met_truth_etx", &met_truth_etx);
   if (skimLevel == 0) fChain->Branch("met_truth_ety", &met_truth_ety);
   fChain->Branch("met_tst_et", &met_tst_et);
   fChain->Branch("met_tst_phi", &met_tst_phi);
   if (skimLevel == 0) fChain->Branch("met_tst_etx", &met_tst_etx);
   if (skimLevel == 0) fChain->Branch("met_tst_ety", &met_tst_ety);
   fChain->Branch("mu_pt", &mu_pt);
   fChain->Branch("mu_SF", &mu_SF);
   fChain->Branch("mu_eta", &mu_eta);
   fChain->Branch("mu_phi", &mu_phi);
   fChain->Branch("mu_SF_iso", &mu_SF_iso);
   fChain->Branch("mu_m", &mu_m);
   fChain->Branch("mu_charge", &mu_charge);
   fChain->Branch("mu_ptcone20", &mu_ptcone20);
   if (skimLevel == 0) fChain->Branch("mu_baseline_pt", &mu_baseline_pt);
   if (skimLevel == 0) fChain->Branch("mu_baseline_ptcone20", &mu_baseline_ptcone20);
   if (skimLevel == 0) fChain->Branch("mu_baseline_SF", &mu_baseline_SF);
   if (skimLevel == 0) fChain->Branch("mu_baseline_eta", &mu_baseline_eta);
   if (skimLevel == 0) fChain->Branch("mu_baseline_phi", &mu_baseline_phi);
   fChain->Branch("mu_baseline_isLooseID", &mu_baseline_isLooseID);
   fChain->Branch("mu_baseline_isMediumID", &mu_baseline_isMediumID);
   fChain->Branch("mu_baseline_isTightID", &mu_baseline_isTightID);
   fChain->Branch("el_pt", &el_pt);
   fChain->Branch("el_eta", &el_eta);
   fChain->Branch("el_phi", &el_phi);
   fChain->Branch("el_SF", &el_SF);
   fChain->Branch("el_SF_iso", &el_SF_iso);
   fChain->Branch("el_SF_trigger", &el_SF_trigger);
   fChain->Branch("el_eff_trigger", &el_eff_trigger);
   fChain->Branch("el_topoetcone20", &el_topoetcone20);
   fChain->Branch("el_ptcone20", &el_ptcone20);
   fChain->Branch("el_m", &el_m);
   fChain->Branch("el_charge", &el_charge);
   fChain->Branch("el_ptvarcone20_TightTTVA_pt1000", &el_ptvarcone20_TightTTVA_pt1000);
   if (skimLevel == 0) fChain->Branch("el_baseline_pt", &el_baseline_pt);
   if (skimLevel == 0) fChain->Branch("el_baseline_topoetcone20", &el_baseline_topoetcone20);
   if (skimLevel == 0) fChain->Branch("el_baseline_ptcone20", &el_baseline_ptcone20);
   if (skimLevel == 0) fChain->Branch("el_baseline_SF", &el_baseline_SF);
   if (skimLevel == 0) fChain->Branch("el_baseline_eta", &el_baseline_eta);
   if (skimLevel == 0) fChain->Branch("el_baseline_phi", &el_baseline_phi);
   fChain->Branch("el_baseline_isLooseID", &el_baseline_isLooseID);
   fChain->Branch("el_baseline_isMediumID", &el_baseline_isMediumID);
   fChain->Branch("el_baseline_isTightID", &el_baseline_isTightID);
   fChain->Branch("jet_pt", &jet_pt);
   fChain->Branch("jet_eta", &jet_eta);
   fChain->Branch("jet_phi", &jet_phi);
   fChain->Branch("jet_m", &jet_m);
   fChain->Branch("jet_fmax", &jet_fmax);
   fChain->Branch("jet_fch", &jet_fch);
   fChain->Branch("jet_isbjet", &jet_isbjet);
   fChain->Branch("jet_PartonTruthLabelID", &jet_PartonTruthLabelID);
   fChain->Branch("jet_ConeTruthLabelID", &jet_ConeTruthLabelID);
   fChain->Branch("jet_timing", &jet_timing);
   fChain->Branch("jet_emfrac", &jet_emfrac);
   if (skimLevel == 0) fChain->Branch("jet_hecf", &jet_hecf);
   if (skimLevel == 0) fChain->Branch("jet_hecq", &jet_hecq);
   if (skimLevel == 0) fChain->Branch("jet_larq", &jet_larq);
   if (skimLevel == 0) fChain->Branch("jet_avglarq", &jet_avglarq);
   if (skimLevel == 0) fChain->Branch("jet_negE", &jet_negE);
   if (skimLevel == 0) fChain->Branch("jet_lambda", &jet_lambda);
   if (skimLevel == 0) fChain->Branch("jet_lambda2", &jet_lambda2);
   if (skimLevel == 0) fChain->Branch("jet_jvtxf", &jet_jvtxf);
   if (skimLevel == 0) fChain->Branch("jet_fmaxi", &jet_fmaxi);
   fChain->Branch("jet_isbjet_loose", &jet_isbjet_loose);
   fChain->Branch("jet_jvt", &jet_jvt);
   fChain->Branch("jet_cleaning", &jet_cleaning);
   fChain->Branch("jet_DFCommonJets_QGTagger_NTracks", &jet_DFCommonJets_QGTagger_NTracks);
   fChain->Branch("jet_DFCommonJets_QGTagger_TracksWidth", &jet_DFCommonJets_QGTagger_TracksWidth);
   fChain->Branch("jet_DFCommonJets_QGTagger_TracksC1", &jet_DFCommonJets_QGTagger_TracksC1);
   if (skimLevel == 0) fChain->Branch("jet_truth_pt", &jet_truth_pt);
   if (skimLevel == 0) fChain->Branch("jet_truth_eta", &jet_truth_eta);
   if (skimLevel == 0) fChain->Branch("jet_truth_phi", &jet_truth_phi);
   fChain->Branch("LCTopoJet_pt", &LCTopoJet_pt);
   fChain->Branch("LCTopoJet_eta", &LCTopoJet_eta);
   fChain->Branch("LCTopoJet_phi", &LCTopoJet_phi);
   fChain->Branch("LCTopoJet_m", &LCTopoJet_m);
   fChain->Branch("LCTopoJet_tau21", &LCTopoJet_tau21);
   fChain->Branch("LCTopoJet_D2", &LCTopoJet_D2);
   fChain->Branch("LCTopoJet_C2", &LCTopoJet_C2);
   fChain->Branch("LCTopoJet_nTrk", &LCTopoJet_nTrk);
   fChain->Branch("LCTopoJet_nConstit", &LCTopoJet_nConstit);
   fChain->Branch("LCTopoJet_passD2_W50", &LCTopoJet_passD2_W50);
   fChain->Branch("LCTopoJet_passD2_Z50", &LCTopoJet_passD2_Z50);
   fChain->Branch("LCTopoJet_passD2_W80", &LCTopoJet_passD2_W80);
   fChain->Branch("LCTopoJet_passD2_Z80", &LCTopoJet_passD2_Z80);
   fChain->Branch("LCTopoJet_passMass_W50", &LCTopoJet_passMass_W50);
   fChain->Branch("LCTopoJet_passMass_Z50", &LCTopoJet_passMass_Z50);
   fChain->Branch("LCTopoJet_passMass_W80", &LCTopoJet_passMass_W80);
   fChain->Branch("LCTopoJet_passMass_Z80", &LCTopoJet_passMass_Z80);
   if (skimLevel == 0) fChain->Branch("TruthFatJet_pt", &TruthFatJet_pt);
   if (skimLevel == 0) fChain->Branch("TruthFatJet_eta", &TruthFatJet_eta);
   if (skimLevel == 0) fChain->Branch("TruthFatJet_phi", &TruthFatJet_phi);
   if (skimLevel == 0) fChain->Branch("TruthFatJet_m", &TruthFatJet_m);
   if (skimLevel == 0) fChain->Branch("TruthFatJet_tau21", &TruthFatJet_tau21);
   if (skimLevel == 0) fChain->Branch("TruthFatJet_D2", &TruthFatJet_D2);
   if (skimLevel == 0) fChain->Branch("TruthFatJet_C2", &TruthFatJet_C2);
   if (skimLevel == 0) fChain->Branch("TruthFatJet_nTrk", &TruthFatJet_nTrk);
   if (skimLevel == 0) fChain->Branch("TruthFatJet_nConstit", &TruthFatJet_nConstit);
   if (skimLevel == 0) fChain->Branch("tau_loose_multiplicity", &tau_loose_multiplicity);
   if (skimLevel == 0) fChain->Branch("tau_medium_multiplicity", &tau_medium_multiplicity);
   if (skimLevel == 0) fChain->Branch("tau_tight_multiplicity", &tau_tight_multiplicity);
   fChain->Branch("tau_baseline_loose_multiplicity", &tau_baseline_loose_multiplicity);
   fChain->Branch("tau_baseline_medium_multiplicity", &tau_baseline_medium_multiplicity);
   fChain->Branch("tau_baseline_tight_multiplicity", &tau_baseline_tight_multiplicity);

   fChain->Branch("weight", &weight);
   fChain->Branch("mc_weight_sum", &mc_weight_sum);
   fChain->Branch("extra_weight", &extra_weight);
   fChain->Branch("campaign_lumi", &campaign_lumi);

}

Bool_t MergedTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void MergedTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t MergedTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef MergedTree_cxx
