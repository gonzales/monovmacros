//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jan 28 14:34:19 2020 by ROOT version 6.18/04
// from TTree nominal/
// found on file: /eos/user/g/gonzales/MonoV/Jan2020Prod_mc16a/user.gonzales.mc16aJan2020ProdPrime.mc16_13TeV.364168.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV500_1000_MonoVReaders.root/user.gonzales.20249499._000001.MonoVReaders.root
//////////////////////////////////////////////////////////

#ifndef MonoVReader_h
#define MonoVReader_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "MergeJob.h"
#include "MergedTree.C"
#include "PlotList.h"
#include "PlotTools.C"


class TProofOutputFile;

class MonoVReader : public TSelector {
private:
   TProofOutputFile *m_prooffile; //!
   TFile *m_outfile; //!
   MergeJob * m_mergeJob; //->
   MergedTree * shortTree; //!
   Bool_t m_isMC; //!
   Bool_t m_UsePROOF; //!

   TString m_channel; //!

   Bool_t m_merging; //!
   Bool_t m_tables; //!

   TGraph * graph_xSec; //!
   TGraph * graph_filterEff; //!
   TGraph * graph_kFactor; //!
   TGraph * graph_MCsum; //!
   TGraph * graph_rawEvents; //!


public :

   TTreeReader fReader; //!the tree reader
   TTree          *fChain=0;   //!pointer to the analyzed TTree or TChain

   virtual void FillMinitree();

   void SetParameters(MergeJob * mergeJob) {
      m_isMC = !(mergeJob->GetIsData());
      m_channel = mergeJob->GetChannelName();
      m_merging = mergeJob->GetIsMerging();
      m_tables = mergeJob->GetDoTables();
   }

// Fixed size dimensions of array or collections stored in the TTree if any.

   Bool_t isData;

   TTreeReaderValue<Int_t> run = {fReader, "run"};
   TTreeReaderValue<ULong64_t> event = {fReader, "event"};
   TTreeReaderValue<Int_t> last = {fReader, "last"};
   TTreeReaderValue<Int_t> year = {fReader, "year"};
   TTreeReaderValue<Float_t> met_tst_sig = {fReader, "met_tst_sig"};
   TTreeReaderValue<Float_t> xSec_SUSY = {fReader, "xSec_SUSY"};
   TTreeReaderValue<Float_t> k_factor = {fReader, "k_factor"};
   TTreeReaderValue<Float_t> filter_eff = {fReader, "filter_eff"};
   TTreeReaderValue<Int_t> n_tau_baseline = {fReader, "n_tau_baseline"};
   TTreeReaderValue<Float_t> mconly_weight = {fReader, "mconly_weight"};
   TTreeReaderValue<Float_t> pu_weight = {fReader, "pu_weight"};
   TTreeReaderValue<Float_t> btag_weight = {fReader, "btag_weight"};
   TTreeReaderValue<Float_t> btagloose_weight = {fReader, "btagloose_weight"};
   TTreeReaderValue<Float_t> trkbtag_weight = {fReader, "trkbtag_weight"};
   TTreeReaderValue<Float_t> trkbtagloose_weight = {fReader, "trkbtagloose_weight"};
   TTreeReaderValue<Float_t> jvt_weight = {fReader, "jvt_weight"};
   TTreeReaderValue<Float_t> munu_mT = {fReader, "munu_mT"};
   TTreeReaderValue<Float_t> enu_mT = {fReader, "enu_mT"};
   TTreeReaderValue<Float_t> mumu_m = {fReader, "mumu_m"};
   TTreeReaderValue<Float_t> ee_m = {fReader, "ee_m"};
   TTreeReaderValue<Float_t> dPhiLCTopoJetMet = {fReader, "dPhiLCTopoJetMet"};
   TTreeReaderValue<Float_t> dPhiLCTopoJetMetNoElectron = {fReader, "dPhiLCTopoJetMetNoElectron"};
   TTreeReaderValue<Float_t> dPhiLCTopoJetMetNoMuon = {fReader, "dPhiLCTopoJetMetNoMuon"};
   TTreeReaderValue<Float_t> dPhiDijetMet = {fReader, "dPhiDijetMet"};
   TTreeReaderValue<Float_t> dPhiDijetMetNoElectron = {fReader, "dPhiDijetMetNoElectron"};
   TTreeReaderValue<Float_t> dPhiDijetMetNoMuon = {fReader, "dPhiDijetMetNoMuon"};
   TTreeReaderValue<Float_t> dPhiDijet = {fReader, "dPhiDijet"};
   TTreeReaderValue<Float_t> dRDijet = {fReader, "dRDijet"};
   TTreeReaderValue<Float_t> DijetSumPt = {fReader, "DijetSumPt"};
   TTreeReaderValue<Float_t> TrijetSumPt = {fReader, "TrijetSumPt"};
   TTreeReaderValue<Float_t> DijetMass = {fReader, "DijetMass"};
   TTreeReaderValue<Int_t> n_trackjet = {fReader, "n_trackjet"};
   TTreeReaderValue<Int_t> n_btrackJet = {fReader, "n_btrackJet"};
   TTreeReaderValue<Int_t> n_btrackJet_loose = {fReader, "n_btrackJet_loose"};
   TTreeReaderValue<Int_t> n_trackLCTopoAssociatedBjet = {fReader, "n_trackLCTopoAssociatedBjet"};
   TTreeReaderValue<Int_t> n_trackLCTopoSeparatedBjet = {fReader, "n_trackLCTopoSeparatedBjet"};
   TTreeReaderValue<Int_t> n_trackLCTopoAssociatedBjetLoose = {fReader, "n_trackLCTopoAssociatedBjetLoose"};
   TTreeReaderValue<Int_t> n_trackLCTopoSeparatedBjetLoose = {fReader, "n_trackLCTopoSeparatedBjetLoose"};
   TTreeReaderValue<Int_t> n_jet = {fReader, "n_jet"};
   TTreeReaderValue<Int_t> n_bjet = {fReader, "n_bjet"};
   TTreeReaderValue<Int_t> n_bjet_loose = {fReader, "n_bjet_loose"};
   TTreeReaderValue<Int_t> n_el = {fReader, "n_el"};
   TTreeReaderValue<Int_t> n_el_baseline = {fReader, "n_el_baseline"};
   TTreeReaderValue<Int_t> n_mu_baseline = {fReader, "n_mu_baseline"};
   TTreeReaderValue<Int_t> n_tau = {fReader, "n_tau"};
   TTreeReaderValue<Int_t> n_mu = {fReader, "n_mu"};
   TTreeReaderValue<Int_t> n_truthFatJet = {fReader, "n_truthFatJet"};
   TTreeReaderValue<Int_t> n_LCTopoJet = {fReader, "n_LCTopoJet"};
   TTreeReaderValue<Float_t> averageIntPerXing = {fReader, "averageIntPerXing"};
   TTreeReaderValue<Float_t> actualIntPerXing = {fReader, "actualIntPerXing"};
   TTreeReaderValue<Float_t> corAverageIntPerXing = {fReader, "corAverageIntPerXing"};
   TTreeReaderValue<Float_t> corActualIntPerXing = {fReader, "corActualIntPerXing"};
   TTreeReaderValue<Int_t> n_vx = {fReader, "n_vx"};
   TTreeReaderValue<Float_t> pu_hash = {fReader, "pu_hash"};
   TTreeReaderValue<Int_t> trigger_HLT_e120_lhloose = {fReader, "trigger_HLT_e120_lhloose"};
   TTreeReaderValue<Int_t> trigger_HLT_e140_lhloose_nod0 = {fReader, "trigger_HLT_e140_lhloose_nod0"};
   TTreeReaderValue<Int_t> trigger_HLT_e24_lhmedium_L1EM20VH = {fReader, "trigger_HLT_e24_lhmedium_L1EM20VH"};
   TTreeReaderValue<Int_t> trigger_HLT_e24_lhtight_nod0_ivarloose = {fReader, "trigger_HLT_e24_lhtight_nod0_ivarloose"};
   TTreeReaderValue<Int_t> trigger_HLT_e26_lhtight_nod0_ivarloose = {fReader, "trigger_HLT_e26_lhtight_nod0_ivarloose"};
   TTreeReaderValue<Int_t> trigger_HLT_e60_lhmedium = {fReader, "trigger_HLT_e60_lhmedium"};
   TTreeReaderValue<Int_t> trigger_HLT_e60_lhmedium_nod0 = {fReader, "trigger_HLT_e60_lhmedium_nod0"};
   TTreeReaderValue<Int_t> trigger_HLT_e60_medium = {fReader, "trigger_HLT_e60_medium"};
   TTreeReaderValue<Int_t> trigger_HLT_mu20_iloose_L1MU15 = {fReader, "trigger_HLT_mu20_iloose_L1MU15"};
   TTreeReaderValue<Int_t> trigger_HLT_mu24_iloose = {fReader, "trigger_HLT_mu24_iloose"};
   TTreeReaderValue<Int_t> trigger_HLT_mu24_ivarmedium = {fReader, "trigger_HLT_mu24_ivarmedium"};
   TTreeReaderValue<Int_t> trigger_HLT_mu26_imedium = {fReader, "trigger_HLT_mu26_imedium"};
   TTreeReaderValue<Int_t> trigger_HLT_mu26_ivarmedium = {fReader, "trigger_HLT_mu26_ivarmedium"};
   TTreeReaderValue<Int_t> trigger_HLT_mu40 = {fReader, "trigger_HLT_mu40"};
   TTreeReaderValue<Int_t> trigger_HLT_mu50 = {fReader, "trigger_HLT_mu50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100_mht_L1XE50 = {fReader, "trigger_HLT_xe100_mht_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe110_mht_L1XE50 = {fReader, "trigger_HLT_xe110_mht_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe130_mht_L1XE50 = {fReader, "trigger_HLT_xe130_mht_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe70 = {fReader, "trigger_HLT_xe70"};
   TTreeReaderValue<Int_t> trigger_HLT_xe70_mht = {fReader, "trigger_HLT_xe70_mht"};
   TTreeReaderValue<Int_t> trigger_HLT_xe80_tc_lcw_L1XE50 = {fReader, "trigger_HLT_xe80_tc_lcw_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe90_mht_L1XE50 = {fReader, "trigger_HLT_xe90_mht_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe90_mht_wEFMu_L1XE50 = {fReader, "trigger_HLT_xe90_mht_wEFMu_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_e26_lhtight_nod0 = {fReader, "trigger_HLT_e26_lhtight_nod0"};
   TTreeReaderValue<Int_t> trigger_HLT_e300_etcut = {fReader, "trigger_HLT_e300_etcut"};
   TTreeReaderValue<Int_t> trigger_HLT_mu24_iloose_L1MU15 = {fReader, "trigger_HLT_mu24_iloose_L1MU15"};
   TTreeReaderValue<Int_t> trigger_HLT_mu24_imedium = {fReader, "trigger_HLT_mu24_imedium"};
   TTreeReaderValue<Int_t> trigger_HLT_mu24_ivarloose = {fReader, "trigger_HLT_mu24_ivarloose"};
   TTreeReaderValue<Int_t> trigger_HLT_mu24_ivarloose_L1MU15 = {fReader, "trigger_HLT_mu24_ivarloose_L1MU15"};
   TTreeReaderValue<Int_t> trigger_HLT_mu60_0eta105_msonly = {fReader, "trigger_HLT_mu60_0eta105_msonly"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100_L1XE50 = {fReader, "trigger_HLT_xe100_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100_pufit_L1XE50 = {fReader, "trigger_HLT_xe100_pufit_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100_pufit_L1XE55 = {fReader, "trigger_HLT_xe100_pufit_L1XE55"};
   TTreeReaderValue<Int_t> trigger_HLT_xe100_tc_em_L1XE50 = {fReader, "trigger_HLT_xe100_tc_em_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe110_pueta_L1XE50 = {fReader, "trigger_HLT_xe110_pueta_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe110_pufit_L1XE50 = {fReader, "trigger_HLT_xe110_pufit_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe110_pufit_L1XE55 = {fReader, "trigger_HLT_xe110_pufit_L1XE55"};
   TTreeReaderValue<Int_t> trigger_HLT_xe110_pufit_xe65_L1XE50 = {fReader, "trigger_HLT_xe110_pufit_xe65_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe110_pufit_xe70_L1XE50 = {fReader, "trigger_HLT_xe110_pufit_xe70_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe120_pueta = {fReader, "trigger_HLT_xe120_pueta"};
   TTreeReaderValue<Int_t> trigger_HLT_xe120_pufit = {fReader, "trigger_HLT_xe120_pufit"};
   TTreeReaderValue<Int_t> trigger_HLT_xe120_pufit_L1XE50 = {fReader, "trigger_HLT_xe120_pufit_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe120_tc_lcw_L1XE50 = {fReader, "trigger_HLT_xe120_tc_lcw_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe70_tc_lcw = {fReader, "trigger_HLT_xe70_tc_lcw"};
   TTreeReaderValue<Int_t> trigger_HLT_xe90_pufit_L1XE50 = {fReader, "trigger_HLT_xe90_pufit_L1XE50"};
   TTreeReaderValue<Int_t> trigger_HLT_xe90_tc_lcw_wEFMu_L1XE50 = {fReader, "trigger_HLT_xe90_tc_lcw_wEFMu_L1XE50"};
   TTreeReaderValue<Int_t> n_ph = {fReader, "n_ph"};
   TTreeReaderValue<Int_t> n_ph_baseline = {fReader, "n_ph_baseline"};
   TTreeReaderValue<Int_t> n_jet_truth = {fReader, "n_jet_truth"};
   TTreeReaderValue<Float_t> met_noelectron_tst_et = {fReader, "met_noelectron_tst_et"};
   TTreeReaderValue<Float_t> met_noelectron_tst_phi = {fReader, "met_noelectron_tst_phi"};
   TTreeReaderValue<Float_t> met_noelectron_tst_etx = {fReader, "met_noelectron_tst_etx"};
   TTreeReaderValue<Float_t> met_noelectron_tst_ety = {fReader, "met_noelectron_tst_ety"};
   TTreeReaderValue<Float_t> met_nomuon_tst_et = {fReader, "met_nomuon_tst_et"};
   TTreeReaderValue<Float_t> met_nomuon_tst_phi = {fReader, "met_nomuon_tst_phi"};
   TTreeReaderValue<Float_t> met_nomuon_tst_etx = {fReader, "met_nomuon_tst_etx"};
   TTreeReaderValue<Float_t> met_nomuon_tst_ety = {fReader, "met_nomuon_tst_ety"};
   TTreeReaderValue<Float_t> met_track_et = {fReader, "met_track_et"};
   TTreeReaderValue<Float_t> met_track_phi = {fReader, "met_track_phi"};
   TTreeReaderValue<Float_t> met_track_etx = {fReader, "met_track_etx"};
   TTreeReaderValue<Float_t> met_track_ety = {fReader, "met_track_ety"};
   TTreeReaderValue<Float_t> met_track_noelectron_et = {fReader, "met_track_noelectron_et"};
   TTreeReaderValue<Float_t> met_track_noelectron_phi = {fReader, "met_track_noelectron_phi"};
   TTreeReaderValue<Float_t> met_track_noelectron_etx = {fReader, "met_track_noelectron_etx"};
   TTreeReaderValue<Float_t> met_track_noelectron_ety = {fReader, "met_track_noelectron_ety"};
   TTreeReaderValue<Float_t> met_track_nomuon_et = {fReader, "met_track_nomuon_et"};
   TTreeReaderValue<Float_t> met_track_nomuon_phi = {fReader, "met_track_nomuon_phi"};
   TTreeReaderValue<Float_t> met_track_nomuon_etx = {fReader, "met_track_nomuon_etx"};
   TTreeReaderValue<Float_t> met_track_nomuon_ety = {fReader, "met_track_nomuon_ety"};
   TTreeReaderValue<Float_t> met_truth_et = {fReader, "met_truth_et"};
   TTreeReaderValue<Float_t> met_truth_phi = {fReader, "met_truth_phi"};
   TTreeReaderValue<Float_t> met_truth_etx = {fReader, "met_truth_etx"};
   TTreeReaderValue<Float_t> met_truth_ety = {fReader, "met_truth_ety"};
   TTreeReaderValue<Float_t> met_tst_et = {fReader, "met_tst_et"};
   TTreeReaderValue<Float_t> met_tst_phi = {fReader, "met_tst_phi"};
   TTreeReaderValue<Float_t> met_tst_etx = {fReader, "met_tst_etx"};
   TTreeReaderValue<Float_t> met_tst_ety = {fReader, "met_tst_ety"};
   TTreeReaderValue<Int_t> tau_loose_multiplicity = {fReader, "tau_loose_multiplicity"};
   TTreeReaderValue<Int_t> tau_medium_multiplicity = {fReader, "tau_medium_multiplicity"};
   TTreeReaderValue<Int_t> tau_tight_multiplicity = {fReader, "tau_tight_multiplicity"};
   TTreeReaderValue<Int_t> tau_baseline_loose_multiplicity = {fReader, "tau_baseline_loose_multiplicity"};
   TTreeReaderValue<Int_t> tau_baseline_medium_multiplicity = {fReader, "tau_baseline_medium_multiplicity"};
   TTreeReaderValue<Int_t> tau_baseline_tight_multiplicity = {fReader, "tau_baseline_tight_multiplicity"};

   TTreeReaderValue<vector<bool>> mu_baseline_isLooseID = {fReader, "mu_baseline_isLooseID"};
   TTreeReaderValue<vector<bool>> mu_baseline_isMediumID = {fReader, "mu_baseline_isMediumID"};
   TTreeReaderValue<vector<bool>> mu_baseline_isTightID = {fReader, "mu_baseline_isTightID"};
   TTreeReaderValue<vector<bool>> el_baseline_isLooseID = {fReader, "el_baseline_isLooseID"};
   TTreeReaderValue<vector<bool>> el_baseline_isMediumID = {fReader, "el_baseline_isMediumID"};
   TTreeReaderValue<vector<bool>> el_baseline_isTightID = {fReader, "el_baseline_isTightID"};
   TTreeReaderValue<vector<float>> el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down = {fReader, "el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down"};
   TTreeReaderValue<vector<float>> el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up = {fReader, "el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up"};
   TTreeReaderValue<vector<float>> el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down = {fReader, "el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down"};
   TTreeReaderValue<vector<float>> el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up = {fReader, "el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up"};
   TTreeReaderValue<vector<float>> el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down = {fReader, "el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down"};
   TTreeReaderValue<vector<float>> el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up = {fReader, "el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up"};
   TTreeReaderValue<vector<float>> el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down = {fReader, "el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down"};
   TTreeReaderValue<vector<float>> el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up = {fReader, "el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up"};
   TTreeReaderValue<vector<float>> el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down = {fReader, "el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down"};
   TTreeReaderValue<vector<float>> el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up = {fReader, "el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up"};
   TTreeReaderValue<vector<float>> el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down = {fReader, "el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down"};
   TTreeReaderValue<vector<float>> el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up = {fReader, "el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_BADMUON_SYS__1down = {fReader, "mu_SF_syst_MUON_EFF_BADMUON_SYS__1down"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_BADMUON_SYS__1up = {fReader, "mu_SF_syst_MUON_EFF_BADMUON_SYS__1up"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_ISO_STAT__1down = {fReader, "mu_SF_syst_MUON_EFF_ISO_STAT__1down"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_ISO_STAT__1up = {fReader, "mu_SF_syst_MUON_EFF_ISO_STAT__1up"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_ISO_SYS__1down = {fReader, "mu_SF_syst_MUON_EFF_ISO_SYS__1down"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_ISO_SYS__1up = {fReader, "mu_SF_syst_MUON_EFF_ISO_SYS__1up"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down = {fReader, "mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up = {fReader, "mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_RECO_STAT__1down = {fReader, "mu_SF_syst_MUON_EFF_RECO_STAT__1down"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_RECO_STAT__1up = {fReader, "mu_SF_syst_MUON_EFF_RECO_STAT__1up"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down = {fReader, "mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up = {fReader, "mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_RECO_SYS__1down = {fReader, "mu_SF_syst_MUON_EFF_RECO_SYS__1down"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_RECO_SYS__1up = {fReader, "mu_SF_syst_MUON_EFF_RECO_SYS__1up"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_TTVA_STAT__1down = {fReader, "mu_SF_syst_MUON_EFF_TTVA_STAT__1down"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_TTVA_STAT__1up = {fReader, "mu_SF_syst_MUON_EFF_TTVA_STAT__1up"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_TTVA_SYS__1down = {fReader, "mu_SF_syst_MUON_EFF_TTVA_SYS__1down"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_TTVA_SYS__1up = {fReader, "mu_SF_syst_MUON_EFF_TTVA_SYS__1up"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_TrigStatUncertainty__1down = {fReader, "mu_SF_syst_MUON_EFF_TrigStatUncertainty__1down"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_TrigStatUncertainty__1up = {fReader, "mu_SF_syst_MUON_EFF_TrigStatUncertainty__1up"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_TrigSystUncertainty__1down = {fReader, "mu_SF_syst_MUON_EFF_TrigSystUncertainty__1down"};
   TTreeReaderValue<vector<float>> mu_SF_syst_MUON_EFF_TrigSystUncertainty__1up = {fReader, "mu_SF_syst_MUON_EFF_TrigSystUncertainty__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down = {fReader, "tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up = {fReader, "tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down = {fReader, "tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up = {fReader, "tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1up"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1down = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1down"};
   TTreeReaderValue<vector<float>> tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1up = {fReader, "tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1up"};
   TTreeReaderValue<vector<float>> ph_SF_syst_PH_EFF_ID_Uncertainty__1down = {fReader, "ph_SF_syst_PH_EFF_ID_Uncertainty__1down"};
   TTreeReaderValue<vector<float>> ph_SF_syst_PH_EFF_ID_Uncertainty__1up = {fReader, "ph_SF_syst_PH_EFF_ID_Uncertainty__1up"};
   TTreeReaderValue<vector<float>> ph_SF_syst_PH_EFF_ISO_Uncertainty__1down = {fReader, "ph_SF_syst_PH_EFF_ISO_Uncertainty__1down"};
   TTreeReaderValue<vector<float>> ph_SF_syst_PH_EFF_ISO_Uncertainty__1up = {fReader, "ph_SF_syst_PH_EFF_ISO_Uncertainty__1up"};
   TTreeReaderValue<vector<float>> ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1down = {fReader, "ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1down"};
   TTreeReaderValue<vector<float>> ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1up = {fReader, "ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1up"};
   TTreeReaderValue<vector<float>> jet_SF_syst_JET_JvtEfficiency__1down = {fReader, "jet_SF_syst_JET_JvtEfficiency__1down"};
   TTreeReaderValue<vector<float>> jet_SF_syst_JET_JvtEfficiency__1up = {fReader, "jet_SF_syst_JET_JvtEfficiency__1up"};
   TTreeReaderValue<vector<float>> pu_weight_syst_PRW_DATASF__1down = {fReader, "pu_weight_syst_PRW_DATASF__1down"};
   TTreeReaderValue<vector<float>> pu_weight_syst_PRW_DATASF__1up = {fReader, "pu_weight_syst_PRW_DATASF__1up"};
   TTreeReaderValue<vector<float>> btag_weight_syst_FT_EFF_B_systematics__1down = {fReader, "btag_weight_syst_FT_EFF_B_systematics__1down"};
   TTreeReaderValue<vector<float>> btag_weight_syst_FT_EFF_B_systematics__1up = {fReader, "btag_weight_syst_FT_EFF_B_systematics__1up"};
   TTreeReaderValue<vector<float>> btag_weight_syst_FT_EFF_C_systematics__1down = {fReader, "btag_weight_syst_FT_EFF_C_systematics__1down"};
   TTreeReaderValue<vector<float>> btag_weight_syst_FT_EFF_C_systematics__1up = {fReader, "btag_weight_syst_FT_EFF_C_systematics__1up"};
   TTreeReaderValue<vector<float>> btag_weight_syst_FT_EFF_Light_systematics__1down = {fReader, "btag_weight_syst_FT_EFF_Light_systematics__1down"};
   TTreeReaderValue<vector<float>> btag_weight_syst_FT_EFF_Light_systematics__1up = {fReader, "btag_weight_syst_FT_EFF_Light_systematics__1up"};
   TTreeReaderValue<vector<float>> btag_weight_syst_FT_EFF_extrapolation__1down = {fReader, "btag_weight_syst_FT_EFF_extrapolation__1down"};
   TTreeReaderValue<vector<float>> btag_weight_syst_FT_EFF_extrapolation__1up = {fReader, "btag_weight_syst_FT_EFF_extrapolation__1up"};
   TTreeReaderValue<vector<float>> btag_weight_syst_FT_EFF_extrapolation_from_charm__1down = {fReader, "btag_weight_syst_FT_EFF_extrapolation_from_charm__1down"};
   TTreeReaderValue<vector<float>> btag_weight_syst_FT_EFF_extrapolation_from_charm__1up = {fReader, "btag_weight_syst_FT_EFF_extrapolation_from_charm__1up"};
   TTreeReaderValue<vector<float>> btagloose_weight_syst_FT_EFF_B_systematics__1down = {fReader, "btagloose_weight_syst_FT_EFF_B_systematics__1down"};
   TTreeReaderValue<vector<float>> btagloose_weight_syst_FT_EFF_B_systematics__1up = {fReader, "btagloose_weight_syst_FT_EFF_B_systematics__1up"};
   TTreeReaderValue<vector<float>> btagloose_weight_syst_FT_EFF_C_systematics__1down = {fReader, "btagloose_weight_syst_FT_EFF_C_systematics__1down"};
   TTreeReaderValue<vector<float>> btagloose_weight_syst_FT_EFF_C_systematics__1up = {fReader, "btagloose_weight_syst_FT_EFF_C_systematics__1up"};
   TTreeReaderValue<vector<float>> btagloose_weight_syst_FT_EFF_Light_systematics__1down = {fReader, "btagloose_weight_syst_FT_EFF_Light_systematics__1down"};
   TTreeReaderValue<vector<float>> btagloose_weight_syst_FT_EFF_Light_systematics__1up = {fReader, "btagloose_weight_syst_FT_EFF_Light_systematics__1up"};
   TTreeReaderValue<vector<float>> btagloose_weight_syst_FT_EFF_extrapolation__1down = {fReader, "btagloose_weight_syst_FT_EFF_extrapolation__1down"};
   TTreeReaderValue<vector<float>> btagloose_weight_syst_FT_EFF_extrapolation__1up = {fReader, "btagloose_weight_syst_FT_EFF_extrapolation__1up"};
   TTreeReaderValue<vector<float>> btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down = {fReader, "btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down"};
   TTreeReaderValue<vector<float>> btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up = {fReader, "btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up"};
   TTreeReaderValue<vector<float>> trkbtag_weight_syst_FT_EFF_B_systematics__1down = {fReader, "trkbtag_weight_syst_FT_EFF_B_systematics__1down"};
   TTreeReaderValue<vector<float>> trkbtag_weight_syst_FT_EFF_B_systematics__1up = {fReader, "trkbtag_weight_syst_FT_EFF_B_systematics__1up"};
   TTreeReaderValue<vector<float>> trkbtag_weight_syst_FT_EFF_C_systematics__1down = {fReader, "trkbtag_weight_syst_FT_EFF_C_systematics__1down"};
   TTreeReaderValue<vector<float>> trkbtag_weight_syst_FT_EFF_C_systematics__1up = {fReader, "trkbtag_weight_syst_FT_EFF_C_systematics__1up"};
   TTreeReaderValue<vector<float>> trkbtag_weight_syst_FT_EFF_Light_systematics__1down = {fReader, "trkbtag_weight_syst_FT_EFF_Light_systematics__1down"};
   TTreeReaderValue<vector<float>> trkbtag_weight_syst_FT_EFF_Light_systematics__1up = {fReader, "trkbtag_weight_syst_FT_EFF_Light_systematics__1up"};
   TTreeReaderValue<vector<float>> trkbtag_weight_syst_FT_EFF_extrapolation__1down = {fReader, "trkbtag_weight_syst_FT_EFF_extrapolation__1down"};
   TTreeReaderValue<vector<float>> trkbtag_weight_syst_FT_EFF_extrapolation__1up = {fReader, "trkbtag_weight_syst_FT_EFF_extrapolation__1up"};
   TTreeReaderValue<vector<float>> trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1down = {fReader, "trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1down"};
   TTreeReaderValue<vector<float>> trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1up = {fReader, "trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1up"};
   TTreeReaderValue<vector<float>> trkbtagloose_weight_syst_FT_EFF_B_systematics__1down = {fReader, "trkbtagloose_weight_syst_FT_EFF_B_systematics__1down"};
   TTreeReaderValue<vector<float>> trkbtagloose_weight_syst_FT_EFF_B_systematics__1up = {fReader, "trkbtagloose_weight_syst_FT_EFF_B_systematics__1up"};
   TTreeReaderValue<vector<float>> trkbtagloose_weight_syst_FT_EFF_C_systematics__1down = {fReader, "trkbtagloose_weight_syst_FT_EFF_C_systematics__1down"};
   TTreeReaderValue<vector<float>> trkbtagloose_weight_syst_FT_EFF_C_systematics__1up = {fReader, "trkbtagloose_weight_syst_FT_EFF_C_systematics__1up"};
   TTreeReaderValue<vector<float>> trkbtagloose_weight_syst_FT_EFF_Light_systematics__1down = {fReader, "trkbtagloose_weight_syst_FT_EFF_Light_systematics__1down"};
   TTreeReaderValue<vector<float>> trkbtagloose_weight_syst_FT_EFF_Light_systematics__1up = {fReader, "trkbtagloose_weight_syst_FT_EFF_Light_systematics__1up"};
   TTreeReaderValue<vector<float>> trkbtagloose_weight_syst_FT_EFF_extrapolation__1down = {fReader, "trkbtagloose_weight_syst_FT_EFF_extrapolation__1down"};
   TTreeReaderValue<vector<float>> trkbtagloose_weight_syst_FT_EFF_extrapolation__1up = {fReader, "trkbtagloose_weight_syst_FT_EFF_extrapolation__1up"};
   TTreeReaderValue<vector<float>> trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down = {fReader, "trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down"};
   TTreeReaderValue<vector<float>> trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up = {fReader, "trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up"};
   TTreeReaderValue<vector<float>> mconly_weights = {fReader, "mconly_weights"};
   TTreeReaderValue<vector<float>> mu_pt = {fReader, "mu_pt"};
   TTreeReaderValue<vector<double>> mu_SF = {fReader, "mu_SF"};
   TTreeReaderValue<vector<float>> mu_eta = {fReader, "mu_eta"};
   TTreeReaderValue<vector<float>> mu_phi = {fReader, "mu_phi"};
   TTreeReaderValue<vector<double>> mu_SF_iso = {fReader, "mu_SF_iso"};
   TTreeReaderValue<vector<float>> mu_m = {fReader, "mu_m"};
   TTreeReaderValue<vector<float>> mu_charge = {fReader, "mu_charge"};
   TTreeReaderValue<vector<float>> mu_ptcone20 = {fReader, "mu_ptcone20"};
   TTreeReaderValue<vector<float>> mu_baseline_pt = {fReader, "mu_baseline_pt"};
   TTreeReaderValue<vector<float>> mu_baseline_ptcone20 = {fReader, "mu_baseline_ptcone20"};
   TTreeReaderValue<vector<double>> mu_baseline_SF = {fReader, "mu_baseline_SF"};
   TTreeReaderValue<vector<float>> mu_baseline_eta = {fReader, "mu_baseline_eta"};
   TTreeReaderValue<vector<float>> mu_baseline_phi = {fReader, "mu_baseline_phi"};
   TTreeReaderValue<vector<float>> el_pt = {fReader, "el_pt"};
   TTreeReaderValue<vector<float>> el_eta = {fReader, "el_eta"};
   TTreeReaderValue<vector<float>> el_phi = {fReader, "el_phi"};
   TTreeReaderValue<vector<double>> el_SF = {fReader, "el_SF"};
   TTreeReaderValue<vector<double>> el_SF_iso = {fReader, "el_SF_iso"};
   TTreeReaderValue<vector<double>> el_SF_trigger = {fReader, "el_SF_trigger"};
   TTreeReaderValue<vector<double>> el_eff_trigger = {fReader, "el_eff_trigger"};
   TTreeReaderValue<vector<float>> el_topoetcone20 = {fReader, "el_topoetcone20"};
   TTreeReaderValue<vector<float>> el_ptcone20 = {fReader, "el_ptcone20"};
   TTreeReaderValue<vector<float>> el_m = {fReader, "el_m"};
   TTreeReaderValue<vector<float>> el_charge = {fReader, "el_charge"};
   TTreeReaderValue<vector<float>> el_ptvarcone20_TightTTVA_pt1000 = {fReader, "el_ptvarcone20_TightTTVA_pt1000"};
   TTreeReaderValue<vector<float>> el_baseline_pt = {fReader, "el_baseline_pt"};
   TTreeReaderValue<vector<float>> el_baseline_topoetcone20 = {fReader, "el_baseline_topoetcone20"};
   TTreeReaderValue<vector<float>> el_baseline_ptcone20 = {fReader, "el_baseline_ptcone20"};
   TTreeReaderValue<vector<double>> el_baseline_SF = {fReader, "el_baseline_SF"};
   TTreeReaderValue<vector<float>> el_baseline_eta = {fReader, "el_baseline_eta"};
   TTreeReaderValue<vector<float>> el_baseline_phi = {fReader, "el_baseline_phi"};
   TTreeReaderValue<vector<float>> jet_pt = {fReader, "jet_pt"};
   TTreeReaderValue<vector<float>> jet_eta = {fReader, "jet_eta"};
   TTreeReaderValue<vector<float>> jet_phi = {fReader, "jet_phi"};
   TTreeReaderValue<vector<float>> jet_m = {fReader, "jet_m"};
   TTreeReaderValue<vector<float>> jet_fmax = {fReader, "jet_fmax"};
   TTreeReaderValue<vector<float>> jet_fch = {fReader, "jet_fch"};
   TTreeReaderValue<vector<int>> jet_isbjet = {fReader, "jet_isbjet"};
   TTreeReaderValue<vector<int>> jet_PartonTruthLabelID = {fReader, "jet_PartonTruthLabelID"};
   TTreeReaderValue<vector<int>> jet_ConeTruthLabelID = {fReader, "jet_ConeTruthLabelID"};
   TTreeReaderValue<vector<float>> jet_timing = {fReader, "jet_timing"};
   TTreeReaderValue<vector<float>> jet_emfrac = {fReader, "jet_emfrac"};
   TTreeReaderValue<vector<float>> jet_hecf = {fReader, "jet_hecf"};
   TTreeReaderValue<vector<float>> jet_hecq = {fReader, "jet_hecq"};
   TTreeReaderValue<vector<float>> jet_larq = {fReader, "jet_larq"};
   TTreeReaderValue<vector<float>> jet_avglarq = {fReader, "jet_avglarq"};
   TTreeReaderValue<vector<float>> jet_negE = {fReader, "jet_negE"};
   TTreeReaderValue<vector<float>> jet_lambda = {fReader, "jet_lambda"};
   TTreeReaderValue<vector<float>> jet_lambda2 = {fReader, "jet_lambda2"};
   TTreeReaderValue<vector<float>> jet_jvtxf = {fReader, "jet_jvtxf"};
   TTreeReaderValue<vector<int>> jet_fmaxi = {fReader, "jet_fmaxi"};
   TTreeReaderValue<vector<int>> jet_isbjet_loose = {fReader, "jet_isbjet_loose"};
   TTreeReaderValue<vector<float>> jet_jvt = {fReader, "jet_jvt"};
   TTreeReaderValue<vector<int>> jet_cleaning = {fReader, "jet_cleaning"};
   TTreeReaderValue<vector<int>> jet_DFCommonJets_QGTagger_NTracks = {fReader, "jet_DFCommonJets_QGTagger_NTracks"};
   TTreeReaderValue<vector<float>> jet_DFCommonJets_QGTagger_TracksWidth = {fReader, "jet_DFCommonJets_QGTagger_TracksWidth"};
   TTreeReaderValue<vector<float>> jet_DFCommonJets_QGTagger_TracksC1 = {fReader, "jet_DFCommonJets_QGTagger_TracksC1"};
   TTreeReaderValue<vector<float>> jet_truth_pt = {fReader, "jet_truth_pt"};
   TTreeReaderValue<vector<float>> jet_truth_eta = {fReader, "jet_truth_eta"};
   TTreeReaderValue<vector<float>> jet_truth_phi = {fReader, "jet_truth_phi"};
   TTreeReaderValue<vector<float>> LCTopoJet_pt = {fReader, "LCTopoJet_pt"};
   TTreeReaderValue<vector<float>> LCTopoJet_eta = {fReader, "LCTopoJet_eta"};
   TTreeReaderValue<vector<float>> LCTopoJet_phi = {fReader, "LCTopoJet_phi"};
   TTreeReaderValue<vector<float>> LCTopoJet_m = {fReader, "LCTopoJet_m"};
   TTreeReaderValue<vector<float>> LCTopoJet_tau21 = {fReader, "LCTopoJet_tau21"};
   TTreeReaderValue<vector<float>> LCTopoJet_D2 = {fReader, "LCTopoJet_D2"};
   TTreeReaderValue<vector<float>> LCTopoJet_C2 = {fReader, "LCTopoJet_C2"};
   TTreeReaderValue<vector<int>> LCTopoJet_nTrk = {fReader, "LCTopoJet_nTrk"};
   TTreeReaderValue<vector<int>> LCTopoJet_nConstit = {fReader, "LCTopoJet_nConstit"};
   TTreeReaderValue<vector<int>> LCTopoJet_passD2_W50 = {fReader, "LCTopoJet_passD2_W50"};
   TTreeReaderValue<vector<int>> LCTopoJet_passD2_Z50 = {fReader, "LCTopoJet_passD2_Z50"};
   TTreeReaderValue<vector<int>> LCTopoJet_passD2_W80 = {fReader, "LCTopoJet_passD2_W80"};
   TTreeReaderValue<vector<int>> LCTopoJet_passD2_Z80 = {fReader, "LCTopoJet_passD2_Z80"};
   TTreeReaderValue<vector<int>> LCTopoJet_passMass_W50 = {fReader, "LCTopoJet_passMass_W50"};
   TTreeReaderValue<vector<int>> LCTopoJet_passMass_Z50 = {fReader, "LCTopoJet_passMass_Z50"};
   TTreeReaderValue<vector<int>> LCTopoJet_passMass_W80 = {fReader, "LCTopoJet_passMass_W80"};
   TTreeReaderValue<vector<int>> LCTopoJet_passMass_Z80 = {fReader, "LCTopoJet_passMass_Z80"};
   TTreeReaderValue<vector<float>> TruthFatJet_pt = {fReader, "TruthFatJet_pt"};
   TTreeReaderValue<vector<float>> TruthFatJet_eta = {fReader, "TruthFatJet_eta"};
   TTreeReaderValue<vector<float>> TruthFatJet_phi = {fReader, "TruthFatJet_phi"};
   TTreeReaderValue<vector<float>> TruthFatJet_m = {fReader, "TruthFatJet_m"};
   TTreeReaderValue<vector<float>> TruthFatJet_tau21 = {fReader, "TruthFatJet_tau21"};
   TTreeReaderValue<vector<float>> TruthFatJet_D2 = {fReader, "TruthFatJet_D2"};
   TTreeReaderValue<vector<float>> TruthFatJet_C2 = {fReader, "TruthFatJet_C2"};
   TTreeReaderValue<vector<int>> TruthFatJet_nTrk = {fReader, "TruthFatJet_nTrk"};
   TTreeReaderValue<vector<int>> TruthFatJet_nConstit = {fReader, "TruthFatJet_nConstit"};

   

   MonoVReader(TTree * /*tree*/ = 0) {

      graph_xSec = 0;
      graph_filterEff = 0;
      graph_kFactor = 0;
      graph_MCsum = 0;
      graph_rawEvents = 0;

   };

    virtual ~MonoVReader() { };
    virtual Int_t   Version() const
   {
      return 2;
   }
    virtual void    Begin(TTree *tree);
    virtual void    SlaveBegin(TTree *tree);
    virtual void    Init(TTree *tree);
    virtual Bool_t  Notify();
    virtual Bool_t  Process(Long64_t entry);
    virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0)
   {
      return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0;
   }
   virtual void    SetOption(const char *option)
   {
      fOption = option;
   }
   virtual void    SetObject(TObject *obj)
   {
      fObject = obj;
   }
   virtual void    SetInputList(TList *input)
   {
      fInput = input;
   }
   virtual TList  *GetOutputList() const
   {
      return fOutput;
   }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(MonoVReader, 0);

};

void MonoVReader::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t MonoVReader::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


void MonoVReader::FillMinitree()
{
   shortTree->run = *run;
   shortTree->event = *event;
   shortTree->last = *last;
   shortTree->year = *year;
   shortTree->met_tst_sig = *met_tst_sig;
   shortTree->xSec_SUSY = *xSec_SUSY;
   shortTree->k_factor = *k_factor;
   shortTree->filter_eff = *filter_eff;
   shortTree->n_tau_baseline = *n_tau_baseline;
   shortTree->mconly_weight = *mconly_weight;
   shortTree->pu_weight = *pu_weight;
   shortTree->btag_weight = *btag_weight;
   shortTree->btagloose_weight = *btagloose_weight;
   shortTree->trkbtag_weight = *trkbtag_weight;
   shortTree->trkbtagloose_weight = *trkbtagloose_weight;
   shortTree->jvt_weight = *jvt_weight;
   shortTree->munu_mT = *munu_mT;
   shortTree->enu_mT = *enu_mT;
   shortTree->mumu_m = *mumu_m;
   shortTree->ee_m = *ee_m;
   shortTree->dPhiLCTopoJetMet = *dPhiLCTopoJetMet;
   shortTree->dPhiLCTopoJetMetNoElectron = *dPhiLCTopoJetMetNoElectron;
   shortTree->dPhiLCTopoJetMetNoMuon = *dPhiLCTopoJetMetNoMuon;
   shortTree->dPhiDijetMet = *dPhiDijetMet;
   shortTree->dPhiDijetMetNoElectron = *dPhiDijetMetNoElectron;
   shortTree->dPhiDijetMetNoMuon = *dPhiDijetMetNoMuon;
   shortTree->dPhiDijet = *dPhiDijet;
   shortTree->dRDijet = *dRDijet;
   shortTree->DijetSumPt = *DijetSumPt;
   shortTree->TrijetSumPt = *TrijetSumPt;
   shortTree->DijetMass = *DijetMass;
   shortTree->n_trackjet = *n_trackjet;
   shortTree->n_btrackJet = *n_btrackJet;
   shortTree->n_btrackJet_loose = *n_btrackJet_loose;
   shortTree->n_trackLCTopoAssociatedBjet = *n_trackLCTopoAssociatedBjet;
   shortTree->n_trackLCTopoSeparatedBjet = *n_trackLCTopoSeparatedBjet;
   shortTree->n_trackLCTopoAssociatedBjetLoose = *n_trackLCTopoAssociatedBjetLoose;
   shortTree->n_trackLCTopoSeparatedBjetLoose = *n_trackLCTopoSeparatedBjetLoose;
   shortTree->n_jet = *n_jet;
   shortTree->n_bjet = *n_bjet;
   shortTree->n_bjet_loose = *n_bjet_loose;
   shortTree->n_el = *n_el;
   shortTree->n_el_baseline = *n_el_baseline;
   shortTree->n_mu_baseline = *n_mu_baseline;
   shortTree->n_tau = *n_tau;
   shortTree->n_mu = *n_mu;
   shortTree->n_truthFatJet = *n_truthFatJet;
   shortTree->n_LCTopoJet = *n_LCTopoJet;
   shortTree->averageIntPerXing = *averageIntPerXing;
   shortTree->actualIntPerXing = *actualIntPerXing;
   shortTree->corAverageIntPerXing = *corAverageIntPerXing;
   shortTree->corActualIntPerXing = *corActualIntPerXing;
   shortTree->n_vx = *n_vx;
   shortTree->pu_hash = *pu_hash;
   shortTree->trigger_HLT_e120_lhloose = *trigger_HLT_e120_lhloose;
   shortTree->trigger_HLT_e140_lhloose_nod0 = *trigger_HLT_e140_lhloose_nod0;
   shortTree->trigger_HLT_e24_lhmedium_L1EM20VH = *trigger_HLT_e24_lhmedium_L1EM20VH;
   shortTree->trigger_HLT_e24_lhtight_nod0_ivarloose = *trigger_HLT_e24_lhtight_nod0_ivarloose;
   shortTree->trigger_HLT_e26_lhtight_nod0_ivarloose = *trigger_HLT_e26_lhtight_nod0_ivarloose;
   shortTree->trigger_HLT_e60_lhmedium = *trigger_HLT_e60_lhmedium;
   shortTree->trigger_HLT_e60_lhmedium_nod0 = *trigger_HLT_e60_lhmedium_nod0;
   shortTree->trigger_HLT_e60_medium = *trigger_HLT_e60_medium;
   shortTree->trigger_HLT_mu20_iloose_L1MU15 = *trigger_HLT_mu20_iloose_L1MU15;
   shortTree->trigger_HLT_mu24_iloose = *trigger_HLT_mu24_iloose;
   shortTree->trigger_HLT_mu24_ivarmedium = *trigger_HLT_mu24_ivarmedium;
   shortTree->trigger_HLT_mu26_imedium = *trigger_HLT_mu26_imedium;
   shortTree->trigger_HLT_mu26_ivarmedium = *trigger_HLT_mu26_ivarmedium;
   shortTree->trigger_HLT_mu40 = *trigger_HLT_mu40;
   shortTree->trigger_HLT_mu50 = *trigger_HLT_mu50;
   shortTree->trigger_HLT_xe100_mht_L1XE50 = *trigger_HLT_xe100_mht_L1XE50;
   shortTree->trigger_HLT_xe110_mht_L1XE50 = *trigger_HLT_xe110_mht_L1XE50;
   shortTree->trigger_HLT_xe130_mht_L1XE50 = *trigger_HLT_xe130_mht_L1XE50;
   shortTree->trigger_HLT_xe70 = *trigger_HLT_xe70;
   shortTree->trigger_HLT_xe70_mht = *trigger_HLT_xe70_mht;
   shortTree->trigger_HLT_xe80_tc_lcw_L1XE50 = *trigger_HLT_xe80_tc_lcw_L1XE50;
   shortTree->trigger_HLT_xe90_mht_L1XE50 = *trigger_HLT_xe90_mht_L1XE50;
   shortTree->trigger_HLT_xe90_mht_wEFMu_L1XE50 = *trigger_HLT_xe90_mht_wEFMu_L1XE50;
   shortTree->trigger_HLT_e26_lhtight_nod0 = *trigger_HLT_e26_lhtight_nod0;
   shortTree->trigger_HLT_e300_etcut = *trigger_HLT_e300_etcut;
   shortTree->trigger_HLT_mu24_iloose_L1MU15 = *trigger_HLT_mu24_iloose_L1MU15;
   shortTree->trigger_HLT_mu24_imedium = *trigger_HLT_mu24_imedium;
   shortTree->trigger_HLT_mu24_ivarloose = *trigger_HLT_mu24_ivarloose;
   shortTree->trigger_HLT_mu24_ivarloose_L1MU15 = *trigger_HLT_mu24_ivarloose_L1MU15;
   shortTree->trigger_HLT_mu60_0eta105_msonly = *trigger_HLT_mu60_0eta105_msonly;
   shortTree->trigger_HLT_xe100_L1XE50 = *trigger_HLT_xe100_L1XE50;
   shortTree->trigger_HLT_xe100_pufit_L1XE50 = *trigger_HLT_xe100_pufit_L1XE50;
   shortTree->trigger_HLT_xe100_pufit_L1XE55 = *trigger_HLT_xe100_pufit_L1XE55;
   shortTree->trigger_HLT_xe100_tc_em_L1XE50 = *trigger_HLT_xe100_tc_em_L1XE50;
   shortTree->trigger_HLT_xe110_pueta_L1XE50 = *trigger_HLT_xe110_pueta_L1XE50;
   shortTree->trigger_HLT_xe110_pufit_L1XE50 = *trigger_HLT_xe110_pufit_L1XE50;
   shortTree->trigger_HLT_xe110_pufit_L1XE55 = *trigger_HLT_xe110_pufit_L1XE55;
   shortTree->trigger_HLT_xe110_pufit_xe65_L1XE50 = *trigger_HLT_xe110_pufit_xe65_L1XE50;
   shortTree->trigger_HLT_xe110_pufit_xe70_L1XE50 = *trigger_HLT_xe110_pufit_xe70_L1XE50;
   shortTree->trigger_HLT_xe120_pueta = *trigger_HLT_xe120_pueta;
   shortTree->trigger_HLT_xe120_pufit = *trigger_HLT_xe120_pufit;
   shortTree->trigger_HLT_xe120_pufit_L1XE50 = *trigger_HLT_xe120_pufit_L1XE50;
   shortTree->trigger_HLT_xe120_tc_lcw_L1XE50 = *trigger_HLT_xe120_tc_lcw_L1XE50;
   shortTree->trigger_HLT_xe70_tc_lcw = *trigger_HLT_xe70_tc_lcw;
   shortTree->trigger_HLT_xe90_pufit_L1XE50 = *trigger_HLT_xe90_pufit_L1XE50;
   shortTree->trigger_HLT_xe90_tc_lcw_wEFMu_L1XE50 = *trigger_HLT_xe90_tc_lcw_wEFMu_L1XE50;
   shortTree->n_ph = *n_ph;
   shortTree->n_ph_baseline = *n_ph_baseline;
   shortTree->n_jet_truth = *n_jet_truth;
   shortTree->met_noelectron_tst_et = *met_noelectron_tst_et;
   shortTree->met_noelectron_tst_phi = *met_noelectron_tst_phi;
   shortTree->met_noelectron_tst_etx = *met_noelectron_tst_etx;
   shortTree->met_noelectron_tst_ety = *met_noelectron_tst_ety;
   shortTree->met_nomuon_tst_et = *met_nomuon_tst_et;
   shortTree->met_nomuon_tst_phi = *met_nomuon_tst_phi;
   shortTree->met_nomuon_tst_etx = *met_nomuon_tst_etx;
   shortTree->met_nomuon_tst_ety = *met_nomuon_tst_ety;
   shortTree->met_track_et = *met_track_et;
   shortTree->met_track_phi = *met_track_phi;
   shortTree->met_track_etx = *met_track_etx;
   shortTree->met_track_ety = *met_track_ety;
   shortTree->met_track_noelectron_et = *met_track_noelectron_et;
   shortTree->met_track_noelectron_phi = *met_track_noelectron_phi;
   shortTree->met_track_noelectron_etx = *met_track_noelectron_etx;
   shortTree->met_track_noelectron_ety = *met_track_noelectron_ety;
   shortTree->met_track_nomuon_et = *met_track_nomuon_et;
   shortTree->met_track_nomuon_phi = *met_track_nomuon_phi;
   shortTree->met_track_nomuon_etx = *met_track_nomuon_etx;
   shortTree->met_track_nomuon_ety = *met_track_nomuon_ety;
   shortTree->met_truth_et = *met_truth_et;
   shortTree->met_truth_phi = *met_truth_phi;
   shortTree->met_truth_etx = *met_truth_etx;
   shortTree->met_truth_ety = *met_truth_ety;
   shortTree->met_tst_et = *met_tst_et;
   shortTree->met_tst_phi = *met_tst_phi;
   shortTree->met_tst_etx = *met_tst_etx;
   shortTree->met_tst_ety = *met_tst_ety;
   shortTree->tau_loose_multiplicity = *tau_loose_multiplicity;
   shortTree->tau_medium_multiplicity = *tau_medium_multiplicity;
   shortTree->tau_tight_multiplicity = *tau_tight_multiplicity;
   shortTree->tau_baseline_loose_multiplicity = *tau_baseline_loose_multiplicity;
   shortTree->tau_baseline_medium_multiplicity = *tau_baseline_medium_multiplicity;
   shortTree->tau_baseline_tight_multiplicity = *tau_baseline_tight_multiplicity;

   shortTree->mu_baseline_isLooseID = &(*mu_baseline_isLooseID);
   shortTree->mu_baseline_isMediumID = &(*mu_baseline_isMediumID);
   shortTree->mu_baseline_isTightID = &(*mu_baseline_isTightID);
   shortTree->el_baseline_isLooseID = &(*el_baseline_isLooseID);
   shortTree->el_baseline_isMediumID = &(*el_baseline_isMediumID);
   shortTree->el_baseline_isTightID = &(*el_baseline_isTightID);
   shortTree->el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down = &(*el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   shortTree->el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up = &(*el_SF_syst_EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   shortTree->el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down = &(*el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   shortTree->el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up = &(*el_SF_syst_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   shortTree->el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down = &(*el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   shortTree->el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up = &(*el_SF_syst_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   shortTree->el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down = &(*el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   shortTree->el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up = &(*el_SF_syst_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   shortTree->el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down = &(*el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   shortTree->el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up = &(*el_SF_syst_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   shortTree->el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down = &(*el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   shortTree->el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up = &(*el_SF_syst_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   shortTree->mu_SF_syst_MUON_EFF_BADMUON_SYS__1down = &(*mu_SF_syst_MUON_EFF_BADMUON_SYS__1down);
   shortTree->mu_SF_syst_MUON_EFF_BADMUON_SYS__1up = &(*mu_SF_syst_MUON_EFF_BADMUON_SYS__1up);
   shortTree->mu_SF_syst_MUON_EFF_ISO_STAT__1down = &(*mu_SF_syst_MUON_EFF_ISO_STAT__1down);
   shortTree->mu_SF_syst_MUON_EFF_ISO_STAT__1up = &(*mu_SF_syst_MUON_EFF_ISO_STAT__1up);
   shortTree->mu_SF_syst_MUON_EFF_ISO_SYS__1down = &(*mu_SF_syst_MUON_EFF_ISO_SYS__1down);
   shortTree->mu_SF_syst_MUON_EFF_ISO_SYS__1up = &(*mu_SF_syst_MUON_EFF_ISO_SYS__1up);
   shortTree->mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down = &(*mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1down);
   shortTree->mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up = &(*mu_SF_syst_MUON_EFF_RECO_STAT_LOWPT__1up);
   shortTree->mu_SF_syst_MUON_EFF_RECO_STAT__1down = &(*mu_SF_syst_MUON_EFF_RECO_STAT__1down);
   shortTree->mu_SF_syst_MUON_EFF_RECO_STAT__1up = &(*mu_SF_syst_MUON_EFF_RECO_STAT__1up);
   shortTree->mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down = &(*mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1down);
   shortTree->mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up = &(*mu_SF_syst_MUON_EFF_RECO_SYS_LOWPT__1up);
   shortTree->mu_SF_syst_MUON_EFF_RECO_SYS__1down = &(*mu_SF_syst_MUON_EFF_RECO_SYS__1down);
   shortTree->mu_SF_syst_MUON_EFF_RECO_SYS__1up = &(*mu_SF_syst_MUON_EFF_RECO_SYS__1up);
   shortTree->mu_SF_syst_MUON_EFF_TTVA_STAT__1down = &(*mu_SF_syst_MUON_EFF_TTVA_STAT__1down);
   shortTree->mu_SF_syst_MUON_EFF_TTVA_STAT__1up = &(*mu_SF_syst_MUON_EFF_TTVA_STAT__1up);
   shortTree->mu_SF_syst_MUON_EFF_TTVA_SYS__1down = &(*mu_SF_syst_MUON_EFF_TTVA_SYS__1down);
   shortTree->mu_SF_syst_MUON_EFF_TTVA_SYS__1up = &(*mu_SF_syst_MUON_EFF_TTVA_SYS__1up);
   shortTree->mu_SF_syst_MUON_EFF_TrigStatUncertainty__1down = &(*mu_SF_syst_MUON_EFF_TrigStatUncertainty__1down);
   shortTree->mu_SF_syst_MUON_EFF_TrigStatUncertainty__1up = &(*mu_SF_syst_MUON_EFF_TrigStatUncertainty__1up);
   shortTree->mu_SF_syst_MUON_EFF_TrigSystUncertainty__1down = &(*mu_SF_syst_MUON_EFF_TrigSystUncertainty__1down);
   shortTree->mu_SF_syst_MUON_EFF_TrigSystUncertainty__1up = &(*mu_SF_syst_MUON_EFF_TrigSystUncertainty__1up);
   shortTree->tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down = &(*tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1down);
   shortTree->tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up = &(*tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT__1up);
   shortTree->tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down = &(*tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down);
   shortTree->tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up = &(*tau_SF_syst_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETAHIGH__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTETALOW__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETAHIGH__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTETALOW__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1__1up);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1down = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1down);
   shortTree->tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1up = &(*tau_SF_syst_TAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018__1up);
   shortTree->ph_SF_syst_PH_EFF_ID_Uncertainty__1down = &(*ph_SF_syst_PH_EFF_ID_Uncertainty__1down);
   shortTree->ph_SF_syst_PH_EFF_ID_Uncertainty__1up = &(*ph_SF_syst_PH_EFF_ID_Uncertainty__1up);
   shortTree->ph_SF_syst_PH_EFF_ISO_Uncertainty__1down = &(*ph_SF_syst_PH_EFF_ISO_Uncertainty__1down);
   shortTree->ph_SF_syst_PH_EFF_ISO_Uncertainty__1up = &(*ph_SF_syst_PH_EFF_ISO_Uncertainty__1up);
   shortTree->ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1down = &(*ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1down);
   shortTree->ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1up = &(*ph_SF_syst_PH_EFF_TRIGGER_Uncertainty__1up);
   shortTree->jet_SF_syst_JET_JvtEfficiency__1down = &(*jet_SF_syst_JET_JvtEfficiency__1down);
   shortTree->jet_SF_syst_JET_JvtEfficiency__1up = &(*jet_SF_syst_JET_JvtEfficiency__1up);
   shortTree->pu_weight_syst_PRW_DATASF__1down = &(*pu_weight_syst_PRW_DATASF__1down);
   shortTree->pu_weight_syst_PRW_DATASF__1up = &(*pu_weight_syst_PRW_DATASF__1up);
   shortTree->btag_weight_syst_FT_EFF_B_systematics__1down = &(*btag_weight_syst_FT_EFF_B_systematics__1down);
   shortTree->btag_weight_syst_FT_EFF_B_systematics__1up = &(*btag_weight_syst_FT_EFF_B_systematics__1up);
   shortTree->btag_weight_syst_FT_EFF_C_systematics__1down = &(*btag_weight_syst_FT_EFF_C_systematics__1down);
   shortTree->btag_weight_syst_FT_EFF_C_systematics__1up = &(*btag_weight_syst_FT_EFF_C_systematics__1up);
   shortTree->btag_weight_syst_FT_EFF_Light_systematics__1down = &(*btag_weight_syst_FT_EFF_Light_systematics__1down);
   shortTree->btag_weight_syst_FT_EFF_Light_systematics__1up = &(*btag_weight_syst_FT_EFF_Light_systematics__1up);
   shortTree->btag_weight_syst_FT_EFF_extrapolation__1down = &(*btag_weight_syst_FT_EFF_extrapolation__1down);
   shortTree->btag_weight_syst_FT_EFF_extrapolation__1up = &(*btag_weight_syst_FT_EFF_extrapolation__1up);
   shortTree->btag_weight_syst_FT_EFF_extrapolation_from_charm__1down = &(*btag_weight_syst_FT_EFF_extrapolation_from_charm__1down);
   shortTree->btag_weight_syst_FT_EFF_extrapolation_from_charm__1up = &(*btag_weight_syst_FT_EFF_extrapolation_from_charm__1up);
   shortTree->btagloose_weight_syst_FT_EFF_B_systematics__1down = &(*btagloose_weight_syst_FT_EFF_B_systematics__1down);
   shortTree->btagloose_weight_syst_FT_EFF_B_systematics__1up = &(*btagloose_weight_syst_FT_EFF_B_systematics__1up);
   shortTree->btagloose_weight_syst_FT_EFF_C_systematics__1down = &(*btagloose_weight_syst_FT_EFF_C_systematics__1down);
   shortTree->btagloose_weight_syst_FT_EFF_C_systematics__1up = &(*btagloose_weight_syst_FT_EFF_C_systematics__1up);
   shortTree->btagloose_weight_syst_FT_EFF_Light_systematics__1down = &(*btagloose_weight_syst_FT_EFF_Light_systematics__1down);
   shortTree->btagloose_weight_syst_FT_EFF_Light_systematics__1up = &(*btagloose_weight_syst_FT_EFF_Light_systematics__1up);
   shortTree->btagloose_weight_syst_FT_EFF_extrapolation__1down = &(*btagloose_weight_syst_FT_EFF_extrapolation__1down);
   shortTree->btagloose_weight_syst_FT_EFF_extrapolation__1up = &(*btagloose_weight_syst_FT_EFF_extrapolation__1up);
   shortTree->btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down = &(*btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down);
   shortTree->btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up = &(*btagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up);
   shortTree->trkbtag_weight_syst_FT_EFF_B_systematics__1down = &(*trkbtag_weight_syst_FT_EFF_B_systematics__1down);
   shortTree->trkbtag_weight_syst_FT_EFF_B_systematics__1up = &(*trkbtag_weight_syst_FT_EFF_B_systematics__1up);
   shortTree->trkbtag_weight_syst_FT_EFF_C_systematics__1down = &(*trkbtag_weight_syst_FT_EFF_C_systematics__1down);
   shortTree->trkbtag_weight_syst_FT_EFF_C_systematics__1up = &(*trkbtag_weight_syst_FT_EFF_C_systematics__1up);
   shortTree->trkbtag_weight_syst_FT_EFF_Light_systematics__1down = &(*trkbtag_weight_syst_FT_EFF_Light_systematics__1down);
   shortTree->trkbtag_weight_syst_FT_EFF_Light_systematics__1up = &(*trkbtag_weight_syst_FT_EFF_Light_systematics__1up);
   shortTree->trkbtag_weight_syst_FT_EFF_extrapolation__1down = &(*trkbtag_weight_syst_FT_EFF_extrapolation__1down);
   shortTree->trkbtag_weight_syst_FT_EFF_extrapolation__1up = &(*trkbtag_weight_syst_FT_EFF_extrapolation__1up);
   shortTree->trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1down = &(*trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1down);
   shortTree->trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1up = &(*trkbtag_weight_syst_FT_EFF_extrapolation_from_charm__1up);
   shortTree->trkbtagloose_weight_syst_FT_EFF_B_systematics__1down = &(*trkbtagloose_weight_syst_FT_EFF_B_systematics__1down);
   shortTree->trkbtagloose_weight_syst_FT_EFF_B_systematics__1up = &(*trkbtagloose_weight_syst_FT_EFF_B_systematics__1up);
   shortTree->trkbtagloose_weight_syst_FT_EFF_C_systematics__1down = &(*trkbtagloose_weight_syst_FT_EFF_C_systematics__1down);
   shortTree->trkbtagloose_weight_syst_FT_EFF_C_systematics__1up = &(*trkbtagloose_weight_syst_FT_EFF_C_systematics__1up);
   shortTree->trkbtagloose_weight_syst_FT_EFF_Light_systematics__1down = &(*trkbtagloose_weight_syst_FT_EFF_Light_systematics__1down);
   shortTree->trkbtagloose_weight_syst_FT_EFF_Light_systematics__1up = &(*trkbtagloose_weight_syst_FT_EFF_Light_systematics__1up);
   shortTree->trkbtagloose_weight_syst_FT_EFF_extrapolation__1down = &(*trkbtagloose_weight_syst_FT_EFF_extrapolation__1down);
   shortTree->trkbtagloose_weight_syst_FT_EFF_extrapolation__1up = &(*trkbtagloose_weight_syst_FT_EFF_extrapolation__1up);
   shortTree->trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down = &(*trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1down);
   shortTree->trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up = &(*trkbtagloose_weight_syst_FT_EFF_extrapolation_from_charm__1up);
   shortTree->mconly_weights = &(*mconly_weights);
   shortTree->mu_pt = &(*mu_pt);
   shortTree->mu_SF = &(*mu_SF);
   shortTree->mu_eta = &(*mu_eta);
   shortTree->mu_phi = &(*mu_phi);
   shortTree->mu_SF_iso = &(*mu_SF_iso);
   shortTree->mu_m = &(*mu_m);
   shortTree->mu_charge = &(*mu_charge);
   shortTree->mu_ptcone20 = &(*mu_ptcone20);
   shortTree->mu_baseline_pt = &(*mu_baseline_pt);
   shortTree->mu_baseline_ptcone20 = &(*mu_baseline_ptcone20);
   shortTree->mu_baseline_SF = &(*mu_baseline_SF);
   shortTree->mu_baseline_eta = &(*mu_baseline_eta);
   shortTree->mu_baseline_phi = &(*mu_baseline_phi);
   shortTree->el_pt = &(*el_pt);
   shortTree->el_eta = &(*el_eta);
   shortTree->el_phi = &(*el_phi);
   shortTree->el_SF = &(*el_SF);
   shortTree->el_SF_iso = &(*el_SF_iso);
   shortTree->el_SF_trigger = &(*el_SF_trigger);
   shortTree->el_eff_trigger = &(*el_eff_trigger);
   shortTree->el_topoetcone20 = &(*el_topoetcone20);
   shortTree->el_ptcone20 = &(*el_ptcone20);
   shortTree->el_m = &(*el_m);
   shortTree->el_charge = &(*el_charge);
   shortTree->el_ptvarcone20_TightTTVA_pt1000 = &(*el_ptvarcone20_TightTTVA_pt1000);
   shortTree->el_baseline_pt = &(*el_baseline_pt);
   shortTree->el_baseline_topoetcone20 = &(*el_baseline_topoetcone20);
   shortTree->el_baseline_ptcone20 = &(*el_baseline_ptcone20);
   shortTree->el_baseline_SF = &(*el_baseline_SF);
   shortTree->el_baseline_eta = &(*el_baseline_eta);
   shortTree->el_baseline_phi = &(*el_baseline_phi);
   shortTree->jet_pt = &(*jet_pt);
   shortTree->jet_eta = &(*jet_eta);
   shortTree->jet_phi = &(*jet_phi);
   shortTree->jet_m = &(*jet_m);
   shortTree->jet_fmax = &(*jet_fmax);
   shortTree->jet_fch = &(*jet_fch);
   shortTree->jet_isbjet = &(*jet_isbjet);
   shortTree->jet_PartonTruthLabelID = &(*jet_PartonTruthLabelID);
   shortTree->jet_ConeTruthLabelID = &(*jet_ConeTruthLabelID);
   shortTree->jet_timing = &(*jet_timing);
   shortTree->jet_emfrac = &(*jet_emfrac);
   shortTree->jet_hecf = &(*jet_hecf);
   shortTree->jet_hecq = &(*jet_hecq);
   shortTree->jet_larq = &(*jet_larq);
   shortTree->jet_avglarq = &(*jet_avglarq);
   shortTree->jet_negE = &(*jet_negE);
   shortTree->jet_lambda = &(*jet_lambda);
   shortTree->jet_lambda2 = &(*jet_lambda2);
   shortTree->jet_jvtxf = &(*jet_jvtxf);
   shortTree->jet_fmaxi = &(*jet_fmaxi);
   shortTree->jet_isbjet_loose = &(*jet_isbjet_loose);
   shortTree->jet_jvt = &(*jet_jvt);
   shortTree->jet_cleaning = &(*jet_cleaning);
   shortTree->jet_DFCommonJets_QGTagger_NTracks = &(*jet_DFCommonJets_QGTagger_NTracks);
   shortTree->jet_DFCommonJets_QGTagger_TracksWidth = &(*jet_DFCommonJets_QGTagger_TracksWidth);
   shortTree->jet_DFCommonJets_QGTagger_TracksC1 = &(*jet_DFCommonJets_QGTagger_TracksC1);
   shortTree->jet_truth_pt = &(*jet_truth_pt);
   shortTree->jet_truth_eta = &(*jet_truth_eta);
   shortTree->jet_truth_phi = &(*jet_truth_phi);
   shortTree->LCTopoJet_pt = &(*LCTopoJet_pt);
   shortTree->LCTopoJet_eta = &(*LCTopoJet_eta);
   shortTree->LCTopoJet_phi = &(*LCTopoJet_phi);
   shortTree->LCTopoJet_m = &(*LCTopoJet_m);
   shortTree->LCTopoJet_tau21 = &(*LCTopoJet_tau21);
   shortTree->LCTopoJet_D2 = &(*LCTopoJet_D2);
   shortTree->LCTopoJet_C2 = &(*LCTopoJet_C2);
   shortTree->LCTopoJet_nTrk = &(*LCTopoJet_nTrk);
   shortTree->LCTopoJet_nConstit = &(*LCTopoJet_nConstit);
   shortTree->LCTopoJet_passD2_W50 = &(*LCTopoJet_passD2_W50);
   shortTree->LCTopoJet_passD2_Z50 = &(*LCTopoJet_passD2_Z50);
   shortTree->LCTopoJet_passD2_W80 = &(*LCTopoJet_passD2_W80);
   shortTree->LCTopoJet_passD2_Z80 = &(*LCTopoJet_passD2_Z80);
   shortTree->LCTopoJet_passMass_W50 = &(*LCTopoJet_passMass_W50);
   shortTree->LCTopoJet_passMass_Z50 = &(*LCTopoJet_passMass_Z50);
   shortTree->LCTopoJet_passMass_W80 = &(*LCTopoJet_passMass_W80);
   shortTree->LCTopoJet_passMass_Z80 = &(*LCTopoJet_passMass_Z80);
   shortTree->TruthFatJet_pt = &(*TruthFatJet_pt);
   shortTree->TruthFatJet_eta = &(*TruthFatJet_eta);
   shortTree->TruthFatJet_phi = &(*TruthFatJet_phi);
   shortTree->TruthFatJet_m = &(*TruthFatJet_m);
   shortTree->TruthFatJet_tau21 = &(*TruthFatJet_tau21);
   shortTree->TruthFatJet_D2 = &(*TruthFatJet_D2);
   shortTree->TruthFatJet_C2 = &(*TruthFatJet_C2);
   shortTree->TruthFatJet_nTrk = &(*TruthFatJet_nTrk);
   shortTree->TruthFatJet_nConstit = &(*TruthFatJet_nConstit);
}


#endif // #ifdef MonoVReader_cxx