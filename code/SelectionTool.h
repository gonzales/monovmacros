#ifndef SelectionTool_h
#define SelectionTool_h

#include "methods.h"
#include "Cuts.h"
#include "MergedTree.C"
#include <TLorentzVector.h>


namespace SelectionTool {

    Bool_t passSelection(MergedTree * t, TString selection, bool debug);
    Bool_t passSelection(MergedTree * t, int cut_id, bool useTCC, bool debug);
    TString getSelectionString(TString selection);

    TString getSelectionLabel(TString selection);
    TString getSelectionLabel(int cut_id, bool useTCC);
    TString getSelectionShortLabel(TString selection);
    
    pair<vector<Int_t>,vector<Double_t>> GetSelectionsYields(vector<TString> selection_chain, MergedTree * t, TString str_weight);

    vector<TString> getSelectionChainLabel(TString selection);
    vector<int> getSelectionChain(TString selection);

    bool dataFriendly(TString selection);
    double getSelectionWeight(TString selection, MergedTree * t);

    TLorentzVector getJiangliuMetTrackNoMuon(MergedTree * t);

};

Bool_t SelectionTool::passSelection(MergedTree * t, TString selection, bool debug = false) {

    if (debug) cout << "============ DEBUG SelectionTool::passSelection =====================" << endl;
    if (debug) cout << "Processing: " << selection << endl;

    //-----------   Selection Chains  ----------------------------
    vector<int> selection_chain = getSelectionChain(selection);
    if (selection_chain.size() > 0) {
        if (debug) cout << "SelectionTool::passSelection -- " << selection << endl;
        bool useTCC = selection.Contains("_useTCC") || selection.Contains("_TCC");
        bool pass = true;
        for (int cut_id : selection_chain){
            if (debug) cout << "    SelectionTool::passSelection -- " << CUTS::AsString(cut_id) << endl;
            if (!passSelection(t,cut_id,useTCC,debug)) {
                pass = false;
                break;
            }
        }
        return pass;
    }

    //-----------   Trim  ----------------------------
    if (selection.Contains("cleanMet")) {
        if (debug) cout << "SelectionTool::passSelection -- cleanMet" << endl;
        if (t->met_tst_et < 100*GeV && t->met_noelectron_tst_et < 100*GeV && t->met_nomuon_tst_et < 100*GeV) return false;
    }

    return true;
}

Bool_t SelectionTool::passSelection(MergedTree * t, int cut_id, bool useTCC = false, bool debug = false) {

    if (debug) cout << "============ DEBUG SelectionTool::passSelection =====================" << endl;

    TString cut_string = CUTS::AsString(cut_id);

    Int_t n_fatJet = t->n_LCTopoJet;
    Double_t m_J = (t->n_LCTopoJet > 0) ? t->LCTopoJet_m->at(0) : -999;
    Double_t dPhiFatJetMet = t->dPhiLCTopoJetMet;
    Int_t n_trackSeparatedBjet = t->n_trackLCTopoSeparatedBjet;
    Int_t n_trackAssociatedBjet = t->n_trackLCTopoAssociatedBjet;
    Int_t n_bcentralJet = 0;

    for (int i_jet = 0 ; i_jet < t->n_jet ; i_jet++) {
        if (fabs(t->jet_eta->at(i_jet)) < 2.5) {
            if (t->jet_isbjet->at(i_jet) == 1) n_bcentralJet++;
        }
    }

    TLorentzVector fatJetVector;
    if (n_fatJet > 0){
        // if (useTCC)   fatJetVector.SetPtEtaPhiM(t->TCCJet_pt->at(0),t->TCCJet_eta->at(0),t->TCCJet_phi->at(0),t->TCCJet_m->at(0));
        fatJetVector.SetPtEtaPhiM(t->LCTopoJet_pt->at(0),t->LCTopoJet_eta->at(0),t->LCTopoJet_phi->at(0),t->LCTopoJet_m->at(0));
    }

    TLorentzVector metVector;
    metVector.SetPxPyPzE(t->met_tst_etx,t->met_tst_ety,0,t->met_tst_et);

    TLorentzVector metTrackVector;
    // metTrackVector.SetPxPyPzE(t->met_track_etx,t->met_track_ety,0,t->met_track_et);
    metTrackVector.SetPxPyPzE(t->met_track_etx, t->met_track_ety, (t->met_track_sumet - t->met_track_ety - t->met_track_etx), sqrt(t->met_track_etx * t->met_track_etx + t->met_track_ety * t->met_track_ety + (t->met_track_sumet - t->met_track_ety - t->met_track_etx) * (t->met_track_sumet - t->met_track_ety - t->met_track_etx)));
    

    TLorentzVector metTrackNoMuonVector;
    for (int i_mu = 0 ; i_mu < t->n_mu ; i_mu++) {
        TLorentzVector muonVector;
        muonVector.SetPtEtaPhiM(t->mu_pt->at(i_mu),t->mu_eta->at(i_mu),t->mu_phi->at(i_mu),t->mu_m->at(i_mu));
        metTrackNoMuonVector = metTrackVector + muonVector;
    }

    TLorentzVector metTrackNoElectronVector;
    for (int i_el = 0 ; i_el < t->n_el ; i_el++) {
        TLorentzVector electronVector;
        electronVector.SetPtEtaPhiM(t->el_pt->at(i_el),t->el_eta->at(i_el),t->el_phi->at(i_el),t->el_m->at(i_el));
        metTrackNoElectronVector = metTrackVector + electronVector;
    }

    TLorentzVector metNoMuonVector;
    metNoMuonVector.SetPxPyPzE(t->met_nomuon_tst_etx, t->met_nomuon_tst_ety, 0, t->met_nomuon_tst_et);

    TLorentzVector metNoElectronVector;
    metNoElectronVector.SetPxPyPzE(t->met_noelectron_tst_etx, t->met_noelectron_tst_ety, 0, t->met_noelectron_tst_et);

    TLorentzVector jetCentral1_vector, jetCentral2_vector;
    bool filledJet1 = false; bool filledJet2 = false;
    for (unsigned int i_jet = 0 ; i_jet < t->jet_pt->size() ; i_jet++) {
        if (fabs(t->jet_eta->at(i_jet)) < 2.5) {
            if (!filledJet1) {jetCentral1_vector.SetPtEtaPhiM(t->jet_pt->at(i_jet),t->jet_eta->at(i_jet),t->jet_phi->at(i_jet),t->jet_m->at(i_jet)); filledJet1 = true; continue;}
            if (!filledJet2) {jetCentral2_vector.SetPtEtaPhiM(t->jet_pt->at(i_jet),t->jet_eta->at(i_jet),t->jet_phi->at(i_jet),t->jet_m->at(i_jet)); filledJet2 = true; continue;}
        }
    }

    if (cut_id == CUTS::PRESELECTION) {
        if (!passSelection(t,CUTS::MET_TRIGGER,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::FCH,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::EMFRAC,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::TIMING,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::MIN_DPHI_MULTIJET,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::MET_TRACK,useTCC,debug)) return false;
    }
    if (cut_id == CUTS::PRESELECTION_NOMUON) {
        if (!passSelection(t,CUTS::MET_TRIGGER,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::MIN_DPHI_MULTIJET_NOMUON,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::MET_TRACK_NOMUON,useTCC,debug)) return false;
    }
    if (cut_id == CUTS::PRESELECTION_NOELECTRON) {
        if (!passSelection(t,CUTS::EL_TRIGGER,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::MIN_DPHI_MULTIJET_NOELECTRON,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::MET_TRACK_NOELECTRON,useTCC,debug)) return false;
    }
    if (cut_id == CUTS::PRESELECTION_NOELECTRON_WITH_JET_CLEANING) {
        if (!passSelection(t,CUTS::EL_TRIGGER,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::FCH,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::EMFRAC,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::TIMING,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::MIN_DPHI_MULTIJET_NOELECTRON,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::MET_TRACK_NOELECTRON,useTCC,debug)) return false;
    }
    if (cut_id == CUTS::PRESELECTION_NOELECTRON_INCLUSIVE_EL_TRIGGER) {
        if (!passSelection(t,CUTS::EL_TRIGGER_INCLUSIVE,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::MIN_DPHI_MULTIJET_NOELECTRON,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::MET_TRACK_NOELECTRON,useTCC,debug)) return false;
    }
    if (cut_id == CUTS::PRESELECTION_NOELECTRON_USEMETTRIGGER) {
        if (!passSelection(t,CUTS::MET_TRIGGER,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::MIN_DPHI_MULTIJET_NOELECTRON,useTCC,debug)) return false;
        if (!passSelection(t,CUTS::MET_TRACK_NOELECTRON,useTCC,debug)) return false;
    }
    if (cut_id == CUTS::JET_CLEANING) {
        float cleaning = 0;
        for (int i = 0; i < t->n_jet; i++) {
            cleaning += t->jet_cleaning->at(i);
        }
        if (cleaning < 1) return false;
    }
    if (cut_id == CUTS::JIANGLIU_PRESELECTION) {
        float min_dphi = 999.;
        for (int i = 0; i < t->n_jet; i++) {
            float phi13 = TVector2::Phi_0_2pi(t->met_noelectron_tst_phi);
            float phi15 = TVector2::Phi_0_2pi(t->jet_phi->at(i));
            if (min_dphi > fabs(TVector2::Phi_mpi_pi(phi15 - phi13))) {
                min_dphi = fabs(TVector2::Phi_mpi_pi(phi15 - phi13));
            }
        }
        TLorentzVector met_track; met_track.SetPxPyPzE(t->met_track_etx, t->met_track_ety, 0, t->met_track_et);
        TLorentzVector met_track_noel;
        float electron1pt = 0;
        float electron1eta = 0;
        float electron1phi = 0;
        float electron1m = 0;
        float electron2pt = 0;
        float electron2eta = 0;
        float electron2phi = 0;
        float electron2m = 0;
        if (t->n_el >= 2) {
            electron1pt = t->el_pt->at(0) / 1000;
            electron1eta = t->el_eta->at(0);
            electron1phi = t->el_phi->at(0);
            electron1m = t->el_m->at(0) / 1000;

            electron2pt = t->el_pt->at(1) / 1000;
            electron2eta = t->el_eta->at(1);
            electron2phi = t->el_phi->at(1);
            electron2m = t->el_m->at(1) / 1000;
        }
        if (t->n_el >= 1) {
            electron1pt = t->el_pt->at(0) / 1000;
            electron1eta = t->el_eta->at(0);
            electron1phi = t->el_phi->at(0);
            electron1m = t->el_m->at(0) / 1000;
        }
        TLorentzVector v1;
        v1.SetPtEtaPhiM(electron1pt, electron1eta, electron1phi, electron1m);
        TLorentzVector v2;
        v2.SetPtEtaPhiM(electron2pt, electron2eta, electron2phi, electron2m);
        TLorentzVector v3 = v1 + v2;
        met_track_noel = met_track + v3;
        float phi11 = TVector2::Phi_0_2pi(t->met_noelectron_tst_phi);
        float phi12 = TVector2::Phi_0_2pi(met_track_noel.Phi());
        float dphi_MPT_MET = fabs(TVector2::Phi_mpi_pi(phi12 - phi11));
        int n_bjetc = 0;
        for (int i = 0; i < t->n_jet; i++) {
            if (fabs(t->jet_eta->at(i)) < 2.5) {
                n_bjetc += t->jet_isbjet->at(i);
            }
        }
        bool pass_trigger = (t->trigger_HLT_e24_lhmedium_L1EM20VH == 1 || t->trigger_HLT_e60_lhmedium == 1 || t->trigger_HLT_e120_lhloose == 1 || t->trigger_HLT_e60_lhmedium_nod0 == 1 || t->trigger_HLT_e140_lhloose_nod0 == 1 || t->trigger_HLT_e26_lhtight_nod0_ivarloose == 1);
        bool pass_dphi = min_dphi > 0.349 && dphi_MPT_MET < 1.57;
        bool pass_trk = ((n_bjetc < 2 && met_track_noel.Pt() > 30) || (n_bjetc >= 2));
        if (!pass_trigger) return false;
        if (!pass_dphi) return false;
        if (!pass_trk) return false;
        
    }
    if (cut_id == CUTS::JIANGLIU_PRESELECTION_NOMUON) {
        if (!passSelection(t,CUTS::MET_TRIGGER_FROM_MJ_NOTE,useTCC,debug)) return false;
        // Multijet cut
        double min_dphi = 999.;
        for (int i = 0; i < t->n_jet; i++) {
            double phi13 = TVector2::Phi_0_2pi(t->met_nomuon_tst_phi);
            double phi15 = TVector2::Phi_0_2pi(t->jet_phi->at(i));
            if (min_dphi > fabs(TVector2::Phi_mpi_pi(phi15 - phi13))) {
                min_dphi = fabs(TVector2::Phi_mpi_pi(phi15 - phi13));
            }
        }
        if (min_dphi <= 0.349) return false;
        // Dphi met_track_nomuon and met_nomuon
        TLorentzVector met_track_nomu = getJiangliuMetTrackNoMuon(t);
        double phi11 = TVector2::Phi_0_2pi(t->met_nomuon_tst_phi);
        double phi12 = TVector2::Phi_0_2pi(met_track_nomu.Phi());
        double dphi_MPT_MET = fabs(TVector2::Phi_mpi_pi(phi12 - phi11));
        if (dphi_MPT_MET >= 1.57) return false;
        bool pass_track_cut = ((n_bcentralJet < 2 && met_track_nomu.Pt() > 30) || (n_bcentralJet >= 2));
        if (!pass_track_cut) return false;
    }
    if (cut_id == CUTS::JIANGLIU_ELECTRON_ID) {
        if (t->n_el_baseline == 0) return false;
        int n_el_LooseID=0;
        int n_el_MediumID=0;
        for(int i_el = 0 ; i_el < t->n_el_baseline ; i_el++){
            n_el_LooseID += t->el_baseline_isLooseID->at(i_el);
            n_el_MediumID += t->el_baseline_isMediumID->at(i_el);
        }
        if (n_el_LooseID != 2 || n_el_MediumID == 0 || t->n_el != 2) return false;
        if (t->el_pt->at(0) <= 25*GeV) return false;
        if (t->el_pt->at(1) <= 25*GeV) return false;
        if (fabs(t->el_eta->at(0)) >= 2.5) return false;
        if (fabs(t->el_eta->at(1)) >= 2.5) return false;
        if (t->el_ptcone20->at(0) / 1000 >= 0.1 * t->el_pt->at(0) / 1000) return false;   
        if (t->el_ptcone20->at(1) / 1000 >= 0.1 * t->el_pt->at(1) / 1000) return false;   
        if (t->n_mu_baseline != 0 || t->n_ph_baseline != 0) return false;                 
        if (t->n_tau != 0) return false;
        if (t->el_charge->at(0) + t->el_charge->at(1) != 0) return false;
    }
    if (cut_id == CUTS::JIANGLIU_ELECTRON_ID_WP) {
        if (t->n_el_baseline == 0) return false;
        int n_el_LooseID=0;
        int n_el_MediumID=0;
        for(int i_el = 0 ; i_el < t->n_el_baseline ; i_el++){
            n_el_LooseID += t->el_baseline_isLooseID->at(i_el);
            n_el_MediumID += t->el_baseline_isMediumID->at(i_el);
        }
        if (n_el_LooseID != 2 || n_el_MediumID == 0 || t->n_el != 2) return false;
        if (t->el_pt->at(0) <= 25*GeV) return false;
        if (t->el_pt->at(1) <= 25*GeV) return false;
        if (fabs(t->el_eta->at(0)) >= 2.5) return false;
        if (fabs(t->el_eta->at(1)) >= 2.5) return false;
        if (t->el_charge->at(0) + t->el_charge->at(1) != 0) return false;
    }
    if (cut_id == CUTS::JIANGLIU_ELECTRON_ID_ISO) {
        if (t->n_el < 2) return false;
        if (t->el_ptcone20->at(0) / 1000 >= 0.1 * t->el_pt->at(0) / 1000) return false; 
        if (t->el_ptcone20->at(1) / 1000 >= 0.1 * t->el_pt->at(1) / 1000) return false; 
    }
    if (cut_id == CUTS::MUON_VETO) {
        if (t->n_mu_baseline != 0) return false;
    }
    if (cut_id == CUTS::PH_VETO) {
        if (t->n_ph_baseline != 0) return false; 
    }
    if (cut_id == CUTS::TAU_VETO) {
        if (t->n_tau != 0) return false;
    }
    if (cut_id == CUTS::JIANGLIU_DPHI_DIJET_MET_NOELECTRON) {
        if (t->n_jet < 2) return false;
        double jet1pt = t->jet_pt->at(0) / 1000; double jet1m = t->jet_m->at(0) / 1000; double jet1eta = t->jet_eta->at(0); double jet1phi = t->jet_phi->at(0); 
        TLorentzVector v4; v4.SetPxPyPzE((jet1pt * cos(jet1phi)), (jet1pt * sin(jet1phi)), (jet1pt * sinh(jet1eta)), sqrt((jet1pt * cosh(jet1eta)) * (jet1pt * cosh(jet1eta)) + jet1m * jet1m));
        double jet2pt = t->jet_pt->at(1) / 1000; double jet2m = t->jet_m->at(1) / 1000; double jet2eta = t->jet_eta->at(1); double jet2phi = t->jet_phi->at(1); 
        TLorentzVector v5; v5.SetPxPyPzE((jet2pt * cos(jet2phi)), (jet2pt * sin(jet2phi)), (jet2pt * sinh(jet2eta)), sqrt((jet2pt * cosh(jet2eta)) * (jet2pt * cosh(jet2eta)) + jet2m * jet2m));
        TLorentzVector v6 = v4 + v5;
        float dijet_phi = v6.Phi();
        float phi3 = TVector2::Phi_0_2pi(t->met_noelectron_tst_phi);
        float phi4 = TVector2::Phi_0_2pi(dijet_phi);
        float dphi_MET_jetc_min = fabs(TVector2::Phi_mpi_pi(phi4 - phi3));
        if (dphi_MET_jetc_min <= 2.09) return false;
    }
    if (cut_id == CUTS::MET_TRIGGER) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        Bool_t is2015 = getYear(t->year,t->run) == 2015;
        Bool_t is2016 = getYear(t->year,t->run) == 2016;
        Bool_t is2017 = getYear(t->year,t->run) == 2017;
        Bool_t is2018 = getYear(t->year,t->run) == 2018;

        bool pass = ((is2015 && t->trigger_HLT_xe70 == 1) ||
                    (is2016 && (t->trigger_HLT_xe80_tc_lcw_L1XE50 == 1 || t->trigger_HLT_xe90_mht_L1XE50 == 1 || t->trigger_HLT_xe100_mht_L1XE50 == 1 || t->trigger_HLT_xe110_mht_L1XE50 == 1 || t->trigger_HLT_xe130_mht_L1XE50 == 1)) ||
                    (is2017 && (t->trigger_HLT_xe90_pufit_L1XE50 == 1 || t->trigger_HLT_xe110_pufit_L1XE55 == 1 ||  t->trigger_HLT_xe110_pufit_L1XE50 == 1 ||  t->trigger_HLT_xe100_pufit_L1XE55 == 1 ||  t->trigger_HLT_xe100_pufit_L1XE50 == 1)) ||
                    (is2018 && (t->trigger_HLT_xe110_pufit_xe70_L1XE50 == 1 || t->trigger_HLT_xe110_pufit_xe65_L1XE50 == 1 || t->trigger_HLT_xe120_pufit_L1XE50 == 1 ))
           );
        if (!pass) return false;
    }
    if (cut_id == CUTS::MET_TRIGGER_FROM_MJ_NOTE) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        Bool_t is2015 = getYear(t->year,t->run) == 2015;
        Bool_t is2016 = getYear(t->year,t->run) == 2016;
        Bool_t is2017 = getYear(t->year,t->run) == 2017;
        Bool_t is2018 = getYear(t->year,t->run) == 2018;

        bool pass = ((is2015 && t->trigger_HLT_xe70 == 1) ||
                    (is2016 && (t->trigger_HLT_xe90_mht_L1XE50 == 1 || t->trigger_HLT_xe100_mht_L1XE50 == 1 || t->trigger_HLT_xe110_mht_L1XE50 == 1)) ||
                    (is2017 && (t->trigger_HLT_xe110_pufit_L1XE55 == 1 ||  t->trigger_HLT_xe110_pufit_L1XE50 == 1)) ||
                    (is2018 && (t->trigger_HLT_xe110_pufit_xe70_L1XE50 == 1 || t->trigger_HLT_xe110_pufit_xe65_L1XE50 == 1))
           );
        if (!pass) return false;
    }
    if (cut_id == CUTS::EL_TRIGGER) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        Bool_t is2015 = getYear(t->year,t->run) == 2015;

        bool pass = (
                    (is2015 && (t->trigger_HLT_e24_lhmedium_L1EM20VH == 1 || t->trigger_HLT_e60_lhmedium == 1 || t->trigger_HLT_e120_lhloose == 1)) ||
                    (!is2015 && (t->trigger_HLT_e26_lhtight_nod0_ivarloose == 1 || t->trigger_HLT_e60_lhmedium_nod0 == 1 || t->trigger_HLT_e140_lhloose_nod0 == 1))
                    );
        if (!pass) return false;
    }
    if (cut_id == CUTS::EL_TRIGGER_INCLUSIVE) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        Bool_t is2015 = getYear(t->year,t->run) == 2015;

        bool pass = (t->trigger_HLT_e24_lhmedium_L1EM20VH == 1 || t->trigger_HLT_e60_lhmedium == 1 || t->trigger_HLT_e120_lhloose == 1 || t->trigger_HLT_e26_lhtight_nod0_ivarloose == 1 || t->trigger_HLT_e60_lhmedium_nod0 == 1 || t->trigger_HLT_e140_lhloose_nod0 == 1);
        if (!pass) return false;
    }
    if (cut_id == CUTS::TRIGGER_HLT_xe70_mht) {
        if (t->trigger_HLT_xe70 != 1) return false;
    }
    if (cut_id == CUTS::TRIGGER_HLT_xe90_mht_L1XE50) {
        if (t->trigger_HLT_xe90_mht_L1XE50 != 1) return false;
    }
    if (cut_id == CUTS::TRIGGER_HLT_xe100_mht_L1XE50) {
        if (t->trigger_HLT_xe100_mht_L1XE50 != 1) return false;
    }
    if (cut_id == CUTS::TRIGGER_HLT_xe110_mht_L1XE50) {
        if (t->trigger_HLT_xe110_mht_L1XE50 != 1) return false;
    }
    if (cut_id == CUTS::TRIGGER_HLT_xe110_pufit_L1XE55) {
        if (t->trigger_HLT_xe110_pufit_L1XE55 != 1) return false;
    }
    if (cut_id == CUTS::TRIGGER_HLT_xe110_pufit_L1XE50) {
        if (t->trigger_HLT_xe110_pufit_L1XE50 != 1) return false;
    }
    if (cut_id == CUTS::TRIGGER_HLT_xe110_pufit_xe70_L1XE50) {
        if (t->trigger_HLT_xe110_pufit_xe70_L1XE50 != 1) return false;
    }
    if (cut_id == CUTS::TRIGGER_HLT_xe110_pufit_xe65_L1XE50) {
        if (t->trigger_HLT_xe110_pufit_xe65_L1XE50 != 1) return false;
    }
    if (cut_id == CUTS::TRIGGER_HLT_e24_lhmedium_L1EM20VH) {
        if (t->trigger_HLT_e24_lhmedium_L1EM20VH != 1) return false;
    }
    if (cut_id == CUTS::TRIGGER_HLT_e60_lhmedium) {
        if (t->trigger_HLT_e60_lhmedium != 1) return false;
    }
    if (cut_id == CUTS::TRIGGER_HLT_e120_lhloose) {
        if (t->trigger_HLT_e120_lhloose != 1) return false;
    }
    if (cut_id == CUTS::TRIGGER_HLT_e26_lhtight_nod0_ivarloose) {
        if (t->trigger_HLT_e26_lhtight_nod0_ivarloose != 1) return false;
    }
    if (cut_id == CUTS::TRIGGER_HLT_e60_lhmedium_nod0) {
        if (t->trigger_HLT_e60_lhmedium_nod0 != 1) return false;
    }
    if (cut_id == CUTS::TRIGGER_HLT_e140_lhloose_nod0) {
        if (t->trigger_HLT_e140_lhloose_nod0 != 1) return false;
    }
    if (cut_id == CUTS::IS_2015) {
        if (getYear(t->year, t->run) != 2015) return false;
    }
    if (cut_id == -1*CUTS::IS_2015) {
        if (getYear(t->year, t->run) == 2015) return false;
    }
    if (cut_id == CUTS::IS_2016) {
        if (getYear(t->year, t->run) != 2016) return false;
    }
    if (cut_id == CUTS::IS_2016_A) {
        if (getYear(t->year, t->run) != 2016) return false;
        if (t->year == 0) {
            if (t->run < 296939) return false;
            if (t->run > 302872) return false;
        }
    }
    if (cut_id == CUTS::IS_2016_B) {
        if (getYear(t->year, t->run) != 2016) return false;
        if (t->year == 0) {
            if (t->run < 302919) return false;
            if (t->run > 303892) return false;
        }
    }
    if (cut_id == CUTS::IS_2016_C) {
        if (getYear(t->year, t->run) != 2016) return false;
        if (t->year == 0) {
            if (t->run < 303943) return false;
            if (t->run > 311481) return false;
        }
    }
    if (cut_id == CUTS::IS_2017) {
        if (getYear(t->year, t->run) != 2017) return false;
    }
    if (cut_id == CUTS::IS_2017_A) {
        if (getYear(t->year, t->run) != 2017) return false;
        if (t->year == 0) {
            if (t->run < 325713) return false;
            if (t->run > 331975) return false;
        }
    }
    if (cut_id == CUTS::IS_2017_B) {
        if (getYear(t->year, t->run) != 2017) return false;
        if (t->year == 0) {
            if (t->run < 332303) return false;
            if (t->run > 341649) return false;
        }
    }
    if (cut_id == -1*CUTS::IS_2017) {
        if (getYear(t->year, t->run) == 2017) return false;
    }
    if (cut_id == CUTS::IS_2018) {
        if (getYear(t->year, t->run) != 2018) return false;
    }
    if (cut_id == CUTS::IS_2018_A) {
        if (getYear(t->year, t->run) != 2018) return false;
        if (t->year == 0) {
            if (t->run < 348197) return false;
            if (t->run > 350066) return false;
        }
    }
    if (cut_id == CUTS::IS_2018_B) {
        if (getYear(t->year, t->run) != 2018) return false;
        if (t->year == 0) {
            if (t->run < 350067) return false;
            if (t->run > 363400) return false;
        }
    }
    if (cut_id == CUTS::RUN_A) { // mc16a
        if (t->year == 0) {
            if (t->run < 276262) return false;
            if (t->run > 284484) return false;
        }
    }
    if (cut_id == CUTS::RUN_B) { // mc16a
        if (t->year == 0) {
            if (t->run < 297730) return false;
            if (t->run > 303560) return false;
        }
    }
    if (cut_id == CUTS::RUN_C) { // mc16a
        if (t->year == 0) {
            if (t->run < 303638) return false;
            if (t->run > 307394) return false;
        }
    }
    if (cut_id == CUTS::RUN_D) { // mc16a
        if (t->year == 0) {
            if (t->run < 307454) return false;
            if (t->run > 311481) return false;
        }
    }
    if (cut_id == CUTS::RUN_E) {  // mc16d
        if (t->year == 0) {
            if (t->run < 325713) return false;
            if (t->run > 331742) return false;
        }
    }
    if (cut_id == CUTS::RUN_F) { // mc16d
        if (t->year == 0) {
            if (t->run < 331772) return false;
            if (t->run > 335177) return false;
        }
    }
    if (cut_id == CUTS::RUN_G) { // mc16d
        if (t->year == 0) {
            if (t->run < 335222) return false;
            if (t->run > 338183) return false;
        }
    }
    if (cut_id == CUTS::RUN_H) { // mc16d
        if (t->year == 0) {
            if (t->run < 338220) return false;
            if (t->run > 340453) return false;
        }
    } 
    if (cut_id == CUTS::RUN_I) { // mc16e
        if (t->year == 0) {
            if (t->run < 348885) return false;
            if (t->run > 350682) return false;
        }
    }
    if (cut_id == CUTS::RUN_J) { // mc16e
        if (t->year == 0) {
            if (t->run < 350749) return false;
            if (t->run > 355273) return false;
        }
    }
    if (cut_id == CUTS::RUN_K) { // mc16e
        if (t->year == 0) {
            if (t->run < 355529) return false;
            if (t->run > 358215) return false;
        }
    }
    if (cut_id == CUTS::RUN_L) { // mc16e
        if (t->year == 0) {
            if (t->run < 358233) return false;
            if (t->run > 359735) return false;
        }
    }
    if (cut_id == CUTS::RUN_M) { // mc16e
        if (t->year == 0) {
            if (t->run < 359766) return false;
            if (t->run > 363129) return false;
        }
    }
    if (cut_id == CUTS::RUN_N) { // mc16e
        if (t->year == 0) {
            if (t->run < 363198) return false;
            if (t->run > 364292) return false;
        }
    }
    if (cut_id == CUTS::POOR_METSIG5) {
        Float_t HT = 0; // in GeV
        for (Int_t i_jet = 0; i_jet < t->n_jet; i_jet++) HT += t->jet_pt->at(i_jet) / 1000.;
        if (HT == 0) return false;
        Float_t poorMetSig = t->met_tst_et / 1000. / sqrt(HT) ;
        if (poorMetSig < 5) return false;
    }
    if (cut_id == CUTS::MET_TRACK) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (n_bcentralJet < 2 && t->met_track_et < 30*GeV) return false;
        if (fabs(metTrackVector.DeltaPhi(metVector)) >= 1.57) return false;
    }
    if (cut_id == CUTS::MET_TRACK_NOMUON) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (n_bcentralJet < 2 && metTrackNoMuonVector.Pt() < 30*GeV) return false;
        if (fabs(metTrackNoMuonVector.DeltaPhi(metNoMuonVector)) >= 1.57) return false;
    }
    if (cut_id == CUTS::MET_TRACK_NOELECTRON) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (n_bcentralJet < 2 && metTrackNoElectronVector.Pt() < 30*GeV) return false;
        if (fabs(metTrackNoElectronVector.DeltaPhi(metNoElectronVector)) >= 1.57) return false;
    }
    if (cut_id == CUTS::MET_NOMUON_MASS) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (t->n_mu > 0) {
            TLorentzVector v1;v1.SetPtEtaPhiM(t->mu_pt->at(0),t->mu_eta->at(0),t->mu_phi->at(0),t->mu_m->at(0));
            TLorentzVector v3 = v1 + metVector;
            float transverse_m = v3.M();
            if (transverse_m <= 30*GeV)  return false;
            if (transverse_m >= 100*GeV) return false;
        } else {
            return false;
        }
    }
    if (cut_id == CUTS::MUNU_MT) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (t->n_mu > 0) {
            TLorentzVector muVector; muVector.SetPtEtaPhiM(t->mu_pt->at(0),t->mu_eta->at(0),t->mu_phi->at(0),t->mu_m->at(0));
            float transverse_m = TMath::Sqrt(2.0 * muVector.Pt() * metVector.Pt() * (1 - TMath::Cos(muVector.DeltaPhi(metVector))));
            if (transverse_m <= 30*GeV)  return false;
            if (transverse_m >= 100*GeV) return false;
        } else {
            return false;
        }
    }
    if (cut_id == CUTS::ENU_MT) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (t->n_el > 0) {
            TLorentzVector elVector; elVector.SetPtEtaPhiM(t->el_pt->at(0),t->el_eta->at(0),t->el_phi->at(0),t->el_m->at(0));
            float transverse_m = TMath::Sqrt(2.0 * elVector.Pt() * metVector.Pt() * (1 - TMath::Cos(elVector.DeltaPhi(metVector))));
            if (transverse_m <= 30*GeV)  return false;
            if (transverse_m >= 100*GeV) return false;
        } else {
            return false;
        }
    }
    if (cut_id == CUTS::DIMUON_MASS) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        TLorentzVector lepton1, lepton2;
        if (t->n_mu == 2) {
            lepton1.SetPtEtaPhiM(t->mu_pt->at(0),t->mu_eta->at(0),t->mu_phi->at(0),t->mu_m->at(0));
            lepton2.SetPtEtaPhiM(t->mu_pt->at(1),t->mu_eta->at(1),t->mu_phi->at(1),t->mu_m->at(1));
            double dimuon_mass = (lepton1 + lepton2).M();
            if (dimuon_mass < 66*GeV) return false;
            if (dimuon_mass > 116*GeV) return false;
        }
    }
    if (cut_id == CUTS::DIELECTRON_MASS) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        TLorentzVector lepton1, lepton2;
        if (t->n_el == 2) {
            lepton1.SetPtEtaPhiM(t->el_pt->at(0),t->el_eta->at(0),t->el_phi->at(0),t->el_m->at(0));
            lepton2.SetPtEtaPhiM(t->el_pt->at(1),t->el_eta->at(1),t->el_phi->at(1),t->el_m->at(1));
            double dielectron_mass = (lepton1 + lepton2).M();
            if (dielectron_mass < 66*GeV) return false;
            if (dielectron_mass > 116*GeV) return false;
        }
    }
    if (cut_id == CUTS::VETO_LARGE_JET) { 
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (n_fatJet > 0) return false;
    }
    if (cut_id == CUTS::MET70) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (t->met_tst_et <= 70*GeV) return false;
    } 
    if (cut_id == CUTS::MET250) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (t->met_tst_et <= 250*GeV) return false;
    } 
    if (cut_id == CUTS::MET150) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (t->met_tst_et <= 150*GeV) return false;
    }
    if (cut_id == CUTS::MET_NOMUON150) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (t->met_nomuon_tst_et <= 150*GeV) return false;
    }
    if (cut_id == CUTS::MET_NOMUON250) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (t->met_nomuon_tst_et <= 250*GeV) return false;
    }
    if (cut_id == CUTS::MET_NOELECTRON150) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (t->met_noelectron_tst_et <= 150*GeV) return false;
    }
    if (cut_id == CUTS::MET_NOELECTRON250) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (t->met_noelectron_tst_et <= 250*GeV) return false;
    }
    if (cut_id == CUTS::GEQ_ONE_LARGE_JET) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (n_fatJet == 0) return false;
    }
    if (cut_id == CUTS::TWO_CENTRAL_JETS) {
        if (debug) cout << "SelectionTool::passSelection -- "  << cut_string << endl;
        if (t->n_jet < 2) return false;
    }
    if (cut_id == CUTS::LEPTON_VETO) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->n_el_baseline != 0 || t->n_mu_baseline != 0) return false;
    }
    if (cut_id == CUTS::ONE_TIGHT_ISO_MUON) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->n_mu_baseline == 0) return false;
        int n_mu_LooseID=0;
        int n_mu_TightID=0;
        for(int i_mu = 0 ; i_mu < t->n_mu_baseline ; i_mu++){
            n_mu_LooseID += t->mu_baseline_isLooseID->at(i_mu);
            n_mu_TightID += t->mu_baseline_isTightID->at(i_mu);
        }
        if (n_mu_LooseID != 1 || n_mu_TightID != 1 || t->n_mu != 1) return false;
        if (t->mu_pt->at(0) <= 25*GeV) return false;
        if (fabs(t->mu_eta->at(0) >= 2.5)) return false;
        if (t->mu_ptcone20->at(0) >= 0.1*t->mu_pt->at(0)) return false;
        if (t->n_el_baseline != 0 || t->n_ph_baseline != 0) return false; 
    }
    if (cut_id == CUTS::ONE_TIGHT_ISO_ELECTRON) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->n_el_baseline == 0) return false;
        int n_el_LooseID=0;
        int n_el_TightID=0;
        for(int i_el = 0 ; i_el < t->n_el_baseline ; i_el++){
            n_el_LooseID += t->el_baseline_isLooseID->at(i_el);
            n_el_TightID += t->el_baseline_isTightID->at(i_el);
        }
        if (n_el_LooseID != 1 || n_el_TightID != 1 || t->n_el != 1) return false;
        if (t->el_pt->at(0) <= 25*GeV) return false;
        if (fabs(t->el_eta->at(0) >= 2.47)) return false;
        if (fabs(t->el_eta->at(0)) <= 1.52 && fabs(t->el_eta->at(0)) >= 1.37) return false;
        if (t->el_ptvarcone20_TightTTVA_pt1000->at(0) / t->el_pt->at(0) >= 0.06) return false;
        if (t->el_topoetcone20->at(0) / t->el_pt->at(0) >= 0.06) return false; 
        if (t->n_mu_baseline != 0 || t->n_ph_baseline != 0) return false; 
    }
    if (cut_id == CUTS::ONE_MEDIUM_ID_TIGHT_ISO_ELECTRON) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->n_el_baseline == 0) return false;
        int n_el_LooseID=0;
        int n_el_MediumID=0;
        for(int i_el = 0 ; i_el < t->n_el_baseline ; i_el++){
            n_el_LooseID += t->el_baseline_isLooseID->at(i_el);
            n_el_MediumID += t->el_baseline_isMediumID->at(i_el);
        }
        if (n_el_LooseID != 1 || n_el_MediumID != 1 || t->n_el != 1) return false;
        if (t->el_pt->at(0) <= 25*GeV) return false;
        if (fabs(t->el_eta->at(0) >= 2.47)) return false;
        if (fabs(t->el_eta->at(0)) <= 1.52 && fabs(t->el_eta->at(0)) >= 1.37) return false;
        if (t->el_ptvarcone20_TightTTVA_pt1000->at(0) / t->el_pt->at(0) >= 0.06) return false;
        if (t->el_topoetcone20->at(0) / t->el_pt->at(0) >= 0.06) return false; 
        if (t->n_mu_baseline != 0 || t->n_ph_baseline != 0) return false; 
    }
    if (cut_id == CUTS::ALL_EL_ISO) {
        for (int i_el = 0 ; i_el < t->n_el ; i_el++) {
            if (t->el_ptvarcone20_TightTTVA_pt1000->at(i_el) / t->el_pt->at(i_el) >= 0.06) return false;
            if (t->el_topoetcone20->at(i_el) / t->el_pt->at(i_el) >= 0.06) return false; 
        }
    }
    if (cut_id == CUTS::EL_ISO_PTVARCONE_NONZERO) {
        for (int i_el = 0 ; i_el < t->n_el ; i_el++) {
            if (t->el_ptvarcone20_TightTTVA_pt1000->at(i_el) / t->el_pt->at(i_el) == 0.0) return false;
        }
    }
    if (cut_id == CUTS::JIANGLIU_TWO_TIGHTID_ISO_MUONS) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->n_mu_baseline < 2) return false;
        if (t->n_el_baseline != 0) return false;
        int n_mu_MediumID = 0;
        int n_mu_LooseID = 0;
        int n_mu_TightID = 0;
        for (int i_mu = 0 ; i_mu < t->n_mu_baseline ; i_mu++) {
            if (t->mu_baseline_isLooseID->at(i_mu)) n_mu_LooseID += 1;
            if (t->mu_baseline_isMediumID->at(i_mu)) n_mu_MediumID += 1;
            if (t->mu_baseline_isTightID->at(i_mu)) n_mu_TightID += 1;
        }
        if (n_mu_LooseID != 2 || n_mu_MediumID == 0 || n_mu_TightID != 2 || t->n_mu != 2) return false;
        int n_mu_iso = 0;
        for (int i_mu = 0 ; i_mu < 2 ; i_mu++) {
            if (t->mu_pt->at(i_mu) > 25*GeV) {
                if (fabs(t->mu_eta->at(i_mu)) < 2.5 && t->mu_ptcone20->at(i_mu) < 0.1 * t->mu_pt->at(i_mu)) {
                    n_mu_iso += 1;
                }
            }
        }
        if (n_mu_iso < 2) return false;
        if (t->mu_charge->at(0) + t->mu_charge->at(1) != 0) return false;
    }
    if (cut_id == CUTS::JIANGLIU_TWO_MEDIUMID_ISO_MUONS) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->n_mu_baseline < 2) return false;
        if (t->n_el_baseline != 0) return false;
        int n_mu_MediumID = 0;
        int n_mu_LooseID = 0;
        int n_mu_TightID = 0;
        for (int i_mu = 0 ; i_mu < t->n_mu_baseline ; i_mu++) {
            if (t->mu_baseline_isLooseID->at(i_mu)) n_mu_LooseID += 1;
            if (t->mu_baseline_isMediumID->at(i_mu)) n_mu_MediumID += 1;
            if (t->mu_baseline_isTightID->at(i_mu)) n_mu_TightID += 1;
        }
        if (n_mu_LooseID != 2 || n_mu_MediumID != 2 || t->n_mu != 2) return false;
        int n_mu_iso = 0;
        for (int i_mu = 0 ; i_mu < 2 ; i_mu++) {
            if (t->mu_pt->at(i_mu) > 25*GeV) {
                if (fabs(t->mu_eta->at(i_mu)) < 2.5 && t->mu_ptcone20->at(i_mu) < 0.1 * t->mu_pt->at(i_mu)) {
                    n_mu_iso += 1;
                }
            }
        }
        if (n_mu_iso < 2) return false;
        if (t->mu_charge->at(0) + t->mu_charge->at(1) != 0) return false;
    }
    if (cut_id == CUTS::JIANGLIU_TWO_LOOSEID_ISO_MUONS) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->n_mu_baseline < 2) return false;
        if (t->n_el_baseline != 0) return false;
        int n_mu_MediumID = 0;
        int n_mu_LooseID = 0;
        int n_mu_TightID = 0;
        for (int i_mu = 0 ; i_mu < t->n_mu_baseline ; i_mu++) {
            if (t->mu_baseline_isLooseID->at(i_mu)) n_mu_LooseID += 1;
            if (t->mu_baseline_isMediumID->at(i_mu)) n_mu_MediumID += 1;
            if (t->mu_baseline_isTightID->at(i_mu)) n_mu_TightID += 1;
        }
        if (n_mu_LooseID != 2 || t->n_mu != 2) return false;
        int n_mu_iso = 0;
        for (int i_mu = 0 ; i_mu < 2 ; i_mu++) {
            if (t->mu_pt->at(i_mu) > 25*GeV) {
                if (fabs(t->mu_eta->at(i_mu)) < 2.5 && t->mu_ptcone20->at(i_mu) < 0.1 * t->mu_pt->at(i_mu)) {
                    n_mu_iso += 1;
                }
            }
        }
        if (n_mu_iso < 2) return false;
        if (t->mu_charge->at(0) + t->mu_charge->at(1) != 0) return false;
    }
    if (cut_id == CUTS::TWO_LOOSE_ONE_MEDIUM_MUON) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->n_mu_baseline == 0) return false;
        int n_mu_LooseID=0;
        int n_mu_MediumID=0;
        for(int i_mu = 0 ; i_mu < t->n_mu_baseline ; i_mu++){
            n_mu_LooseID += t->mu_baseline_isLooseID->at(i_mu);
            n_mu_MediumID += t->mu_baseline_isMediumID->at(i_mu);
        }
        if (n_mu_LooseID != 2 || n_mu_MediumID == 0 || t->n_mu != 2) return false;
        if (t->mu_pt->at(0) <= 25*GeV) return false;
        if (t->n_el_baseline != 0 || t->n_ph_baseline != 0) return false; 
        if (t->mu_charge->at(0) + t->mu_charge->at(1) != 0) return false;
    }
    if (cut_id == CUTS::TWO_LOOSE_ONE_MEDIUM_ELECTRON) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->n_el_baseline == 0) return false;
        int n_el_LooseID=0;
        int n_el_MediumID=0;
        for(int i_el = 0 ; i_el < t->n_el_baseline ; i_el++){
            n_el_LooseID += t->el_baseline_isLooseID->at(i_el);
            n_el_MediumID += t->el_baseline_isMediumID->at(i_el);
        }
        if (n_el_LooseID != 2 || n_el_MediumID == 0 || t->n_el != 2) return false;
        if (t->el_pt->at(0) <= 25*GeV) return false;
        if (t->n_mu_baseline != 0 || t->n_ph_baseline != 0) return false; 
        if (t->el_charge->at(0) + t->el_charge->at(1) != 0) return false;
    }
    if (cut_id == CUTS::FCH) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        for (int i_jet = 0 ; i_jet < t->n_jet ; i_jet++) {
            if (t->jet_fch->at(i_jet) / t->jet_fmax->at(i_jet) < 0.1) return false;
        }
    }
    if (cut_id == CUTS::EMFRAC) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        for (int i_jet = 0 ; i_jet < t->n_jet ; i_jet++) {
            if (t->jet_emfrac->at(i_jet) > 0.95) return false;
            if (t->jet_emfrac->at(i_jet) < 0.05) return false;
        }
    }
    if (cut_id == CUTS::TIMING) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->n_jet > 0) {
            if (abs(t->jet_timing->at(0)) > 5) return false;
        }
    }
    if (cut_id == CUTS::MIN_DPHI_MULTIJET) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        double mindPhiJetMet = -1;
        for (int i_jet = 0 ; i_jet < t->n_jet ; i_jet++) {
            double curr = TMath::Abs(DeltaPhi(t->met_tst_phi, t->jet_phi->at(i_jet)));
            if (mindPhiJetMet == -1 || curr < mindPhiJetMet) mindPhiJetMet = curr;
        }
        if (mindPhiJetMet != -1 && mindPhiJetMet <= 0.349) return false;
    }
    if (cut_id == CUTS::MIN_DPHI_MULTIJET_NOMUON) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        double mindPhiJetMet = -1;
        for (int i_jet = 0 ; i_jet < t->n_jet ; i_jet++) {
            double curr = TMath::Abs(DeltaPhi(t->met_nomuon_tst_phi, t->jet_phi->at(i_jet)));
            if (mindPhiJetMet == -1 || curr < mindPhiJetMet) mindPhiJetMet = curr;
        }
        if (mindPhiJetMet != -1 && mindPhiJetMet <= 0.349) return false;
    }
    if (cut_id == CUTS::MIN_DPHI_MULTIJET_NOELECTRON) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        double mindPhiJetMet = -1;
        for (int i_jet = 0 ; i_jet < t->n_jet ; i_jet++) {
            double curr = TMath::Abs(DeltaPhi(t->met_noelectron_tst_phi, t->jet_phi->at(i_jet)));
            if (mindPhiJetMet == -1 || curr < mindPhiJetMet) mindPhiJetMet = curr;
        }
        if (mindPhiJetMet != -1 && mindPhiJetMet <= 0.349) return false;
    }
    if (cut_id == CUTS::DPHI_FATJET_MET) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_fatJet > 0) {
            if (dPhiFatJetMet <= 2.09) return false;
        }
    }
    if (cut_id == CUTS::DPHI_FATJET_MET_NOMUON) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_fatJet > 0) {
            double dPhiFatJetMetNoMuon = fatJetVector.DeltaPhi(metNoMuonVector);
            if (dPhiFatJetMetNoMuon <= 2.09) return false;
        }
    }
    if (cut_id == CUTS::DPHI_FATJET_MET_NOELECTRON) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_fatJet > 0) {
            double dPhiFatJetMetNoElectron = fatJetVector.DeltaPhi(metNoElectronVector);
            if (dPhiFatJetMetNoElectron <= 2.09) return false;
        }
    }
    if (cut_id == CUTS::VETO_SEPARATED_TRACKBJET) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_trackSeparatedBjet != 0) return false;
    }
    if (abs(cut_id) == CUTS::VTAG_80_D2) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_fatJet < 1) return false;
        if (n_trackAssociatedBjet < 2){
            bool isWJet = t->LCTopoJet_passD2_W80->at(0) == 1;
            bool isZJet = t->LCTopoJet_passD2_Z80->at(0) == 1;
            if (cut_id > 0 && !(isWJet && isZJet)) return false;
            if (cut_id < 0 && (isWJet && isZJet)) return false;
        } 
    }
    if (abs(cut_id) == CUTS::VTAG_50_D2) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_fatJet < 1) return false;
        if (n_trackAssociatedBjet < 2){
            bool isWJet = t->LCTopoJet_passD2_W50->at(0) == 1;
            bool isZJet = t->LCTopoJet_passD2_Z50->at(0) == 1;
            if (cut_id > 0 && !(isWJet && isZJet)) return false;
            if (cut_id < 0 && (isWJet && isZJet)) return false;
        } 
    }
    if (abs(cut_id) == CUTS::VTAG_80_NTRKS) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_fatJet < 1) return false;
        // if (n_fatJet == 1) {
            // Using tagger fit from: https://indico.cern.ch/event/806078/contributions/3354584/attachments/1812138/2960029/JSS_14mar2019.pdf
            Float_t fatJet_pt = t->LCTopoJet_pt->at(0);
            Double_t fit_TCC_W = 22.150 + TMath::Exp(1.29906 * fatJet_pt * 1e-6 - 2.88212);
            Double_t fit_TCC_Z = 23.690 + TMath::Exp(1.59960 * fatJet_pt * 1e-6 - 4.09244);
            Double_t fit_LCTopo_W = 32.7780 + TMath::Exp(-0.270491 * fatJet_pt * 1e-6 - 4.37904);
            Double_t fit_LCTopo_Z = 33.7497 + TMath::Exp(-0.275590 * fatJet_pt * 1e-6 - 3.55880);
            bool isWJet = (t->LCTopoJet_nTrk->at(0) < fit_LCTopo_W);
            bool isZJet = (t->LCTopoJet_nTrk->at(0) < fit_LCTopo_Z);
            if (cut_id > 0 && !(isWJet && isZJet)) return false;
            if (cut_id < 0 && (isWJet && isZJet)) return false;
        // }
    }
    if (abs(cut_id) == CUTS::VTAG_50_NTRKS) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_fatJet < 1) return false;
        // if (n_fatJet == 1) {
            // Using tagger fit from: https://indico.cern.ch/event/806078/contributions/3354584/attachments/1812138/2960029/JSS_14mar2019.pdf
            Float_t fatJet_pt = t->LCTopoJet_pt->at(0);
            Double_t fit_TCC_W = 22.150 + TMath::Exp(1.29906 * fatJet_pt * 1e-6 - 2.88212);
            Double_t fit_TCC_Z = 23.690 + TMath::Exp(1.59960 * fatJet_pt * 1e-6 - 4.09244);
            Double_t fit_LCTopo_W = 25.8773 + TMath::Exp(-0.314739 * fatJet_pt * 1e-6 - 3.94590);
            Double_t fit_LCTopo_Z = 26.6653 + TMath::Exp(-0.305787 * fatJet_pt * 1e-6 - 3.97550);
            bool isWJet = t->LCTopoJet_nTrk->at(0) < fit_LCTopo_W;
            bool isZJet = t->LCTopoJet_nTrk->at(0) < fit_LCTopo_Z;
            if (cut_id > 0 && !(isWJet && isZJet)) return false;
            if (cut_id < 0 && (isWJet && isZJet)) return false;
        // }
    }
    if (cut_id == CUTS::VTAG_80_MASS) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_fatJet < 1) return false;
        if (n_trackAssociatedBjet < 2){
            bool isWJet = t->LCTopoJet_passMass_W80->at(0) == 1;
            bool isZJet = t->LCTopoJet_passMass_Z80->at(0) == 1;
            if (!(isWJet && isZJet)) return false;
        } else if (n_trackAssociatedBjet == 2) {
            if (m_J >= 100*GeV || m_J <= 75*GeV) return false;
        }
    }
    if (cut_id == CUTS::VTAG_50_MASS) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_fatJet < 1) return false;
        if (n_trackAssociatedBjet < 2){
            bool isWJet = t->LCTopoJet_passMass_W50->at(0) == 1;
            bool isZJet = t->LCTopoJet_passMass_Z50->at(0) == 1;
            if (!(isWJet && isZJet)) return false;
        } else if (n_trackAssociatedBjet == 2) {
            if (m_J >= 100*GeV || m_J <= 75*GeV) return false;
        }
    }
    if (cut_id == CUTS::LEQ_TWO_ASSOCIATED_TRACKBJET){
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_trackAssociatedBjet > 2) return false;
    }
    if (cut_id == CUTS::GEQ_ONE_ASSOCIATED_TRACKBJET){
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_trackAssociatedBjet == 0) return false;
    }
    if (cut_id == CUTS::VETO_ASSOCIATED_TRACKBJET){
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_trackAssociatedBjet != 0) return false;
    }
    if (cut_id == CUTS::ONE_OR_TWO_ASSOCIATED_TRACKBJET){
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_trackAssociatedBjet == 0) return false;
        if (n_trackAssociatedBjet > 2) return false;
    }
    if (cut_id == CUTS::ONE_ASSOCIATED_TRACKBJET){
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_trackAssociatedBjet != 1) return false;
    }
    if (cut_id == CUTS::TWO_ASSOCIATED_TRACKBJET){
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_trackAssociatedBjet != 2) return false;
    }
    if (cut_id == CUTS::DPHI_DIJET_MET) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->dPhiDijetMet <= 2.09) return false;
    }
    if (cut_id == CUTS::DPHI_DIJET_MET_NOMUON) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        double dPhiDijetMetNoMuon = fabs((jetCentral1_vector + jetCentral2_vector).DeltaPhi(metNoMuonVector));
        if (dPhiDijetMetNoMuon <= 2.09) return false;
    }
    if (cut_id == CUTS::DPHI_DIJET_MET_NOELECTRON) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        double dPhiDijetMetNoElectron = fabs((jetCentral1_vector + jetCentral2_vector).DeltaPhi(metNoElectronVector));
        // if (dPhiDijetMetNoElectron <= 2.09) return false;
        if (dPhiDijetMetNoElectron <= 2.09) return false;
    }
    if (cut_id == CUTS::DPHI_DIJET) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->dPhiDijet >= 2.44) return false;
    }
    if (cut_id == CUTS::LEAD_CENTRAL_JET_PT45) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->n_jet == 0) return false;
        if (t->jet_pt->at(0) <= 45*GeV) return false;
    }
    if (cut_id == CUTS::SUBLEAD_CENTRAL_JET_PT20) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (jetCentral2_vector.Pt() <= 20*GeV) return false;
    }
    if (cut_id == CUTS::DIJET_DELTAR) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_bcentralJet < 2) {
            if (t->dRDijet >= 1.4) return false;
        } else if (n_bcentralJet == 2) {
            if (t->dRDijet >= 1.25) return false;
        }
    }
    if (cut_id == CUTS::DIJET_MASS) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_bcentralJet < 2) {
            if (t->DijetMass >= 105*GeV || t->DijetMass <= 65*GeV) return false;
        } else if (n_bcentralJet == 2) {
            if (t->DijetMass >= 100*GeV || t->DijetMass <= 65*GeV) return false;
        } 
    }
    if (cut_id == CUTS::LEQ_TWO_CENTRAL_BJET){
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_bcentralJet > 2) return false;
    }
    if (cut_id == CUTS::VETO_CENTRAL_BJET){
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_bcentralJet != 0) 
        return false;
    }
    if (cut_id == CUTS::ONE_OR_TWO_CENTRAL_BJET){
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_bcentralJet == 0) return false;
        if (n_bcentralJet > 2) return false;
    }
    if (cut_id == CUTS::ONE_CENTRAL_BJET){
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_bcentralJet != 1) return false;
    }
    if (cut_id == CUTS::GEQ_ONE_CENTRAL_BJET) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_bcentralJet == 0) return false;
    }
    if (cut_id == CUTS::TWO_CENTRAL_BJET){
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (n_bcentralJet != 2) return false;
    }
    if (cut_id == CUTS::SUMPT) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->n_jet == 2 && t->DijetSumPt <= 120*GeV) return false;
        if (t->n_jet > 2 && t->TrijetSumPt <= 150*GeV) return false;
    }

    if (cut_id == CUTS::FAIL_80MERGED) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        TString collection_str = (useTCC) ? "_useTCC" : "";
        if (passSelection(t, "monoV_merged_80tag_0b_HP" + collection_str, false)) return false;
        if (passSelection(t, "monoV_merged_80tag_0b_LP" + collection_str, false)) return false;
        if (passSelection(t, "monoV_merged_80tag_1b_HP" + collection_str, false)) return false;
        if (passSelection(t, "monoV_merged_80tag_1b_LP" + collection_str, false)) return false;
        if (passSelection(t, "monoV_merged_80tag_2b" + collection_str, false)) return false;
    }
    if (cut_id == CUTS::FAIL_50MERGED) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        TString collection_str = (useTCC) ? "_useTCC" : "";
        if (passSelection(t, "monoV_merged_50tag_0b_HP" + collection_str, false)) return false;
        if (passSelection(t, "monoV_merged_50tag_0b_LP" + collection_str, false)) return false;
        if (passSelection(t, "monoV_merged_50tag_1b_HP" + collection_str, false)) return false;
        if (passSelection(t, "monoV_merged_50tag_1b_LP" + collection_str, false)) return false;
        if (passSelection(t, "monoV_merged_50tag_2b" + collection_str, false)) return false;
    }
    if (cut_id == CUTS::TEST) {
        if (t->n_jet > 2) return false;
        if (t->met_tst_et <= 250*GeV) return false;
        if (t->jet_pt->at(0) <= 40*GeV) return false;
    }
    if (cut_id == CUTS::C2) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        // if (useTCC) {
            // if (t->TCCJet_C2->at(0) > 0.2) return false;
        // } else {
            if (t->LCTopoJet_C2->at(0) > 0.2) return false;
        // }
    }
    if (cut_id == CUTS::C2_HARD) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        // if (useTCC) {
            // if (t->TCCJet_C2->at(0) > 0.15) return false;
        // } else {
            if (t->LCTopoJet_C2->at(0) > 0.15) return false;
        // }
    }
    if (cut_id == CUTS::MET_SIG8) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->met_tst_sig < 8) return false;
    }
    if (cut_id == CUTS::MET_SIG10) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->met_tst_sig < 10) return false;
    }
    if (cut_id == CUTS::MET_SIG12) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->met_tst_sig < 12) return false;
    }
    if (cut_id == CUTS::MET_SIG14) {
        if (debug) cout << "SelectionTool::passSelection --  " << cut_string  << endl;
        if (t->met_tst_sig < 14) return false;
    }

    if (cut_id == CUTS::ACTUALMU_LEQ20) {
        if (t->actualIntPerXing > 20) return false;
    }
    if (cut_id == CUTS::ACTUALMU_GEQ20_LEQ40) {
        if (t->actualIntPerXing < 20) return false;
        if (t->actualIntPerXing > 40) return false;
    }
    if (cut_id == CUTS::ACTUALMU_GEQ40) {
        if (t->actualIntPerXing < 40) return false;
    }
    if (cut_id == CUTS::AVERAGEMU_LEQ20) {
        if (t->averageIntPerXing > 20) return false;
    }
    if (cut_id == CUTS::AVERAGEMU_GEQ20_LEQ40) {
        if (t->averageIntPerXing < 20) return false;
        if (t->averageIntPerXing > 40) return false;
    }
    if (cut_id == CUTS::AVERAGEMU_GEQ40) {
        if (t->averageIntPerXing < 40) return false;
    }

    return true;

}


TString SelectionTool::getSelectionString(TString selection) {

    TString sel_str = "(1==1)";

    //-----Main selection
    if (selection.Contains("MonoJet")) sel_str = "(met_tst_et > 200e3) && (n_jet > 0 && n_jet <= 4) && (jet_pt[0] > 150e3) && (abs(jet_eta[0]) < 2.4) && (n_el == 0 && n_mu == 0) && (MJ_passDeltaPhi == 1)";
    else if (selection.Contains("monoV_0LeptonSR_merged")) {
        for (TString sel_step : getSelectionChain(selection)) {
            sel_str += " && " + getSelectionString(sel_step);
        }
    }
    else if (selection.Contains("monoV_0LeptonSR_resolved")) {
        for (TString sel_step : getSelectionChain(selection)) {
            sel_str += " && " + getSelectionString(sel_step);
        }
    }
    else if (selection.Contains("METTrigger")) {
        sel_str = "((((year == 0 && run >= 276262 && run <= 284484) || year == 2015) && trigger_HLT_xe70 == 1) || (((year == 0 && run > 284484 && run <= 311481) || year == 2016) && (trigger_HLT_xe80_tc_lcw_L1XE50 == 1 || trigger_HLT_xe90_mht_L1XE50 == 1 || trigger_HLT_xe100_mht_L1XE50 == 1 || trigger_HLT_xe110_mht_L1XE50 == 1 || trigger_HLT_xe130_mht_L1XE50 == 1)) || (((year == 0 && run >= 325713) || year == 2017)  && (trigger_HLT_xe90_pufit_L1XE50 == 1 || trigger_HLT_xe110_pufit_L1XE55 == 1 ||  trigger_HLT_xe110_pufit_L1XE50 == 1 ||  trigger_HLT_xe100_pufit_L1XE55 == 1 ||  trigger_HLT_xe100_pufit_L1XE50 == 1)) || (((year == 0 && run >= 348885 && run <=364292) || year == 2018) && (trigger_HLT_xe110_pufit_xe70_L1XE50 == 1 || trigger_HLT_xe110_pufit_xe65_L1XE50 == 1 || trigger_HLT_xe120_pufit_L1XE50 == 1 )))";
    }

    //-----Extra cuts
    if (selection.Contains("fatJetVeto_useTCC")) sel_str += " && (n_TCCJet == 0)";
    else if (selection.Contains("fatJetVeto")) sel_str += " && (n_LCTopoJet == 0)";
    if (selection.Contains("noVtagJet_useTCC")) sel_str += " && (n_TCCJet == 0 || (n_TCCJet > 0 && ((TCCJet_passD2_W80[0] == 0 || TCCJet_passMass_W80[0] == 0) && (TCCJet_passD2_Z80[0] == 0 || TCCJet_passMass_Z80[0] == 0))))";
    else if (selection.Contains("noVtagJet")) sel_str += " && (n_LCTopoJet == 0 || (n_LCTopoJet > 0 && ((LCTopoJet_passD2_W80[0] == 0 || LCTopoJet_passMass_W80[0] == 0) && (LCTopoJet_passD2_Z80[0] == 0 || LCTopoJet_passMass_Z80[0] == 0))))";
    if (selection.Contains("outFatMassCut_useTCC")) sel_str += " && (n_TCCJet == 0 || (n_TCCJet > 0 && (m_TCCJet > 100e3 || m_TCCJet < 75e3)))";
    else if (selection.Contains("outFatMassCut")) sel_str += " && (n_LCTopoJet == 0 || (n_LCTopoJet > 0 && (m_LCTopoJet > 100e3 || m_LCTopoJet < 75e3)))";
    if (selection.Contains("outDijetMassCut")) sel_str += " && (n_jet_central < 2 || (n_jet_central >= 2 && (DijetMass > 100e3 || DijetMass < 65e3) ) )";
    if (selection.Contains("1LargeJet_useTCC")) sel_str += " && (n_TCCJet > 0)";
    else if (selection.Contains("1LargeJet")) sel_str += " && (n_LCTopoJet > 0)";
    if (selection.Contains("1ExclLargeJet_useTCC")) sel_str += " && (n_TCCJet == 1)";
    else if (selection.Contains("1ExclLargeJet")) sel_str += " && (n_LCTopoJet == 1)";
    if (selection.Contains("1ExclLargeJet_useTCC")) sel_str += " && (n_TCCJet == 0)";
    else if (selection.Contains("1ExclLargeJet")) sel_str += " && (n_LCTopoJet == 0)";
    if (selection.Contains("2SmallJets")) sel_str += " && (n_jet >= 2)";
    if (selection.Contains("met200")) sel_str += " && (met_tst_et > 200e3)";
    if (selection.Contains("met250")) sel_str += " && (met_tst_et > 250e3)";
    if (selection.Contains("met150")) sel_str += " && (met_tst_et > 150e3)";
    if (selection.Contains("leadJet100")) sel_str += " && (n_jet > 0) && (jet_pt[0] > 100e3)";
    if (selection.Contains("leptonVeto")) sel_str += " && (n_el_baseline == 0) && (n_mu_baseline == 0)";
    if (selection.Contains("2CentralJets")) sel_str += " && (n_jet_central >= 2)";
    if (selection.Contains("dPhiFatJetMet_useTCC")) sel_str += " && (dPhiTCCJetMet > 120.*TMath::Pi()/180.)";
    else if (selection.Contains("dPhiFatJetMet")) sel_str += " && (dPhiLCTopoJetMet > 120.*TMath::Pi()/180.)";
    if (selection.Contains("trackSeparatedBJetVeto_useTCC")) sel_str += " && (n_trackTCCSeparatedBjet == 0)";
    else if (selection.Contains("trackSeparatedBJetVeto")) sel_str += " && (n_trackLCTopoSeparatedBjet == 0)";
    if (selection.Contains("VTagFatJet_useTCC")) sel_str += " && ((n_trackTCCAssociatedBjet < 2 && ((TCCJet_passD2_W80[0] != 0 && TCCJet_passMass_W80[0] != 0) || (TCCJet_passD2_Z80[0] != 0 && TCCJet_passMass_Z80[0] != 0))) || (n_trackTCCAssociatedBjet == 2 && m_TCCJet < 100e3 && m_TCCJet > 75e3))"; 
    else if (selection.Contains("VTagFatJet")) sel_str += " && ((n_trackLCTopoAssociatedBjet < 2 && ((LCTopoJet_passD2_W80[0] != 0 && LCTopoJet_passMass_W80[0] != 0) || (LCTopoJet_passD2_Z80[0] != 0 && LCTopoJet_passMass_Z80[0] != 0))) || (n_trackLCTopoAssociatedBjet == 2 && m_LCTopoJet < 100e3 && m_LCTopoJet > 75e3))"; 
    if (selection.Contains("VTagD2FatJet_useTCC")) sel_str += " && ((n_trackTCCAssociatedBjet < 2 && (TCCJet_passD2_W80[0] != 0 || TCCJet_passD2_Z80[0] != 0)) || n_trackTCCAssociatedBjet >= 2)";
    else if (selection.Contains("VTagD2FatJet")) sel_str += " && ((n_trackLCTopoAssociatedBjet < 2 && (LCTopoJet_passD2_W80[0] != 0 || LCTopoJet_passD2_Z80[0] != 0)) || n_trackLCTopoAssociatedBjet >= 2)";
    if (selection.Contains("VTag80D2FatJet_useTCC")) sel_str += " && ((n_trackTCCAssociatedBjet < 2 && (TCCJet_passD2_W80[0] != 0 && TCCJet_passD2_Z80[0] != 0)) || n_trackTCCAssociatedBjet >= 2)";
    else if (selection.Contains("VTag80D2FatJet")) sel_str += " && ((n_trackLCTopoAssociatedBjet < 2 && (LCTopoJet_passD2_W80[0] != 0 && LCTopoJet_passD2_Z80[0] != 0)) || n_trackLCTopoAssociatedBjet >= 2)";
    if (selection.Contains("VTag50D2FatJet_useTCC")) sel_str += " && ((n_trackTCCAssociatedBjet < 2 && (TCCJet_passD2_W50[0] != 0 && TCCJet_passD2_Z50[0] != 0)) || n_trackTCCAssociatedBjet >= 2)";
    else if (selection.Contains("VTag50D2FatJet")) sel_str += " && ((n_trackLCTopoAssociatedBjet < 2 && (LCTopoJet_passD2_W50[0] != 0 && LCTopoJet_passD2_Z50[0] != 0)) || n_trackLCTopoAssociatedBjet >= 2)";
    if (selection.Contains("VTagFail80D2FatJet_useTCC")) sel_str += " && ((n_trackTCCAssociatedBjet < 2 && (TCCJet_passD2_W80[0] == 0 && TCCJet_passD2_Z80[0] == 0)) || n_trackTCCAssociatedBjet >= 2)";
    else if (selection.Contains("VTagFail80D2FatJet")) sel_str += " && ((n_trackLCTopoAssociatedBjet < 2 && (LCTopoJet_passD2_W80[0] == 0 && LCTopoJet_passD2_Z80[0] == 0)) || n_trackLCTopoAssociatedBjet >= 2)";
    if (selection.Contains("VTagFail50D2FatJet_useTCC")) sel_str += " && ((n_trackTCCAssociatedBjet < 2 && (TCCJet_passD2_W50[0] == 0 && TCCJet_passD2_Z50[0] == 0)) || n_trackTCCAssociatedBjet >= 2)";
    else if (selection.Contains("VTagFail50D2FatJet")) sel_str += " && ((n_trackLCTopoAssociatedBjet < 2 && (LCTopoJet_passD2_W50[0] == 0 && LCTopoJet_passD2_Z50[0] == 0)) || n_trackLCTopoAssociatedBjet >= 2)";
    if (selection.Contains("VTagNtrksFatJet_useTCC")) sel_str += " && ((n_TCCJet == 1 && ( (TCCJet_ntrk < 23.690 + TMath::Exp(1.59960 * TCCJet_pt[0] * 1e-6 - 4.09244))  ||  (TCCJet_ntrk < 22.150 + TMath::Exp(1.29906 * TCCJet_pt[0] * 1e-6 - 2.88212)) ) ) || (n_TCCJet > 1))";
    else if (selection.Contains("VTagNtrksFatJet")) sel_str += " && ((n_LCTopoJet == 1 && ( (LCTopoJet_ntrk < 22.2859 + TMath::Exp(-0.982743 * LCTopoJet_pt[0] * 1e-6 + 1.21341))  ||  (LCTopoJet_ntrk < 22.5555 + TMath::Exp(-0.453112 * LCTopoJet_pt[0] * 1e-6 - 3.79629)) ) ) || (n_LCTopoJet > 1))";
    if (selection.Contains("VTagMassFatJet_useTCC")) sel_str += " && ((n_trackTCCAssociatedBjet < 2 && (TCCJet_passMass_W80[0] != 0 || TCCJet_passMass_Z80[0] != 0)) || (n_trackTCCAssociatedBjet == 2 && m_TCCJet < 100e3 && m_TCCJet > 75e3))"; 
    else if (selection.Contains("VTagMassFatJet")) sel_str += " && ((n_trackLCTopoAssociatedBjet < 2 && (LCTopoJet_passMass_W80[0] != 0 || LCTopoJet_passMass_Z80[0] != 0)) || (n_trackLCTopoAssociatedBjet == 2 && m_LCTopoJet < 100e3 && m_LCTopoJet > 75e3))"; 
    if (selection.Contains("VTag80MassFatJet_useTCC")) sel_str += " && ((n_trackTCCAssociatedBjet < 2 && (TCCJet_passMass_W80[0] != 0 && TCCJet_passMass_Z80[0] != 0)) || (n_trackTCCAssociatedBjet == 2 && m_TCCJet < 100e3 && m_TCCJet > 75e3))"; 
    else if (selection.Contains("VTag80MassFatJet")) sel_str += " && ((n_trackLCTopoAssociatedBjet < 2 && (LCTopoJet_passMass_W80[0] != 0 && LCTopoJet_passMass_Z80[0] != 0)) || (n_trackLCTopoAssociatedBjet == 2 && m_LCTopoJet < 100e3 && m_LCTopoJet > 75e3))"; 
    if (selection.Contains("CutMassFatJet_useTCC")) sel_str += " && (n_trackTCCAssociatedBjet <= 2 && m_TCCJet < 100e3 && m_TCCJet > 75e3)";
    else if (selection.Contains("CutMassFatJet")) sel_str += " && (m_LCTopoJet < 100e3 && m_LCTopoJet > 75e3)";
    if (selection.Contains("2orLessTrackAssociatedBjet_useTCC")) sel_str += " && (n_trackTCCAssociatedBjet <= 2)";
    else if (selection.Contains("2orLessTrackAssociatedBjet")) sel_str += " && (n_trackLCTopoAssociatedBjet <= 2)";
    if (selection.Contains("vetotrackAssociatedBjet_useTCC")) sel_str += " && (n_trackTCCAssociatedBjet == 0)";
    else if (selection.Contains("vetotrackAssociatedBjet")) sel_str += " && (n_trackLCTopoAssociatedBjet == 0)";
    if (selection.Contains("1trackAssociatedBjet_useTCC")) sel_str += " && (n_trackTCCAssociatedBjet == 1)";
    else if (selection.Contains("1trackAssociatedBjet")) sel_str += " && (n_trackLCTopoAssociatedBjet == 1)";
    if (selection.Contains("2trackAssociatedBjet_useTCC")) sel_str += " && (n_trackTCCAssociatedBjet == 2)";
    else if (selection.Contains("2trackAssociatedBjet")) sel_str += " && (n_trackLCTopoAssociatedBjet == 2)";
    if (selection.Contains("dPhiDijetMet")) sel_str += " && (dPhiDijetMet > 120.*TMath::Pi()/180.)";
    if (selection.Contains("dPhiDiCentralJets")) sel_str += " && (dPhiDijet < 140.*TMath::Pi()/180.)";
    if (selection.Contains("leadJetCentralPt45")) sel_str += " && (lead_jet_central_pt > 45e3)";
    if (selection.Contains("VDiJet")) sel_str += " && ((n_bcentralJet < 2 && (dRDijet < 1.4) && (DijetMass < 105e3 && DijetMass > 65e3)) || (n_bcentralJet == 2 && (dRDijet < 1.25) && (DijetMass < 100e3 && DijetMass > 65e3)))";
    if (selection.Contains("VDRDiJet")) sel_str += " && ((n_bcentralJet < 2 && dRDijet < 1.4) || (n_bcentralJet == 2 && dRDijet < 1.25))";
    if (selection.Contains("VMassDiJet")) sel_str += " && ((n_bcentralJet < 2 && DijetMass < 105e3 && DijetMass > 65e3) || (n_bcentralJet == 2 && DijetMass < 100e3 && DijetMass > 65e3))";
    if (selection.Contains("2orLessbCentralJet")) sel_str += " && (n_bcentralJet <= 2)";
    if (selection.Contains("SumPtCut")) sel_str += " && ((n_jet_central == 2 && DijetSumPt > 120e3) || (n_jet_central > 2 && TrijetSumPt > 150e3))";

    return sel_str;

}

TString SelectionTool::getSelectionLabel(TString selection) {

    if (selection == "monoV_0LeptonSR_merged") return "Merged (LCTopo)";
    if (selection == "monoV_0LeptonSR_merged_useTCC") return "Merged (TCC)";
    if (selection == "monoV_0LeptonSR_resolved") return "Resolved (LCTopo)";
    if (selection == "monoV_0LeptonSR_resolved_useTCC") return "Resolved (TCC)";
    if (selection == "monoV_0LeptonSR_merged_noMassTag") return "Merged no V-Mass cut (LCTopo)";
    if (selection == "monoV_0LeptonSR_merged_noMassTag_useTCC") return "Merged no V-Mass cut (TCC)";
    if (selection == "monoV_0LeptonSR_merged_NtrksCut") return "Merged Ntrks cut (LCTopo)";
    if (selection == "monoV_0LeptonSR_merged_NtrksCut_useTCC") return "Merged Ntrks cut (TCC)";

    if (selection.Contains("monoV_merged")) {
        TString label = "Merged";
        if (selection.Contains("CR1mu")) label += " CR1mu";
        if (selection.Contains("CR2mu")) label += " CR2mu";
        if (selection.Contains("CR1el")) label += " CR1el";
        if (selection.Contains("CR2el")) label += " CR2el";
        if (selection.Contains("_0b_")) label += " (0b)";
        if (selection.Contains("_01b_")) label += " 01b";
        if (selection.Contains("_geq1b")) label += " (#geq 1b)";
        if (selection.Contains("_1b_")) label += " (1b)";
        if (selection.Contains("_12b_")) label += " 12b";
        if (selection.Contains("_2b")) label += " (2b)";
        if (selection.Contains("_Mb")) label += " 012b";
        if (selection.Contains("_useMETTrigger")) label += " Using MET triggers";
        if (selection.Contains("HP")) label += " HP";
        if (selection.Contains("LP")) label += " LP";
        if (selection.Contains("80")) label += " 80tag";
        if (selection.Contains("50")) label += " 50tag";
        if (selection.Contains("noMassTag")) label += " (Mass cut)";
        if (selection.Contains("NtrksCut")) label += " (Ntrks cut)";
        if (selection.Contains("_useTCC")) label += " (TCC)";
        if (!selection.Contains("_useTCC")) label += " (LCTopo)";
        if (selection.Contains("_C2Cut")) label += " (C2 < 0.2)";
        if (selection.Contains("_C2HardCut")) label += " (C2 < 0.15)";
        if (selection.Contains("_metSign8")) label += " (MET Sign > 8)";
        if (selection.Contains("_metSign10")) label += " (MET Sign > 10)";
        if (selection.Contains("_metSign12")) label += " (MET Sign > 12)";
        if (selection.Contains("_metSign14")) label += " (MET Sign > 14)";
        return label;
    }

    if (selection.Contains("monoV_resolved")) {
        TString label = "Resolved";
        if (selection.Contains("CR1mu")) label += " CR1mu";
        if (selection.Contains("CR2mu")) label += " CR2mu";
        if (selection.Contains("CR1el")) label += " CR1el";
        if (selection.Contains("CR2el")) label += " CR2el";
        if (selection.Contains("_0b")) label += " (0b)";
        if (selection.Contains("_01b")) label += " 01b";
        if (selection.Contains("_geq1b")) label += " (#geq 1b)";
        if (selection.Contains("_1b")) label += " (1b)";
        if (selection.Contains("_12b")) label += " 12b";
        if (selection.Contains("_2b")) label += " (2b)";
        if (selection.Contains("_Mb")) label += " 012b";
        if (selection.Contains("LCTopo80Fail")) label += " 80fail (LCTopo)";
        if (selection.Contains("TCC80Fail")) label += " 80fail (TCC)";
        if (selection.Contains("LCTopo50Fail")) label += " 50fail (LCTopo)";
        if (selection.Contains("TCC50Fail")) label += " 50fail (TCC)";
        if (selection.Contains("_useMETTrigger")) label += " Using MET triggers";
        if (selection.Contains("TCCVeto")) label += " (TCC veto)";
        if (selection.Contains("LCTopoVeto")) label += " (LCTopo veto)";
        if (selection.Contains("_metSign8")) label += " (MET Sign > 8)";
        if (selection.Contains("_metSign10")) label += " (MET Sign > 10)";
        if (selection.Contains("_metSign12")) label += " (MET Sign > 12)";
        if (selection.Contains("_metSign14")) label += " (MET Sign > 14)";
        if (selection.Contains("_actualMuCut_leq20")) label += " (#mu_{act} < 20)";
        if (selection.Contains("_actualMuCut_geq20_leq40")) label += " (20 < #mu_{act} < 40)";
        if (selection.Contains("_actualMuCut_geq40")) label += " (40 < #mu_{act})";
        if (selection.Contains("_averageMuCut_leq20")) label += " (#mu_{avg} < 20)";
        if (selection.Contains("_averageMuCut_geq20_leq40")) label += " (20 < #mu_{avg} < 40)";
        if (selection.Contains("_averageMuCut_geq40")) label += " (40 < #mu_{avg})";
        return label;
    }

    if (selection == "Preselection") return "Preselection";
    if (selection == "leptonVeto") return "Lepton veto";
    if (selection == "1LargeJet") return "#geq 1 J_{LCTopo}";
    if (selection == "1LargeJet_useTCC") return "#geq 1 J_{TCC}";
    if (selection == "1ExclLargeJet") return "1 J_{LCTopo}";
    if (selection == "1ExclLargeJet_useTCC") return "1 J_{TCC}";
    if (selection == "fatJetVeto") return "J_{LCTopo} veto";
    if (selection == "fatJetVeto_useTCC") return "J_{TCC} veto";
    if (selection == "dPhiFatJetMet") return "#Delta#phi(J,E_{T}^{miss}) > 120^{o}";
    if (selection == "dPhiFatJetMet_useTCC") return "#Delta#phi(J,E_{T}^{miss}) > 120^{o}";
    if (selection == "trackSeparatedBJetVeto") return "outJ b-trkJet veto";
    if (selection == "trackSeparatedBJetVeto_useTCC") return "outJ b-trkJet veto";
    if (selection.Contains("2orLessTrackAssociatedBjet")) return "inJ b-trkJet #leq 2";
    if (selection.Contains("vetotrackAssociatedBjet")) return "inJ b-trkJet 0";
    if (selection.Contains("1trackAssociatedBjet")) return "inJ b-trkJet 1";
    if (selection.Contains("2trackAssociatedBjet")) return "inJ b-trkJet 2";
    if (selection == "VTagFatJet") return "VTagging";
    if (selection == "VTagD2FatJet" || selection == "VTagD2FatJet_useTCC") return "D2 VTagging";
    if (selection == "VTag80D2FatJet" || selection == "VTag80D2FatJet_useTCC") return "D2 VTagging 80";
    if (selection == "VTag50D2FatJet" || selection == "VTag50D2FatJet_useTCC") return "D2 VTagging 50";
    if (selection == "fail80Merged" || selection == "fail80Merged_useTCC") return "Fail 80Merged";
    if (selection == "fail50Merged" || selection == "fail50Merged_useTCC") return "Fail 50Merged";
    if (selection == "VTagNtrksFatJet" || selection == "VTagNtrksFatJet_useTCC") return "NTrks VTagging";
    if (selection == "VTag80NtrksFatJet" || selection == "VTag80NtrksFatJet_useTCC") return "NTrks VTagging 80";
    if (selection == "VTag50NtrksFatJet" || selection == "VTag50NtrksFatJet_useTCC") return "NTrks VTagging 50";
    if (selection == "VTagMassFatJet" || selection == "VTagMassFatJet_useTCC") return "Mass VTagging";
    if (selection == "VTag80MassFatJet" || selection == "VTag80MassFatJet_useTCC") return "Mass VTagging 80";
    if (selection == "VTag50MassFatJet" || selection == "VTag50MassFatJet_useTCC") return "Mass VTagging 50";
    if (selection == "CutMassFatJet") return "J_{LCTopo} Mass";
    if (selection == "CutMassFatJet_useTCC") return "J_{TCC} Mass";
    if (selection == "met250") return "E_{T}^{miss} > 250 GeV";
    if (selection == "2CentralJets") return "2 central jets";
    if (selection == "leadJetCentralPt45") return "p_{T}^{j_{1}^{c}} > 45 GeV";
    if (selection == "dPhiDijetMet") return "#Delta#phi(j_{1}j_{2},E_{T}^{miss}) > 120^{o}";
    if (selection == "dPhiDiCentralJets") return "#Delta#phi(j_{1},j_{2}) < 140^{o}";
    if (selection == "VDiJet") return "VTagging";
    if (selection == "VDRDiJet") return "#Delta R(j_{1},j_{2})";
    if (selection == "VMassDiJet") return "M_{j1j2}";
    if (selection == "2orLessbCentralJet") return "bjet_{c} #leq 2";
    if (selection == "vetobCentralJet") return "bjet_{c} = 0";
    if (selection == "1bCentralJet") return "bjet_{c} = 1";
    if (selection == "2bCentralJet") return "bjet_{c} = 2";
    if (selection == "SumPtCut") return "SumPtCut";
    if (selection == "met150") return "E_{T}^{miss} > 150 GeV";

    return selection;
}


TString SelectionTool::getSelectionLabel(int cut_id, bool useTCC = false) {


    if (cut_id == CUTS::PRESELECTION)                                   return "Preselection";
    if (cut_id == CUTS::PRESELECTION_NOMUON)                            return "Preselection";
    if (cut_id == CUTS::PRESELECTION_NOELECTRON)                        return "Preselection";
    if (cut_id == CUTS::PRESELECTION_NOELECTRON_USEMETTRIGGER)          return "Preselection (MET Trigger)";
    if (cut_id == CUTS::PRESELECTION_NOELECTRON_INCLUSIVE_EL_TRIGGER)   return "Preselection";
    if (cut_id == CUTS::PRESELECTION_NOELECTRON_WITH_JET_CLEANING)      return "Preselection";
    if (cut_id == CUTS::LEPTON_VETO)                                    return "Lepton veto";
    if (cut_id == CUTS::GEQ_ONE_LARGE_JET && !useTCC)                   return "#geq 1 J_{LCTopo}";
    if (cut_id == CUTS::GEQ_ONE_LARGE_JET && useTCC)                    return "#geq 1 J_{TCC}";
    if (cut_id == CUTS::VETO_LARGE_JET && !useTCC)                      return "J_{LCTopo} veto";
    if (cut_id == CUTS::VETO_LARGE_JET && useTCC)                       return "J_{TCC} veto";
    if (cut_id == CUTS::DPHI_FATJET_MET)                                return "#Delta#phi(J,E_{T}^{miss}) > 120^{o}";
    if (cut_id == CUTS::DPHI_FATJET_MET_NOMUON)                         return "#Delta#phi(J,E_{T,nomu}^{miss}) > 120^{o}";
    if (cut_id == CUTS::DPHI_FATJET_MET_NOELECTRON)                     return "#Delta#phi(J,E_{T,noel}^{miss}) > 120^{o}";
    if (cut_id == CUTS::VETO_SEPARATED_TRACKBJET)                       return "outJ b-trkJet veto";
    if (cut_id == CUTS::LEQ_TWO_ASSOCIATED_TRACKBJET)                   return "inJ b-trkJet #leq 2";
    if (cut_id == CUTS::VETO_ASSOCIATED_TRACKBJET)                      return "inJ b-trkJet 0";
    if (cut_id == CUTS::ONE_ASSOCIATED_TRACKBJET)                       return "inJ b-trkJet 1";
    if (cut_id == CUTS::TWO_ASSOCIATED_TRACKBJET)                       return "inJ b-trkJet 2";
    if (cut_id == CUTS::VTAG_80_D2)                                     return "D2 VTagging 80";
    if (cut_id == CUTS::VTAG_50_D2)                                     return "D2 VTagging 50";
    if (cut_id == -1*CUTS::VTAG_80_D2)                                  return "Fail D2 VTagging 80";
    if (cut_id == -1*CUTS::VTAG_50_D2)                                  return "Fail D2 VTagging 50";
    if (cut_id == CUTS::FAIL_80MERGED)                                  return "Fail 80Merged";
    if (cut_id == CUTS::FAIL_50MERGED)                                  return "Fail 50Merged";
    if (cut_id == CUTS::VTAG_80_NTRKS)                                  return "NTrks VTagging 80";
    if (cut_id == CUTS::VTAG_50_NTRKS)                                  return "NTrks VTagging 50";
    if (cut_id == CUTS::VTAG_80_MASS)                                   return "Mass VTagging 80";
    if (cut_id == CUTS::VTAG_50_MASS)                                   return "Mass VTagging 50";
    if (cut_id == CUTS::MET250)                                         return "E_{T}^{miss} > 250 GeV";
    if (cut_id == CUTS::MET_NOMUON250)                                  return "E_{T,nomu}^{miss} > 250 GeV";
    if (cut_id == CUTS::MET_NOELECTRON250)                              return "E_{T,noel}^{miss} > 250 GeV";
    if (cut_id == CUTS::TWO_CENTRAL_JETS)                               return "2 central jets";
    if (cut_id == CUTS::LEAD_CENTRAL_JET_PT45)                          return "p_{T}^{j_{1}^{c}} > 45 GeV";
    if (cut_id == CUTS::SUBLEAD_CENTRAL_JET_PT20)                       return "p_{T}^{j_{2}^{c}} > 20 GeV";
    if (cut_id == CUTS::DPHI_DIJET_MET)                                 return "#Delta#phi(j_{1}j_{2},E_{T}^{miss}) > 120^{o}";
    if (cut_id == CUTS::DPHI_DIJET_MET_NOMUON)                          return "#Delta#phi(j_{1}j_{2},E_{T,nomu}^{miss}) > 120^{o}";
    if (cut_id == CUTS::DPHI_DIJET_MET_NOELECTRON)                      return "#Delta#phi(j_{1}j_{2},E_{T,noel}^{miss}) > 120^{o}";
    if (cut_id == CUTS::DPHI_DIJET)                                     return "#Delta#phi(j_{1},j_{2}) < 140^{o}";
    if (cut_id == CUTS::DIJET_DELTAR)                                   return "#Delta R(j_{1},j_{2})";
    if (cut_id == CUTS::DIJET_MASS)                                     return "M_{j1j2}";
    if (cut_id == CUTS::LEQ_TWO_CENTRAL_BJET)                           return "bjet_{c} #leq 2";
    if (cut_id == CUTS::VETO_CENTRAL_BJET)                              return "bjet_{c} = 0";
    if (cut_id == CUTS::ONE_CENTRAL_BJET)                               return "bjet_{c} = 1";
    if (cut_id == CUTS::TWO_CENTRAL_BJET)                               return "bjet_{c} = 2";
    if (cut_id == CUTS::GEQ_ONE_CENTRAL_BJET)                           return "bjet_{c} #geq 1";
    if (cut_id == CUTS::SUMPT)                                          return "SumPtCut";
    if (cut_id == CUTS::MET150)                                         return "E_{T}^{miss} > 150 GeV";
    if (cut_id == CUTS::MET_NOMUON150)                                  return "E_{T,nomu}^{miss} > 150 GeV";
    if (cut_id == CUTS::MET_NOELECTRON150)                              return "E_{T,noel}^{miss} > 150 GeV";
    if (cut_id == CUTS::MET_NOMUON_MASS)                                return "M_{#mu#nu} #in [30,100] GeV";
    if (cut_id == CUTS::MUNU_MT)                                        return "M_{#mu#nu} #in [30,100] GeV";
    if (cut_id == CUTS::C2)                                             return "C_2 < 0.2";
    if (cut_id == CUTS::C2_HARD)                                        return "C_2 < 0.15";
    if (cut_id == CUTS::ONE_TIGHT_ISO_MUON)                             return "1 tight muon";
    if (cut_id == CUTS::TWO_LOOSE_ONE_MEDIUM_MUON)                      return "2 loose 1 med muons";
    if (cut_id == CUTS::DIMUON_MASS)                                    return "m_{#mu#mu} #in [66,116] GeV";
    if (cut_id == CUTS::ONE_TIGHT_ISO_ELECTRON)                         return "1 tight el";
    if (cut_id == CUTS::ONE_MEDIUM_ID_TIGHT_ISO_ELECTRON)               return "1 mediumID tightIso el";
    if (cut_id == CUTS::ALL_EL_ISO)                                     return "Apply el isolation";
    if (cut_id == CUTS::TWO_LOOSE_ONE_MEDIUM_ELECTRON)                  return "2 loose 1 med el";
    if (cut_id == CUTS::ENU_MT)                                         return "M_{e#nu} #in [30,100] GeV";
    if (cut_id == CUTS::DIELECTRON_MASS)                                return "m_{ee} #in [66,116] GeV";
    if (cut_id == CUTS::NO_CUT)                                         return "No cut";
    if (cut_id == CUTS::EL_TRIGGER)                                     return "El. Trigger";
    if (cut_id == CUTS::MIN_DPHI_MULTIJET_NOELECTRON)                   return "|#Delta#phi(j_i,E_{T,noel}^{miss})| > 20^o";
    if (cut_id == CUTS::MET_TRACK_NOELECTRON)                           return "E_{T,noel}^{track} cuts";
    if (cut_id == CUTS::JIANGLIU_DPHI_DIJET_MET_NOELECTRON)             return "#Delta#phi(j_{1}j_{2},E_{T,noel}^{miss}) > 120^{o}";
    if (cut_id == CUTS::JIANGLIU_ELECTRON_ID)                           return "2 loose 1 med el";
    if (cut_id == CUTS::JIANGLIU_PRESELECTION)                          return "Preselection";
    if (cut_id == CUTS::JIANGLIU_ELECTRON_ID_WP)                        return "2 loose 1 med ID el";
    if (cut_id == CUTS::JIANGLIU_ELECTRON_ID_ISO)                       return "El isolation";
    if (cut_id == CUTS::MUON_VETO)                                      return "Muon veto";
    if (cut_id == CUTS::PH_VETO)                                        return "Photon veto";
    if (cut_id == CUTS::TAU_VETO)                                       return "Tau veto";
    if (cut_id == CUTS::JET_CLEANING)                                   return "Jet cleaning";

    return CUTS::AsString(cut_id);
}

TString SelectionTool::getSelectionShortLabel(TString selection) {

    if (selection == "monoV_0LeptonSR_merged") return "merged_LCTopo";
    if (selection == "monoV_0LeptonSR_merged_useTCC") return "merged_TCC";
    if (selection == "monoV_0LeptonSR_merged50") return "merged_LCTopo50";
    if (selection == "monoV_0LeptonSR_merged50_useTCC") return "merged_TCC50";
    if (selection == "monoV_0LeptonSR_resolved") return "resolved_LCTopo";
    if (selection == "monoV_0LeptonSR_resolved_useTCC") return "resolved_TCC";
    if (selection == "monoV_0LeptonSR_merged_noMassTag") return "merged_noMassTag_LCTopo";
    if (selection == "monoV_0LeptonSR_merged_noMassTag_useTCC") return "merged_noMassTag_TCC";
    if (selection == "monoV_0LeptonSR_merged_NtrksCut") return "merged_NtrksCut";
    if (selection == "monoV_0LeptonSR_merged_NtrksCut_useTCC") return "merged_NtrksCut_TCC";
    if (selection == "monoV_0LeptonSR_merged_changeOrder") return "merged_LCTopo_changeOrder";
    if (selection == "monoV_0LeptonSR_merged_changeOrder_useTCC") return "merged_TCC_changeOrder";

    if (selection == "leadJetCentralPt45") return "Central Jet pT";
    if (selection == "dPhiDiCentralJets") return "dPhiDijet";

    return selection;
}

pair<vector<Int_t>,vector<Double_t>> SelectionTool::GetSelectionsYields(vector<TString> selection_chain, MergedTree * t, TString str_weight) {

    vector<Int_t> raw_yields;
    vector<Double_t> sumw_yields;
    for (unsigned int i_sel = 0 ; i_sel < selection_chain.size() ; i_sel++) {
        raw_yields.push_back(0);
        sumw_yields.push_back(0);
    }

    TString selection_str = "(1==1)";
    for (unsigned int i_sel = 0 ; i_sel < selection_chain.size() ; i_sel++) {
        TString selection = selection_chain.at(i_sel);
        selection_str += " && " + getSelectionString(selection);

        cout << "Cutflow: " << selection_str << endl;
        TH1D * hist_to_draw = new TH1D("drawHist","",1,0,1000);
        t->fChain->Draw("n_jet >> drawHist", "(" + selection_str + ") * " + str_weight, "goff");
        raw_yields.at(i_sel) += hist_to_draw->GetEntries();
        sumw_yields.at(i_sel) += hist_to_draw->Integral();
        delete hist_to_draw;
    }

    return make_pair(raw_yields,sumw_yields);

}

bool SelectionTool::dataFriendly(TString selection) {
    if (selection.Contains("Preselection")) return true;
    if (selection == "METTrigger") return true;
    if (selection.Contains("METTurnOn")) return true;
    if (selection.Contains("ELTurnOn")) return true;
    if (selection == "TriggerAndLeptonVeto") return true;
    if (selection == "isNCB") return true;
    if (selection == "NCBCwCleaning") return true;
    if (selection.Contains("CR1")) return true;
    if (selection.Contains("CR2")) return true;
    if (selection.Contains("testSelection")) return true;
    if (selection.Contains("dateStudy")) return true;
    return false;
}

vector<TString> SelectionTool::getSelectionChainLabel(TString selection) {
    bool useTCC = selection.Contains("_useTCC") || selection.Contains("_TCC");
    vector<TString> label_chain = {};
    vector<int> selection_chain = getSelectionChain(selection);
    for (int cut_id : selection_chain) {
        label_chain.push_back(getSelectionLabel(cut_id,useTCC));
    }
    return label_chain;
}

vector<int> SelectionTool::getSelectionChain(TString selection) {

    vector<int> selection_chain;

    if (selection == "Preselection") selection_chain = {
                                                            CUTS::MET_TRIGGER,
                                                            CUTS::FCH,
                                                            CUTS::EMFRAC,
                                                            CUTS::TIMING,
                                                            CUTS::MIN_DPHI_MULTIJET,
                                                            CUTS::MET_TRACK,
                                                        };
    else if (selection == "Preselection_CRel") selection_chain = {
                                                            CUTS::NO_CUT,
                                                            CUTS::EL_TRIGGER,
                                                            CUTS::MIN_DPHI_MULTIJET_NOELECTRON,
                                                            CUTS::MET_TRACK_NOELECTRON,
                                                        };

    else if (selection == "testSelection") selection_chain = {
                                                            CUTS::NO_CUT,
                                                            CUTS::MET_TRIGGER,
                                                            CUTS::FCH,
                                                            CUTS::EMFRAC,
                                                            CUTS::TIMING,
                                                            CUTS::MIN_DPHI_MULTIJET,
                                                            CUTS::MET_TRACK,
                                                        };

    else if (selection.Contains("Preselection_CRel")) {
                                                         selection_chain = {
                                                            CUTS::NO_CUT,
                                                            CUTS::EL_TRIGGER,
                                                            CUTS::MIN_DPHI_MULTIJET_NOELECTRON,
                                                            CUTS::MET_TRACK_NOELECTRON,
                                                        };

        if (selection.Contains("_mediumIDTightIsoel")) selection_chain.push_back(CUTS::ONE_MEDIUM_ID_TIGHT_ISO_ELECTRON);
        if (selection.Contains("_twoLooseOneMediumEl")) selection_chain.push_back(CUTS::TWO_LOOSE_ONE_MEDIUM_ELECTRON);

    }

    else if (selection.Contains("Preselection_")) {
                                                        selection_chain = {
                                                            CUTS::MET_TRIGGER,
                                                            CUTS::FCH,
                                                            CUTS::EMFRAC,
                                                            CUTS::TIMING,
                                                            CUTS::MIN_DPHI_MULTIJET,
                                                            CUTS::MET_TRACK,
                                                        };
        if (selection.Contains("_not2017")) selection_chain.push_back(-1*CUTS::IS_2017);
        if (selection.Contains("_is2017")) selection_chain.push_back(CUTS::IS_2017);
    }

    //////////////////////////////////////////////////////////////////////////////////////
    else if (selection == "monoV_merged_80tag_0b_HP" || selection == "monoV_merged_80tag_0b_HP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::VETO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_80_D2,
                                                                    CUTS::VTAG_80_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_80tag_0b_LP" || selection == "monoV_merged_80tag_0b_LP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::VETO_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_80_D2,
                                                                    CUTS::VTAG_80_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_80tag_1b_HP" || selection == "monoV_merged_80tag_1b_HP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_80_D2,
                                                                    CUTS::VTAG_80_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_80tag_1b_LP" || selection == "monoV_merged_80tag_1b_LP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_80_D2,
                                                                    CUTS::VTAG_80_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                };
    else if (selection == "monoV_merged_80tag_2b" || selection == "monoV_merged_80tag_2b_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::TWO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_80_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 

    else if (selection == "monoV_merged_50tag_0b_HP" || selection == "monoV_merged_50tag_0b_HP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::VETO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_50tag_0b_LP" || selection == "monoV_merged_50tag_0b_LP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::VETO_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_50tag_1b_HP" || selection == "monoV_merged_50tag_1b_HP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_50tag_1b_LP" || selection == "monoV_merged_50tag_1b_LP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                };
    else if (selection == "monoV_merged_50tag_2b" || selection == "monoV_merged_50tag_2b_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::TWO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 


    ///////////////////////////////////////////////////////////////////////////////////////////////////
    else if (selection == "monoV_merged_80tag_Mb_HP" || selection == "monoV_merged_80tag_Mb_HP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::LEQ_TWO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_80_D2,
                                                                    CUTS::VTAG_80_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_50tag_Mb_HP" || selection == "monoV_merged_50tag_Mb_HP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::LEQ_TWO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_80tag_Mb_LP" || selection == "monoV_merged_80tag_Mb_LP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::LEQ_TWO_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_80_D2,
                                                                    CUTS::VTAG_80_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_50tag_Mb_LP" || selection == "monoV_merged_50tag_Mb_LP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::LEQ_TWO_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    else if (selection == "monoV_resolved_Mb_LCTopo80Fail" || selection == "monoV_resolved_Mb_TCC80Fail") 
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::FAIL_80MERGED,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::DPHI_DIJET_MET,
                                                                    CUTS::DPHI_DIJET,
                                                                    CUTS::SUMPT,
                                                                    CUTS::LEQ_TWO_CENTRAL_BJET,
                                                                    CUTS::DIJET_DELTAR,
                                                                    CUTS::DIJET_MASS,
                                                                };
    else if (selection == "monoV_resolved_Mb_LCTopo50Fail" || selection == "monoV_resolved_Mb_TCC50Fail") 
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::FAIL_50MERGED,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::DPHI_DIJET_MET,
                                                                    CUTS::DPHI_DIJET,
                                                                    CUTS::SUMPT,
                                                                    CUTS::LEQ_TWO_CENTRAL_BJET,
                                                                    CUTS::DIJET_DELTAR,
                                                                    CUTS::DIJET_MASS,
                                                                };
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    else if (selection == "monoV_merged_NtrksCut_80tag_0b_HP" || selection == "monoV_merged_NtrksCut_80tag_0b_HP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::VETO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_80_D2,
                                                                    CUTS::VTAG_80_MASS,
                                                                    CUTS::VTAG_80_NTRKS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_NtrksCut_80tag_0b_LP" || selection == "monoV_merged_NtrksCut_80tag_0b_LP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::VETO_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_80_D2,
                                                                    CUTS::VTAG_80_MASS,
                                                                    -1*CUTS::VTAG_80_NTRKS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_NtrksCut_80tag_1b_HP" || selection == "monoV_merged_NtrksCut_80tag_1b_HP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_80_D2,
                                                                    CUTS::VTAG_80_MASS,
                                                                    CUTS::VTAG_80_NTRKS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_NtrksCut_80tag_1b_LP" || selection == "monoV_merged_NtrksCut_80tag_1b_LP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_80_D2,
                                                                    CUTS::VTAG_80_MASS,
                                                                    -1*CUTS::VTAG_80_NTRKS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_NtrksCut_80tag_2b" || selection == "monoV_merged_NtrksCut_80tag_2b_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::TWO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_80_D2,
                                                                    CUTS::VTAG_80_MASS,
                                                                    CUTS::VTAG_80_NTRKS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_NtrksCut_50tag_0b_HP" || selection == "monoV_merged_NtrksCut_50tag_0b_HP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::VETO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VTAG_50_NTRKS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_NtrksCut_50tag_0b_LP" || selection == "monoV_merged_NtrksCut_50tag_0b_LP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::VETO_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    -1*CUTS::VTAG_50_NTRKS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_NtrksCut_50tag_1b_HP" || selection == "monoV_merged_NtrksCut_50tag_1b_HP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VTAG_50_NTRKS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_NtrksCut_50tag_1b_LP" || selection == "monoV_merged_NtrksCut_50tag_1b_LP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    -1*CUTS::VTAG_50_NTRKS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_NtrksCut_50tag_2b" || selection == "monoV_merged_NtrksCut_50tag_2b_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::TWO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VTAG_50_NTRKS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 


    //////////////////////////////////////////////////////////////////////////////////////////
    else if (selection == "monoV_resolved_0b_LCTopoVeto" || selection == "monoV_resolved_0b_TCCVeto") 
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::VETO_LARGE_JET,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::DPHI_DIJET_MET,
                                                                    CUTS::DPHI_DIJET,
                                                                    CUTS::SUMPT,
                                                                    CUTS::VETO_CENTRAL_BJET,
                                                                    CUTS::DIJET_DELTAR,
                                                                    CUTS::DIJET_MASS,
                                                                };
    else if (selection == "monoV_resolved_1b_LCTopoVeto" || selection == "monoV_resolved_1b_TCCVeto") 
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::VETO_LARGE_JET,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::DPHI_DIJET_MET,
                                                                    CUTS::DPHI_DIJET,
                                                                    CUTS::SUMPT,
                                                                    CUTS::ONE_CENTRAL_BJET,
                                                                    CUTS::DIJET_DELTAR,
                                                                    CUTS::DIJET_MASS,
                                                                };
    else if (selection == "monoV_resolved_2b_LCTopoVeto" || selection == "monoV_resolved_2b_TCCVeto") 
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::VETO_LARGE_JET,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::DPHI_DIJET_MET,
                                                                    CUTS::DPHI_DIJET,
                                                                    CUTS::SUMPT,
                                                                    CUTS::TWO_CENTRAL_BJET,
                                                                    CUTS::DIJET_DELTAR,
                                                                    CUTS::DIJET_MASS,
                                                                };
    //////////////////////////////////////////////////////////////////////////////////////////
    else if (selection == "monoV_resolved_0b_LCTopo80Fail" || selection == "monoV_resolved_0b_TCC80Fail") 
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::FAIL_80MERGED,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::DPHI_DIJET_MET,
                                                                    CUTS::DPHI_DIJET,
                                                                    CUTS::SUMPT,
                                                                    CUTS::VETO_CENTRAL_BJET,
                                                                    CUTS::DIJET_DELTAR,
                                                                    CUTS::DIJET_MASS,
                                                                };
    else if (selection == "monoV_resolved_1b_LCTopo80Fail" || selection == "monoV_resolved_1b_TCC80Fail") 
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::FAIL_80MERGED,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::DPHI_DIJET_MET,
                                                                    CUTS::DPHI_DIJET,
                                                                    CUTS::SUMPT,
                                                                    CUTS::ONE_CENTRAL_BJET,
                                                                    CUTS::DIJET_DELTAR,
                                                                    CUTS::DIJET_MASS,
                                                                };
    else if (selection == "monoV_resolved_2b_LCTopo80Fail" || selection == "monoV_resolved_2b_TCC80Fail") 
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::FAIL_80MERGED,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::DPHI_DIJET_MET,
                                                                    CUTS::DPHI_DIJET,
                                                                    CUTS::SUMPT,
                                                                    CUTS::TWO_CENTRAL_BJET,
                                                                    CUTS::DIJET_DELTAR,
                                                                    CUTS::DIJET_MASS,
                                                                };



    else if (selection == "monoV_resolved_0b_LCTopo50Fail" || selection == "monoV_resolved_0b_TCC50Fail") 
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::FAIL_50MERGED,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::DPHI_DIJET_MET,
                                                                    CUTS::DPHI_DIJET,
                                                                    CUTS::SUMPT,
                                                                    CUTS::VETO_CENTRAL_BJET,
                                                                    CUTS::DIJET_DELTAR,
                                                                    CUTS::DIJET_MASS,
                                                                };
    else if (selection == "monoV_resolved_1b_LCTopo50Fail" || selection == "monoV_resolved_1b_TCC50Fail") 
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::FAIL_50MERGED,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::DPHI_DIJET_MET,
                                                                    CUTS::DPHI_DIJET,
                                                                    CUTS::SUMPT,
                                                                    CUTS::ONE_CENTRAL_BJET,
                                                                    CUTS::DIJET_DELTAR,
                                                                    CUTS::DIJET_MASS,
                                                                };
    else if (selection == "monoV_resolved_2b_LCTopo50Fail" || selection == "monoV_resolved_2b_TCC50Fail") 
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::FAIL_50MERGED,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::DPHI_DIJET_MET,
                                                                    CUTS::DPHI_DIJET,
                                                                    CUTS::SUMPT,
                                                                    CUTS::TWO_CENTRAL_BJET,
                                                                    CUTS::DIJET_DELTAR,
                                                                    CUTS::DIJET_MASS,
                                                                };
    //////////////////////////////////////////////////////////////////////////////////////
    else if (selection == "monoV_merged_80tag_12b_HP" || selection == "monoV_merged_80tag_12b_HP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_OR_TWO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_80_D2,
                                                                    CUTS::VTAG_80_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_80tag_12b_LP" || selection == "monoV_merged_80tag_12b_LP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_OR_TWO_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_80_D2,
                                                                    CUTS::VTAG_80_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_50tag_12b_HP" || selection == "monoV_merged_50tag_12b_HP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_OR_TWO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    else if (selection == "monoV_merged_50tag_12b_LP" || selection == "monoV_merged_50tag_12b_LP_useTCC") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_OR_TWO_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                }; 
    //////////////////////////////////////////////////////////////////////////////////////////
    else if (selection == "monoV_resolved_12b_LCTopo80Fail" || selection == "monoV_resolved_12b_TCC80Fail") 
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::FAIL_80MERGED,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::DPHI_DIJET_MET,
                                                                    CUTS::DPHI_DIJET,
                                                                    CUTS::SUMPT,
                                                                    CUTS::ONE_OR_TWO_CENTRAL_BJET,
                                                                    CUTS::DIJET_DELTAR,
                                                                    CUTS::DIJET_MASS,
                                                                };
    else if (selection == "monoV_resolved_12b_LCTopo50Fail" || selection == "monoV_resolved_12b_TCC50Fail") 
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::FAIL_50MERGED,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::DPHI_DIJET_MET,
                                                                    CUTS::DPHI_DIJET,
                                                                    CUTS::SUMPT,
                                                                    CUTS::ONE_OR_TWO_CENTRAL_BJET,
                                                                    CUTS::DIJET_DELTAR,
                                                                    CUTS::DIJET_MASS,
                                                                };
    //////////////////////////////////////////////////////////////////////////////////////////
    else if (selection.Contains("monoV_resolved_CR1mu")) {
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION_NOMUON,
                                                                    CUTS::ONE_TIGHT_ISO_MUON,
                                                                    CUTS::MUNU_MT,
                                                                    CUTS::MET_NOMUON150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::SUBLEAD_CENTRAL_JET_PT20,
                                                                    CUTS::SUMPT,
                                                                    CUTS::DPHI_DIJET_MET_NOMUON,
                                                                    CUTS::DPHI_DIJET,
                                                            };
        if (selection.Contains("_0b")) selection_chain.push_back(CUTS::VETO_CENTRAL_BJET);
        else if (selection.Contains("_1b")) selection_chain.push_back(CUTS::ONE_CENTRAL_BJET);
        else if (selection.Contains("_2b")) selection_chain.push_back(CUTS::TWO_CENTRAL_BJET);
        else if (selection.Contains("_geq1b")) selection_chain.push_back(CUTS::GEQ_ONE_CENTRAL_BJET);
        else if (selection.Contains("_leq2b")) selection_chain.push_back(CUTS::LEQ_TWO_CENTRAL_BJET);
    }
    else if (selection.Contains("monoV_resolved_CR2mu")) {
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION_NOMUON,
                                                                    CUTS::TWO_LOOSE_ONE_MEDIUM_MUON,
                                                                    CUTS::DIMUON_MASS,
                                                                    CUTS::MET_NOMUON150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::SUMPT,
                                                                    CUTS::DPHI_DIJET_MET_NOMUON,
                                                                    CUTS::DPHI_DIJET,
                                                            };
        if (selection.Contains("_dijetMassCut")) selection_chain.push_back(CUTS::DIJET_MASS);
        if (selection.Contains("_0b")) selection_chain.push_back(CUTS::VETO_CENTRAL_BJET);
        else if (selection.Contains("_1b")) selection_chain.push_back(CUTS::ONE_CENTRAL_BJET);
        else if (selection.Contains("_2b")) selection_chain.push_back(CUTS::TWO_CENTRAL_BJET);
        else if (selection.Contains("_geq1b")) selection_chain.push_back(CUTS::GEQ_ONE_CENTRAL_BJET);
        else if (selection.Contains("_leq2b")) selection_chain.push_back(CUTS::LEQ_TWO_CENTRAL_BJET);
    }
    else if (selection.Contains("monoV_merged_CR1mu")) {
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION_NOMUON,
                                                                    CUTS::ONE_TIGHT_ISO_MUON,
                                                                    CUTS::MUNU_MT,
                                                                    CUTS::MET_NOMUON250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET_NOMUON,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                            };
        if (selection.Contains("_0b")) selection_chain.push_back(CUTS::VETO_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_1b")) selection_chain.push_back(CUTS::ONE_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_2b")) selection_chain.push_back(CUTS::TWO_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_geq1b")) selection_chain.push_back(CUTS::GEQ_ONE_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_leq2b")) selection_chain.push_back(CUTS::LEQ_TWO_ASSOCIATED_TRACKBJET);
    }
    else if (selection.Contains("monoV_merged_CR2mu")) {
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION_NOMUON,
                                                                    CUTS::TWO_LOOSE_ONE_MEDIUM_MUON,
                                                                    CUTS::DIMUON_MASS,
                                                                    CUTS::MET_NOMUON250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET_NOMUON,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                            };
        if (selection.Contains("_0b")) selection_chain.push_back(CUTS::VETO_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_1b")) selection_chain.push_back(CUTS::ONE_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_2b")) selection_chain.push_back(CUTS::TWO_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_geq1b")) selection_chain.push_back(CUTS::GEQ_ONE_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_leq2b")) selection_chain.push_back(CUTS::LEQ_TWO_ASSOCIATED_TRACKBJET);
    }
    //////////////////////////////////////////////////////////////////////////////////////////
    else if (selection.Contains("monoV_resolved_CR1el")) {
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION_NOELECTRON,
                                                                    CUTS::ONE_TIGHT_ISO_ELECTRON,
                                                                    CUTS::ENU_MT,
                                                                    CUTS::MET_NOELECTRON150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::SUBLEAD_CENTRAL_JET_PT20,
                                                                    CUTS::SUMPT,
                                                                    CUTS::DPHI_DIJET_MET_NOELECTRON,
                                                                    CUTS::DPHI_DIJET,
                                                            };
        if (selection.Contains("_useMETTrigger")) selection_chain[0] = CUTS::PRESELECTION_NOELECTRON_USEMETTRIGGER;
        if (selection.Contains("_mediumIDTightIsoel")) selection_chain[1] = CUTS::ONE_MEDIUM_ID_TIGHT_ISO_ELECTRON;
        if (selection.Contains("_MonojetCR1el")) {
            selection_chain.push_back(CUTS::MET70);
            selection_chain.push_back(CUTS::POOR_METSIG5);
        }

        if (selection.Contains("_0b")) selection_chain.push_back(CUTS::VETO_CENTRAL_BJET);
        else if (selection.Contains("_1b")) selection_chain.push_back(CUTS::ONE_CENTRAL_BJET);
        else if (selection.Contains("_2b")) selection_chain.push_back(CUTS::TWO_CENTRAL_BJET);
        else if (selection.Contains("_geq1b")) selection_chain.push_back(CUTS::GEQ_ONE_CENTRAL_BJET);
        else if (selection.Contains("_leq2b")) selection_chain.push_back(CUTS::LEQ_TWO_CENTRAL_BJET);
    }
    else if (selection.Contains("monoV_resolved_CR2el")) {
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION_NOELECTRON,
                                                                    CUTS::TWO_LOOSE_ONE_MEDIUM_ELECTRON,
                                                                    CUTS::DIELECTRON_MASS,
                                                                    CUTS::MET_NOELECTRON150,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::SUMPT,
                                                                    CUTS::DPHI_DIJET_MET_NOELECTRON,
                                                                    CUTS::DPHI_DIJET,
                                                            };
        if (selection.Contains("_useMETTrigger")) selection_chain[0] = CUTS::PRESELECTION_NOELECTRON_USEMETTRIGGER;

        if (selection.Contains("_dijetMassCut")) selection_chain.push_back(CUTS::DIJET_MASS);
        if (selection.Contains("_0b")) selection_chain.push_back(CUTS::VETO_CENTRAL_BJET);
        else if (selection.Contains("_1b")) selection_chain.push_back(CUTS::ONE_CENTRAL_BJET);
        else if (selection.Contains("_2b")) selection_chain.push_back(CUTS::TWO_CENTRAL_BJET);
        else if (selection.Contains("_geq1b")) selection_chain.push_back(CUTS::GEQ_ONE_CENTRAL_BJET);
        else if (selection.Contains("_leq2b")) selection_chain.push_back(CUTS::LEQ_TWO_CENTRAL_BJET);
    }
    else if (selection.Contains("monoV_merged_CR1el")) {
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION_NOELECTRON,
                                                                    CUTS::ONE_TIGHT_ISO_ELECTRON,
                                                                    CUTS::ENU_MT,
                                                                    CUTS::MET_NOELECTRON250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET_NOELECTRON,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                            };

        if (selection.Contains("_useMETTrigger")) selection_chain[0] = CUTS::PRESELECTION_NOELECTRON_USEMETTRIGGER;
        if (selection.Contains("_mediumIDTightIsoel")) selection_chain[1] = CUTS::ONE_MEDIUM_ID_TIGHT_ISO_ELECTRON;

        if (selection.Contains("_0b")) selection_chain.push_back(CUTS::VETO_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_1b")) selection_chain.push_back(CUTS::ONE_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_2b")) selection_chain.push_back(CUTS::TWO_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_geq1b")) selection_chain.push_back(CUTS::GEQ_ONE_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_leq2b")) selection_chain.push_back(CUTS::LEQ_TWO_ASSOCIATED_TRACKBJET);
    }
    else if (selection.Contains("monoV_merged_CR2el")) {
                                                            selection_chain= {
                                                                    CUTS::PRESELECTION_NOELECTRON,
                                                                    CUTS::TWO_LOOSE_ONE_MEDIUM_ELECTRON,
                                                                    CUTS::DIELECTRON_MASS,
                                                                    CUTS::MET_NOELECTRON250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET_NOELECTRON,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                            };

        if (selection.Contains("_useMETTrigger")) selection_chain[0] = CUTS::PRESELECTION_NOELECTRON_USEMETTRIGGER;
        if (selection.Contains("_useInclusiveElTrigger")) selection_chain[0] = CUTS::PRESELECTION_NOELECTRON_INCLUSIVE_EL_TRIGGER;
        if (selection.Contains("_withJetCleaning")) selection_chain[0] = CUTS::PRESELECTION_NOELECTRON_WITH_JET_CLEANING;
        if (selection.Contains("_allElIso")) selection_chain.insert(selection_chain.begin()+2,CUTS::ALL_EL_ISO);
        if (selection.Contains("_nonZeroIso")) selection_chain.insert(selection_chain.begin()+2,CUTS::EL_ISO_PTVARCONE_NONZERO);

        if (selection.Contains("_0b")) selection_chain.push_back(CUTS::VETO_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_1b")) selection_chain.push_back(CUTS::ONE_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_2b")) selection_chain.push_back(CUTS::TWO_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_geq1b")) selection_chain.push_back(CUTS::GEQ_ONE_ASSOCIATED_TRACKBJET);
        else if (selection.Contains("_leq2b")) selection_chain.push_back(CUTS::LEQ_TWO_ASSOCIATED_TRACKBJET);
    }
    //////////////////////////////////////////////////////////////////////////////////////////
    else if (selection == "checkJiangliuZnunu")
                                                            selection_chain = {
                                                                CUTS::NO_CUT,
                                                                CUTS::JET_CLEANING,
                                                                CUTS::JIANGLIU_PRESELECTION_NOMUON,
                                                                CUTS::LEPTON_VETO,
                                                                CUTS::PH_VETO,
                                                                CUTS::TAU_VETO,
                                                                CUTS::MET_NOMUON150
                                                            };
    else if (selection == "checkJiangliuZmumu")
                                                            selection_chain = {
                                                                CUTS::NO_CUT,
                                                                CUTS::JET_CLEANING,
                                                                CUTS::JIANGLIU_PRESELECTION_NOMUON,
                                                                CUTS::JIANGLIU_TWO_TIGHTID_ISO_MUONS,
                                                                CUTS::PH_VETO,
                                                                CUTS::TAU_VETO,
                                                                CUTS::DIMUON_MASS,
                                                                CUTS::MET_NOMUON150
                                                            };

    else if (selection == "checkJiangliuZnunu1")
                                                            selection_chain = {
                                                                CUTS::NO_CUT,
                                                                CUTS::JET_CLEANING,
                                                            };
    else if (selection == "checkJiangliuZmumu1")
                                                            selection_chain = {
                                                                CUTS::NO_CUT,
                                                                CUTS::JET_CLEANING,
                                                            };    

    else if (selection == "checkJiangliuZnunu2")
                                                            selection_chain = {
                                                                CUTS::NO_CUT,
                                                                CUTS::JET_CLEANING,
                                                                CUTS::JIANGLIU_PRESELECTION_NOMUON,
                                                            };
    else if (selection == "checkJiangliuZmumu2")
                                                            selection_chain = {
                                                                CUTS::NO_CUT,
                                                                CUTS::JET_CLEANING,
                                                                CUTS::JIANGLIU_PRESELECTION_NOMUON,
                                                            };  
    else if (selection == "checkJiangliuZnunu3")
                                                            selection_chain = {
                                                                CUTS::NO_CUT,
                                                                CUTS::JET_CLEANING,
                                                                CUTS::JIANGLIU_PRESELECTION_NOMUON,
                                                                CUTS::LEPTON_VETO,
                                                            };
    else if (selection == "checkJiangliuZmumu3")
                                                            selection_chain = {
                                                                CUTS::NO_CUT,
                                                                CUTS::JET_CLEANING,
                                                                CUTS::JIANGLIU_PRESELECTION_NOMUON,
                                                                CUTS::JIANGLIU_TWO_TIGHTID_ISO_MUONS,
                                                            };    
    else if (selection == "checkJiangliuZnunu4")
                                                            selection_chain = {
                                                                CUTS::NO_CUT,
                                                                CUTS::JET_CLEANING,
                                                                CUTS::JIANGLIU_PRESELECTION_NOMUON,
                                                                CUTS::MET_NOMUON150,
                                                            };
    else if (selection == "checkJiangliuZmumu4")
                                                            selection_chain = {
                                                                CUTS::NO_CUT,
                                                                CUTS::JET_CLEANING,
                                                                CUTS::JIANGLIU_PRESELECTION_NOMUON,
                                                                CUTS::MET_NOMUON150,
                                                            };      
    else if (selection == "checkJiangliuZmumuM")
                                                            selection_chain = {
                                                                CUTS::NO_CUT,
                                                                CUTS::JET_CLEANING,
                                                                CUTS::JIANGLIU_PRESELECTION_NOMUON,
                                                                CUTS::JIANGLIU_TWO_MEDIUMID_ISO_MUONS,
                                                                CUTS::PH_VETO,
                                                                CUTS::TAU_VETO,
                                                                CUTS::DIMUON_MASS,
                                                                CUTS::MET_NOMUON150
                                                            };   
    else if (selection == "checkJiangliuZmumuL")
                                                            selection_chain = {
                                                                CUTS::NO_CUT,
                                                                CUTS::JET_CLEANING,
                                                                CUTS::JIANGLIU_PRESELECTION_NOMUON,
                                                                CUTS::JIANGLIU_TWO_LOOSEID_ISO_MUONS,
                                                                CUTS::PH_VETO,
                                                                CUTS::TAU_VETO,
                                                                CUTS::DIMUON_MASS,
                                                                CUTS::MET_NOMUON150
                                                            };                                   
    //////////////////////////////////////////////////////////////////////////////////////////
    // else if (selection == "testSelection") 
    //                                                         selection_chain= {
    //                                                             CUTS::TEST
    //                                                         };

    else if (selection == "monoV_merged_C2Cut_50tag_0b_HP") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::VETO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                    CUTS::C2,
                                                                }; 
    else if (selection == "monoV_merged_C2Cut_50tag_0b_LP") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::VETO_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                    CUTS::C2,
                                                                }; 
    else if (selection == "monoV_merged_C2Cut_50tag_1b_HP") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                    CUTS::C2,
                                                                }; 
    else if (selection == "monoV_merged_C2Cut_50tag_1b_LP") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                    CUTS::C2,
                                                                };
    else if (selection == "monoV_merged_C2Cut_50tag_2b") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::TWO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                    CUTS::C2,
                                                                }; 
    //////////////////////////////////////////////////////////////////////////////////////////
    else if (selection == "monoV_merged_C2HardCut_50tag_0b_HP") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::VETO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                    CUTS::C2_HARD,
                                                                }; 
    else if (selection == "monoV_merged_C2HardCut_50tag_0b_LP") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::VETO_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                    CUTS::C2_HARD,
                                                                }; 
    else if (selection == "monoV_merged_C2HardCut_50tag_1b_HP") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                    CUTS::C2_HARD,
                                                                }; 
    else if (selection == "monoV_merged_C2HardCut_50tag_1b_LP") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::ONE_ASSOCIATED_TRACKBJET,
                                                                    -1*CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                    CUTS::C2_HARD,
                                                                };
    else if (selection == "monoV_merged_C2HardCut_50tag_2b") 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::LEPTON_VETO,
                                                                    CUTS::MET250,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::DPHI_FATJET_MET,
                                                                    CUTS::TWO_ASSOCIATED_TRACKBJET,
                                                                    CUTS::VTAG_50_MASS,
                                                                    CUTS::VETO_SEPARATED_TRACKBJET,
                                                                    CUTS::C2_HARD,
                                                                }; 
    //////////////////////////////////////////////////////////////////////////////////////////
    else if (selection.Contains("monoV_dateStudy_VTagger")) 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::GEQ_ONE_LARGE_JET,
                                                                    CUTS::VTAG_50_D2,
                                                                    CUTS::VTAG_50_MASS,
                                                                }; 
    else if (selection.Contains("monoV_dateStudy_0b")) 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::VETO_CENTRAL_BJET,
                                                                }; 
    else if (selection.Contains("monoV_dateStudy_geq1b")) 
                                                            selection_chain = {
                                                                    CUTS::PRESELECTION,
                                                                    CUTS::GEQ_ONE_CENTRAL_BJET,
                                                                }; 
    //////////////////////////////////////////////////////////////////////////////////////////
    else if (selection.Contains("METTurnOn_")) {
                                                            selection_chain= {
                                                                    CUTS::MIN_DPHI_MULTIJET_NOMUON,
                                                                    CUTS::MET_TRACK_NOMUON,
                                                                    CUTS::ONE_TIGHT_ISO_MUON,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::SUBLEAD_CENTRAL_JET_PT20,
                                                                    CUTS::SUMPT,
                                                                    CUTS::DPHI_DIJET_MET_NOMUON,
                                                                    CUTS::DPHI_DIJET,
                                                                    CUTS::DIJET_MASS,
                                                            };
        if (selection.Contains("HLT_xe70_mht")) {
            selection_chain.push_back(CUTS::IS_2015);
            if (selection.Contains("__num")) selection_chain.push_back(CUTS::TRIGGER_HLT_xe70_mht);
        }
        if (selection.Contains("HLT_xe90_mht_L1XE50")) {
            selection_chain.push_back(CUTS::IS_2016_A);
            if (selection.Contains("__num")) selection_chain.push_back(CUTS::TRIGGER_HLT_xe90_mht_L1XE50);
        }
        if (selection.Contains("HLT_xe100_mht_L1XE50")) {
            selection_chain.push_back(CUTS::IS_2016_B);
            if (selection.Contains("__num")) selection_chain.push_back(CUTS::TRIGGER_HLT_xe100_mht_L1XE50);
        }
        if (selection.Contains("HLT_xe110_mht_L1XE50")) {
            selection_chain.push_back(CUTS::IS_2016_C);
            if (selection.Contains("__num")) selection_chain.push_back(CUTS::TRIGGER_HLT_xe110_mht_L1XE50);
        }
        if (selection.Contains("HLT_xe110_pufit_L1XE55")) {
            selection_chain.push_back(CUTS::IS_2017_A);
            if (selection.Contains("__num")) selection_chain.push_back(CUTS::TRIGGER_HLT_xe110_pufit_L1XE55);
        }
        if (selection.Contains("HLT_xe110_pufit_L1XE50")) {
            selection_chain.push_back(CUTS::IS_2017_B);
            if (selection.Contains("__num")) selection_chain.push_back(CUTS::TRIGGER_HLT_xe110_pufit_L1XE50);
        }
        if (selection.Contains("HLT_xe110_pufit_xe70_L1XE50")) {
            selection_chain.push_back(CUTS::IS_2018_A);
            if (selection.Contains("__num")) selection_chain.push_back(CUTS::TRIGGER_HLT_xe110_pufit_xe70_L1XE50);
        }
        if (selection.Contains("HLT_xe110_pufit_xe65_L1XE50")) {
            selection_chain.push_back(CUTS::IS_2018_B);
            if (selection.Contains("__num")) selection_chain.push_back(CUTS::TRIGGER_HLT_xe110_pufit_xe65_L1XE50);
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////
    else if (selection.Contains("ELTurnOn_")) {
                                                            selection_chain= {
                                                                    CUTS::MIN_DPHI_MULTIJET_NOELECTRON,
                                                                    CUTS::MET_TRACK_NOELECTRON,
                                                                    CUTS::ONE_TIGHT_ISO_ELECTRON,
                                                                    CUTS::TWO_CENTRAL_JETS,
                                                                    CUTS::LEAD_CENTRAL_JET_PT45,
                                                                    CUTS::SUBLEAD_CENTRAL_JET_PT20,
                                                                    CUTS::SUMPT,
                                                                    CUTS::DPHI_DIJET_MET_NOELECTRON,
                                                                    CUTS::DPHI_DIJET,
                                                                    CUTS::DIJET_MASS,
                                                            };
        if (selection.Contains("HLT_e24_lhmedium_L1EM20VH__")) {
            selection_chain.push_back(CUTS::IS_2015);
            if (selection.Contains("__num")) selection_chain.push_back(CUTS::TRIGGER_HLT_e24_lhmedium_L1EM20VH);
        }
        if (selection.Contains("HLT_e60_lhmedium__")) {
            selection_chain.push_back(CUTS::IS_2015);
            if (selection.Contains("__num")) selection_chain.push_back(CUTS::TRIGGER_HLT_e60_lhmedium);
        }
        if (selection.Contains("HLT_e120_lhloose__")) {
            selection_chain.push_back(CUTS::IS_2015);
            if (selection.Contains("__num")) selection_chain.push_back(CUTS::TRIGGER_HLT_e120_lhloose);
        }
        if (selection.Contains("HLT_e26_lhtight_nod0_ivarloose__")) {
            selection_chain.push_back(-1*CUTS::IS_2015);
            if (selection.Contains("__num")) selection_chain.push_back(CUTS::TRIGGER_HLT_e26_lhtight_nod0_ivarloose);
        }
        if (selection.Contains("HLT_e60_lhmedium_nod0__")) {
            selection_chain.push_back(-1*CUTS::IS_2015);
            if (selection.Contains("__num")) selection_chain.push_back(CUTS::TRIGGER_HLT_e60_lhmedium_nod0);
        }
        if (selection.Contains("HLT_e140_lhloose_nod0__")) {
            selection_chain.push_back(-1*CUTS::IS_2015);
            if (selection.Contains("__num")) selection_chain.push_back(CUTS::TRIGGER_HLT_e140_lhloose_nod0);
        }
    }

    if (selection == "CutflowComparison_CR2el_resolved") {
                                                                    selection_chain = {
                                                                        CUTS::NO_CUT,
                                                                        CUTS::EL_TRIGGER,
                                                                        CUTS::MIN_DPHI_MULTIJET_NOELECTRON,
                                                                        CUTS::MET_TRACK_NOELECTRON,
                                                                        CUTS::TWO_LOOSE_ONE_MEDIUM_ELECTRON,
                                                                        CUTS::DIELECTRON_MASS,
                                                                        CUTS::MET_NOELECTRON150,
                                                                        CUTS::TWO_CENTRAL_JETS,
                                                                        CUTS::LEAD_CENTRAL_JET_PT45,
                                                                        CUTS::SUMPT,
                                                                        CUTS::DPHI_DIJET_MET_NOELECTRON,
                                                                        CUTS::DPHI_DIJET
                                                                    };
    }
    if (selection == "CutflowComparison_CR2el_merged") {
                                                                    selection_chain = {
                                                                        CUTS::NO_CUT,
                                                                        CUTS::EL_TRIGGER,
                                                                        CUTS::MIN_DPHI_MULTIJET_NOELECTRON,
                                                                        CUTS::MET_TRACK_NOELECTRON,
                                                                        CUTS::TWO_LOOSE_ONE_MEDIUM_ELECTRON,
                                                                        CUTS::DIELECTRON_MASS,
                                                                        CUTS::MET_NOELECTRON250,
                                                                        CUTS::GEQ_ONE_LARGE_JET,
                                                                        CUTS::DPHI_FATJET_MET_NOELECTRON,
                                                                        CUTS::VETO_SEPARATED_TRACKBJET
                                                                    };
    }

    if (selection == "CutflowSteps_CR2el_resolved") {
                                                                    selection_chain = {
                                                                        CUTS::NO_CUT,
                                                                        CUTS::JET_CLEANING,
                                                                        CUTS::JIANGLIU_PRESELECTION,
                                                                        CUTS::JIANGLIU_ELECTRON_ID_WP,
                                                                        CUTS::JIANGLIU_ELECTRON_ID_ISO,
                                                                        CUTS::MUON_VETO,
                                                                        CUTS::PH_VETO,
                                                                        CUTS::TAU_VETO,
                                                                        CUTS::DIELECTRON_MASS,
                                                                        CUTS::MET_NOELECTRON150,
                                                                        CUTS::TWO_CENTRAL_JETS,
                                                                        CUTS::LEAD_CENTRAL_JET_PT45,
                                                                        CUTS::SUMPT,
                                                                        CUTS::JIANGLIU_DPHI_DIJET_MET_NOELECTRON,
                                                                        CUTS::DPHI_DIJET,
                                                                    };
    }

    if (selection.Contains("_actualMuCut_leq20")) selection_chain.push_back(CUTS::ACTUALMU_LEQ20);
    if (selection.Contains("_actualMuCut_geq20_leq40")) selection_chain.push_back(CUTS::ACTUALMU_GEQ20_LEQ40);
    if (selection.Contains("_actualMuCut_geq40")) selection_chain.push_back(CUTS::ACTUALMU_GEQ40);
    if (selection.Contains("_averageMuCut_leq20")) selection_chain.push_back(CUTS::AVERAGEMU_LEQ20);
    if (selection.Contains("_averageMuCut_geq20_leq40")) selection_chain.push_back(CUTS::AVERAGEMU_GEQ20_LEQ40);
    if (selection.Contains("_averageMuCut_geq40")) selection_chain.push_back(CUTS::AVERAGEMU_GEQ40);
    if (selection.Contains("_runCutA")) selection_chain.push_back(CUTS::RUN_A);
    if (selection.Contains("_runCutB")) selection_chain.push_back(CUTS::RUN_B);
    if (selection.Contains("_runCutC")) selection_chain.push_back(CUTS::RUN_C);
    if (selection.Contains("_runCutD")) selection_chain.push_back(CUTS::RUN_D);
    if (selection.Contains("_runCutE")) selection_chain.push_back(CUTS::RUN_E);
    if (selection.Contains("_runCutF")) selection_chain.push_back(CUTS::RUN_F);
    if (selection.Contains("_runCutG")) selection_chain.push_back(CUTS::RUN_G);
    if (selection.Contains("_runCutH")) selection_chain.push_back(CUTS::RUN_H);
    if (selection.Contains("_runCutI")) selection_chain.push_back(CUTS::RUN_I);
    if (selection.Contains("_runCutJ")) selection_chain.push_back(CUTS::RUN_J);
    if (selection.Contains("_runCutK")) selection_chain.push_back(CUTS::RUN_K);
    if (selection.Contains("_runCutL")) selection_chain.push_back(CUTS::RUN_L);
    if (selection.Contains("_runCutM")) selection_chain.push_back(CUTS::RUN_M);
    if (selection.Contains("_runCutN")) selection_chain.push_back(CUTS::RUN_N);

    if (selection == "NoCut") selection_chain = {CUTS::NO_CUT};

    return selection_chain;

}

double SelectionTool::getSelectionWeight(TString selection, MergedTree * t) {

    if (t->year == 0) return 1.0;

    // weight set in merging as:
    // shortTree->weight = (shortTree->mc_weight_sum != 0 && m_isMC) ? shortTree->pu_weight * shortTree->xSec_SUSY * shortTree->filter_eff * shortTree->k_factor * shortTree->extra_weight / shortTree->mc_weight_sum : 1;

    double weight = t->campaign_lumi * t->weight * 1e3;
    if (selection.Contains("CR1mu") || selection.Contains("CR2mu")) {
        for (UInt_t i_mu = 0 ; i_mu < t->mu_SF->size() ; i_mu++) {
            weight *= t->mu_SF->at(i_mu);
        }
    }
    if (selection.Contains("CR1el")) {
        for (UInt_t i_el = 0 ; i_el < t->el_SF->size() ; i_el++) {
            weight *= t->el_SF->at(i_el) * t->el_SF_iso->at(i_el) * t->el_SF_trigger->at(i_el);
        }
    }
    if (selection.Contains("CR2el")) {
        for (UInt_t i_el = 0; i_el < t->el_SF->size(); i_el++) {
            weight *= t->el_SF->at(i_el);
            if (i_el == 1) {
                //Di-electron final state (like Z-> ee), both electrons able to fire the trigger 
                //(i.e. with an energy above trigger threshold):
                //single electron trigger: SFevent = [1-(1-eps1xSF1)x(1-eps2xSF2)]/[1-(1-eps1)x(1-eps2)] 
                //where eps1, and eps2 are the single electron MC efficiencies corresponding to your two electrons
                //https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EfficiencyMeasurements2012#Trigger

                Float_t CR2e_SF_trigger = (1 - (1 - t->el_eff_trigger->at(0) * t->el_SF_trigger->at(0)) * (1 - t->el_eff_trigger->at(1) * t->el_SF_trigger->at(1))) / (1 - (1 - t->el_eff_trigger->at(0)) * (1 - t->el_eff_trigger->at(1)));
                if ((1 - (1 - t->el_eff_trigger->at(0)) * (1 - t->el_eff_trigger->at(1))) == 0) CR2e_SF_trigger = 0;
                weight *= CR2e_SF_trigger;
            }
        }
    }

    vector<int> selection_chain = getSelectionChain(selection);
    if (contains((int)CUTS::VETO_CENTRAL_BJET,selection_chain) || 
        contains((int)CUTS::ONE_CENTRAL_BJET,selection_chain) ||
        contains((int)CUTS::TWO_CENTRAL_BJET,selection_chain) ||
        contains((int)CUTS::GEQ_ONE_CENTRAL_BJET,selection_chain) ||
        contains((int)CUTS::LEQ_TWO_CENTRAL_BJET,selection_chain)) 
    {
        weight *= t->btag_weight;
    }

    // TODO btag_weight for trackJets

    weight *= t->jvt_weight;

    return weight;

}

TLorentzVector SelectionTool::getJiangliuMetTrackNoMuon(MergedTree * t) {
    double muon1pt = 0;
    double muon2pt = 0;
    double muon1eta = 0;
    double muon2eta = 0;
    double muon1phi = 0;
    double muon2phi = 0;
    double muon1m = 0;
    double muon2m = 0;
    if (t->n_mu >= 2) {
        muon1pt = t->mu_pt->at(0) / 1000;
        muon1eta = t->mu_eta->at(0);
        muon1phi = t->mu_phi->at(0);
        muon1m = t->mu_m->at(0) / 1000;

        muon2pt = t->mu_pt->at(1) / 1000;
        muon2eta = t->mu_eta->at(1);
        muon2phi = t->mu_phi->at(1);
        muon2m = t->mu_m->at(1) / 1000;
    }
    if (t->n_mu == 1) {
        muon1pt = t->mu_pt->at(0) / 1000;
        muon1eta = t->mu_eta->at(0);
        muon1phi = t->mu_phi->at(0);
        muon1m = t->mu_m->at(0) / 1000;
    }
    TLorentzVector v1;
    v1.SetPtEtaPhiM(muon1pt, muon1eta, muon1phi, muon1m);
    TLorentzVector v2;
    v2.SetPtEtaPhiM(muon2pt, muon2eta, muon2phi, muon2m);
    TLorentzVector v3 = v1 + v2;
    TLorentzVector met_track;
    met_track.SetPxPyPzE(t->met_track_etx / 1000, t->met_track_ety / 1000, (t->met_track_sumet - t->met_track_ety - t->met_track_etx) / 1000, sqrt(t->met_track_etx / 1000 * t->met_track_etx / 1000 + t->met_track_ety / 1000 * t->met_track_ety / 1000 + (t->met_track_sumet - t->met_track_ety - t->met_track_etx) / 1000 * (t->met_track_sumet - t->met_track_ety - t->met_track_etx) / 1000));
    TLorentzVector met_track_nomu;
    met_track_nomu = met_track + v3;
    return met_track_nomu;
}

#endif