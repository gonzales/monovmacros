#include "PlotList.h"
#include "PlotUtils.h"
#include "SampleList.h"
#include "SelectionTool.h"

TH1D * GetMetTurnOnHistogram(TString plotsDir, TString channel_name, TString trigger, TString num_or_den);
bool notReadeMetTurnOn(TString plotsDir, TString channel_name, vector<TString> trigger_list, bool checking_files);

void MakeMetTurnOnPlot(
                        TString config_file, 
                        TString bkg_group = "BKG", 
                        TString label = "", 
                        bool checking_files = false, 
                        TString campaign = "", 
                        TString useOutDir = ""
                    ) 
{

    TStopwatch timer; timer.Start();
    gStyle->SetOptStat(0);

    /////////////////////////////////////////////////////////////////////////////////
    // Retrieve channels
    vector<TString> channels = SampleList::getChannelGroup(bkg_group);
    /////////////////////////////////////////////////////////////////////////////////
    // Reading config file
    TEnv env; 
    if (env.ReadFile(config_file,
     kEnvAll)) {
        coutError("Unable to read configuration file from PathResolverFindCalibFile of input " + config_file);
    }
    TString plotsDir = env.GetValue("output_plots_folder", "EMPTY"); if (plotsDir == "EMPTY" && useOutDir == "") {coutError("No output folder set in config file."); return;}
    TString dataPlotsDir = env.GetValue("data_plots_folder", "EMPTY");

    if (useOutDir != "") {
        plotsDir = useOutDir;
        dataPlotsDir = useOutDir;
    }

    if (label != "") plotsDir += "_" + label;
    if (label != "" && dataPlotsDir != "EMPTY") dataPlotsDir += "_" + label;

    bool addData = dataPlotsDir != "EMPTY" && bkg_group.Contains("BKG");

    if (campaign == "") {
        if (plotsDir.Contains("mc16a")) campaign = "mc16a";
        if (plotsDir.Contains("mc16d")) campaign = "mc16d";
        if (plotsDir.Contains("mc16e")) campaign = "mc16e";
        if (plotsDir.Contains("full")) campaign = "full";
    }
    if (campaign == "") {
        coutError("Campaign couldn't be extracted");
        addData = false;
    }

    double lumi = GetCampaignLumi(campaign);

    // vector<TString> trigger_list = {"HLT_xe70_mht","HLT_xe90_mht_L1XE50","HLT_xe100_mht_L1XE50","HLT_xe110_mht_L1XE50","HLT_xe110_pufit_L1XE55","HLT_xe110_pufit_L1XE50","HLT_xe110_pufit_xe70_L1XE50","HLT_xe110_pufit_xe65_L1XE50"};
    vector<TString> trigger_list = {"HLT_e24_lhmedium_L1EM20VH","HLT_e60_lhmedium","HLT_e120_lhloose","HLT_e26_lhtight_nod0_ivarloose","HLT_e60_lhmedium_nod0","HLT_e140_lhloose_nod0"};
    
    /////////////////////////////////////////////////////////////////////////////////
    // CHECKING PLOT FILES EXIST
    if (checking_files) {cout << "Checking for histograms.root files for each channel..." << endl;}
    for (TString channel_name : channels) {
        cout << "------- " <<  channel_name << " -----------" << endl;
        notReadeMetTurnOn(plotsDir, channel_name, trigger_list, checking_files);
    }
    if (addData) {
        if (campaign == "mc16a" && notReadeMetTurnOn(dataPlotsDir, "data15", trigger_list, checking_files)) addData = false;
        if (campaign == "mc16a" && notReadeMetTurnOn(dataPlotsDir, "data16", trigger_list, checking_files)) addData = false;
        if (campaign == "mc16d" && notReadeMetTurnOn(dataPlotsDir, "data17", trigger_list, checking_files)) addData = false;
        if (campaign == "mc16e" && notReadeMetTurnOn(dataPlotsDir, "data18", trigger_list, checking_files)) addData = false;
    }
    if (checking_files) return;

    /////////////////////////////////////////////////////////////////////////////////
    // Stacking plots
    for (TString trigger : trigger_list) {
        cout << "===== Producing plots for the trigger " << trigger << " ==============" << endl;
        map<TString,TH1D*> map_hists_perName_num;
        map<TString,TH1D*> map_hists_perName_den;
        for (TString channel_name : channels) {
            TH1D * hist_num = GetMetTurnOnHistogram(plotsDir, channel_name, trigger, "num");
            TH1D * hist_den = GetMetTurnOnHistogram(plotsDir, channel_name, trigger, "den");
            if (hist_num != NULL && hist_den != NULL) {
                if (hist_num->GetEntries() > 0) {
                    map_hists_perName_num[channel_name] = hist_num;
                    map_hists_perName_den[channel_name] = hist_den;
                }
            }
        } // add MC
        if (addData) {
            if (campaign == "mc16a") {
                TH1D * hist_data15_num = GetMetTurnOnHistogram(dataPlotsDir, "data15", trigger, "num");
                TH1D * hist_data15_den = GetMetTurnOnHistogram(dataPlotsDir, "data15", trigger, "den");
                TH1D * hist_data16_num = GetMetTurnOnHistogram(dataPlotsDir, "data16", trigger, "num");
                TH1D * hist_data16_den = GetMetTurnOnHistogram(dataPlotsDir, "data16", trigger, "den");
                if (hist_data15_num != NULL && hist_data16_num != NULL && hist_data15_den != NULL && hist_data16_den != NULL) {
                    if (hist_data15_num->GetEntries() > 0) {
                        hist_data15_num->SetMarkerStyle(20);
                        map_hists_perName_num["data"] = hist_data15_num;
                        map_hists_perName_den["data"] = hist_data15_den;
                    } else if (hist_data16_num->GetEntries() > 0) {
                        hist_data16_num->SetMarkerStyle(20);
                        map_hists_perName_num["data"] = hist_data16_num;
                        map_hists_perName_den["data"] = hist_data16_den;
                    }
                }
            } else if (campaign == "mc16d") {
                TH1D * hist_data17_num = GetMetTurnOnHistogram(dataPlotsDir, "data17", trigger, "num");
                TH1D * hist_data17_den = GetMetTurnOnHistogram(dataPlotsDir, "data17", trigger, "den");
                if (hist_data17_num != NULL && hist_data17_den != NULL) {
                    if (hist_data17_num->GetEntries() > 0) {
                        hist_data17_num->SetMarkerStyle(20);
                        map_hists_perName_num["data"] = hist_data17_num;
                        map_hists_perName_den["data"] = hist_data17_den;
                    }
                }
            } else if (campaign == "mc16e") {
                TH1D * hist_data18_num = GetMetTurnOnHistogram(dataPlotsDir, "data18", trigger, "num");
                TH1D * hist_data18_den = GetMetTurnOnHistogram(dataPlotsDir, "data18", trigger, "den");
                if (hist_data18_num != NULL && hist_data18_den != NULL) {
                    if (hist_data18_num->GetEntries() > 0) {
                        hist_data18_num->SetMarkerStyle(20);
                        map_hists_perName_num["data"] = hist_data18_num;
                        map_hists_perName_den["data"] = hist_data18_den;
                    }
                }
            }
        } // addData

        if (map_hists_perName_num.size() > 0) {
            makeDirectory(plotsDir + "/" + bkg_group + "_METTurnOn");
            PlotUtils::makeTurnOnPlot(map_hists_perName_num, map_hists_perName_den, PlotList::getXTitle("met_tst_et"), "Trigger efficiency", plotsDir + "/" + bkg_group + "_METTurnOn/" + trigger, -1, {trigger}, {0.13}, {0.87});
        }

    } //Loop triggers

}

TH1D * GetMetTurnOnHistogram(TString plotsDir, TString channel_name, TString trigger, TString num_or_den) {
    TString file_name = plotsDir + "/" + channel_name + "/METTurnOn/histograms_" + channel_name + "_METTurnOn.root";
    TFile * file = new TFile(file_name);
    TString histKeyName = trigger + "_stdMET_" + num_or_den + "__" + channel_name + "_METTurnOn";
    if (!file->GetListOfKeys()->Contains(histKeyName)) return NULL;
    TH1D * hist = (TH1D*)file->Get(histKeyName)->Clone();
    hist->SetDirectory(0);
    if (hist == NULL) {
        coutError("No hist for " + trigger + " in " + file_name);
    } 
    file->Close();

    return hist;
}

bool notReadeMetTurnOn(TString plotsDir, TString channel_name, vector<TString> trigger_list, bool checking_files) {
    TString file_name = plotsDir + "/" + channel_name + "/METTurnOn/histograms_" + channel_name + "_METTurnOn.root";
    if (!fileExists(file_name)) {
        coutError("No plots here " + file_name);
        return true;
    } else if (checking_files) {
        TFile * file = new TFile(file_name);
        for (TString trigger : trigger_list) {
            TString histKeyName_den = trigger + "_stdMET_den__" + channel_name + "_METTurnOn";
            TString histKeyName_num = trigger + "_stdMET_num__" + channel_name + "_METTurnOn";
            if (!file->GetListOfKeys()->Contains(histKeyName_den)) {
                coutError("The plot for the denominator of " + trigger + " is not done for " + channel_name);
                return true;
            }
            if (!file->GetListOfKeys()->Contains(histKeyName_num)) {
                coutError("The plot for the numerator of " + trigger + " is not done for " + channel_name);
                return true;
            }
        }
        file->Close();
    }
    return false;
}
