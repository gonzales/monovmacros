#!/usr/bin/env python

import sys, os
import ROOT

from optparse import OptionParser

###############################################################################

parser = OptionParser()
parser.add_option('-c', '--config-file', type='string', help='Path to the config file', dest = "config_file",default="None")
parser.add_option('-b', '--bkg-group', type='string', help='Bkg group name', dest = "bkg_group",default="BKG_nojetjet")
parser.add_option('-s', '--sig-group', type='string', help='Sig group name', dest = "sig_group",default="INVH")
parser.add_option('-l', '--label', type='string', help='Label', dest = "label",default="")
parser.add_option('--check-files', dest="checkingFiles", action="store_true",help='Just check the files exist', default=False)
parser.add_option('-p', dest="plots", action="store_true",help='Run CombinePlots.C', default=False)
parser.add_option('-n', dest="n1plots", action="store_true",help='Run MakeN1Plots.C', default=False)
parser.add_option('-f', dest="cutflow", action="store_true",help='Run CutflowPlot.C', default=False)
parser.add_option('-m', dest="metturnon", action="store_true",help='Run MakeMetTurnOnPlot.C', default=False)
parser.add_option('--combine-campaigns', dest="combinecampaigns", action="store_true",help='Run CombineCampaigns.C', default=False)
parser.add_option('--contribution', type='string', help='Make contribution table in respect to this channel', dest = "contribution",default="")
parser.add_option('--nostack', dest="noStack", action="store_true",help='Don\'t stack bkg histograms', default=False)
parser.add_option('--normalize', dest="doNormalize", action="store_true",help='Normalize histograms', default=False)
parser.add_option('--do', dest="doSelection", type='string',help='Selection to use',default="")
parser.add_option('--doN1', dest="doN1Selection", type='string',help='N1 Selection to use',default="")
parser.add_option('--filter', dest="filterSelection", type='string',help='Regular expression to filter the selections for summary cutflow table',default="")
parser.add_option('--campaign', dest="campaign", type='string',help='mc16a/mc16d/mc16e/',default="")
parser.add_option('--outDir', dest="useOutDir", type='string',help='use this output folder',default="")
parser.add_option('--midrule-step', type='int', help='Add midrule in summary table after this step', dest = "midruleStep",default=-1)
parser.add_option('--folder-mc16a', dest="folder_mc16a", type='string',help='Folder for mc16a',default="")
parser.add_option('--folder-mc16d', dest="folder_mc16d", type='string',help='Folder for mc16d',default="")
parser.add_option('--folder-mc16e', dest="folder_mc16e", type='string',help='Folder for mc16e',default="")


(options, args) = parser.parse_args()

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(".L CombinePlots.C")
ROOT.gROOT.ProcessLine(".L MakeN1Plots.C")
ROOT.gROOT.ProcessLine(".L CutflowPlot.C")
ROOT.gROOT.ProcessLine(".L MakeMetTurnOnPlot.C")
ROOT.gROOT.ProcessLine(".L CombineCampaigns.C")

if options.plots:
  ROOT.CombinePlots(options.config_file, 
                    options.bkg_group,
                    options.sig_group,
                    options.label,
                    options.doSelection,
                    options.checkingFiles,
                    options.filterSelection,
                    options.campaign,
                    options.useOutDir,
                    not options.noStack,
                    options.doNormalize
                  )

if options.n1plots:
  ROOT.MakeN1Plots(options.config_file,
                   options.bkg_group,
                   options.sig_group,
                   options.label,
                   options.checkingFiles,
                   options.filterSelection,
                   options.doSelection,
                   options.useOutDir,
                   options.doN1Selection,
                  )

if options.cutflow:
  ROOT.CutflowPlot(options.config_file,
                   options.bkg_group,
                   options.sig_group,
                   options.contribution,
                   options.label,
                   options.campaign,
                   options.doSelection,
                   options.checkingFiles,
                   options.filterSelection,
                   options.useOutDir,
                   options.midruleStep
                  )

if options.metturnon:
  ROOT.MakeMetTurnOnPlot(
                   options.config_file,
                   options.bkg_group,
                   options.label,
                   options.checkingFiles,
                   options.campaign,
                   options.useOutDir
                  )

if options.combinecampaigns:
  ROOT.CombineCampaigns(
                   options.config_file,
                   options.folder_mc16a,
                   options.folder_mc16d,
                   options.folder_mc16e,
                   options.doSelection,
                   options.doN1Selection
                  )
