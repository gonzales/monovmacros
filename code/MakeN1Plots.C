using namespace RooFit;
using namespace RooStats;

#include "methods.h"
#include "PlotList.h"
#include "SampleList.h"
#include "SelectionTool.h"
#include "PlotUtils.h"
#include <RooStats/RooStatsUtils.h>



void getHistogram(TH1D * &hist, TString output_plots_folder, TString plotName, TString channel, TString tag, TString N1_selection);
void  GetSignificanceHists(map<TString,TH1D*> map_hists, vector<TH1D*> &v_zhist, vector<TH1D*> &v_phist, double computation_type);
void doThePlots(map<TString,TH1D*> map_hist_sumw, TString output_plots_folder, TString bkg_group, TString sig_channel, TString plotName, TString N1_selection, TString n1_tag, double lumi, TString selection);

void MakeN1Plots(TString config_file, TString bkg_group = "BKG", TString sig_group = "SIG_ex", TString label = "", bool checkingFiles = false, TString filter_summary = "", TString doSelection = "", TString useOutDir = "", TString doN1Selection = "") {
    gStyle->SetOptStat(0);

    //////////////////////
    //Parsing of the parameters
    TEnv env; 
    if (env.ReadFile(config_file, kEnvAll)) {
        coutError("Unable to read configuration file from PathResolverFindCalibFile of input " + config_file);
        // return;
    }
    TString output_plots_folder = env.GetValue("output_plots_folder","EMPTY"); if (output_plots_folder == "EMPTY" && useOutDir == "") {coutError("No output_plots_folder in the configuration file"); return;}
    vector<TString> selections = getTokens(env.GetValue("plot_selections",""), " "); if (selections.size() == 0 && doSelection == "") {coutError("No selections defined in the config file or given!"); return;}
    vector<TString> N1_selections = getTokens(env.GetValue("N1_selections",""), " ");  if (N1_selections.size() == 0 && doN1Selection == "") {coutError("No N1_selections defined in config_file"); return;}

    if (useOutDir != "") output_plots_folder = useOutDir;

    if (label != "") output_plots_folder += "_" + label;

    if (doSelection != "") selections = {doSelection};
    if (doN1Selection != "") N1_selections = {doN1Selection};

    //////////////////////
    // Retrieve channels
    vector<TString> bkg_channels = SampleList::getChannelGroup(bkg_group);
    vector<TString> sig_channels = SampleList::getChannelGroup(sig_group);

    double lumi = -1;
    if (output_plots_folder.Contains("mc16a")) GetCampaignLumi("mc16a");
    if (output_plots_folder.Contains("mc16d")) GetCampaignLumi("mc16d");
    if (output_plots_folder.Contains("mc16e")) GetCampaignLumi("mc16e");
    if (output_plots_folder.Contains("full")) GetCampaignLumi("full");


    for (TString N1_selection : N1_selections) {
        cout << "==========================================" << endl;
        cout << "                 " << N1_selection << "             " << endl;
        TString plotName = PlotList::getVariableForSelection(N1_selection);
        if (PlotList::getVariableForSelection(N1_selection) == "") continue;

        for (TString selection : selections) {
            // for (TString sig_channel : sig_channels) {

            vector<TString> channels = bkg_channels; 
            channels.insert(channels.end(), sig_channels.begin(), sig_channels.end());
            // channels.push_back(sig_channel);

            //////////////////////
            // Build histograms

            if (filter_summary != "" && !selection.Contains((TRegexp)filter_summary)) continue;
            if (selection == "Preselection") continue;
            // vector<int> selection_chain = SelectionTool::getSelectionChain(selection);
            if (PlotList::validForN1Plot(N1_selection, selection)) {
                cout << "----------" << selection << " ---------------" << endl;
                TString n1_tag = SelectionTool::getSelectionShortLabel(selection);

                map<TString,TH1D*> map_hist_sumw;

                for (TString channel : channels) {

                    // TString loadFile_sumw = output_plots_folder + "/" + channel + "/N1Plots/histograms_" + channel + "_N1Plots_" + n1_tag + "_sumw_" + N1_selection + ".root";
                    TH1D * hist_sumw; getHistogram(hist_sumw, output_plots_folder, plotName, channel, n1_tag + "_sumw", N1_selection);
                    if (!hist_sumw) continue;

                    // if (plotName == "LCTopoD2") hist_sumw->GetXaxis()->SetRangeUser(0.,1.);

                    if (contains(channel,sig_channels)) hist_sumw->SetLineStyle(2); // Marking as signal

                    map_hist_sumw[channel] = hist_sumw;

                }

                if (!checkingFiles && map_hist_sumw.size() > 0) doThePlots(map_hist_sumw,output_plots_folder,bkg_group,sig_group,plotName,N1_selection,n1_tag,lumi,selection);

            }
            // }

        }

    }

}

void getHistogram(TH1D * &hist, TString output_plots_folder, TString plotName, TString channel, TString tag, TString N1_selection) {

    TString channel_main = (channel.Contains("_DSID")) ? channel(0,channel.Index("_DSID")) : channel;
    TString dsid_str = (channel.Contains("_DSID")) ? channel(channel.Index("_DSID"),channel.Length()) : (TString)"";

    TString loadFile = output_plots_folder + "/" + channel_main + "/N1Plots/histograms_" + channel_main + "_N1Plots_" + tag + "_" + N1_selection + ".root";
                        
    TFile * load_file = new TFile(loadFile);

    hist = (TH1D*)load_file->Get(plotName + "__" + channel_main + "_N1Plots_" + tag + "_" + N1_selection + dsid_str); 

    if (hist) {hist = (TH1D*)hist->Clone(); hist->SetDirectory(0);}
    else {coutError("Error retrieving " + plotName + " from " + loadFile); hist = nullptr; return;}

    int nBins = hist->GetNbinsX();
    hist->SetBinContent(nBins, hist->GetBinContent(nBins) + hist->GetBinContent(nBins+1));
    hist->SetBinContent(1, hist->GetBinContent(1) + hist->GetBinContent(0));

    if (load_file->IsOpen()) load_file->Close();

}

void GetSignificanceHists(map<TString,TH1D*> map_hists, vector<TH1D*> &v_zhist, vector<TH1D*> &v_phist, double computation_type) {

    TH1D * hist_bkg_sum = nullptr;
    TH1D * hist_sig_all = nullptr;
    map<TString,TH1D*> map_hist_sig_sum;

    for (auto kv : map_hists) {
        TString channel = kv.first;
        TH1D * hist = kv.second;

        if (hist->GetLineStyle() != 1) { // signal hist
            if (hist_sig_all) hist_sig_all->Add(hist);
            else hist_sig_all = (TH1D*)hist->Clone();
            if (map_hist_sig_sum.count(channel) > 0) map_hist_sig_sum[channel]->Add(hist);
            else map_hist_sig_sum[channel] = (TH1D*)hist->Clone();
        } else { // bkg hist
            if (hist_bkg_sum) hist_bkg_sum->Add(hist);
            else hist_bkg_sum = (TH1D*)hist->Clone();
        }
    }

    bool forward = GetMedian(hist_sig_all) > GetMedian(hist_bkg_sum);
    if (forward) cout << "forward" << endl;
    else cout << "backward" << endl;


    // Z-hist
    for (auto kv : map_hist_sig_sum) {
        TString sig_channel = kv.first;
        TH1D * hist_sig_sum = map_hist_sig_sum[sig_channel];

        TH1D *zhist, *phist;

        zhist = (TH1D*)hist_sig_sum->Clone(); zhist->Reset(); zhist->SetTitle("");
        phist = (TH1D*)hist_sig_sum->Clone(); zhist->Reset(); zhist->SetTitle("");
        int max_bin = hist_bkg_sum->GetNbinsX();
        vector<int> inf_bins;
        Double_t z_minValue = 9999;
        for (int i_bin = 1 ; i_bin <= max_bin ; i_bin++) {
            Double_t s = (forward) ? hist_sig_sum->Integral(i_bin, max_bin) : hist_sig_sum->Integral(0,i_bin);
            Double_t db; 
            Double_t b = (forward) ? hist_bkg_sum->IntegralAndError(i_bin, max_bin, db) : hist_bkg_sum->IntegralAndError(0, i_bin, db);

            Double_t z = NumberCountingUtils::BinomialExpZ(s, b, db/b);
            Double_t p = NumberCountingUtils::BinomialExpP(s, b, db/b);

            if (computation_type == 1) {
                db = 0.092895988;
                // db = 0.021;
                z = NumberCountingUtils::BinomialExpZ(s, b, db);
                p = NumberCountingUtils::BinomialExpP(s, b, db);
            }

            if (computation_type < 1) {
                z = s / sqrt(b + pow(computation_type*b,2));
            }

            if (isinf(z)) {
                z = 0;
                inf_bins.push_back(i_bin);
            }
            zhist->SetBinContent(i_bin, z);
            phist->SetBinContent(i_bin, p);

            if (z_minValue > z) z_minValue = z;
        }
        for (int i_bin : inf_bins) zhist->SetBinContent(i_bin,z_minValue);

        v_zhist.push_back(zhist);
        v_phist.push_back(phist);

    }

}

void doThePlots(map<TString,TH1D*> map_hist_sumw, TString output_plots_folder, TString bkg_group, TString sig_channel, TString plotName, TString N1_selection, TString n1_tag, double lumi, TString selection) {
    
    vector<TH1D*> zhist_sumw, phist_sumw;

    // ----- Paper error
    zhist_sumw.clear();
    phist_sumw.clear();
    GetSignificanceHists(map_hist_sumw, zhist_sumw, phist_sumw, 1);
    TString folderName = output_plots_folder + "/N1Plots/" + bkg_group + "_" + sig_channel + "_PaperError";
    makeDirectory(folderName);
    PlotUtils::makeSignificancePlot(map_hist_sumw, zhist_sumw, PlotList::getXTitle(plotName), "", "Z", folderName + "/" + plotName + "_" + N1_selection + "_" + n1_tag, lumi, {"N-1 plot for " + SelectionTool::getSelectionLabel(selection)}, {0.13}, {0.87});
    // PlotUtils::makeSignificancePlot(map_hist_sumw, phist_sumw, PlotList::getXTitle(plotName), "", "P", folderName + "/" + plotName + "_" + N1_selection + "_" + n1_tag + "_pvalue", lumi);

    // // ---- Integral error
    // zhist_sumw.clear();
    // phist_sumw.clear();
    // GetSignificanceHists(map_hist_sumw, zhist_sumw, phist_sumw, 0);
    // folderName = output_plots_folder + "/N1Plots/" + bkg_group + "_" + sig_channel + "_IntegralFractionError";
    // makeDirectory(folderName);
    // // PlotUtils::makeSignificancePlot(map_hist_sumw, zhist_sumw, PlotList::getXTitle(plotName), "", "Z", folderName + "/" + plotName + "_" + N1_selection + "_" + n1_tag, lumi);
    // // PlotUtils::makeSignificancePlot(map_hist_sumw, phist_sumw, PlotList::getXTitle(plotName), "", "P", folderName + "/" + plotName + "_" + N1_selection + "_" + n1_tag + "_pvalue", lumi);


    // // ----- Mario Check
    // zhist_sumw.clear();
    // phist_sumw.clear();
    // GetSignificanceHists(map_hist_sumw, zhist_sumw, phist_sumw, 0.021);
    // folderName = output_plots_folder + "/N1Plots/" + bkg_group + "_" + sig_channel + "_MarioCheck0021";
    // makeDirectory(folderName);
    // // PlotUtils::makeSignificancePlot(map_hist_sumw, zhist_sumw, PlotList::getXTitle(plotName), "", "S/#sqrt{B + (B #upoint #sigma_{B})^{2}}", folderName + "/" + plotName + "_" + N1_selection + "_" + n1_tag, lumi);

    // zhist_sumw.clear();
    // phist_sumw.clear();
    // GetSignificanceHists(map_hist_sumw, zhist_sumw, phist_sumw, 0.1);
    // folderName = output_plots_folder + "/N1Plots/" + bkg_group + "_" + sig_channel + "_MarioCheck01";
    // makeDirectory(folderName);
    // // PlotUtils::makeSignificancePlot(map_hist_sumw, zhist_sumw, PlotList::getXTitle(plotName), "", "S/#sqrt{B + (B #upoint #sigma_{B})^{2}}", folderName + "/" + plotName + "_" + N1_selection + "_" + n1_tag, lumi);

}