#ifndef PlotTools_h
#define PlotTools_h

using namespace std;

#include "MergedTree.C"
#include <TDSet.h>
#include <TCanvas.h>
#include <TFile.h>
#include <ROOT/RDataFrame.hxx>
#include <map>

class PlotTools {
public:
 
    TString toolName;
    TString outDir;
    Bool_t splitDSIDs = false;
    map<TString,TH1D*> histograms;
    map<TString,TString> xLabels;
    map<TString,TString> yLabels;
    map<TString,map<int,TH1D*>> perDSIDHists;
    map<TString,TH1D*> histograms_averaging;
    map<TString,pair<TString,TString>> variables_averaging;

    PlotTools(): toolName(""),outDir("") {};
    PlotTools(TString toolName, TString outDir) : toolName(toolName), outDir(outDir) {}
    virtual ~PlotTools() {}
    virtual void addHistogram(TString name, Int_t n_bins, Double_t x_low, Double_t x_high, TString xLabel, TString yLabel);
    virtual void addHistogram(TString name, vector<Double_t> binning);
    virtual void addHistogramsFromMap(map<TString,vector<Double_t>> map_binning);
    virtual void addAveragingHistogram(TString name, Int_t n_bins, Double_t x_low, Double_t x_high, TString xVariable, TString yVariable);
    virtual void addAveragingHistogramsFromMap(map<TString,vector<Double_t>> map_binning, map<TString,vector<TString>> map_variables);
    virtual void splitByDSIDs(vector<int> vector_dsids);
    virtual Float_t getBranchValue(TString name, MergedTree * t);
    virtual void Fill(TH1D * hist, Double_t value, Int_t dsid, Double_t weight, TString nameKey);
    virtual void Fill(MergedTree * t, TString selection, Double_t weight, bool debug);
    virtual void Fill(MergedTree * t, vector<int> cut_ids, bool useTCC, Double_t weight, bool debug);
    virtual void Fill(MergedTree *t, TH1D * hist, TString branch_name, TString selection, TString str_weight, TString nameKey);
    virtual void Fill(MergedTree *t, TString selection, TString str_weight);
    virtual void FillAtBin(MergedTree *t, TString selection, Double_t weight, Double_t xValue, TString histName, bool debug);
    virtual void FillAtBin(MergedTree *t, TString histName, Double_t xValue, Double_t increment);
    virtual void FillAtBin(MergedTree * t, TString selection, Double_t weight);
    virtual void Write();

};



#endif
