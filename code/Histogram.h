#ifndef Histogram_h
#define Histogram_h

#include <TH1D.h>
#include <TString.h>


class Histogram {

    public:

    TH1D * hist;
    TString xLabel;
    TString yLabel;
    // map<int,TH1D*> perDSIDHists;

    Histogram(): xLabel(""), yLabel("") {}
    Histogram(TString name, Int_t n_bins, Double_t x_low, Double_t x_high, TString xLabel, TString yLabel): xLabel(xLabel), yLabel(yLabel) {
        hist = new TH1D(name, name, n_bins, x_low, x_high);
    }

    virtual ~Histogram() {}

};

#endif