#ifndef PlotUtils_h
#define PlotUtils_h

#include "methods.h"
#include "SampleList.h"
#include "SelectionTool.h"
#include <TCanvas.h>
#include <TLatex.h>
#include <TPie.h>
#include <TPieSlice.h>

namespace PlotUtils {

    TCanvas * getDefaultCanvas(bool logy = true, double rightMargin = 0.2) {
        TCanvas * c = new TCanvas("","",700*1.618, 700);
        if (logy) c->SetLogy(); 
        c->SetLeftMargin(0.1); 
        c->SetRightMargin(rightMargin);
        c->SetBottomMargin(0.11);

        return c;
    }

    TCanvas * getDefaultDividedCanvas(bool logy = true) {
        TCanvas * c = new TCanvas("","", 700*1.618, 700);
        c->Divide(2);
        c->GetPad(1)->SetPad(0.,0.3,1.,1.);
        c->GetPad(2)->SetPad(0.,0.,1.,0.3);
        c->GetPad(1)->SetBottomMargin(0.01);
        c->GetPad(1)->SetLeftMargin(0.1);
        c->GetPad(2)->SetLeftMargin(0.1);
        c->GetPad(1)->SetRightMargin(0.2);
        c->GetPad(2)->SetRightMargin(0.2);
        c->GetPad(2)->SetBottomMargin(0.3);
        c->GetPad(2)->SetGrid();
        if (logy) c->GetPad(1)->SetLogy();
        return c;
    }

    TCanvas * getDefaultDoubleDividedCanvas(bool logy = true) {
        TCanvas * c = new TCanvas("","", 700*1.618, 700);
        c->Divide(3);
        c->GetPad(1)->SetPad(0.,0.4,1.,1.);
        c->GetPad(2)->SetPad(0.,0.2,1.,0.4);
        c->GetPad(3)->SetPad(0.,0.,1.,0.2);
        c->GetPad(1)->SetBottomMargin(0.01);
        c->GetPad(2)->SetBottomMargin(0.01);
        c->GetPad(1)->SetLeftMargin(0.1);
        c->GetPad(2)->SetLeftMargin(0.1);
        c->GetPad(3)->SetLeftMargin(0.1);
        c->GetPad(1)->SetRightMargin(0.2);
        c->GetPad(2)->SetRightMargin(0.2);
        c->GetPad(3)->SetRightMargin(0.2);
        c->GetPad(3)->SetBottomMargin(0.3);
        c->GetPad(2)->SetGrid();
        c->GetPad(3)->SetGrid();
        if (logy) c->GetPad(1)->SetLogy();
        return c;
    }

    TLegend * getDefaultLegend(vector<TString> legends) {
        TLegend * legend = new TLegend(0.8,0.4,0.98,0.6);
        if (legends.size() > 5) legend = new TLegend(0.8,0.3,0.98,0.7);
        if (legends.size() > 10) legend = new TLegend(0.8,0.2,0.98,0.8);
        legend->SetBorderSize(0.00);
        legend->SetFillStyle(0);
        legend->SetTextSize(0.04);
        for (TString legend_text : legends) {
            if (legend_text.Length() > 11) legend->SetTextSize(0.03);
        }

        return legend;
    }

    TLegend * getDefaultLegend(Int_t n_legends) {
        TLegend * legend =              new TLegend(0.8,0.4,0.98,0.6);
        if (n_legends > 5) legend =     new TLegend(0.8,0.3,0.98,0.7);
        if (n_legends > 10) legend =    new TLegend(0.8,0.2,0.98,0.8);
        if (n_legends > 20) legend =    new TLegend(0.8,0.15,0.98,0.85);
        legend->SetBorderSize(0.00);
        legend->SetFillStyle(0);
        legend->SetTextSize(0.04);

        return legend;
    }

    void drawLumiLabel(double lumi, double xstr = 0.13, double ystr = 0.8) {
        TLatex latex; latex.SetNDC(); latex.SetTextSize(0.040); latex.SetTextAlign(13); latex.SetTextColor(kBlack);
        TString lumiText = "#int L dt = " + to_string_with_precision(lumi,1) + "fb^{-1}";
        latex.DrawLatex(xstr, ystr, lumiText);
    }

    void plotHistogram(TH1D * hist, TString xLabel, TString yLabel, TString plotName, bool logy = true, double minimum = -1, double maximum = -1, vector<TString> strs = {}, vector<double> x_strs = {}, vector<double> y_strs = {}) {
        TCanvas * c = getDefaultCanvas(logy,0.1);

        hist->SetFillColor(kBlue);
        hist->Draw("hist");
        hist->SetTitle("");
        hist->GetXaxis()->SetTitle(xLabel);
        hist->GetXaxis()->SetTitleSize(0.04);
        hist->GetYaxis()->SetTitle(yLabel);
        hist->GetYaxis()->SetTitleSize(0.04);
        if (minimum != -1 && maximum != -1) {
            hist->SetMinimum(minimum);
            hist->SetMaximum(maximum);
        }

        TLatex latex; latex.SetNDC(); latex.SetTextSize(0.040); latex.SetTextAlign(13); latex.SetTextColor(kBlack);
        for (unsigned int i_str = 0 ; i_str < strs.size() ; i_str++) {
            latex.DrawLatex(x_strs.at(i_str), y_strs.at(i_str), strs.at(i_str));
        }

        c->Print(plotName + ".png");
        c->Print(plotName + ".eps");

        delete c;
    }

    void plot2DHistogram(TH2D * hist, TString xLabel, TString yLabel, TString plotName) {
        TCanvas * c = getDefaultCanvas(false);

        hist->Draw("colz");
        hist->SetTitle("");
        hist->GetXaxis()->SetTitle(xLabel);
        hist->GetXaxis()->SetTitleSize(0.04);
        hist->GetYaxis()->SetTitle(yLabel);
        hist->GetYaxis()->SetTitleSize(0.04);

        c->Print(plotName + ".png");
        c->Print(plotName + ".eps");

        delete c;
    }

    void plotHistograms(vector<TH1D*> hists, vector<int> colors, TString xLabel, TString yLabel, TString plotName, bool logy = true, double minimum = -1, double maximum = -1, vector<TString> strs = {}, vector<double> x_strs = {}, vector<double> y_strs = {}, vector<int> c_strs = {}) {

        TCanvas * c = getDefaultCanvas(logy,0.1);

        THStack * hs = new THStack("","");
        for (unsigned int i_hist = 0 ; i_hist < hists.size() ; i_hist++) {
            TH1D * hist = (TH1D*)hists.at(i_hist)->Clone();
            hist->SetFillColor(colors.at(i_hist));
            hist->SetLineColor(colors.at(i_hist));
            hist->SetTitle("");
            hs->Add(hist);
        }

        hs->Draw("hist nostack");
        hs->GetXaxis()->SetTitle(xLabel);
        hs->GetXaxis()->SetTitleSize(0.04);
        hs->GetYaxis()->SetTitle(yLabel);
        hs->GetYaxis()->SetTitleSize(0.04);
        if (minimum != -1 && maximum != -1) {
            hs->SetMinimum(minimum);
            hs->SetMaximum(maximum);
        }

        TLatex latex; latex.SetNDC(); latex.SetTextSize(0.040); latex.SetTextAlign(13);
        latex.SetTextColor(kBlack);
        for (unsigned int i_str = 0 ; i_str < strs.size() ; i_str++) {
            if (c_strs.size() == strs.size()) latex.SetTextColor(c_strs.at(i_str));
            latex.DrawLatex(x_strs.at(i_str), y_strs.at(i_str), strs.at(i_str));
        }


        c->Print(plotName + ".png");
        c->Print(plotName + ".eps");

        delete c;
    }

    void plotHistograms(TH1D * hist1, TH1D * hist2, TString legend1, TString legend2, 
                        TString xLabel, TString yLabel, TString plotName, 
                        Int_t color1 = kBlue, Int_t color2 = kRed, Int_t lineStyle1 = 1, Int_t lineStyle2 = 9, double lumi = -1) {

        TCanvas * c = getDefaultCanvas();
        TLegend * legend = getDefaultLegend({legend1,legend2});
        
        hist1->SetLineColor(color1);
        hist1->SetLineWidth(3);
        hist1->SetLineStyle(lineStyle1);
        hist2->SetLineColor(color2);
        hist2->SetLineWidth(3);
        hist2->SetLineStyle(lineStyle2);

        THStack * hs = new THStack("","");

        hs->Add(hist1);
        hs->Add(hist2);
        
        legend->AddEntry(hist1,legend1,"fe");
        legend->AddEntry(hist2,legend2,"fe");

        double minimum = hs->GetMinimum();
        double maximum = hs->GetMaximum();
        double margin = 25;

        hs->SetMaximum(maximum + (maximum - minimum) * margin);
        hs->Draw("hist nostack");

        hs->SetTitle("");
        hs->GetXaxis()->SetTitle(xLabel);
        hs->GetXaxis()->SetTitleSize(0.04);
        hs->GetYaxis()->SetTitle(yLabel);
        hs->GetYaxis()->SetTitleSize(0.04);

        legend->Draw();

        if (lumi != -1) drawLumiLabel(lumi);

        c->Print(plotName + ".png");
        c->Print(plotName + ".eps");

        delete c;
    }

    void plotHistogramsRatio(TH1D * hist1, TH1D * hist2, TString legend1, TString legend2, 
                        TString xLabel, TString yLabel, TString plotName, 
                        Int_t color1 = kBlue, Int_t color2 = kRed, Int_t lineStyle1 = 1, Int_t lineStyle2 = 9, double lumi = -1, double ratioLine = -999) {

        TCanvas * c = getDefaultDividedCanvas();
        TLegend * legend = getDefaultLegend({legend1,legend2});
        
        hist1->SetLineColor(color1);
        hist1->SetLineWidth(3);
        hist1->SetLineStyle(lineStyle1);
        hist2->SetLineColor(color2);
        hist2->SetLineWidth(3);
        hist2->SetLineStyle(lineStyle2);

        THStack * hs = new THStack("","");

        hs->Add(hist1);
        hs->Add(hist2);
        
        legend->AddEntry(hist1,legend1,"fe");
        legend->AddEntry(hist2,legend2,"fe");

        double minimum = hs->GetMinimum();
        double maximum = hs->GetMaximum();
        double margin = 25;

        c->cd(1);

        hs->SetMaximum(maximum + (maximum - minimum) * margin);
        hs->Draw("hist nostack");

        hs->SetTitle("");
        // hs->GetXaxis()->SetTitle(xLabel);
        hs->GetXaxis()->SetTitleSize(0.04);
        hs->GetYaxis()->SetTitle(yLabel);
        hs->GetYaxis()->SetTitleSize(0.04);

        legend->Draw();

        if (lumi != -1) drawLumiLabel(lumi);

        c->cd(2);
        TH1D * hist_ratio = (TH1D*)hist1->Clone();
        hist_ratio->Sumw2();
        hist_ratio->Divide(hist2);
        hist_ratio->Draw("p");
        hist_ratio->SetTitle("");
        hist_ratio->GetXaxis()->SetTitle(xLabel);
        hist_ratio->GetYaxis()->SetTitle(legend1 + "/" + legend2);
        hist_ratio->GetXaxis()->SetTitleSize(0.13);
        hist_ratio->GetXaxis()->SetLabelSize(0.15);
        hist_ratio->GetYaxis()->SetTitleSize(0.13);
        hist_ratio->GetYaxis()->SetTitleOffset(0.25);
        hist_ratio->GetYaxis()->SetLabelSize(0.1);

        if (ratioLine != -999) {
            TLine * line = new TLine(hist_ratio->GetXaxis()->GetXmin(), ratioLine, hist_ratio->GetXaxis()->GetXmax(), ratioLine);
            line->SetLineWidth(3);
            line->SetLineColor(kRed);
            line->Draw("same");
        }

        c->Print(plotName + ".png");
        c->Print(plotName + ".eps");

        delete c;
    }

    void stackPlots(map<TString,TH1D*> map_hists_perName, TString xLabel, TString yLabel, bool doStack, bool useLogy, TString plotName, double lumi = -1, vector<TString> strs = {}, vector<double> x_str = {}, vector<double> y_str = {}) {
        vector<long double> hists_yields, hists_yields_2; 
        vector<long double> hists_yields_signals, hists_yields_signals_2; 
        vector<TString> legends, legends_signals;
        vector<TH1D*> hist_list, hist_list_signals;

        THStack * hs = new THStack("","");
        THStack * hs_signals = new THStack("","");

        for (auto kv : map_hists_perName) {
            TString legend = SampleList::getLabelForChannel(kv.first);
            TH1D * hist = kv.second;

            if (hist->Integral() == 0) {
                continue;
            }

            if (hist->GetLineStyle() != 1) {  // is a signal
                if (contains(legend, legends_signals)) {
                    int index = indexOf(legend, legends_signals);
                    hist_list_signals.at(index)->Add(hist);
                    hists_yields_signals.at(index) += hist->Integral(); 
                    hists_yields_signals_2.at(index) += hist->Integral();
                } else {
                    legends_signals.push_back(legend);
                    hist_list_signals.push_back(hist);
                    hists_yields_signals.push_back(hist->Integral()); 
                    hists_yields_signals_2.push_back(hist->Integral());
                }
            } else {  // is a background
                if (contains(legend, legends)) {
                    int index = indexOf(legend, legends);
                    hist_list.at(index)->Add(hist);
                    hists_yields.at(index) += hist->Integral(); 
                    hists_yields_2.at(index) += hist->Integral();
                }
                else {
                    legends.push_back(legend);
                    hist_list.push_back(hist);
                    hists_yields.push_back(hist->Integral()); 
                    hists_yields_2.push_back(hist->Integral());
                }
            }
        }

        if (hist_list.size() == 0 && hist_list_signals.size() == 0) {
            coutError("No histogram list given to PlotUtils::stackPlots for " + plotName);
            return;
        }

        sortTwoLists(hists_yields, hist_list);
        sortTwoLists(hists_yields_2, legends);

        sortTwoLists(hists_yields_signals, hist_list_signals);
        sortTwoLists(hists_yields_signals_2, legends_signals);

        if (doStack) for (unsigned int i_hist = 0 ; i_hist < hist_list.size() ; i_hist++) hs->Add(hist_list.at(i_hist));
        else for (int i_hist = hist_list.size() - 1 ; i_hist >= 0 ; i_hist--) hs->Add(hist_list.at(i_hist));

        for (unsigned int i_hist = 0 ; i_hist < hist_list_signals.size() ; i_hist++) hs_signals->Add(hist_list_signals.at(i_hist));

        TCanvas * c = getDefaultCanvas(useLogy);

        vector<TString> allLegends = legends; allLegends.insert(allLegends.end(),legends_signals.begin(), legends_signals.end());

        TLegend * legend = getDefaultLegend(allLegends);
        vector<int> colors, colors_signal;
        if (legends.size() == 1) { // Just one bkg
            vector<int> allColors_decr = getColors(legends.size() + legends_signals.size() - 1, -1);
            vector<int> allColors_incr = getColors(legends.size() + legends_signals.size() - 1, 1);
            colors.insert(colors.end(),allColors_decr.begin(),allColors_decr.begin() + legends.size());
            colors_signal.insert(colors_signal.end(),allColors_incr.begin(),allColors_incr.begin() + legends_signals.size());
        } else {
            colors = getColors(legends.size() - 1);
            colors_signal = getColors(legends_signals.size() - 1, 1);
        }


        // Styles
        for (unsigned int i_hist = 0 ; i_hist < hist_list.size() ; i_hist++) {
            hist_list.at(i_hist)->SetLineColor(colors.at(i_hist));  
            hist_list.at(i_hist)->SetFillColor(colors.at(i_hist));
            if (!doStack) {
                hist_list.at(i_hist)->SetFillColorAlpha(colors.at(i_hist),0.4);

            }
        }
        for (unsigned int i_hist = 0 ; i_hist < hist_list_signals.size() ; i_hist++) {
            int color = SampleList::getColorForChannel(legends_signals.at(i_hist), colors_signal.at( colors_signal.size() - 1 - i_hist ));
            hist_list_signals.at(i_hist)->SetLineColor(color); 
            hist_list_signals.at(i_hist)->SetFillColorAlpha(kBlack,0);
            hist_list_signals.at(i_hist)->SetLineStyle(2);   
            hist_list_signals.at(i_hist)->SetLineWidth(3);
        }

        for (int i_hist = hist_list_signals.size()-1 ; i_hist >= 0 ; i_hist--) legend->AddEntry(hist_list_signals.at(i_hist),legends_signals.at(i_hist),"l");
        for (int i_hist = hist_list.size()-1 ; i_hist >= 0 ; i_hist--) legend->AddEntry(hist_list.at(i_hist),legends.at(i_hist),"fe");

        // double maximum = (hist_list.size() > 0) ? hs->GetMaximum() : hs_signals->GetMaximum();
        // double minimum = (hist_list_signals.size() > 0) ? hs_signals->GetMinimum() : hs->GetMinimum();
        // if (maximum  > 1) {
            // minimum = 1;
        // }
        // if (minimum <= 0) minimum = 1e-5;
        // double margin = 25;

        double minimum = (hist_list_signals.size() > 0) ? hs_signals->GetMinimum() : hs->GetMinimum();
        if (minimum <= 0) minimum = 1e-0;
        // if (hist_list_signals.size() > 0) {
        //     TH1 *last = (TH1*)hs_signals->GetStack()->Last();
        //     double minBinContent = -1;
        //     for (int i_bin = 1 ; i_bin <= last->GetNbinsX() ; i_bin++) {
        //         double currBinContent = last->GetBinContent(i_bin);
        //         if (plotName.Contains("met_tst_sig")) {cout << i_bin << " : " << minimum << endl;}
        //         if (minBinContent == -1 || (minBinContent > currBinContent && currBinContent > 0)) minBinContent = currBinContent;
        //     }
        //     minimum = minBinContent;
        // }
        double maximum = hs->GetMaximum();
        double margin = 25;
        // if (minimum > 0) hs->SetMinimum(minimum);
        // hs->SetMaximum(maximum + (maximum - minimum) * margin);


        if (hist_list.size() > 0) {
            if (minimum > 0) hs->SetMinimum(minimum);
            if (useLogy) hs->SetMaximum(maximum + (maximum - minimum) * margin);
            if (doStack) hs->Draw("hist");
            else         hs->Draw("hist nostack");
            hs->GetXaxis()->SetTitle(xLabel);
            hs->GetYaxis()->SetTitle(yLabel);
            hs->GetXaxis()->SetTitleSize(0.05);
            hs->GetXaxis()->SetLabelSize(0.04);
            hs->GetYaxis()->SetTitleSize(0.05);
            hs->GetYaxis()->SetLabelSize(0.04);
            if (hist_list_signals.size() > 0) hs_signals->Draw("hist nostack same");
        } else if (hist_list_signals.size() > 0) {
            if (minimum > 0) hs_signals->SetMinimum(minimum);
            if (useLogy) hs_signals->SetMaximum(maximum + (maximum - minimum) * margin);
            hs_signals->Draw("hist nostack");
            hs_signals->GetXaxis()->SetTitle(xLabel);
            hs_signals->GetYaxis()->SetTitle(yLabel);
            hs_signals->GetXaxis()->SetTitleSize(0.05);
            hs_signals->GetXaxis()->SetLabelSize(0.04);
            hs_signals->GetYaxis()->SetTitleSize(0.05);
            hs_signals->GetYaxis()->SetLabelSize(0.04);
        }

        legend->Draw();

        if (lumi != -1) drawLumiLabel(lumi);

        TLatex latex; latex.SetNDC(); latex.SetTextSize(0.040); latex.SetTextAlign(13); latex.SetTextColor(kBlack);
        for (unsigned int i_str = 0 ; i_str < strs.size() ; i_str++) {
            latex.DrawLatex(x_str.at(i_str), y_str.at(i_str), strs.at(i_str));
        }

        c->Print(plotName + ".png");
        c->Print(plotName + ".eps");

        delete c;
    }

    void stackPlotsWithData(map<TString,TH1D*> map_hists_perName, TString xLabel, TString yLabel, bool doStack, TString plotName, double lumi = -1, vector<TString> strs = {}, vector<double> x_str = {}, vector<double> y_str = {}, double ratio_min = -1, double ratio_max = -1) {
        vector<long double> hists_yields_bkg, hists_yields_bkg_2; 
        vector<long double> hists_yields_signals, hists_yields_signals_2; 
        vector<TString> legends_bkg, legends_signals, legends_data;
        vector<TH1D*> hist_list_bkg, hist_list_signals;
        TH1D * hist_data = NULL;
        TH1D * hist_bkg = NULL;

        THStack * hs_bkg = new THStack("","");
        THStack * hs_signals = new THStack("","");

        for (auto kv : map_hists_perName) {
            TString legend = SampleList::getLabelForChannel(kv.first);
            TH1D * hist = kv.second;

            if (hist->Integral() == 0) {
                continue;
            }

            // if (plotName.Contains("met_tst_et__from100_to1200")) hist->Rebin(5);

            if (hist->GetLineStyle() != 1) {  // is a signal
                legends_signals.push_back(legend);
                hist_list_signals.push_back(hist);
                hists_yields_signals.push_back(hist->Integral()); 
                hists_yields_signals_2.push_back(hist->Integral());
            } else if (hist->GetMarkerStyle() == 20) { // is data
                legends_data.push_back("Data");
                hist_data = hist;
            } else {  // is a background
                legends_bkg.push_back(legend);
                hist_list_bkg.push_back(hist);
                hists_yields_bkg.push_back(hist->Integral()); 
                hists_yields_bkg_2.push_back(hist->Integral());
                if (hist_bkg == NULL) hist_bkg = (TH1D*)hist->Clone();
                else {
                    hist_bkg->Add(hist);
                }
            }
        }

        if (hist_list_bkg.size() == 0 && hist_list_signals.size() == 0) {
            coutError("No histogram list given to PlotUtils::stackPlots for " + plotName);
            return;
        }
        if (hist_data == NULL) {
            coutError("No data histogram given to PlotUtils::stackPlots for " + plotName);
            coutError("Maybe you didn't add the selection to the friendly data list");
            return;
        }

        sortTwoLists(hists_yields_bkg, hist_list_bkg);
        sortTwoLists(hists_yields_bkg_2, legends_bkg);

        sortTwoLists(hists_yields_signals, hist_list_signals);
        sortTwoLists(hists_yields_signals_2, legends_signals);

        for (unsigned int i_hist = 0 ; i_hist < hist_list_bkg.size() ; i_hist++) hs_bkg->Add(hist_list_bkg.at(i_hist));
        for (unsigned int i_hist = 0 ; i_hist < hist_list_signals.size() ; i_hist++) hs_signals->Add(hist_list_signals.at(i_hist));

        TCanvas * c = getDefaultDividedCanvas();

        vector<TString> allLegends = legends_bkg; allLegends.insert(allLegends.end(),legends_signals.begin(), legends_signals.end()); allLegends.push_back("Data");

        TLegend * legend = getDefaultLegend(allLegends);
        vector<int> colors = getColors(legends_bkg.size() - 1);
        vector<int> colors_signal = getColors(legends_signals.size() - 1);

        // Styles
        for (unsigned int i_hist = 0 ; i_hist < hist_list_bkg.size() ; i_hist++) {
            hist_list_bkg.at(i_hist)->SetLineColor(colors.at(i_hist));  
            hist_list_bkg.at(i_hist)->SetFillColor(colors.at(i_hist));
        }
        for (unsigned int i_hist = 0 ; i_hist < hist_list_signals.size() ; i_hist++) {
            hist_list_signals.at(i_hist)->SetLineColor(colors_signal.at( colors_signal.size() - 1 - i_hist )); 
            hist_list_signals.at(i_hist)->SetFillColorAlpha(kBlack,0);
            hist_list_signals.at(i_hist)->SetLineStyle(2);   
            hist_list_signals.at(i_hist)->SetLineWidth(2);
        }

        for (int i_hist = hist_list_signals.size()-1 ; i_hist >= 0 ; i_hist--) legend->AddEntry(hist_list_signals.at(i_hist),legends_signals.at(i_hist),"l");
        for (int i_hist = hist_list_bkg.size()-1 ; i_hist >= 0 ; i_hist--) legend->AddEntry(hist_list_bkg.at(i_hist),legends_bkg.at(i_hist),"fe");
        legend->AddEntry(hist_data, "Data", "p");

        c->cd(1);
        // double minimum = (hist_list_signals.size() > 0) ? hs_signals->GetMinimum() : hs_bkg->GetMinimum();
        // if (minimum <= 0) minimum = 1e-5;
        // double maximum = hs_bkg->GetMaximum();
        // double margin = 25;
        // hs_bkg->SetMinimum(minimum);
        // hs_bkg->SetMaximum(maximum + (maximum - minimum) * margin);

        double minimum = (hist_list_signals.size() > 0) ? hs_signals->GetMinimum() : hs_bkg->GetMinimum();
        double maximum = hs_bkg->GetMaximum();
        double margin = 25;
        if (minimum > 0) hs_bkg->SetMinimum(minimum);
        hs_bkg->SetMaximum(maximum + (maximum - minimum) * margin);

        if (doStack) hs_bkg->Draw("hist");
        else         hs_bkg->Draw("hist nostack");
        hs_bkg->GetYaxis()->SetTitle(yLabel);
        hs_bkg->GetXaxis()->SetTitleSize(0.05);
        hs_bkg->GetXaxis()->SetLabelSize(0.04);
        hs_bkg->GetYaxis()->SetTitleSize(0.05);
        hs_bkg->GetYaxis()->SetLabelSize(0.04);
        if (hist_list_signals.size() > 0) hs_signals->Draw("hist nostack same");
        hist_data->Draw("p same");

        legend->Draw();

        if (lumi != -1) drawLumiLabel(lumi);

        TLatex latex; latex.SetNDC(); latex.SetTextSize(0.040); latex.SetTextAlign(13); latex.SetTextColor(kBlack);
        for (unsigned int i_str = 0 ; i_str < strs.size() ; i_str++) {
            latex.DrawLatex(x_str.at(i_str), y_str.at(i_str), strs.at(i_str));
        }

        c->cd(2);
        TH1D * hist_ratio = (TH1D*)hist_data->Clone();
        hist_ratio->Sumw2();
        hist_ratio->Divide(hist_bkg);
        if (ratio_min != ratio_max) {
            hist_ratio->SetMinimum(ratio_min);
            hist_ratio->SetMaximum(ratio_max);
        }
        hist_ratio->Draw("p");
        hist_ratio->SetTitle("");
        hist_ratio->GetXaxis()->SetTitle(xLabel);
        hist_ratio->GetYaxis()->SetTitle("Data/MC");
        hist_ratio->GetXaxis()->SetTitleSize(0.13);
        hist_ratio->GetXaxis()->SetLabelSize(0.15);
        hist_ratio->GetYaxis()->SetTitleSize(0.13);
        hist_ratio->GetYaxis()->SetTitleOffset(0.25);
        hist_ratio->GetYaxis()->SetLabelSize(0.1);

        TLine * hLine = new TLine(hist_ratio->GetXaxis()->GetXmin(),1,hist_ratio->GetXaxis()->GetXmax(),1);
        hLine->SetLineStyle(2);
        hLine->SetLineWidth(2);
        hLine->Draw("same");

        c->Print(plotName + ".png");
        c->Print(plotName + ".eps");

        delete c;
    }

    void makePiePlot(vector<Float_t> vals, vector<Int_t> colors, vector<TString> labels, TString plotName) {
        
        Float_t sum = std::accumulate(vals.begin(),vals.end(),0.0);
        Int_t nvals = vals.size();
        TCanvas *cpie = new TCanvas("cpie","TPie test",700,700);

        cpie->cd(1);
        TPie *pie = new TPie("pie","",nvals,&vals.at(0),&colors.at(0));
        for (int i_slice = 0 ; i_slice < nvals ; i_slice++) {
            Float_t perc = 100. * pie->GetSlice(i_slice)->GetValue() / sum;
            pie->GetSlice(i_slice)->SetTitle("#splitline{" + labels.at(i_slice) + "}{(" + to_string_with_precision(perc,1) + "%)}");
            if (perc < 1) {
                pie->GetSlice(i_slice)->SetValue(0);
                pie->GetSlice(i_slice)->SetTitle("");
            }
        }
        pie->SetTextSize(0.03);
        pie->SetTextAlign(12);
        pie->SetRadius(.35);
        pie->SetLabelsOffset(.02);
        pie->Draw("<");

        cpie->Print(plotName + ".png");
        cpie->Print(plotName + ".eps");

        delete cpie;

    }

    void makeSignificancePlot(map<TString,TH1D*> map_hists, vector<TH1D*> v_zhist, TString xLabel, TString yLabel, TString zyLabel, TString plotName, double lumi = -1, vector<TString> strs = {}, vector<double> x_str = {}, vector<double> y_str = {}) {

        if (v_zhist.size() == 0) {coutError("PlotUtils::makeSignificancePlot -- No zhist provided"); return;}

        TH1D * hist_bkg_sum = nullptr;
        TH1D * hist_sig_all = nullptr;
        vector<TH1D*> hist_list_bkg, hist_list_signals;
        vector<TString> legends_bkg ,legends_signals;
        vector<long double> hists_yields_bkg, hists_yields_bkg_2;
        vector<long double> hists_yields_sig, hists_yields_sig_2;
        TH1D * zhist_sum = (TH1D*)v_zhist.at(0)->Clone(); zhist_sum->Reset();
        // Sorting
        for (auto kv : map_hists) {
            TString channel = kv.first;
            TH1D * hist = kv.second;

            if (hist->GetLineStyle() != 1) { // signal hist
                hist_list_signals.push_back(hist);
                legends_signals.push_back(SampleList::getLabelForChannel(channel));
                hists_yields_sig.push_back(hist->Integral());
                hists_yields_sig_2.push_back(hist->Integral());
                if (hist_sig_all) hist_sig_all->Add(hist);
                else hist_sig_all = (TH1D*)hist->Clone();
            } else { // bkg hist
                hist_list_bkg.push_back(hist);
                legends_bkg.push_back(SampleList::getLabelForChannel(channel));
                hists_yields_bkg.push_back(hist->Integral());
                hists_yields_bkg_2.push_back(hist->Integral());
                if (hist_bkg_sum) hist_bkg_sum->Add(hist);
                else hist_bkg_sum = (TH1D*)hist->Clone();
            }
        }

        sortTwoLists(hists_yields_bkg, hist_list_bkg);      sortTwoLists(hists_yields_bkg_2, legends_bkg);
        sortTwoLists(hists_yields_sig, hist_list_signals);  sortTwoLists(hists_yields_sig_2, legends_signals);

        // Styles
        TCanvas * c = (v_zhist.size() == 1) ? getDefaultDividedCanvas() : getDefaultDoubleDividedCanvas();
        TLegend * legend = getDefaultLegend(legends_bkg.size() + legends_signals.size());
        vector<int> colors_bkg = getColors(legends_bkg.size() - 1);
        vector<int> colors_signal = getColors(legends_signals.size() - 1);
        THStack * hs_bkg = new THStack("","");
        THStack * hs_sig = new THStack("","");
        THStack * hs_zhist = new THStack("","");

        for (unsigned int i_hist = 0 ; i_hist < hist_list_bkg.size() ; i_hist++) {
            hs_bkg->Add(hist_list_bkg.at(i_hist));
            hist_list_bkg.at(i_hist)->SetLineColor(colors_bkg.at(i_hist));  
            hist_list_bkg.at(i_hist)->SetFillColor(colors_bkg.at(i_hist));
        }
        for (unsigned int i_hist = 0 ; i_hist < hist_list_signals.size() ; i_hist++) {
            int color = SampleList::getColorForChannel(legends_signals.at(i_hist), colors_signal.at( colors_signal.size() - 1 - i_hist ));
            hs_sig->Add(hist_list_signals.at(i_hist));
            hist_list_signals.at(i_hist)->SetLineColor(color); 
            hist_list_signals.at(i_hist)->SetFillColorAlpha(kBlack,0);
            hist_list_signals.at(i_hist)->SetLineStyle(2);   
            hist_list_signals.at(i_hist)->SetLineWidth(2);
            
            hs_zhist->Add(v_zhist.at(i_hist));
            v_zhist.at(i_hist)->SetLineWidth(2);
            v_zhist.at(i_hist)->SetFillStyle(0);
            v_zhist.at(i_hist)->SetLineColor(color);
            zhist_sum->Add(v_zhist.at(i_hist));
        }

        for (int i_hist = hist_list_signals.size()-1 ; i_hist >= 0 ; i_hist--) legend->AddEntry(hist_list_signals.at(i_hist),legends_signals.at(i_hist),"l");
        for (int i_hist = hist_list_bkg.size()-1 ; i_hist >= 0 ; i_hist--) legend->AddEntry(hist_list_bkg.at(i_hist),legends_bkg.at(i_hist),"fe");


        // Drawing
        c->cd(1);
        // double maximum = (hist_list_bkg.size() > 0) ? hs_bkg->GetMaximum() : hs_sig->GetMaximum();
        // double minimum = (hist_list_signals.size() > 0) ? hs_sig->GetMinimum() : hs_bkg->GetMinimum();
        // if (maximum  > 1) {
        //     minimum = 1;
        // }
        // if (minimum <= 0) minimum = 1e-5;
        // double margin = 25;

        double minimum = (hist_list_signals.size() > 0) ? hs_sig->GetMinimum() : hs_bkg->GetMinimum();
        if (minimum <= 0) minimum = 1e-0;
        double maximum = hs_bkg->GetMaximum();
        if (minimum <= 0) minimum = 1e-3;
        double margin = 25;

        if (minimum > 0) hs_bkg->SetMinimum(minimum);
        // hs_bkg->SetMinimum(minimum);
        hs_bkg->SetMaximum(maximum + (maximum - minimum) * margin);
        hs_bkg->Draw("hist");
        hs_bkg->GetYaxis()->SetTitle(yLabel);
        hs_bkg->GetXaxis()->SetTitleSize(0.05);
        hs_bkg->GetXaxis()->SetLabelSize(0.04);
        hs_bkg->GetYaxis()->SetTitleSize(0.05);
        hs_bkg->GetYaxis()->SetLabelSize(0.04);
        if (hist_list_signals.size() > 0) hs_sig->Draw("hist nostack same");

        legend->Draw();
        if (lumi != -1) drawLumiLabel(lumi);


        TLatex latex; latex.SetNDC(); latex.SetTextSize(0.040); latex.SetTextAlign(13); latex.SetTextColor(kBlack);
        for (unsigned int i_str = 0 ; i_str < strs.size() ; i_str++) {
            latex.DrawLatex(x_str.at(i_str), y_str.at(i_str), strs.at(i_str));
        }
        bool forward = GetMedian(hist_sig_all) > GetMedian(hist_bkg_sum);
        if (forward) {
            latex.DrawLatex(0.13,0.84,"Forward");
        } else {
            latex.DrawLatex(0.13,0.84,"Backward");
        }

        c->cd(2);
        hs_zhist->SetMinimum(0);
        hs_zhist->Draw("hist nostack");
        hs_zhist->SetTitle("");
        if (v_zhist.size() == 1) {
            hs_zhist->GetXaxis()->SetTitle(xLabel);
        }
        hs_zhist->GetYaxis()->SetTitle(zyLabel);
        hs_zhist->GetXaxis()->SetTitleSize(0.13);
        hs_zhist->GetXaxis()->SetLabelSize(0.15);
        hs_zhist->GetYaxis()->SetTitleSize(0.13);
        hs_zhist->GetYaxis()->SetTitleOffset(0.25);
        hs_zhist->GetYaxis()->SetLabelSize(0.1);

        if (v_zhist.size() > 1) {
            c->cd(3);
            zhist_sum->SetLineColor(kBlack);
            zhist_sum->SetLineWidth(2);
            zhist_sum->SetMinimum(0);
            zhist_sum->Draw("hist");
            zhist_sum->SetTitle("");
            zhist_sum->GetXaxis()->SetTitle(xLabel);
            zhist_sum->GetYaxis()->SetTitle(zyLabel);
            zhist_sum->GetXaxis()->SetTitleSize(0.13);
            zhist_sum->GetXaxis()->SetLabelSize(0.15);
            zhist_sum->GetYaxis()->SetTitleSize(0.13);
            zhist_sum->GetYaxis()->SetTitleOffset(0.25);
            zhist_sum->GetYaxis()->SetLabelSize(0.1);
        }

        c->Print(plotName + ".png");
        c->Print(plotName + ".eps");

        delete c;

    }

    void makePValuePlot(TH1D * hist_sig, TH1D * hist_bkg, TString legend1, TString legend2, 
                        TString xLabel, TString yLabel, TString plotName, 
                        Int_t color1 = kBlue, Int_t color2 = kRed, Int_t lineStyle1 = 1, Int_t lineStyle2 = 9) {

        Int_t nbins_sig = hist_sig->GetNbinsX();
        Int_t nbins_bkg = hist_bkg->GetNbinsX();

        if (nbins_sig != nbins_bkg) {coutError("PlotUtils::makePValuePlot -> The histograms have different binning!"); return ;}

        TCanvas * c = getDefaultCanvas();
        TLegend * legend = getDefaultLegend({legend1,legend2});

        Double_t mean_sig = hist_sig->GetMean();
        Double_t mean_bkg = hist_bkg->GetMean();

        Double_t integral_sig = hist_sig->Integral();
        Double_t integral_bkg = hist_bkg->Integral();

        vector<Double_t> v_one_minus_alpha, v_sqrtBeta, v_StoB, v_xValues;

        for (int i_bin = 1 ; i_bin <= nbins_sig ; i_bin++) {
            v_xValues.push_back(hist_sig->GetBinCenter(i_bin));
            Double_t one_minus_alpha = (mean_sig > mean_bkg) ? hist_sig->Integral(i_bin, nbins_sig)/integral_sig : hist_sig->Integral(1,i_bin) / integral_sig;
            Double_t beta = (mean_sig > mean_bkg) ? hist_bkg->Integral(i_bin, nbins_bkg)/integral_bkg : hist_bkg->Integral(1,i_bin) / integral_bkg;
            Double_t StoB = (beta > 0) ? one_minus_alpha / sqrt(beta) : 0;
            if (beta < 0) beta = 0;

            v_one_minus_alpha.push_back(one_minus_alpha);
            v_sqrtBeta.push_back(sqrt(beta));
            v_StoB.push_back(StoB);
        }

        TGraph * g_one_minus_alpha = new TGraph(v_xValues.size(), &v_xValues.at(0), &v_one_minus_alpha.at(0));
        TGraph * g_sqrtBeta = new TGraph(v_xValues.size(), &v_xValues.at(0), &v_sqrtBeta.at(0));
        TGraph * g_StoB = new TGraph(v_xValues.size(), &v_xValues.at(0), &v_StoB.at(0));

        hist_sig->Scale(1./integral_sig);
        hist_sig->SetLineColor(color1);
        hist_sig->SetLineWidth(3);
        hist_sig->SetLineStyle(lineStyle1);
        hist_bkg->Scale(1./integral_bkg);
        hist_bkg->SetLineColor(color2);
        hist_bkg->SetLineWidth(3);
        hist_bkg->SetLineStyle(lineStyle2);

        THStack * hs = new THStack("","");
        hs->Add(hist_sig);
        hs->Add(hist_bkg);

        legend->AddEntry(hist_sig,legend1,"fe");
        legend->AddEntry(hist_bkg,legend2,"fe");

        double minimum = hs->GetMinimum();
        double maximum = hs->GetMaximum();
        double margin = 25;
        hs->SetMaximum(maximum + (maximum - minimum) * margin);
        hs->Draw("hist");
        hs->GetXaxis()->SetTitle(xLabel);
        hs->GetYaxis()->SetTitle(yLabel);
        hs->GetXaxis()->SetTitleSize(0.05);
        hs->GetXaxis()->SetLabelSize(0.04);
        hs->GetYaxis()->SetTitleSize(0.05);
        hs->GetYaxis()->SetLabelSize(0.04);

        g_StoB->SetLineColor(kMagenta); g_StoB->SetLineWidth(3);
        g_StoB->Draw("same");
        // legend->AddEntry(g_StoB,"#frac{1-#alpha}{#sqrt{#beta}}","ap");

        Double_t T_cut = v_xValues.at(max_element(v_StoB.begin(), v_StoB.end())-v_StoB.begin());
        TLine * cutLine = new TLine(T_cut, minimum, T_cut, maximum + (maximum - minimum) * margin);
        cutLine->SetLineColor(kBlack); cutLine->SetLineWidth(3); cutLine->SetLineStyle(9);
        cutLine->Draw("same");
        TString cutStr = "T_{cut} = " + to_string_with_precision(T_cut,2);
        legend->AddEntry(cutLine, cutStr,"l");

        int direction = (mean_sig > mean_bkg) ? 1 : -1;
        Double_t xRange = hist_sig->GetBinCenter(nbins_sig) - hist_sig->GetBinCenter(1);
        Double_t yArrow = minimum + (maximum + (maximum - minimum) * margin) / 10.;
        TArrow * arrow = new TArrow(T_cut, yArrow, T_cut + direction * xRange/10., yArrow, 0.07,"|>");
        arrow->SetAngle(40);
        arrow->SetLineWidth(2);
        arrow->Draw("");

        legend->Draw();

        c->Print(plotName + ".png");
        c->Print(plotName + ".eps");

        delete c;

    }

    void makeLabelledPlot(map<TString,TH1D*> map_hists_perName, vector<TString> labels, TString yLabel, TString plotName, double lumi = -1, vector<TString> strs = {}, vector<double> x_str = {}, vector<double> y_str = {}) {

        // SetAtlasStyle();

        vector<TString> legends;
        vector<TString> legends_signals;
        vector<TH1D*> hist_list, hist_list_signals;
        vector<long double> hists_yields, hists_yields_2;
        TH1D * hist_data = NULL;
        TH1D * hist_bkg = NULL;

        THStack * hs = new THStack("","");
        THStack * hs_signals = new THStack("","");
        for (auto kv : map_hists_perName) {
            TString legend = SampleList::getLabelForChannel(kv.first);
            TH1D * hist = kv.second;

            if (hist->GetLineStyle() != 1) {  // is a signal
                // hs_signals->Add(hist);
                legends_signals.push_back(legend);
                hist_list_signals.push_back(hist);
            } else if (hist->GetMarkerStyle() == 20) { // is data
                hist_data = hist;
            } else {  // is a background
                // hs->Add(hist);
                legends.push_back(legend);
                hist_list.push_back(hist);
                hists_yields.push_back(hist->GetBinContent(hist->GetNbinsX()));
                hists_yields_2.push_back(hist->GetBinContent(hist->GetNbinsX()));
                if (hist_bkg == NULL) hist_bkg = (TH1D*)hist->Clone();
                else {
                    hist_bkg->Add(hist);
                }
            }
        }

        TCanvas * c = (hist_data == NULL) ? getDefaultCanvas() : getDefaultDividedCanvas();

        sortTwoLists(hists_yields, hist_list);
        sortTwoLists(hists_yields_2, legends);

        TLegend * legend = getDefaultLegend(legends.size() + legends_signals.size());
        for (unsigned int i_hist = 0 ; i_hist < hist_list_signals.size() ; i_hist++) hs_signals->Add(hist_list_signals.at(i_hist));
        for (unsigned int i_hist = 0 ; i_hist < hist_list.size() ; i_hist++) hs->Add(hist_list.at(i_hist));
        for (int i_hist = hist_list_signals.size()-1 ; i_hist >= 0 ; i_hist--) legend->AddEntry(hist_list_signals.at(i_hist),legends_signals.at(i_hist),"l");
        for (int i_hist = hist_list.size()-1 ; i_hist >= 0 ; i_hist--) legend->AddEntry(hist_list.at(i_hist),legends.at(i_hist),"fe");
        if (hist_data != NULL) legend->AddEntry(hist_data, "Data", "p");

        c->cd(1);
        double minimum = hs->GetMinimum();
        double maximum = hs->GetMaximum();
        double margin = 25;
        hs->SetMaximum(maximum + (maximum - minimum) * margin);
        hs->Draw("hist");
        if (hist_list_signals.size() > 0) hs_signals->Draw("hist nostack same");
        if (hist_data == NULL) {
            for (unsigned int i = 0 ; i < labels.size() ; i++) {
                hs->GetXaxis()->SetBinLabel(hs->GetXaxis()->FindBin(i), SelectionTool::getSelectionLabel(labels.at(i)));
            }
        }
        hs->GetYaxis()->SetTitle(yLabel);
        hs->GetXaxis()->SetTitleSize(0.05);
        hs->GetXaxis()->SetLabelSize(0.05);
        hs->GetYaxis()->SetTitleSize(0.05);
        hs->GetYaxis()->SetLabelSize(0.04);

        if (hist_data != NULL) hist_data->Draw("p same");

        // ATLASLabel(0.15,0.85,"Internal");
        if (lumi != -1) drawLumiLabel(lumi);

        legend->Draw();

        TLatex latex; latex.SetNDC(); latex.SetTextSize(0.040); latex.SetTextAlign(13); latex.SetTextColor(kBlack);
        for (unsigned int i_str = 0 ; i_str < strs.size() ; i_str++) {
            latex.DrawLatex(x_str.at(i_str), y_str.at(i_str), strs.at(i_str));
        }

        if (hist_data != NULL) {
            c->cd(2);
            TH1D * hist_ratio = (TH1D*)hist_data->Clone();
            hist_ratio->Sumw2();
            hist_ratio->Divide(hist_bkg);
            hist_ratio->SetMinimum(0);
            hist_ratio->SetMaximum(2);
            hist_ratio->Draw("p");
            hist_ratio->SetTitle("");
            for (unsigned int i = 0 ; i < labels.size() ; i++) {
                hist_ratio->GetXaxis()->SetBinLabel(hist_ratio->GetXaxis()->FindBin(i), SelectionTool::getSelectionLabel(labels.at(i)));
            }
            hist_ratio->GetYaxis()->SetTitle("Data/MC");
            hist_ratio->GetXaxis()->SetLabelOffset(0.015);
            hist_ratio->GetXaxis()->SetTitleSize(0.13);
            hist_ratio->GetXaxis()->SetLabelSize(0.15);
            hist_ratio->GetYaxis()->SetTitleSize(0.13);
            hist_ratio->GetYaxis()->SetTitleOffset(0.25);
            hist_ratio->GetYaxis()->SetLabelSize(0.1);

            TLine * hLine = new TLine(hist_ratio->GetXaxis()->GetXmin(),1,hist_ratio->GetXaxis()->GetXmax(),1);
            hLine->SetLineStyle(2);
            hLine->SetLineWidth(2);
            hLine->Draw("same");
        }

        c->Print(plotName + ".png");
        c->Print(plotName + ".eps");

        delete c;


    }

    void makeTurnOnPlot(map<TString,TH1D*> map_hists_perName_num, map<TString,TH1D*> map_hists_perName_den, TString xLabel, TString yLabel, TString plotName, double lumi = -1, vector<TString> strs = {}, vector<double> x_str = {}, vector<double> y_str = {}) {
        vector<TString> legends;
        TH1D * hist_bkg_num = nullptr;
        TH1D * hist_bkg_den = nullptr;
        TH1D * hist_data_num = nullptr;
        TH1D * hist_data_den = nullptr;

        for (auto kv : map_hists_perName_num) {
            TString legend = SampleList::getLabelForChannel(kv.first);
            TH1D * hist_num = map_hists_perName_num[kv.first];
            TH1D * hist_den = map_hists_perName_den[kv.first];

            if (hist_num->Integral() == 0) {
                continue;
            }
            if (hist_num->GetMarkerStyle() != 20) {
                if (hist_bkg_num == nullptr) {
                    legends.push_back("Bkg");
                    hist_bkg_num = (TH1D*)hist_num->Clone();
                    hist_bkg_den = (TH1D*)hist_den->Clone();
                } else {
                    hist_bkg_num->Add(hist_num);
                    hist_bkg_den->Add(hist_den);
                }
            } else {
                if (hist_data_num == nullptr) {
                    legends.push_back("Data");
                    hist_data_num = (TH1D*)hist_num->Clone();
                    hist_data_den = (TH1D*)hist_den->Clone();
                } else {
                    hist_data_num->Add(hist_num);
                    hist_data_den->Add(hist_den);
                }
            }
        }

        if (hist_bkg_num->GetEntries() <= 0) {coutError("Empty num histogram for " + plotName); return;}

        hist_bkg_num->Sumw2();
        hist_bkg_num->Divide(hist_bkg_den);
        if (hist_data_num != nullptr) {
            hist_data_num->Sumw2();
            hist_data_num->Divide(hist_data_den);
        }

        TCanvas * c = (hist_data_num == nullptr) ? getDefaultCanvas(false) : getDefaultDividedCanvas(false);
        TLegend * legend = getDefaultLegend(legends);

        // Styles
        hist_bkg_num->SetLineStyle(kBlue);
        legend->AddEntry(hist_bkg_num, "Bkg", "l");
        if (hist_data_num != nullptr) {
            legend->AddEntry(hist_data_num, "Data", "p");
            
        }

        c->cd(1);
        hist_bkg_num->SetMinimum(0);
        hist_bkg_num->SetMaximum(1);
        hist_bkg_num->Draw("hist");
        hist_bkg_num->SetTitle("");
        if (hist_data_num == nullptr) hist_bkg_num->GetXaxis()->SetTitle(xLabel);
        hist_bkg_num->GetYaxis()->SetTitle(yLabel);
        hist_bkg_num->GetXaxis()->SetTitleSize(0.05);
        hist_bkg_num->GetXaxis()->SetLabelSize(0.04);
        hist_bkg_num->GetYaxis()->SetTitleSize(0.05);
        hist_bkg_num->GetYaxis()->SetLabelSize(0.04);

        TLine * vLine = new TLine(150,0,150,1);
        vLine->SetLineStyle(2);
        vLine->SetLineWidth(2);
        vLine->Draw("same");

        if (lumi != -1) drawLumiLabel(lumi);

        TLatex latex; latex.SetNDC(); latex.SetTextSize(0.040); latex.SetTextAlign(13); latex.SetTextColor(kBlack);
        for (unsigned int i_str = 0 ; i_str < strs.size() ; i_str++) {
            latex.DrawLatex(x_str.at(i_str), y_str.at(i_str), strs.at(i_str));
        }
        legend->Draw();

        if (hist_data_num != nullptr) {
            hist_data_num->Draw("ep same");

            c->cd(2);
            TH1D * hist_ratio = (TH1D*)hist_bkg_num->Clone();
            hist_ratio->Sumw2();
            hist_ratio->Divide(hist_data_num);
            hist_ratio->SetMinimum(0);
            hist_ratio->SetMaximum(2);
            hist_ratio->Draw("p");
            hist_ratio->SetTitle("");
            hist_ratio->GetXaxis()->SetTitle(xLabel);
            hist_ratio->GetYaxis()->SetTitle("Data/MC");
            hist_ratio->GetXaxis()->SetTitleSize(0.13);
            hist_ratio->GetXaxis()->SetLabelSize(0.15);
            hist_ratio->GetYaxis()->SetTitleSize(0.13);
            hist_ratio->GetYaxis()->SetTitleOffset(0.25);
            hist_ratio->GetYaxis()->SetLabelSize(0.1);

            TLine * hLine = new TLine(hist_ratio->GetXaxis()->GetXmin(),1,hist_ratio->GetXaxis()->GetXmax(),1);
            hLine->SetLineStyle(2);
            hLine->SetLineWidth(2);
            hLine->Draw("same");

        }



        c->Print(plotName + ".png");
        c->Print(plotName + ".eps");

        delete c;
    }

}

#endif


