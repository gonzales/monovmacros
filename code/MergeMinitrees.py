#!/usr/bin/env python

import sys, os
import ROOT

from optparse import OptionParser

parser = OptionParser()
parser.add_option('-c', '--config-file', type='string', help='Path to the config file', dest = "config_file",default="conf/configuration.conf")
parser.add_option('--do-MC', dest = "doMC", action="store_true", help='Process MC samples', default=False)
parser.add_option('--do-ALPs', dest = "doALPs", action="store_true", help='Process ALPs samples', default=False)
parser.add_option('--do-invH', dest = "doinvH", action="store_true", help='Process invH samples', default=False)
parser.add_option('--do-DM', dest = "doDM", action="store_true", help='Process DM samples', default=False)
parser.add_option('--do-signals', dest = "doSignals", action="store_true", help='Process signal samples', default=False)
parser.add_option('--do-data', dest = "doData", action="store_true", help='Process data', default=False)
parser.add_option('--do', dest = "doThisOne", help='Merge a particular channel', default="")
parser.add_option('-p','--do-plots', dest = "doPlots", action="store_true", help='Make the plots defined in PlotList.h', default=False)
parser.add_option('-f','--do-cutflow', dest = "doCutflow", action="store_true", help='Do cutflow plots', default=False)
parser.add_option('-t','--do-tables', dest = "doTables", action="store_true", help='Do tables with xSec, etc', default=False)
# parser.add_option('-s','--split-by-dsid', dest = "splitDSIDs", action="store_true", help='Produce plots splitting by DSIDs', default=False)
# parser.add_option('--in-condor', dest="inCondor", action="store_true", help='Flag when running in condor', default=False)
# parser.add_option('-t','--n-threads', dest="nThreads", help='Number of threads for processing the minitrees', default=1, type="int")
parser.add_option('-i','--ignore-merging', dest = "ignoreMerging", action="store_true", help='Don\'t merge. In case we just want to make plots, etc.', default=False)
# parser.add_option('--start-from', dest="startFrom", help='Sub part to start from', default=0, type="int")
# parser.add_option('-o','--check-overlap', dest="checkOverlap", action="store_true",help='Make overlap tables for selections defined in the config file', default=False)
parser.add_option('--do-n1plots', dest="doN1Plots", action="store_true",help='Make N1 plots for later processing', default=False)
parser.add_option('--cpus', dest="cpus", type=int,help='Number of CPUs to use in Proof', default=4)
parser.add_option('-l','--label', dest="label", type='string',help='Label to add to the merged file', default='')
parser.add_option('--clean', dest="clean", action="store_true",help='Clean libraries', default=False)
parser.add_option('--check-files', dest="checkingFiles", action="store_true",help='Just check that the minitrees exist for all DSIDs', default=False)
parser.add_option('--disable-proof', dest="disableProof", action="store_true",help='Disable using PROOF', default=False)
parser.add_option('--skim-level', dest="skimLevel", type=int,help='Level of skimming of the mergedTree. The higher the value, less branches in the output tree', default=0)

(options, args) = parser.parse_args()

if options.clean:
    os.system("bash clean.sh")

ROOT.gROOT.SetBatch(True)
ROOT.gROOT.ProcessLine(".L MergeMinitrees.cxx")

if options.doSignals:
    options.doALPs = True 
    options.doinvH = True 
    options.doDM = True 


ROOT.MergeMinitrees(options.config_file, 
                    not options.disableProof,
                    options.doMC, 
                    options.doData,
                    options.doALPs, 
                    options.doinvH, 
                    options.doDM, 
                    options.doThisOne, 
                    options.label,
                    options.cpus,
                    options.doPlots, 
                    options.doCutflow,
                    options.doTables,
                    options.doN1Plots,
                    options.clean,
                    options.checkingFiles,
                    options.ignoreMerging,
                    options.skimLevel,
                    )
