
#include "methods.h"
#include "MergeJob.h"
#include "Job.h"
#include "SampleList.h"
#include "PlotTools.C"
R__LOAD_LIBRARY(MergeJob.h+)   // Avoid the "Cannot stream interpreted class" error

void MergeMinitrees(TString config_file, bool useProof, bool doMC, bool doData, bool doALPs, bool doinvH, bool doDM, TString doThisOne = "", TString label = "", int cpus = 4, bool doPlots = false, bool doCutflow = false, bool doTables = false, bool doN1Plots = false, bool clean = false, bool checkingFiles = false, bool ignoreMerging = false, int skimLevel = 0)
{

   TEnv env; 
      if (env.ReadFile(config_file, kEnvAll)) {
      coutError("Unable to read configuration file from PathResolverFindCalibFile of input " + config_file);
      return;
   }

   if (!doData) {
      if (!label.Contains("mc16a") && !label.Contains("mc16d") && !label.Contains("mc16e")) {
         coutError("The label must contain the campaign when merging MC!");
         return;
      }
   }

   if (useProof && cpus == 1) {cout << "Disabling PROOF since cpus is set to 1." << endl; useProof = false;}
   cout << "Skim level is set as " << skimLevel << endl;

   ///////////////////////////////////////////////////////////////////
   // Read config file
   vector<TString> paths_to_analysis_output = getTokens(env.GetValue("path_to_minitrees",""), " "); if (paths_to_analysis_output.size() == 0) {coutError("No path_to_minitrees in the configuration file"); return;}
   TString output_folder = env.GetValue("merged_files_folder", "EMPTY"); if (output_folder == "EMPTY" && !ignoreMerging) {coutError("No merged_files_folder set in config file."); return;}
   vector<TString> selections = getTokens(env.GetValue("plot_selections","Preselection"), " "); 
   TString outPlotDir = env.GetValue("output_plots_folder", "EMPTY"); if (outPlotDir == "EMPTY" && (doPlots || doCutflow)) {coutError("No output_plots_folder set in config file."); return;}
   TString outTableDir = env.GetValue("output_tables_folder", "EMPTY"); if (outTableDir == "EMPTY" && (doTables)) {coutError("No output_tables_folder set in config file."); return;}
   vector<TString> N1_selections = getTokens(env.GetValue("N1_selections",""), " ");  if (N1_selections.size() == 0 && doN1Plots) {coutError("No N1_selections defined in config_file"); return;}
   TString trim_selection = env.GetValue("trim_selection", "EMPTY");
   vector<TString> list_of_systs = getTokens(env.GetValue("list_of_systs","nominal"), " ");

   // coutError("Setting the trim_selection to remove met_nolepton < 100 GeV");
   // trim_selection = "cleanMet";

   if (doThisOne == "data15" || doThisOne == "data16" || doThisOne == "data17" || doThisOne == "data18") doData = kTRUE;
   if (doData) list_of_systs = {"nominal"};

   if (ignoreMerging && !doPlots && !doCutflow && !doTables && !checkingFiles && !doN1Plots) {coutError("You have to choose something to do!"); return;}

   if (label != "" && (doPlots || doCutflow || doN1Plots)) outPlotDir += "_" + label;
   if (label != "" && (doTables)) outTableDir += "_" + label;

   if (!ignoreMerging) gSystem->Exec("mkdir -p " + output_folder);
   if (doPlots || doCutflow || doN1Plots) gSystem->Exec("mkdir -p " + outPlotDir);
   if (doTables) gSystem->Exec("mkdir -p " + outTableDir);

   ////////////////////////////////////////////////////////////////
   // Preparing map id->{hist,minitrees}
   map<int,Job[3]> map_jobs;
   map<TString,vector<int>> samples_to_process;
   if (!doData) {
      if (doMC) for (auto kv : SampleList::MC_samples()) samples_to_process.insert(kv);
      if (doALPs) for (auto kv : SampleList::ALPs_samples()) samples_to_process.insert(kv);
      if (doinvH) for (auto kv : SampleList::invH_samples()) samples_to_process.insert(kv);
      if (doDM) for (auto kv : SampleList::DM_samples()) samples_to_process.insert(kv);
      if (doThisOne != "") {
         if (SampleList::MC_samples().count(doThisOne) != 0) samples_to_process.insert({doThisOne,SampleList::MC_samples()[doThisOne]});
         if (SampleList::ALPs_samples().count(doThisOne) != 0) samples_to_process.insert({doThisOne,SampleList::ALPs_samples()[doThisOne]});
         if (SampleList::invH_samples().count(doThisOne) != 0) samples_to_process.insert({doThisOne,SampleList::invH_samples()[doThisOne]});
         if (SampleList::DM_samples().count(doThisOne) != 0) samples_to_process.insert({doThisOne,SampleList::DM_samples()[doThisOne]});
      }
   }

   cout << "=====================================================" << endl;
   for (TString path_to_analysis_output : paths_to_analysis_output) {
      cout << "Using the ntuples stored here: " << path_to_analysis_output << endl;
      vector<TString> job_files = getFilesList(path_to_analysis_output);

      for (TString job_file : job_files) {
         if (!job_file.Contains("_hist") && !job_file.Contains("minitrees.root")) continue;
         
         int campaign_index = -1;
         if (!doData) {
            if (job_file.Contains("mc16a") || label.Contains("mc16a")) campaign_index = 0;
            if (job_file.Contains("mc16d") || label.Contains("mc16d")) campaign_index = 1;
            if (job_file.Contains("mc16e") || label.Contains("mc16e")) campaign_index = 2;
            if (campaign_index == -1) {coutError("Couldn't extract campaign from " + job_file); continue;}
         } else {
            campaign_index = 0; // Placeholder
         }

         int job_dsid = (doData) ? makeDataDSID(job_file) : getDSID(job_file);
         if (doData) {
            if (job_file.Contains("data15") && (doThisOne == "" || doThisOne == "data15") && !contains(job_dsid,samples_to_process["data15"]) ) samples_to_process["data15"].push_back(job_dsid);
            if (job_file.Contains("data16") && (doThisOne == "" || doThisOne == "data16") && !contains(job_dsid,samples_to_process["data16"]) ) samples_to_process["data16"].push_back(job_dsid);
            if (job_file.Contains("data17") && (doThisOne == "" || doThisOne == "data17") && !contains(job_dsid,samples_to_process["data17"]) ) samples_to_process["data17"].push_back(job_dsid);
            if (job_file.Contains("data18") && (doThisOne == "" || doThisOne == "data18") && !contains(job_dsid,samples_to_process["data18"]) ) samples_to_process["data18"].push_back(job_dsid);
         }
         if (job_dsid != 0) {
            if (map_jobs.count(job_dsid) == 0 || map_jobs[job_dsid][campaign_index].path == "") {
               Job job; 
               job.path = path_to_analysis_output;
               map_jobs[job_dsid][campaign_index] = job;
            }
            if(job_file.Contains("_hist")) map_jobs[job_dsid][campaign_index].hist_folder = job_file;
            if(job_file.Contains("_minitrees.root")) map_jobs[job_dsid][campaign_index].minitrees_folder = job_file;
         }

      }
   }
   cout << "=====================================================" << endl;

   if (checkingFiles) { 
      for (auto kv : samples_to_process) {
         TString sample = kv.first;
         cout << "----------------" << sample << "---------------------" << endl;
         for (int dsid : kv.second) {
            cout << dsid << " " << sample << endl;
            cout << "   " << map_jobs[dsid][0].minitrees_folder << endl;
            cout << "   " << map_jobs[dsid][1].minitrees_folder << endl;
            cout << "   " << map_jobs[dsid][2].minitrees_folder << endl;
         }
      }
   }
   
   // Process minitrees
   gInterpreter->GenerateDictionary("map<TString,TH1D*>","TString.h;TH1D.h;map");
   gInterpreter->GenerateDictionary("map<TString,TString>","TString.h;map");
   gInterpreter->GenerateDictionary("map<TString,map<int,TH1D*>>","map;TString.h;TH1D.h");

   TStopwatch global_timer; global_timer.Start();
   for (auto kv : samples_to_process) {
      TStopwatch timer; timer.Start();
      TString channel_name = kv.first;
      vector<int> channel_dsids = kv.second;

      cout << "=====================================================" << endl;
      cout << "Channel: " << channel_name << endl;
      cout << "=====================================================" << endl;


      auto mergeJob = new MergeJob("MVParams"); // name is rigid (see MonoVReader.C)
      mergeJob->SetUseProof(useProof);
      mergeJob->SetChannelName(channel_name);
      mergeJob->SetIsMerging(!ignoreMerging);
      mergeJob->SetIsPlotting(doPlots);
      mergeJob->SetIsCutflow(doCutflow);
      mergeJob->SetDoTables(doTables);
      mergeJob->SetDoN1Plots(doN1Plots);
      mergeJob->SetOutputFolder(output_folder);
      mergeJob->SetOutputPlotsFolder(outPlotDir);
      mergeJob->SetOutputTablesFolder(outTableDir);
      mergeJob->SetSelections(selections);
      mergeJob->SetN1Selections(N1_selections);
      mergeJob->SetTrimSelection(trim_selection);
      mergeJob->SetIsData(doData);
      mergeJob->SetLabel(label);
      mergeJob->SetSkimLevel(skimLevel);

      if (list_of_systs.size() > 1 && useProof) {
         makeDirectory(output_folder + "/" + channel_name);
         mergeJob->SetOutputFolder(output_folder + "/" + channel_name + "/");
      }
      
      // Add up the mc_weight_sum
      for (int dsid : channel_dsids) {
         if (map_jobs.count(dsid) == 0) {coutError("The file for dsid " + to_string(dsid) + " is not in any of the input paths"); continue;}

         for (int campaign_index = 0 ; campaign_index < 3 ; campaign_index++) {
            Job job = map_jobs[dsid][campaign_index];
            if (job.path == "") continue;

            vector<TString> hist_files = getFilesList(job.path + "/" + job.hist_folder);

            if (job.hist_folder == "") {
               coutError("WARNING: No histogram folder for DSID : " + to_string(dsid) + " ... Skipping merge");
               return;
            }


            if (!doData) {
               for (TString hist_file : hist_files) {
                  TString path_to_hist_file = job.path + "/" + job.hist_folder + "/" + hist_file;
                  TFile * f_hist = new TFile(path_to_hist_file);
                  if (!(f_hist->GetListOfKeys()->Contains("histoEventCount"))) {
                     coutError("WARNING: No histogram in this file: " + path_to_hist_file + "... Skipping merge");
                     return;
                  }
                  TH1D *h_sumw = (TH1D*) f_hist->Get("histoEventCount");
                  mergeJob->AddMCWeight(dsid, h_sumw->GetBinContent(1), campaign_index);
                  f_hist->Close();
                  delete f_hist;
               }

            }

         }
      }
      if (checkingFiles) {
         cout << "---- MC weight sum -----" << endl;
         for (int dsid : channel_dsids) {
            cout << dsid << ": " << endl;
            for (int campaign_index = 0 ; campaign_index < 3 ; campaign_index++) {
               cout << "   " << campaign_index << ": " << mergeJob->GetMCWeight(dsid,campaign_index) << endl;
            }
         }
         continue;
      }

      // Prepare MergedTree
      cout << "--------------------------------------------" << endl;
      cout << "Starting to merge" << endl;
      cout << "--------------------------------------------" << endl;  

      for (TString curr_systematic : list_of_systs) {
         cout << "------ MERGING " << channel_name << " " << curr_systematic << endl;
         vector<TString> v_input_files;   
         Double_t expected_merged_size = 0;
         int entries_in_inputs = 0;
         mergeJob->ResetRawEvents();
         mergeJob->SetOutputTreeName(channel_name);
         if (curr_systematic != "nominal") mergeJob->SetOutputTreeName(channel_name + "_" + curr_systematic);

         for (int dsid : channel_dsids) {
            if (map_jobs.count(dsid) == 0) {continue;}

            vector<TString> row;
            Long_t rawEvnts = 0;

            for (int campaign_index = 0 ; campaign_index < 3 ; campaign_index++) {
               Job job = map_jobs[dsid][campaign_index];
               if (job.path == "") continue;

               vector<TString> minitrees_files = getFilesList(job.path + "/" + job.minitrees_folder);
               for (TString minitrees_file : minitrees_files) {
                  
                  TString path_to_minitrees_file = job.path + "/" + job.minitrees_folder + "/" + minitrees_file;
                  v_input_files.push_back(path_to_minitrees_file);
                  expected_merged_size += get_file_size(path_to_minitrees_file);

                  TFile * f_minitree = new TFile(path_to_minitrees_file);
                  TTree * tree = (TTree*)f_minitree->Get(curr_systematic);
                  rawEvnts += tree->GetEntries();
                  entries_in_inputs += tree->GetEntries();
                  delete tree;
                  f_minitree->Close();
                  mergeJob->AddRawEvents(dsid, rawEvnts);
               }
            }
         }

         // Prepare Merging
         Double_t sizeThreshold = (ignoreMerging) ? 1000*GB : getSizeThreshold(expected_merged_size); 
         Double_t currentSize = 0;
         vector<TString> list_of_merged_outputs;
         int n_merged = 0;
         TString selectorClass = (doData) ? "MonoVReaderData" : (curr_systematic == "nominal") ? "MonoVReader" : "MonoVReaderSyst";
         TFileCollection * fc_channel = new TFileCollection(channel_name);
         for (int i_file = 0 ; i_file < v_input_files.size() ; i_file++) {
            TString input_file = v_input_files.at(i_file);
            currentSize += get_file_size(input_file);
            fc_channel->Add(input_file);
            cout << "Adding file " << input_file << endl;
            if (currentSize >= sizeThreshold || i_file == v_input_files.size()-1) {
               n_merged++;
               cout << "-----------------------------------" << endl;
               cout << "Start processing" << " - Size " << currentSize/GB << " GB";
               cout << endl;
               TString out_fileName = channel_name;
               if (label != "") out_fileName += "_" + label;
               if (list_of_systs.size() > 1 && useProof) out_fileName += "_" + curr_systematic;
               if (i_file != v_input_files.size()-1 || n_merged > 1) out_fileName += "_p" + to_string(n_merged);
               mergeJob->SetOutputFileName(out_fileName);
               list_of_merged_outputs.push_back(out_fileName);

               // Set up process
               if (useProof) {
                  gEnv->SetValue("Proof.Sandbox",output_folder);
                  auto dset_channel = new TDSet("TTree", curr_systematic);
                  dset_channel->Add(fc_channel->GetList());
                  TString workers_str = "workers=" + to_string(cpus);
                  auto fProof_channel = TProof::Open(workers_str);
                  fProof_channel->SetProgressDialog(kFALSE);
                  fProof_channel->AddInput(mergeJob);
                  if (clean || !fileExists(selectorClass + "_C.so")) {
                     fProof_channel->Load("methods.h+");
                     fProof_channel->Load("SampleList.h+");
                     fProof_channel->Load("Cuts.h+");
                     fProof_channel->Load("PlotList.h+");
                     fProof_channel->Load("MiniTree.C+");
                     fProof_channel->Load("MergedTree.C+");
                     fProof_channel->Load("SelectionTool.h+");
                     fProof_channel->Load("PlotTools.C+");
                  }
                  fProof_channel->Load("MergeJob.h+");
                  fProof_channel->Process(dset_channel, selectorClass + ".C+");
                  // Produce Logs
                  TString logFolderName = "LOG";
                  if (label != "") logFolderName += "_" + label;
                  if (i_file != v_input_files.size()-1 || n_merged > 1) logFolderName += "_p" + to_string(n_merged);

                  gSystem->Exec("mkdir -p tmp");
                  gSystem->Exec("mkdir -p tmp/" + logFolderName);
                  gSystem->Exec("mkdir -p tmp/" + logFolderName + "/" + curr_systematic);
                  auto logs_channel = fProof_channel->GetManager()->GetSessionLogs();
                  logs_channel->Save("*","tmp/" + logFolderName + "/" + curr_systematic + "/" + channel_name + ".txt");
                  delete logs_channel;
                  delete fProof_channel;
                  delete dset_channel;
               } else {
                  if (!fileExists(mergeJob->GetOutputFolder() + mergeJob->GetOutputFile() + ".root")) {
                     TChain chain_channel(curr_systematic);
                     chain_channel.AddFileInfoList(fc_channel->GetList());
                     auto sel_channel = TSelector::GetSelector(selectorClass + ".C+");
                     auto list_channel = new TList();
                     list_channel->Add(mergeJob);
                     sel_channel->SetInputList(list_channel);
                     chain_channel.Process(sel_channel);
                     delete sel_channel;
                     delete list_channel;
                  } else {
                     coutError("The file " + mergeJob->GetOutputFolder() + mergeJob->GetOutputFile() + ".root already exists! If you are running with cpus=1 you need to delete this file first");
                     return;
                  }
               }

               currentSize = 0;
               delete fc_channel;
               fc_channel = new TFileCollection(channel_name);  // Reseting TFileCollection
            }
         }

         // Check that the number of entries make sense
         int entries_in_merged = 0;
         for (TString merged_output : list_of_merged_outputs) {
            TFile * mergedFile = new TFile(mergeJob->GetOutputFolder() + merged_output + ".root");
            TTree * mergedTree = (TTree*)mergedFile->Get(mergeJob->GetOutputTreeName());
            if (!mergedTree) {
               coutError("Problem retrieving " + mergeJob->GetOutputTreeName() + " from " + mergeJob->GetOutputFolder() + merged_output + ".root for getting the number of entries");
            } else {
               entries_in_merged += mergedTree->GetEntries();
               delete mergedTree;
            }
            mergedFile->Close();
         }

         cout << "===========================================================================================" << endl;  
         timer.Stop();
         cout << "There are " << entries_in_merged << " entries in the merged file and " << entries_in_inputs << " entries in the inputs" << endl;
         if (entries_in_merged != entries_in_inputs) coutError("There is a difference in the number of entries in the merged file to the inputs... You should check if it makes sense!!!");
         cout << "Ending processing " << channel_name << " in " << curr_systematic << " after " << timer.RealTime()/60 << " min" << endl;
         if (list_of_merged_outputs.size() == 1) {
            cout << "Output file in " << mergeJob->GetOutputFolder() + mergeJob->GetOutputFile() + ".root" << endl;
         } else {
            cout << "Output files:" << endl;
            for (TString merged_output : list_of_merged_outputs) {
               cout << mergeJob->GetOutputFolder()  << merged_output << ".root" << endl;
            }
         }
         cout << "===========================================================================================" << endl;    

      }


   } // Loop over the samples to process
   global_timer.Stop();
   cout << "Ending processing ALL channels after " << global_timer.RealTime()/60. << " min" << endl;

}

