#!/bin/bash

date +"Script-start "%H:%M:%S" "%D
echo '*************Setup****************'
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
#asetup AnalysisTop,21.2.86
#source /nfs/at3/scratch/salvador/TTHbb/build/*/setup.sh
echo '**********************************'


#echo $PWD
#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
#source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh


cd /nfs/pic.es/user/s/sgonzalez

source .bashrc

cd /nfs/pic.es/user/s/sgonzalez/scratch2/MonoV/monovmacros/code

setupATLAS
lsetup root

echo "LAUNCHING"

config_file=${1}
channel=${2}
label=${3:-""}

root -l -b -q "ExtractPlots.C(\"${config_file}\",{\"${channel}\"},\"${label}\",{},true)"




