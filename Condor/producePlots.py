Executable = launch_ProducePlots.sh
Output = condor/out/outPlots-$(ClusterId).$(ProcId).out
Error = condor/err/errPlots-$(ClusterId).$(ProcId).err
Log = condor/log/logPlots-$(ClusterId).$(ProcId).log

#queue arguments from submit_plots.txt

#+flavour="long"
#request_disk = 30 GB

Queue Arguments from (
	"Zee false"
)

