Executable = launch_extractPlotsReader.sh
Output = condor/out/outExtract-$(ClusterId).$(ProcId).out
Error = condor/err/errExtract-$(ClusterId).$(ProcId).err
Log = condor/log/logExtract-$(ClusterId).$(ProcId).log

#+JobFlavour = "workday"
request_cpus = 10

Queue Arguments from (
        "../conf/configuration_Feb2020_full.conf ttbar running_full2 false"
)


