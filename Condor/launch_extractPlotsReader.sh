#!/bin/bash

date +"Script-start "%H:%M:%S" "%D
echo '*************Setup****************'
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
echo '**********************************'

cd /nfs/pic.es/user/s/sgonzalez

source .bashrc

cd /nfs/pic.es/user/s/sgonzalez/scratch2/MonoV/monovmacros

setupATLAS
lsetup root

config_file=${1}
channel=${2}
folderLabel=${3}
ClusterId=${4}
ProcId=${5}
label=${6:-""}

echo "LAUNCHING ${channel} ${config_file}"

mkdir -p Running
rm -rf Running/code_${channel}_${folderLabel}
cp -rf code_CONDOR Running/code_${channel}_${folderLabel}
cd Running/code_${channel}_${folderLabel}
bash clean.sh

root -l -b -q "ExtractPlotsReader.C(\"${config_file}\",{\"${channel}\"},false,\"${label}\",{},4,${ClusterId},${ProcId})" > outExtract-${ClusterId}.${ProcId}.out
