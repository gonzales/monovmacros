#!/bin/bash

mask=${1}
pathForLinks=${2}
curr_path=$PWD

rm -f list.txt
rucio list-dids --short group.phys-exotics:*${mask}*root* --filter "type=CONTAINER" > list.txt

suffix="_minitrees.root"

while IFS= read -r line
do
	echo "$line"
	folderName=${line#"group.phys-exotics:"}
	histFolderName=$(echo "$folderName" | sed -E -e 's/_minitrees.root+/_hist/')
	mkdir -p ${pathForLinks}/${folderName}
	mkdir -p ${pathForLinks}/${histFolderName}
	cd ${pathForLinks}/${folderName}
	rucio list-file-replicas --rse CERN-PROD_PHYS-EXOTICS --link /eos/:/eos/ group.phys-exotics:${folderName}
	cd ${pathForLinks}/${histFolderName}
	rucio list-file-replicas --rse CERN-PROD_PHYS-EXOTICS --link /eos/:/eos/ group.phys-exotics:${histFolderName}
done < list.txt

cd $curr_path

rm list.txt


