# MonoVMacros

To run these scripts having `lsetup root` should be enough. 

The merging is launched with the [code/MergeMinitrees.py](code/MergeMinitrees.py) which parses the arguments and runs [code/MergeMinitrees.cxx](code/MergeMinitrees.cxx). The merging can be processed via TChain or TProof through a TSelector defined in [code/MonoVReader.h](code/MonoVReader.cxx) (**Note** there are different TSelectors for data and syst but the content is similar).

A configuration file is needed to run the merging. An example can be found in [conf/Jul2020Prod/configuration_test.conf](conf/Jul2020Prod/configuration_test.conf). The content of this file is:

* **path_to_minitrees** Space separated list of paths to the folder that contains the *hist* and *minitrees* folders.
* **merged_files_folder** Folder where the merged file will be stored.
* **list_of_systs** Space separated list of tree names to be considered in the merging (i.e nominal, syst_EG_RESOLUTION_ALL__1down,...)


## Merging

The script to launch the merging is `MergeMinitrees.py`. The parameters it accepts are:

* **-c,--config-file** Path to the config file.
* **--do-MC** Process the MC samples listed in the file [code/SampleList.h](code/SampleList.h)
* **--do-ALPs** Process the ALPs samples listed in the file [code/SampleList.h](code/SampleList.h)
* **--do-invH** Process the invH samples listed in the file [code/SampleList.h](code/SampleList.h)
* **--do-DM** Process the DM samples listed in the file [code/SampleList.h](code/SampleList.h)
* **--do-signals** Process the signal samples
* **--do={CHANNEL}** Process the given channel which should be listed in the file [code/SampleList.h](code/SampleList.h)
* **--cpus={CPUS}** Number of CPUs to use in TPROOF. If set to 1, the merging will be done with TCHain
* **--label={LABEL}** Label to add to the merged file. If merging MC it must contain the campaign.
* **--clean** Clean the folder and recompile before running
* **skim-level={SKIMLEVEL}** Level of skimming of the mergedTree. The higher the value, less branches in the output tree.

### Merging of Jul2020Prod

To test the merging script one first needs to get the minitrees/hist inputs. This can be done either via the [util/produceLinksAtFolder.sh](util/produceLinksAtFolder.sh) or by running `rucio download` on any given dataset. For example:

```
rucio download group.phys-exotics.Jul2020Prod_mc16d.mc16_13TeV.345045.Sherpa_221_NNPDF30NNLO_WlvZbb_hist
rucio download group.phys-exotics.Jul2020Prod_mc16d.mc16_13TeV.345045.Sherpa_221_NNPDF30NNLO_WlvZbb_minitrees.root
```

Then write a configuration file with the following content:

    path_to_minitrees: {path_to_where you did the rucio download}
    merged_files_folder: {path_to_folder_where_the_output_will_be_put}
    list_of_systs: nominal

In the example, the DSID corresponds to the *diboson* channel. You can find the list of DSIDs corresponding to a channel in the xx_samples() methods of [code/SampleList.h](code/SampleList.h).

Now that we have the inputs and know the channel we want to merge, we can use the following command:

```
python MergeMinitrees.py -c ../conf/Jul2020Prod/configuration_test.conf --do diboson --cpus 1 -l mc16d_notSkimmed --skim-level 0
```

To produce a merged file with some removed branches one can use the --skim-level parameter:

```
python MergeMinitrees.py -c ../conf/Jul2020Prod/configuration_test.conf --do diboson --cpus 1 -l mc16d_skimmed --skim-level 1
```

The branches removed for a given skim level are defined in the Book() method of [code/MergedTree.h](code/MergedTree.h). You can edit this to remove more/less branches and defined other levels of skimming.


<!--### Merging of Feb2020Prod-->

<!--First, one can use the script in *util/produceLinksAtFolder.sh*, after doing the setup in *util/setup_rucio*, in order to produce the links to your part of the production in a particular folder:-->

<!--```-->
<!--bash produceLinksAtFolder.sh Feb2020Prod{your_tag} {path_to_where_you_want_the_links}-->
<!--```-->

<!--Then, produce a config file with the following information:-->

<!--* **path_to_minitrees** path to the folder with the links (the same one used for *produceLinksAtFolder.sh*)-->
<!--* **merged_files_folder**: path where you want the output merged trees to be stored.-->

<!--You can find an example config file in *conf/configuration_Feb2020mc16d_bkg.conf*-->

<!--For running the merging using PROOF, go to the **code/** folder and do **lsetup root**. Now you can run:-->

<!--```-->
<!--python MergeMinitrees.py -c {your_config_file} --do {the_channel_to_merge} --cpus {number_of_workers} -l {label_for_file}-->
<!--```-->

<!--This will compile the various files and run the merging.-->

<!--For merging the MC backgrounds, you can use the --do-MC flag. Similarly for the signals you can use --do-signals-->

<!--The merging can take a while, so the use of the screen command or a VNC is recommended. -->

<!--#### Merging data-->

<!--You can find the path to the data links in the config tile *conf/configuration_Feb2020_data.conf* -->

<!--Then the merging can start with:-->

<!--```-->
<!--python MergeMinitrees.py -c {your_config_file} --do-data-->
<!--```-->




